@if(auth()->check())
    <div class="right">
        <div class="notification">
{{--            <ul class="list clearfix">--}}
{{--                <li><a href="#"><img src="/images/icon_message.png" alt=""></a></li>--}}
{{--                <li><a href="#"><i class="fas fa-bell"></i></a></li>--}}
{{--            </ul>--}}
            <div class="avatar" id="header_avatar">
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-display="static">
                        @if(!empty($customerCurrent))
                            @if( jjobCheckImageExit($customerCurrent->logo) )
                                <img src="{{ asset($customerCurrent->logo) }}" alt="" class="">
                            @else
                                <img src="{{ asset('images/icon_avatar.png') }}" alt="">
                            @endif
                        @else
                            @if(empty(auth()->user()->avatar))
                                <img src="{{ asset('images/icon_avatar.png') }}" alt="">
                            @else
                                @if(str_contains(auth()->user()->avatar, 'upload') && jjobCheckImageExit(auth()->user()->avatar))
                                    <img class="member-avatar" src="{{ asset(auth()->user()->avatar) }}" alt="">
                                @else
                                    <img class="member-avatar" src="{{ auth()->user()->avatar }}" alt="">
                                @endif

                            @endif
                        @endif

                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="profile">
                            <div class="info">
                                <div class="d-flex">
                                    <a href="{{ !empty($customerCurrent) ? route('companies.index', ['slug' => $customerCurrent->slug]) : route('profile.index') }}">
                                        <h5>
                                            {{ !empty($customerCurrent) ? $customerCurrent->name : auth()->user()->name }}
                                            {{--<span class="score"></span>--}}
                                        </h5>
                                    </a>
                                </div>
                                <a

                                    href="{{ !empty($customerCurrent) ? route('companies.index', ['slug' => $customerCurrent->slug]) : route('profile.index') }}"
                                    class="edit">
                                    {{ !empty($customerCurrent) ? 'Xem công ty' : 'Chỉnh sửa trang cá nhân' }}
                                </a>
                            </div>
                            <div class="avatar">


                                @if(!empty($customerCurrent))
                                    <a href="{{ route('companies.index', ['slug' => $customerCurrent->slug]) }}">
                                        @if( jjobCheckImageExit($customerCurrent->logo) )
                                            <img src="{{ asset($customerCurrent->logo) }}" alt="" class="">
                                        @else
                                            <img src="{{ asset('images/icon_avatar.png') }}" alt="">
                                        @endif
                                    </a>
                                @else
                                    <a href="{{ route('profile.index') }}">

                                        @if(empty(auth()->user()->avatar))
                                            <img src="{{ asset('images/icon_avatar.png') }}" alt="">
                                        @else
                                            @if(str_contains(auth()->user()->avatar, 'upload') && jjobCheckImageExit(auth()->user()->avatar))
                                                <img class="member-avatar" src="{{ asset(auth()->user()->avatar) }}" alt="">
                                            @else
                                                <img class="member-avatar" src="{{ auth()->user()->avatar }}" alt="">
                                            @endif

                                        @endif

                                    </a>
                                @endif


                            </div>
                        </div>
                        <div class="bookmark">
                            <div class="item">
                                <a href="{{ route('jobs.show-list-apply-job') }}">
                                    <i class="far fa-paper-plane"></i>
                                    <span>Công việc ứng tuyển</span>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{ route('jobs.show-list-save-job') }}">
                                    <i class="far fa-bookmark"></i>
                                    <span>Công việc đã lưu</span>
                                </a>
                            </div>
                            @if(!empty($customerCurrent))
                                <div class="item">
                                    <a
                                        href="{{ route('enterprise.companies.edit', ['id' => $customerCurrent->id]) }}">
                                        <i class="far fa-building"></i>
                                        <span>Chỉnh sửa công ty</span>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="setting">
                            <div class="item">
                                <a href="{{ route('companies.following') }}">
                                    <i class="fas fa-building"></i>
                                    <span>Công ty đang theo dõi</span>
                                </a>
                            </div>
{{--                            <div class="item">--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fas fa-user-plus"></i>--}}
{{--                                    <span>Mời bạn bè tham gia Inplace</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fas fa-user-cog"></i>--}}
{{--                                    <span>Cài đặt tài khoản</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                            <div class="item">
                                <a href="#" class="logout-button"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i>
                                    <span>Thoát</span>
                                </a>
                                <form style="display: none"
                                      method="post"
                                      action="{{ route('logout') }}" id="logout-form">
                                    @csrf
                                </form>

                            </div>
                        </div>

                        <div class="account-list">


                            <div class="item active">
                                <a href="{{ route('enterprise.index', ['customerId' => 'user']) }}">
                                    <div class="avatar">
                                        @if( jjobCheckImageExit(auth()->user()->avatar) )
                                            <img src="{{ asset(auth()->user()->avatar) }}" alt=""
                                                 class="">
                                        @else
                                            <img src="{{ asset('images/icon_avatar.png') }}" alt="avatar">
                                        @endif
                                    </div>
                                    <span class="name">{{ auth()->user()->name }}</span>
                                    @if(empty($customerCurrent))
                                        <div class="check">
                                            <i class="far fa-check"></i>
                                        </div>
                                    @endif
                                </a>
                            </div>
                            @foreach($dataCustomerFollowUserLogin as $itemCustomerFollowUserLogin)
                                <div class="item active">
                                    <a href="{{ route('enterprise.index', ['customerId' => $itemCustomerFollowUserLogin->id]) }}">
                                        <div class="avatar">
                                            @if( jjobCheckImageExit($itemCustomerFollowUserLogin->logo) )
                                                <img src="{{ asset($itemCustomerFollowUserLogin->logo) }}" alt=""
                                                     class="">
                                            @else
                                                <img src="{{ asset('images/icon_avatar.png') }}" alt="avatar">
                                            @endif
                                        </div>
                                        <span class="name">{{ $itemCustomerFollowUserLogin->name }}</span>
                                        @if(isset($customerCurrent) && $itemCustomerFollowUserLogin->id == $customerCurrent->id)
                                            <div class="check">
                                                <i class="far fa-check"></i>
                                            </div>
                                        @endif
                                    </a>
                                </div>
                            @endforeach

                            <div class="item">
                                <a href="{{ route('companies.create') }}">
                                    <div class="avatar">
                                        <div class="add-account">
                                            <i class="far fa-plus"></i>
                                        </div>
                                    </div>
                                    <span class="create">Tạo trang doanh nghiệp</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@else
    <div class="right">
        <div class="login">
            <a class="join_company" href="{{route('register')}}">Đăng ký tài khoản</a>
            <a href="{{route('login')}}" class="btn btn-custom ml-2 join_company">Đăng nhập</a>
        </div>
    </div>

@endif
