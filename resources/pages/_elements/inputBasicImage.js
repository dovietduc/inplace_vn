function validateFileImage(that) {
    let fileExtension = ['png', 'jpg', 'jpeg'];
    let fileExtentionCurrent = $(that).val().split('.').pop().toLowerCase();
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        swal({
            icon: "error",
            title: "Sai định dạng file!",
            text: "Vui lòng upload hình ảnh có định dạng *.png, *.jpg hoặc *.jpeg",
            button: {
                text: "Đóng",
                className: "btn-custom",
                closeModal: true,
                value: false,
            }
        });
        $(that).val('');
    }
}
function readFileImageBig(that) {
    validateFileImage(that)
    if (that.files && that.files[0]) {
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(that.files[0]);
        reader.onload = function(e) {
            image.src = e.target.result;
            image.onload=function(){
                if(this.width < 1366 || this.height < 350){
                    swal({
                        icon: "error",
                        title: "Ảnh quá nhỏ!",
                        text: "Vui lòng chọn hình ảnh rộng ít nhất 1366 pixel và cao ít nhất 350 pixel.",
                        button: {
                            text: "Đóng",
                            className: "btn-custom",
                            closeModal: true,
                        }
                    });
                    $(that).val('');
                }else{
                    $(that).parent().children('img').show().attr('src', e.target.result);
                }
            }
        };
    }
}
function readFileImage(that) {
    validateFileImage(that);
    if (that.files && that.files[0]) {
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(that.files[0]);
        reader.onload = function(e) {
            $(that).parent().children('img').show().attr('src', e.target.result);
        };
    }
}
$('.form-group_upload-image img').on('error',function () {
    $(this).hide();
});

export function inputBasicImage() {
    $('.inp-file-image').on('change', function () {
        readFileImage(this);
    });
}
export function inputBasicImageBig() {
    $('.inp-file-big-image').on('change', function () {
        readFileImageBig(this);
    });
}
