
<button data-url="{{ route('companies.news.like', ['id' => $newItem->id]) }}"
        class="btn-like-heart toogle_like_button">
    @php
        $statusLike = $newItem->newLikes()->where([
            'deleted_flag' => false,
            'user_id' => auth()->id()
        ])->count();
    @endphp
    @if(!empty($statusLike))
        <i class="fas fa-heart active"></i>
    @else
        <i class="far fa-heart"></i>
    @endif
</button>
