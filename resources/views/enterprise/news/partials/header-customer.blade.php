<div class="dashboard-header">
    <div class="container">
        <div class="dashboard-header_content">
            <h1 class="title">Bài viết của
                <strong>{{ !empty($customerCurrent) ? $customerCurrent->name : '' }}</strong></h1>
        </div>
    </div>
</div>
