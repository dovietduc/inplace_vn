<div id="frm-apply_signin"
     class="modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ứng tuyển với J-Job Executive Search</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="frm-apply_signin__box">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="around">
                                <h3 class="title">Bạn chưa có tài khoản</h3>
                                <div class="desc">
                                    Dễ dàng Ứng tuyển bằng cách tạo một tài khoản InPlace. Đừng lo lắng, bạn sẽ chỉ mất
                                    một vài phút để hoàn thành.
                                </div>
                                <a href="{{ route('register') }}" target="_blank" class="d-block btn btn-custom">Đăng ký
                                    tài khoản</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="around">
                                <h3 class="title">Bạn đã có tài khoản</h3>
                                <div class="desc">
                                    Đăng nhập để Ứng tuyển công việc này với hồ sơ InPlace của bạn.
                                </div>
                                <a href="{{ route('login') }}" target="_blank" class="d-block btn btn-custom_light">Đăng
                                    nhập để ứng
                                    tuyển</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(auth()->check())
    <div id="frm-apply"
         class="modal fade"
         tabindex="-1"
         role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $job->title }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle"></i>
                    </button>
                </div>

                <form
                        action="{{ route('jobs.applyJob', ['slug' => $job->slug]) }}"
                        class="form-ApplyJob"
                        method="POST"
                        enctype="multipart/form-data"
                        id="form_apply_job">
                    @csrf
                    <div class="modal-body">
                        <div class="frm-apply__box">
                            <div class="frm-apply__box-row mb-4 mt-0">
                                <div class="left">
                                    <div class="avatar">
                                        @if( jjobCheckImageExit(auth()->user()->avatar) )
                                            <img src="{{ url(auth()->user()->avatar) }}" alt="">
                                        @else
                                            <img src="{{ asset('images/no_user_65x65.jpg') }}" alt="">
                                        @endif
                                    </div>
                                </div>
                                <div class="right">
                                    <h5 class="name">{{ auth()->user()->name }}</h5>
                                    {{--<div class="status">Account Manager</div>--}}
                                    <a href="" class="link-info">Xem trang cá nhân</a>
                                </div>
                            </div>
                            <div class="frm-apply__box-row">
                                <div class="left">
                                    <label for="">Email</label>
                                </div>
                                <div class="right">
                                    <input type="text"
                                           class="form-control"
                                           value="{{ auth()->user()->email }}"
                                           disabled="disabled"
                                           placeholder="Email ứng tuyển"/>
                                </div>
                            </div>
                            <div class="frm-apply__box-row">
                                <div class="left">
                                    <label for="">Điện thoại</label>
                                </div>
                                <div class="right">
                                    <input type="tel"
                                           class="apply-inp-tel form-control"
                                           value="{{ auth()->user()->mobile  }}"
                                           placeholder="Số điện thoại"
                                           maxlength="25"
                                           name="tel"
                                           required pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+"/>
                                    <span class="apply-err-tel inp-err"></span>
                                </div>
                            </div>
                            <div class="frm-apply__box-row">
                                <div class="left">
                                    <label for="">CV</label>
                                </div>
                                <div class="right">
                                    <div class="btn-UpFile apply-inp-btnFile file_upload-choose">
                                        <label class="btn btn-custom_light" for=""><span>Upload CV</span></label>
                                        <div class="hasFile">
                                            <div class="hasFile_around">
                                                <div class="icon-file">
                                                    <img src="" alt="">
                                                </div>
                                                <div class="hasFile_info">
                                                    <div class="info_left">
                                                        <h6 class="name">Upload File</h6>
                                                        <p class="alert-done">Tải lên hoàn tất</p>
                                                    </div>
                                                    <div class="info_right">
                                                        <span class="size">.Kb</span>
                                                        <i class="del-file far fa-trash-alt"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input class="apply-inp-file"
                                               name="file_upload"
                                               type="file"
                                               accept="application/msword, application/pdf, .docx, .doc, .pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                    </div>
                                    <span class="apply-err-file inp-err file_upload-error"></span>
                                    <div class="note">Vui lòng tải lên file định dạng .doc .docx .pdf dưới 2MB</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-close" data-dismiss="modal">Bỏ qua</button>
                        <button type="submit" class="form-ApplyJob_btnSubmit btn btn-custom">Ứng tuyển</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif


<div id="frm-apply_success"
     class="alert-form-success modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ứng tuyển với {{ optional($job->customer)->name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="icon-success"><i class="fal fa-check"></i></div>
                <h2 class="title">Xin chúc mừng</h2>
                <div class="desc">Hồ sơ của bạn đã được gửi tới Nhà tuyển dụng</div>
            </div>
        </div>
    </div>
</div>
