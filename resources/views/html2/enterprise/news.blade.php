@extends('html2.enterprise.layout')
@section('content_page')
    <div class="container enterprise-news-page pt-lg-2 pb-lg-5 mb-lg-5">
        <div class="row">
            <div class="col-12 col-lg-7 offset-lg-1 mb-5 mb-lg-0">
                <div class="list-news">
                    @for ($i = 0; $i < 3; $i++)
                        @include('html2.enterprise.partials.item_news_list')
                    @endfor
                </div>
            </div>
            <div class="col-12 col-lg-3">
                <div class="card card-line border-0">
                    <div class="card-header px-0 pt-lg-0">
                        <span class="card-name">
                            Bài viết nổi bật
                        </span>
                    </div>
                    <div class="card-body list-top-news px-0 py-30">
                        @for ($i = 0; $i < 5; $i++)
                        <div class="item d-flex border-bottom pb-2 mb-3">
                            <div class="post-image mr-3">
                                <a href="#" title="">
                                    <img src="https://via.placeholder.com/100x56" class="rounded-sm">
                                </a>
                            </div>
                            <div class="post-info">
                                <a href="#" title="" class="post-name d-block mb-2">
                                    3 nghề kỹ sư IT có thể kiếm nửa tỷ đồng mỗi năm tại Việt Nam
                                </a>
                                <p class="mb-0 text-size-13 text-grey-200">
                                    128k Thích
                                </p>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('html2.enterprise.partials.related_company')
@endsection
