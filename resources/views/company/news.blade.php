@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('pages/companies/post/post.css') }}">
@stop

@section('content')
    <div class="page-enterprise">
        @include('company.partials.company-head-menu')
        <div class="page-enterprise">
            <div class="post-enterprise-page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 box_content">
                            <div class="list-news">
                                @include('company.partials.item_news_list')
                            </div>
                        </div>
                        @if(!$hotNews->isEmpty())
                            <div class="col-md-4 mt-5 mt-md-0 box_sidebar">
                                <div class="post-enterprise-page_sidebar">
                                    <h3 class="title-sidebar">
                                        Bài viết nổi bật
                                    </h3>
                                    <div>
                                        @include('company.partials.hot_news_company')
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--    @include('html2.enterprise.partials.related_company')--}}
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('vendor/lazyload/lazyload.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('pages/companies/post/post.js') }}"></script>
@stop

