<div class="txt-format">
    <h1>Hướng dẫn Tìm kiếm công ty</h1>
    <p>Để tìm kiếm công ty, bạn cần thực hiện theo các bước sau:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tim_kiem_doanh_nghiep.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Tạo tài khoản cá nhân và tiến hành đăng nhập vào tài khoản. Xem
                    <a href="javascript:;"
                       onclick="$('#tao-trang-ca-nhan-moi-tab').click()">Hướng
                        dẫn tạo trang
                        cá nhân mới</a></li>
                <li><strong>Bước 2:</strong> Nhập thông tin tên doanh nghiệp mà bạn cần tìm và nhấp vào biểu tượng tìm kiếm. Hoặc vào phần Việc làm từ Head Menu và tìm tên Doanh nghiệp từ công cụ tìm kiếm Job</li>
                <li><strong>Bước 3:</strong> Click vào logo của công ty để tới được trang Doanh Nghiệp để đọc
                    các thông tin về doanh nghiệp. Trở thành Fan (Become a fan) của doanh
                    nghiệp đó nếu bạn muốn được ưu tiên cập nhật các thông tin của doanh
                    nghiệp trên Inplace.</li>
            </ul>
        </div>
    </div>
</div>
