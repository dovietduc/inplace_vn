@extends('emails.layouts.for-user')

@section('title', 'Active Acount')

@section('content')
    <p>Xin chào, <strong>{{ $details['name'] }}</strong></p>
    <p>Rất vui được chào đón bạn đến với cộng đồng <strong>INPLACE</strong></p>
    <p>Chỉ còn một bước nữa thôi để hoàn thành quy trình đăng ký này, bạn vui lòng click vào link bên dưới để hoàn thành việc xác nhận.</p>
    <center>
        <a href="{{ url('/email/verify-acount/' . $details['id']) }}">Xác nhận tài khoản</a>
    </center>
@endsection

