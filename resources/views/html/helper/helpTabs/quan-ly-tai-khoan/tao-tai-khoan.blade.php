<div class="txt-format">
    <h1>Hướng dẫn Tạo tài khoản</h1>
    <p>Tất cả
        mọi người đều có thể đăng ký sử dụng Inplace miễn phí.</p>
    <p>Sau khi đăng ký tạo tài khoản, người dùng có thể sử dụng được tất cả các tiện ích mà trang web mang lại bao
        gồm đăng tải CV, đăng tải nội dung yêu thích, kết nối với người dùng khác và doanh nghiệp, tìm kiếm việc
        làm, ứng tuyển và trao đổi thông tin với nhà tuyển dụng về việc làm bất kỳ, chia sẻ thông tin và được hệ
        thống Inplace tự động gợi ý và giới thiệu tới công ty hoặc việc làm phù hợp. Ngoài ra, sau khi tạo tài khoản
        cá nhân xong, người sử dụng có thể tạo trang doanh nghiệp cho mục đích Employer Branding và tuyển dụng của
        công ty mình.</p><h4><strong>Có 3 cách để đăng ký tài khoản tại Inplace:</strong></h4>
    <ul>
        <li>Dùng tài khoản Facebook</li>
        <li>Dùng tài khoản Linkedin</li>
        <li><!--[if !vml]--><!--[endif]-->Dùng email cá nhân</li>
    </ul>
    <p><br></p>
    <h3><strong>Đăng ký nhanh chóng bằng tài khoản Facebook:</strong>&nbsp;</h3>
    <p>Click vào biểu tượng <img src="{{asset('images/icon-login-facebook.jpg')}}" alt=""> trên trang chủ của www.inplace.vn. Hệ thống sẽ
        tự liên kết với tài khoản Facebook của bạn.&nbsp;</p>
    <p>Lưu ý, sau khi đăng ký bằng tài khoản Facebook xong, hệ thống sẽ yêu cầu bạn nhập email và xác thực địa chỉ
        email một lần nữa.</p>
    <p><br></p>
    <h3><strong>Đăng ký bằng tài khoản Linkedin:</strong></h3>
    <p>Click vào biểu tượng <img src="{{asset('images/icon-login-linkedin.jpg')}}" alt=""> trên trang chủ của www.inplace.vn.
        Hệ thống sẽ tự liên kết với tài khoản Linkedin của bạn.</p>
    <p><br></p>
    <h3><strong>Đăng ký bằng email cá nhân:</strong></h3>
    <p>Bạn cần thực hiện các bước sau:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_trang_ca_nhan.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><!--[if !vml]--><!--[endif]--><strong>Bước 1:</strong> Tại trang chủ www.inplace.vn
                    click vào <strong>Đăng ​​​​Ký</strong></li>
                <li><strong>Bước 2:</strong> Nhập thông tin vào trường như <strong>H</strong><strong>ọ
                        và&nbsp;</strong><strong>Tên</strong><strong>&nbsp;</strong>(có thể dùng tiếng Việt có dấu và dấu
                    cách giữa các từ), <strong>Email cá nhân</strong> (1 email chỉ đăng ký được 1 tài khoản cá nhân duy
                    nhất) và <strong>Mật Khẩu</strong> (tối thiểu 6 ký tự).
                </li>
                <li><strong>Bước 3:</strong> Nhấp vào Đăng ký<!--[if !vml]--></li>
                <li><strong>Bước 4:</strong> Để hoàn tất quá trình tạo tài khoản, hệ thống sẽ yêu cầu xác thực tài khoản qua
                    email đã đăng ký.
                </li>
            </ul>
        </div>
    </div>

    <p>Sau khi tài khoản được tạo thành công, bạn có thể tuỳ ý chính sửa Profile hoặc tìm kiếm và tương tác không
        giới hạn với các nội dung trên Inplace rồi.</p>
</div>
