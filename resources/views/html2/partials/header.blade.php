<header class="container-fluid position-relative bg-white shadow-sm">
    <div class="row inner-header">
        <div class="col-2 col-md-6 col-lg-auto logo mb-2 mb-lg-0">
            <a href="#" title="" class="">
                <img src="{{ asset('user/image/logo-short.png') }}" alt=""/>
            </a>
        </div>
        <div class="col-10 col-md-6 col-lg-auto">
            <ul class="header-menu nav mt-lg-1 font-weight-medium">
                <li class="nav-item">
                    <a href="#" title="" class="nav-link pl-lg-0">
                        <i class="icon-menu fa-lg ipl-office-briefcase text-green-300"></i>
                        <span class="d-none d-sm-inline-block">Việc làm</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" title="" class="nav-link active">
                        <i class="icon-menu fa-lg ipl-news-paper text-green-300"></i>
                        <span class="d-none d-md-inline-block">Khám phá</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" title="" class="nav-link ">
                        <i class="icon-menu fa-lg ipl-edit text-green-300"></i>
                        <span class="d-none d-md-inline-block">Viết bài</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-7 col-lg px-lg-0 mb-2 mb-lg-0">
            <form method="GET" action="" class="form-search-nav d-lg-flex mt-1">
                <div class="input-group bg-white-500">
                    <input type="text" name="keyword" placeholder="Nhập tìm kiếm công việc, bạn bè và công ty"
                           class="form-control input-search border-right-0 border-white-600 bg-transparent">
                    <div class="input-group-append ">
                        <button type="submit" class="input-group-text btn-search-icon bg-transparent border-white-600 border-left-0">
                            <i class="fa-lg ipl-search text-green-300"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-5 col-lg-auto ml-auto">
            <div class="profile-tools d-flex align-items-center mt-lg-1">
                <div class="header-comment px-2 px-md-3 mt-1 mt-lg-0">
                    <a href="#" title="" class="">
                        <i class="fa-lg ipl-comment"></i>
                    </a>
                </div>
                <div class="header-notify px-2 px-md-3 mt-1 mt-lg-0">
                    <a href="#" title="" class="">
                        <i class="fa-lg ipl-ring "></i>
                    </a>
                </div>
                <div class="header-profile pl-2 pl-lg-3 dropdown" id="profileDropdown">
                    <button type="button" class="user-avatar bg-grey text-white rounded-circle border text-center p-0 overflow-hidden dropdown-toggle"
                            id="profileDropdownMenuButton" data-toggle="dropdown" data-display="static">
                        <img src="https://via.placeholder.com/40x40">
                    </button>
                    <div class="dropdown-menu border-0 shadow-sm dropdown-menu-right" id="profileDropdownMenu" aria-labelledby="profileDropdownMenuButton">
                        <div class="over-content-menu">
                            <div class="group-item border-bottom d-flex px-20 py-15 profile-info">
                                <div class="info-name">
                                    <div class="text-black-400 d-block">
                                    <span class="username font-weight-bold d-inline-block">
                                        John Doe
                                    </span>
                                        <span class="score bg-green-300 text-white font-weight-black">
                                        1288Đ
                                    </span>
                                    </div>
                                    <a href="#" title="" class="text-size-13">
                                        Chỉnh sửa trang cá nhân
                                    </a>
                                </div>
                                <div class="user-avatar ml-auto mt-n1">
                                    <img src="https://via.placeholder.com/48x48" class="rounded-circle">
                                </div>
                            </div>
                            <div class="group-item border-bottom py-15 bookmark">
                                <div class="menu-line">
                                    <a href="#" title="" class="active">
                                        <i class="icon-link far fa-paper-plane"></i>
                                        <span>
                                        Công việc ứng tuyển
                                    </span>
                                    </a>
                                </div>
                                <div class="menu-line">
                                    <a href="#" title="">
                                        <i class="icon-link far fa-bookmark"></i>
                                        <span>
                                        Công việc đã lưu
                                    </span>
                                    </a>
                                </div>
                            </div>
                            <div class="group-item border-bottom py-15 setting">
                                <div class="menu-line">
                                    <a href="#" title="">
                                        <i class="icon-link fas fa-building"></i>
                                        <span>
                                        Công ty đang theo dõi
                                    </span>
                                    </a>
                                </div>
                                <div class="menu-line">
                                    <a href="#" title="">
                                        <i class="icon-link fas fa-user-plus"></i>
                                        <span>
                                        Mời bạn bè tham gia Inplace
                                    </span>
                                    </a>
                                </div>
                                <div class="menu-line">
                                    <a href="#" title="">
                                        <i class="icon-link fas fa-user-cog"></i>
                                        <span>
                                        Cài đặt tài khoản
                                    </span>
                                    </a>
                                </div>
                                <div class="menu-line">
                                    <a href="#" title="">
                                        <i class="icon-link fas fa-sign-out-alt"></i>
                                        <span>
                                        Thoát
                                    </span>
                                    </a>
                                </div>
                            </div>
                            <div class="group-item py-15 account-list">
                                <div class="item-account">
                                    <a href="#" title="" class="d-flex text-black-400 active">
                                        <div class="account-avt">
                                            <img src="https://via.placeholder.com/40x40" class="rounded-circle">
                                        </div>
                                        <div class="account-name align-self-center">
                                            John Doe
                                        </div>
                                        <div class="check align-self-center ml-auto">
                                            <i class="fas fa-check text-grey-200"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-account">
                                    <a href="#" title="" class="d-flex text-black-400">
                                        <div class="account-avt">
                                            <img src="https://via.placeholder.com/40x40" class="rounded-circle">
                                        </div>
                                        <div class="account-name align-self-center">
                                            HRE Empire Vietnam
                                        </div>
                                        <div class="check align-self-center ml-auto">
                                            <i class="fas fa-check text-grey-200"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-account">
                                    <a href="#" title="" class="d-flex text-black-400">
                                        <div class="account-avt">
                                            <img src="https://via.placeholder.com/40x40" class="rounded-circle">
                                        </div>
                                        <div class="account-name align-self-center">
                                            Aglobe Tech VN
                                        </div>
                                        <div class="check align-self-center ml-auto">
                                            <i class="fas fa-check text-grey-200"></i>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-account action-create-account">
                                    <a href="#" title="" class="d-flex">
                                        <div class="add-account rounded-circle bg-grey d-flex justify-content-center mr-10">
                                            <i class="fas fa-plus align-self-center"></i>
                                        </div>
                                        <div class="account-name  align-self-center">
                                            Tạo trang doanh nghiệp
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
