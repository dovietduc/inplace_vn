@extends('layouts.login')

@section('meta')
    <title>Login - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link href="{{ asset('pages/users/forgot/forgot.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop


@section('content')
    @include('partials.header-forgot')
    <div class="forgot-content">
        <h1 class="tittle">Đặt lại mật khẩu của bạn</h1>
        <p class="txt">Vui lòng tạo một mật khẩu mới cho tài khoản của bạn.</p>
        <form method="POST" action="{{ route('password.update') }}" class="form">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <label>Email</label>
                <input
                    id="email"
                    type="email"
                    class="form-control"
                    name="email" value="{{ $email ?? old('email') }}"
                    required
                    readonly="readonly"
                    autocomplete="email"
                    autofocus
                >
            </div>
            <div class="form-group">
                <label for="password">Mật khẩu mới</label>
                <input id="password"
                       placeholder="Nhập mật khẩu mới"
                       type="password"
                       class="form-control @error('password') is-invalid @enderror"
                       name="password"
                       required
                       minlength="8"
                >

                @error('password')
                    <p class="form-alert"><i class="fas fa-exclamation-circle"></i>{{ $message }}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="password-confirm">Xác nhận mật khẩu mới</label>
                <input
                    id="password-confirm"
                    placeholder="Nhập lại mật khẩu"
                    type="password"
                    class="form-control"
                    name="password_confirmation"
                    required
                    minlength="8"
                    autocomplete="new-password">
            </div>
            <button type="submit" class="btn btn-custom">Đặt mật khẩu mới</button>
        </form>
    </div><!-- /forgot-content -->
    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/users/forgot/forgot.js') }}"></script>
@stop


