
@extends('emails.layouts.for-user')

@section('title', 'Invite Member')

@section('content')
    <p>
        Xin chào,
        <strong>{{ !empty($dataMember['userAdded']) ? $dataMember['userAdded']->name : '' }}</strong>
    </p>
    <p>
        <strong>{{ $dataMember['userLogin']->name }}</strong> đã thêm bạn làm thành viên của <strong>
            {{ $dataMember['customers']->name }}</strong> tại INPLACE. <br>
        Bạn có thể chấp nhận hoặc từ chối lời đề nghị tại đây:
    </p>
    <center>
        <a href={{ url("click/companies/" . $dataMember['customer_id'] . "/employee_invitations/" . $dataMember['token']) }}>
            Xác nhận thành viên
        </a>
    </center>
@endsection




