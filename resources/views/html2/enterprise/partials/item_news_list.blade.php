<div class="item pb-2 border-bottom pt-40">
    <div class="post-cover w-100 mb-4">
        <img src="{{ url('user/image/no-image-lg.png') }}" alt="" class="rounded w-100">
    </div>
    <h3 class="mb-15">
        <a href="#" title="">
            Cảm xúc trong nghề của những Cô gái làm Tuyển dụng
        </a>
    </h3>
    <div class="post-desc mb-3">
        Nếu như trước đây, nhân viên chỉ cần vững chuyên môn thì ngày nay điều đó là chưa đủ. Có lẽ thời thế đã đổi thay, ngoại hình dần trở thành một tiêu chí lựa chọn của nhà tuyển dụng, trở thành một yếu tố ảnh hưởng đến phát triển nghề nghiệp của mỗi nhân viên.
    </div>
    <div class="post-auth d-flex">
        <div class="auth-avatar mr-2 text-center">
            <img src="https://via.placeholder.com/30x30" alt="..." class="rounded-circle border">
        </div>
        <div class="auth-name align-self-center text-size-13 font-weight-medium mr-20 mr-lg-30">
            <a href="#" title="">Thuy Duong</a>
        </div>
        <div class="post-time text-size-13 text-grey-200 align-self-center font-weight-regular">
            <i class="far fa-clock mr-1"></i>
            <span>1 giờ trước</span>
        </div>
        <div class="ml-auto post-like-section align-self-center">
            <div class="counter d-inline-block">
                <span class="text-size-13 text-grey-200">
                    128k Thích
                </span>
            </div>
            <div class="like-action d-inline-block pl-10 pl-lg-15">
                <button class="btn btn-link p-0 line-height-sm">
                    <i class="{{ (rand(0,1))? 'fas': 'far' }} fa-heart text-size-14 text-green-300"></i>
                </button>
            </div>
        </div>
    </div>
</div>
