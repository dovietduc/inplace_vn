<?php

namespace App\Http\Controllers\Admin;

use App\Job;
use App\JobAdmin;
use App\StatusJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class JobController extends Controller
{
    private $job;
    private $statusJobs;
    public function __construct(JobAdmin $job, StatusJobs $statusJobs)
    {
        $this->job = $job;
        $this->statusJobs = $statusJobs;
    }

    public function list(Request $request)
    {

        $jobsStatus = $this->statusJobs->latest()->get();
        if (!empty($request->submit)) {
            $jobs = $this->job->searchJobsAdmin($request);
        } else {
            $jobs = $this->job->getManyWhereOrderPagination();
        }


        return view('admin.jobs.list', compact('jobs', 'jobsStatus'));
    }

    public function changeActiveJob(Request $request, $id)
    {
        $this->job->updateData($id, ['active' => $request->active]);
        return response()->json([
            'code' => 200,
            'message' => 'success'
        ], 200);

    }

    public function changeHotJob(Request $request, $id)
    {
        $job = $this->job->findById($id);
        $this->job->updateData($id, ['hot' => $request->isChecked]);
        return response()->json([
            'code' => 200,
            'message' => 'success',
        ], 200);

    }

    public function changeShowSliderJobHome(Request $request, $id)
    {
        $job = $this->job->findById($id);
        $this->job->updateData($id, ['slider_home' => $request->isChecked]);
        return response()->json([
            'code' => 200,
            'message' => 'success',
        ], 200);
    }

    public function pushTop(Request $request, $id)
    {
        $this->job->updateData($id, ['created_at' => Carbon::now()->toDateTimeString()]);
        return response()->json([
            'message' => 'success',
        ], 200);
    }
}
