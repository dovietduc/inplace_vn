@extends('layouts.login')

@section('meta')
    <title>Vertify - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link href="{{ asset('pages/users/verify/verify.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @include('partials.header-signin')

    <div class="verify-register">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="sologan">
                        <h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
                        <div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex justify-content-center justify-content-lg-end">
                    <div class="form">
                        <div class="title text-center">Kiểm tra email ngay nào!</div>

                        <div class="mgs">
                            <div class="icon"><i class="far fa-check-circle"></i></div>
                            Xin mời vào email để active tài khoản.<br>Hẹn gặp bạn bên trong nhé.
                        </div>

                        <div class="resend">
                            <p>Bạn chưa nhận được email?</p>
                            <a style="color: #3ea37d;" href="{{ route('verification.resend') }}">Gửi lại email</a>
                        </div>

                        <div class="regiter text-center">Bạn chưa có tài khoản? <a href="{{ route('register') }}">Đăng ký ngay</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .forgot -->


    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('pages/users/verify/verify.js') }}"></script>
@stop
