<div class="txt-format">
    <h1>Đổi mật khẩu</h1>
    <p>Nếu bạn đã đăng nhập vào INPLACE, bạn có thể thay đổi mật khẩu từ phần cài đặt của mình.</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_doi_mat_khau.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp <strong>Cài đặt tài khoản</strong> trên Dropdown menu bên tay phải</li>
                <li><strong>Bước 2:</strong> Chọn Đổi mật khẩu của phần Cài đặt tài khoản trên màn hình hiển thị</li>
                <li><strong>Bước 3:</strong> Nhập mật khẩu cũ để xác nhận lại tài khoản.</li>
                <li><strong>Bước 4</strong>: Nhập mật khẩu mới để thay đổi mật khẩu của bạn.</li>
                <li><strong>Bước 5:</strong> Nhấp vào <strong>Xác nhận</strong> để hoàn tất quá trình.</li>
            </ul>
            <p>Sau khi đổi mật khẩu, bạn sẽ phải đăng nhập lại tài khoản bằng mật khẩu mới để có thể sử dụng các tính năng
                của INPLACE.&nbsp;</p>
            <p><i class="fad fa-info-circle"></i> <strong>Lưu ý:</strong> Hãy chọn mật khẩu mạnh mà bạn chưa sử dụng với tài khoản này. Khi tạo mật khẩu mới,
                hãy lưu ý chọn sao cho mật khẩu nên dễ nhớ với bạn nhưng khó đoán với người khác. Bạn có thể kết hợp chữ
                hoa, chữ thường và số cho mật khẩu của mình.</p>
        </div>
    </div>

</div>
