import {searchHeader} from './_elements/boxSearchHeader.js';

let Main = {};


Main.togglePassword = function() {
    let input = $(this).next('input');
    if (input.attr('type') === 'password') {
        input.attr('type', 'text');
    } else {
        input.attr('type', 'password');
    }
    $(this).toggleClass('active');
    input.focus();
    return false;
};

Main.saveJob = function(event) {
    event.preventDefault();
    let urlAjax = $(this).data('url');
    let that = $(this);
    $.ajax({
        url: urlAjax,
        type: 'POST',
        success: function (data) {
            toastr.success(data.message, {timeOut: 5000});
            that.removeClass('save_job').addClass('saved').prop('disabled', true);
            $('.btn-custom_light').removeClass('save_job').addClass('saved').prop('disabled', true);

        },
        error: function () {

        }
    });
};


Main.shareFacebook = function (event) {
    event.preventDefault();
    let popupSize = {
        width: 780,
        height: 550
    };
    let
        verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
        horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

    let popup = window.open($(this).prop('href'), 'social',
        'width=' + popupSize.width + ',height=' + popupSize.height +
        ',left=' + verticalPos + ',top=' + horisontalPos +
        ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

    if (popup) {
        popup.focus();
        event.preventDefault();
    }

};

// Hide Header on on scroll down
let didScrollWindow;
let lastScrollWindowTop = 0;
function hasScrolled() {
    let st = $(window).scrollTop();
    if (st > lastScrollWindowTop && st > $('.header').outerHeight()){// Scroll Down
        $('.header').removeClass('animated fadeInDown sticky-top');
    } else {// Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.header').addClass('animated fadeInDown sticky-top');
        }
    }
    lastScrollWindowTop = st;
}

$(function() {
    //header toggle when scroll page
    $(window).scroll(function(event){
        didScrollWindow = true;
    });
    setInterval(function() {
        if (didScrollWindow) {
            hasScrolled();
            didScrollWindow = false;
        }
    }, 250);


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    searchHeader();
	$(document).on('click', '.js-displayPassword', Main.togglePassword);
	// Save job
    $(document).on("click", ".save_job", Main.saveJob);
    // share facebook
    $(document).on('click', '.share_facebook_social', Main.shareFacebook);


    if ($('.box_content').length && $('.box_sidebar').length) {
        $('.box_content, .box_sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    }


});




