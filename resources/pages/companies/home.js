const axios = require('axios');
function renderHtmlFollow(data) {
    return `<span class="${data.message=='Đã là fan' ? 'active' : ''}">${data.message}</span>`;
}

function toogleStatusFollowCustomer(){
    let url = $(this).data('url');
    let that = $(this);
    axios.get(url, {
        params: {
            follow_type: 'company_detail'
        }
    })
        .then(function (response) {
            that.html(renderHtmlFollow(response.data));
            if ( $( ".label_count_user_follow" ).length ) {
                that.parent().find('.label_count_user_follow').html(response.data.user_follow_count + ' Fan');
            }
        })
        .catch(function (error) {
            console.log(error);
        });

};

$(document).ready(function () {
    $('.job-now_slick').slick({
        infinite: true,
        dots: false,
        lazyLoad: 'ondemand',
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        nextArrow: '<button class="slick_nav slick_next"><i class="fal fa-chevron-right"></i></button>',
        prevArrow: '<button class="slick_nav slick_prev"><i class="fal fa-chevron-left"></i></button>',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    $('.follow_customer_company_detail').on('click', toogleStatusFollowCustomer);
});
$('.tab-who a').hover(function () {
    $(this).tab('show')

});

