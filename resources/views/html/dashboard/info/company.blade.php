@extends('layouts.dashboard')
@section('meta')
    <title>Company Info Dashboard</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/Semantic-UI-Calendar/calendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pages/enterprise/info/company.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Xin chào <strong>J-JOB Executive Search</strong>
                    <span class="title-sub">Giới thiệu về doanh nghiệp của bạn với các ứng viên tiềm năng!</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="dashboard-info-company">
        <div class="container">
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Thông tin công ty
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="section-info_form">
                    <form class="formEditCompanyInfoDashboard" action="" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Slogan</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Slogan" name="Slogan">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Giới thiệu chung</label>
                                </div>
                                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="Introduction"
                                        maxlength="255"
                                    ></textarea>
                                    <span class="note"><span
                                            class="textarea-counter-result">255</span> ký tự còn lại</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Website</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="url" class="form-control inp-url" placeholder="http://..."
                                           name="Website">
                                    <div class="err-inp"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Điện thoại</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control inp-tel"
                                           placeholder="Số điện thoại"
                                           pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+"
                                           name="Phone"
                                           required>
                                    <div class="err-inp"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Giám đốc / Nhà sáng lập</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Tên Giám đốc/ Nhà sáng lập"
                                           name="Manager">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Số Lượng Nhân Sự</label>
                                </div>
                                <div class="col-md-8">
                                    <select name="NumberEmployees" class="select2-noSearch" data-placeholder="Số lượng nhân sự">
                                        <option value=""></option>
                                        <option value="">< 10 người</option>
                                        <option value="">11-20 người</option>
                                        <option value="">21-50 người</option>
                                        <option value="">51-100 người</option>
                                        <option value="">>100 người</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Năm thành lập</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="reset-semantic-calendar">
                                        <label for="founded-year"><i class="far fa-calendar-alt"></i></label>
                                        <input type="text" id="founded-year" name="YearFounded"
                                               placeholder="Năm thành lập"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Lĩnh vực hoạt động</label>
                                </div>
                                <div class="col-md-8">
                                    <select class="select-2-field-activity" name="Field-Activity" multiple>
                                        <option value="Frontend Deverloper">Frontend Deverloper</option>
                                        <option value="Backend Deverloper">Backend Deverloper</option>
                                        <option value="Manager">Manager</option>
                                        <option value="Frontend Deverloper">Frontend Deverloper</option>
                                        <option value="Backend Deverloper">Backend Deverloper</option>
                                        <option value="Manager">Manager</option>
                                        <option value="Frontend Deverloper">Frontend Deverloper</option>
                                        <option value="Backend Deverloper">Backend Deverloper</option>
                                        <option value="Manager">Manager</option>
                                    </select>
                                    <span class="note">Tối đa 3 ngành nghề</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Bản đồ</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group_map">
                                        <input type="text" id="input-map" name="Map" class="form-control"
                                               placeholder="Chia sẻ địa điểm Google Map">
                                        <label for="input-map"><i class="fas fa-map-marker-alt"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Địa chỉ</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="dynamicallRowsAddress form-group_address">
                                        <div class="dynamicallRowsAddress_result">
{{--                     ================List Địa chỉ đã có trong database được show ở đây===========--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-sm-4">--}}
{{--                                                    <div class="form-group_address_city">--}}
{{--                                                        <select class="dynamicallRowsAddress_city select2-city select2-hidden-accessible" data-placeholder="Chọn thành phố" tabindex="-1" aria-hidden="true">--}}
{{--                                                            <option value=""></option>--}}
{{--                                                            <option value="Hà Nội" selected>Hà Nội</option>--}}
{{--                                                            <option value="TP.Hồ Chí Minh">TP.Hồ Chí Minh</option>--}}
{{--                                                            <option value="Quảng Ninh">Quảng Ninh</option>--}}
{{--                                                            <option value="Đà Nẵng">Đà Nẵng</option>--}}
{{--                                                            <option value="Thái Bình">Thái Bình</option>--}}
{{--                                                            <option value="Hải Phòng">Hải Phòng</option>--}}
{{--                                                        </select>--}}
{{--                                                        <label><i class="fas fa-map-marker-alt"></i></label>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-sm-8">--}}
{{--                                                    <div class="form-group_address_branch">--}}
{{--                                                        <input type="text" value="Cầu Giấy" class="dynamicallRowsAddress_branch form-control" placeholder="Nhập đia chỉ văn phòng / chi nhánh của bạn">--}}
{{--                                                        <label><i class="fas fa-map-signs"></i></label>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <button class="dynamicallRowsAddress_remove form-group_address_remove"><i class="fal fa-times"></i></button>--}}
{{--                                            </div>--}}
                                        </div>
                                        <div class="form-group_address_btnAdd">
                                            <button type="button"
                                                    class="dynamicallRowsAddress_btnAdd btn btn-custom_light"><i
                                                    class="fal fa-plus"></i> Thêm địa chỉ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-group_submit">
                                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="section-info">
                <div class="section-info_form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh logo<span>(128x128px)</span></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group_upload-image">
                                        <img src="">
                                        <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh bìa<span>(2560x800px)</span></label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group_upload-image">
                                        <img src="">
                                        <input type="file" class="inp-file-big-image" accept=".png,.jpg,.jpeg"/>
                                    </div>
                                    <div class="custom-checkbox mt-3">
                                        <input class="form-check-input" type="checkbox" id="delete-cover-company">
                                        <label for="delete-cover-company">Xóa ảnh này</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-group_submit">
                                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        What we do
                    </h3>
                    <div class="desc"></div>
                </div>
                <a href="" class="view-more section-info_toggle">Thu gọn</a>
                <div class="section-info_form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Giới thiệu chung</label>
                                </div>
                                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="Introduction"
                                        maxlength="255"
                                    ></textarea>
                                    <span class="note"><span
                                            class="textarea-counter-result">255</span> ký tự còn lại</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 1</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 2</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-group_submit">
                                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="section-info hide">
                <div class="component-title">
                    <h3 class="title">
                        Why we do
                    </h3>
                    <div class="desc"></div>
                </div>
                <a href="" class="view-more section-info_toggle">Chỉnh sửa</a>
                <div class="section-info_form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Giới thiệu chung</label>
                                </div>
                                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="Introduction"
                                        maxlength="255"
                                    ></textarea>
                                    <span class="note"><span
                                            class="textarea-counter-result">255</span> ký tự còn lại</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 1</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 2</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-group_submit">
                                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="section-info hide">
                <div class="component-title">
                    <h3 class="title">
                        How we do
                    </h3>
                    <div class="desc"></div>
                </div>
                <a href="" class="view-more section-info_toggle">Chỉnh sửa</a>
                <div class="section-info_form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Giới thiệu chung</label>
                                </div>
                                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="Introduction"
                                        maxlength="255"
                                    ></textarea>
                                    <span class="note"><span
                                            class="textarea-counter-result">255</span> ký tự còn lại</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 1</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-md-right">
                                    <label class="section-info_label" for="">Ảnh số 2</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8 col-lg-7">
                                            <div class="form-group_upload-image">
                                                <img src="">
                                                <input type="file" class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                                            </div>
                                            <textarea class="form-control autoheight disenter mt-3"
                                                      placeholder="Viết chú thích..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-group_submit">
                                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('vendor/Semantic-UI-Calendar/calendar.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/info/company.js')}}"></script>
@stop
