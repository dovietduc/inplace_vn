@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/companies/members/members.css') }}">
@stop
@section('content')
    <div class="page-enterprise">
        @include('company.partials.company-head-menu')
        <div class="members-enterprise-page">
            <div class="members-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            @include('company.partials.who-is-customer')
                        </div>
                    </div>
                </div>
            </div>

            @if(count($getAllMember) > 4)
                <div class="container">
                    <div class="members-list">
                        <div class="row">
                            @foreach($getAllMember as $memberItem)
                                <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                                    <div class="members-item">
                                        <a href="">
                                            @if(empty(optional($memberItem->user)->avatar))
                                                <img class="avatar" src="{{asset('images/no_user_125x125.jpg')}}"
                                                     alt="">
                                            @else
                                                @if(str_contains(optional($memberItem->user)->avatar, 'upload') && jjobCheckImageExit(optional($memberItem->user)->avatar))
                                                    <img class="avatar"
                                                         src="{{ asset(optional($memberItem->user)->avatar) }}" alt="">
                                                @else
                                                    <img class="avatar" src="{{ optional($memberItem->user)->avatar }}"
                                                         alt="">
                                                @endif
                                            @endif

                                            <h5 class="name">
                                                {{ optional($memberItem->user)->name }}
                                            </h5>
                                            <span class="state">
                                            {{ $memberItem->position }}
                                        </span>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            @endif


        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('pages/companies/members/members.js') }}"></script>
@stop
