<div class="component-posts-item-sidebar">
    <div class="posts-item-thumb">
        <a href="#" title="" class="">
            <img src="/images/no_img_50x50.jpg" alt="..." class="rounded-circle">
            <div class="post-order">
                {{ $number }}
            </div>
        </a>
    </div>
    <div class="posts-item-content">
        <h6 class="posts-item-title">
            <a href="#" title="" class="">
                Cách xây dựng quan hệ lâu bền với khách hàng khi bạn làm sales
            </a>
        </h6>
        <div class="posts-item-auth">
            <a href="" class="name">John Doe</a>
            <span class="job">
                Marketing
            </span>
        </div>
    </div>
</div>
