<div class="cover-page position-relative">
    <img src="https://via.placeholder.com/1920x400" alt="" class="w-100"/>
</div>
<div class="nav-of-page shadow-sm mt-n30 mb-50 pb-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-auto mb-3 mb-lg-0">
                <div class="media">
                    <img alt="..." src="https://via.placeholder.com/122x122"
                         class="company-avatar border border-white rounded-circle mr-20">

                    <div class="media-body align-self-center">
                        <h3 class="company-name text-black-400 font-weight-bold mt-lg-15 mb-1">
                            J-JOB Executive Search
                        </h3>
                        <div class="text-grey-800 text-size-13">
                            Staffing and Recruiting • Hanoi • 2899 Fan
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-auto ml-auto align-self-center mb-3 mb-lg-0 company-actions-with-btn">
                <button class="btn btn-outline-green-300 px-20 mr-2">
                    <span>
                        <i class="far fa-comment-alt"></i> Nhắn tin
                    </span>
                </button>
                <button class="btn btn-green-300 px-20">
                    <span>
                        <i class="far fa-heart"></i> Trở thành Fan
                    </span>
                </button>
            </div>
            <div class="w-100 clearfix"></div>
            <div class="menu-nav-enterprise px-15 px-lg-0 mt-lg-n15">
                <div class="nav nav-line nav-link-padding-y-0">
                    @foreach([
                    'Trang chủ', 'Việc làm', 'Bài viết', 'Thư viện ảnh', 'Video', 'Văn phòng ảo', 'Q&A', 'Quiz'
                    ] as $nav)
                        <div class="nav-item">
                            <a class="nav-link {{ $loop->index == 0 ? 'active' : '' }}" href="#">
                                {{ $nav }}
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
