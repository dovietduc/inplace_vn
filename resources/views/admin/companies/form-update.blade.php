<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update customer</title>
    <link href="{{ asset('vendor/bootstrap-v4/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (session('message'))
                <div class="dashboard-news-alert">
                    <div class="container">
                        <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                            <span>
                               {{ session('message') }}
                            </span>
                            <button type="button" class="close py-2" data-dismiss="alert" aria-label="Close"><i
                                    class="fal fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <form method="post" action="">
                @csrf
                <div class="form-group">
                    <label for="company">Chọn công ty</label>
                    <select class="form-control" id="company" name="customer">
                        <option value=""></option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}">
                                {{ $customer->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="eamil">Email address</label>
                    <input
                        type="email"
                        class="form-control"
                        name="email"
                        id="email"
                        placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input
                        type="text"
                        class="form-control"
                        name="name"
                        id="name"
                        placeholder="Enter name">
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>


<script src="{{ asset('vendor/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-v4/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>

<script>
    $(function () {
        $('#company').select2({
            placeholder: 'Chọn công ty'
        });
    });
</script>
</body>
</html>
