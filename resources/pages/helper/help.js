function tabMenuSidebarClick(){
    $('.menu-sidebar-multi a').removeClass('active');
    $(this).addClass('active');
    if($(this).parent().find('ul').length){
        $(this).removeClass('active');
        $('.menu-sidebar-multi li').removeClass('open');
        $(this).parent().toggleClass('open');
    }
}
function btnMenuHelp(){
    $('.menu-sidebar-multi').slideToggle()
}
$(function () {
    $(document).on('click','.menu-sidebar-multi a', tabMenuSidebarClick);
    $(document).on('click','.menu-sidebar-multi_btn', btnMenuHelp);

})
