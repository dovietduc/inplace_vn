function getOccupation() {
    let occupation = $(this);
    let occupationId = occupation.data('id');
    let occupationName = occupation.data('name');
    let occupationGroup = $('.filter--occupation-group--' + occupationId);
    let occupationText = occupation.parents('.filter-control').find('.filter-control__text');
    let occupationPlaceholder = occupationText.find('span').eq(0);
    let divOccupationSup = $('.filter--occupation-supporting');
    if (occupation.is(':checked')){
        occupationPlaceholder.addClass('d-none');
        occupationText.append('<span class="txt" data-id="'+occupationId+'">'+ occupationName +'</span>');
        divOccupationSup.addClass('d-block');
        occupationGroup.slideDown();
    }else {
        occupationText.find('.txt[data-id="' + occupationId + '"]').remove();
        occupationGroup.slideUp();
        if (occupationText.children().length < 2){
            occupationPlaceholder.removeClass('d-none');
            divOccupationSup.removeClass('d-block');
        }
    }
    $('#search-complete').submit();
}

function getOccupationSupporting(){
    let occupationSupThis = $(this);
    let occupationSupName = $(this).data('name');
    let occupationSupID = $(this).attr('id');
    let occupationSupBase = $('.selected-base--occupations');
    if (occupationSupThis.is(':checked')) {
        let htmlBase = '<label for="' + occupationSupID + '">' + occupationSupName + '</label>'
        occupationSupBase.append(htmlBase);
    } else {
        let thisBase = occupationSupBase.find('label[for="' + occupationSupID + '"]');
        if (thisBase.length) {
            thisBase.remove();
        }
    }
    $('#search-complete').submit();
}

export function addDataOccupationToFormSubmit() {
    $(document).on('change', '.filter--occupation--checkbox', getOccupation);
    $(document).on('change', '.filter--occupation-supporting--checkbox', getOccupationSupporting);
}
