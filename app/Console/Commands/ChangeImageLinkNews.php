<?php

namespace App\Console\Commands;

use App\News;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class ChangeImageLinkNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:image-link-news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change image link news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Them thong tin va apply email
        try {
            DB::beginTransaction();
            News::orderBy('id')->chunk(100, function ($jobs) {
                foreach ($jobs as $job) {
                    $job->update(['thumbnail' => str_replace("https://timcongsu.vn/","", $job->thumbnail)]);
                }
            });
            DB::commit();
            $this->info('Convert news complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
