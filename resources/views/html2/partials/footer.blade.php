<footer class="footer">
    <div class="info">
        <div class="holder">
            <div class="list">
                <div class="item company">
                    <div class="wrap">
                        <h2 class="logo"><a href="#"><img src="/images/logo-footer.png" alt="logo-footer"></a></h2>
                        <p>Match values to place right people to right seats</p>
                        <p>Hotline: + 123 456 789<br>Email: hello@inplace.work</p>
                        <ul class="clearfix social">
                            <li class="linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li class="instagram"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Về chúng tôi</h2>
                        <ul class="clearfix menu">
                            <li><a href="#">Trang chủ</a></li>
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Chăm sóc khác hàng</a></li>
                            <li><a href="#">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Dành cho doanh nghiệp</h2>
                        <ul class="clearfix menu">
                            <li><a href="#">Tạo tranh Doanh nghiệp mới</a></li>
                            <li><a href="#">Đăng tuyển mới</a></li>
                            <li><a href="#">Đăng nội dung mới</a></li>
                            <li><a href="#">Tìm kiếm ứng viên</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Dành cho cá nhân</h2>
                        <ul class="clearfix menu">
                            <li><a href="#">Tạo trang cá nhân mới</a></li>
                            <li><a href="#">Tạo bài viết mới</a></li>
                            <li><a href="#">Update CV</a></li>
                            <li><a href="#">Việc làm của tôi</a></li>
                            <li><a href="#">Tìm kiếm doanh nghiệp</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="holder">
            <div class="left">InPlace © 2019. All Rights Reserved.</div>
            <div class="right">
                <a href="#">Quy định bảo mật</a>
                <span>・</span>
                <a href="#">Điều kiện và Thoả thuận sử dụng</a>
                <span>・</span>
                <a href="#">Quy chế hoạt động</a>
            </div>
        </div>
    </div>
</footer>
