<div class="company-job-now">
    <div class="container">
        <div class="component-title">
            <h3 class="title">
                <span>Job</span> now
            </h3>
            <div class="desc">
                Chúng tôi đang cần bạn
            </div>
        </div>
        <div class="job-now_slick row">
            @foreach ($jobs as $jobItem)
                <div class="col-12">
                    <div class="component-job-item">
                        <div class="job-item-thumb">
                            <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="" class="">
                                @if( jjobCheckImageExit($jobItem->feature_image) )
                                    <img src="{{ url($jobItem->feature_image) }}" class="w-100" alt="">
                                @else
                                    <img src="{{ asset('images/no_img_350x137.jpg') }}" class="w-100" alt="">
                                @endif
                            </a>
                        </div>
                        <div class="job-item-tag">
                            <div class="tag-list">
                                <a href="#" class="tag-item" title="">
                                    {{ $jobItem->short_title }}
                                </a>
                            </div>
                            <div class="locale">
                                 <i class="far fa-map"></i> {{ !empty($jobItem->citys) ? optional($jobItem->citys()->first())->name : '' }}
                            </div>
                        </div>
                        <h5 class="job-item-title">
                            <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="">
                                {{ $jobItem->title }}
                            </a>
                        </h5>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
