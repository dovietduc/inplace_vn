<div class="txt-format">
    <h1>Quên mật khẩu</h1>
    <p>Nếu người dùng quên mật khẩu và không thể đăng nhập vào tài khoản, hãy làm theo các bước sau:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_quen_mat_khau.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào <strong>Bạn quên mật khẩu</strong> trên màn hình đăng nhập.</li>
                <li><strong>Bước 2: </strong>Để đăng nhập lại tài khoản, bạn sẽ được yêu cầu đặt lại mật khẩu. Hãy nhập địa chỉ
                    email mà bạn đã đăng ký vào trường tương ứng trên màn hình sau đó nhấp <strong>Gửi</strong> để được hướng
                    dẫn.
                </li>
                <li><strong>Bước 3: </strong>Hãy kiểm tra email của bạn để đọc hướng dẫn lấy lại mật khẩu. Nếu chưa nhận được
                    email hướng dẫn, vui lòng chọn <strong>Gửi lại email</strong></li>
                <li><strong>Bước 4: </strong>Cuối cùng, nhấp vào link trong email để cập nhật mật khẩu mới cho tài khoản của
                    bạn. <br>Lưu ý: Hãy chọn mật khẩu mạnh mà bạn chưa sử dụng với tài khoản này. Khi tạo mật khẩu mới, hãy lưu
                    ý chọn sao cho mật khẩu nên dễ nhớ với bạn nhưng khó đoán với người khác. Bạn có thể kết hợp chữ hoa, chữ
                    thường và số cho mật khẩu của mình.
                </li>
                <li><strong>Bước 5:</strong> Để hoàn tất quá trình, nhấp <strong>Đặt lại mật khẩu</strong> và tiến hành đăng
                    nhập với mật khẩu vừa đặt.
                </li>
            </ul>
        </div>
    </div>
</div>
