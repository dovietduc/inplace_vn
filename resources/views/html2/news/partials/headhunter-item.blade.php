<div class="component-posts-item-headhunter">
    <div class="posts-item-thumb">
        <a href="#">
            <img src="/images/no_img_90x60.jpg">
        </a>
    </div>
    <div class="posts-item-content">
        <div class="posts-item-tag">
            <span>#Headhunter</span>
        </div>
        <a href="#" title="" class="posts-item-title">
            {{ $name }}
        </a>
    </div>
</div>
