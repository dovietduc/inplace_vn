@foreach($occupationSupportings as $occupationSupportingItem)
    <label
            class="filter-checkbox-item"
            for="checkbox-occupationSub-{{ $occupationSupportingItem->id }}"
            data-name="{{ $occupationSupportingItem->name }}"
            data-id="{{ $occupationSupportingItem->id }}">
        <span>{{ $occupationSupportingItem->name }}<em>Ngành</em></span>
    </label>
@endforeach
@foreach($occupationDetails as $occupationDetailItem)
    <label
            class="filter-checkbox-item"
            for="checkbox-occupationDetail-{{ $occupationDetailItem->id }}"
            data-name="{{ $occupationDetailItem->name }}"
            data-id="{{ $occupationDetailItem->id }}">
        <span>{{ $occupationDetailItem->name }}<em>Vị trí</em></span>
    </label>
@endforeach
