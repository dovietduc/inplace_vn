<div class="txt-format">
    <h1>Xây dựng Network</h1>
    <p>Tạo kết nối với nhiều người chính là cách người dùng cá nhân phát triển các mỗi quan hệ của bản thân (networking). Người có càng nhiều mối liên hệ (connections) sẽ tiếp cận dễ dàng hơn với các cơ hội về nghề nghiệp, phát triển, đào tạo, kinh doanh v.v.. mang lại từ các thành viên khác từ Inplace.</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_xay_dung_network.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <p><strong>Inplace Score:</strong> là điểm số đánh giá mức độ ảnh hưởng của Profile cá nhân hay Doanh Nghiệp trong cộng đồng Inplace. Điểm số này sẽ dựa một phần vào số lượng Kêt Nối (Connections) và số Fan của người dùng. Do đó, tích cực Kết Nối và tạo Fan chính là một trong những việc làm quan trọng trong quá trình xây dựng thương hiệu cá nhân và thương hiệu nhà tuyển dụng trên nền tảng của Inplace.
            </p>
        </div>
    </div>
</div>
