@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/news/list/list.css') }}">
@stop
@section('content')

    @include('news.partials.header-sub-nav')
    <div class="news-page">
        <div class="container">
            <div class="news-page_top">
                <div class="row">

                    @foreach($newsHeader as $newsHeaderItem)
                        <div class="col-sm-6 col-lg-3">
                            <div class="component-posts-item-headhunter">
                                <div class="posts-item-thumb">
                                    @if( jjobCheckImageExit($newsHeaderItem->thumbnail) )
                                        <a href="{{ route('news.show',
                                         ['slugCompany' => optional($newsHeaderItem->customer)->slug, 'id' => $newsHeaderItem->id]) }}">
                                            <img src="{{ asset($newsHeaderItem->thumbnail) }}">
                                        </a>
                                    @else
                                        <a href="#">
                                            <img src="{{ asset('images/no_img_90x60.jpg') }}">
                                        </a>
                                    @endif

                                </div>
                                <div class="posts-item-content">
                                    <div class="posts-item-tag">
                                        {{--<span>#Headhunter</span>--}}
                                    </div>
                                    <a
                                            href="{{ route('news.show', ['slugCompany' => optional($newsHeaderItem->customer)->slug, 'id' => $newsHeaderItem->id]) }}"
                                            title=""
                                            class="posts-item-title">
                                        {{ $newsHeaderItem->title }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="news-page_content">
                <div class="row">
                    <div class="col-md-8 box_content">
                        <div class="list-news">

                            @include('news.partials.item-new')

                        </div>
                        <div class="block-pagination">
                            {{ $news->links('vendor.pagination.default') }}
                        </div>
                    </div>

                    <div class="col-md-4 box_sidebar">
                        <div class="news-page_sidebar">
                            <div class="row">
                                <div class="col-sm-6 col-md-12 mt-5 mt-md-0">
                                    <h3 class="title-sidebar">Đang hot</h3>
                                    <div class="list-hot-news">
                                        @include('news.partials.item_hot_news')
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-12 mt-5">
                                    <h3 class="title-sidebar">Xem nhiều</h3>
                                    <div class="list-hot-news">
                                        @include('news.partials.views_new_count')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/news/list/list.js')}}"></script>
@stop
