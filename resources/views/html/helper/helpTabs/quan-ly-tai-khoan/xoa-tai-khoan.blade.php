<div class="txt-format">
    <h1>Xóa Tài khoản</h1>
    <p>Để ngừng sử dụng Inplace, bạn có thể xóa vĩnh viễn tài khoản cá nhân. Tuy nhiên, hãy chú ý, sau khi xóa tài
        khoản, bạn sẽ không thể lấy lại nội dung mình đã thêm và không thể sử dụng các chức năng của Inplace. Trang cá
        nhân, ảnh, bài viết, video và tất cả nội dung khác mà bạn đã thêm đều bị xóa vĩnh viễn. Hãy cân nhắc kỹ lưỡng
        trước khi xóa tài khoản nhé!
    </p>
    <p>Để xóa tài khoản, hãy làm theo các bước sau:
    </p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_xoa_tai_khoan.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Chọn <strong>Cài đặt tài khoản</strong> tại Dropdown</li>
                <li><strong>Bước 2:</strong> Chọn <strong>Xóa tài khoản </strong></li>
                <li><strong>Bước 3:</strong> Hãy cho chúng tôi biết lý do bạn xóa tài khoản bằng cách lựa chọn hoặc gửi
                    feedback cho quản trị viên.
                </li>
                <li><strong>Bước 4:</strong> Nhấp vào <strong>Xoá tài khoản</strong> để hoàn tất quá trình xóa tài khoản
                    trên Inplace
                </li>
            </ul>
        </div>
    </div>
</div>
