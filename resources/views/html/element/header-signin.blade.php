
<header class="header-signin">
	<div class="holder">
		<div class="logo"><a href="#"><img src="/images/logo.png" alt="logo"></a></div>
		<div class="menu">
			<ul class="clearfix">
				<li><a href="#">Giới thiệu</a></li>
				<li><a href="#">Tìm kiếm</a></li>
				<li><a href="#">Góc doanh nghiệp</a></li>
			</ul>
		</div>
		<div class="button">
			<a href="#">Đăng ký</a>
		</div>
		<div class="clear"></div>
	</div>
</header><!-- .header -->