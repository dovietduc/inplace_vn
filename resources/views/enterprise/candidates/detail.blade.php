@extends('layouts.dashboard')
@section('meta')
    <title>Chi tiết ứng viên</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/enterprise/candidates/detail.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                   Ứng viên <strong>{{ optional($candidate->user)->name }}</strong>
                </h1>
            </div>
        </div>
    </div>
    <div class="dashboard-info-candidate">
        <div class="container">
            <div class="candidate-info_job-apply">
                <div class="mb-2">
                    <span class="job-apply-label">Vị trí ứng tuyển:</span>
                    <div class="job-item-tag">
                        <div class="tag-list">
                            <a href="#" class="tag-item" target="_blank">
                                {{ optional($candidate->job)->title }}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mb-2">
                    <span class="job-apply-label">Thời gian ứng tuyển:</span><span>
                        {{  Carbon::createFromFormat('Y-m-d H:i:s', $candidate->created_at)
                                                ->format('d-m-Y') }}
                    </span>
                </div>
            </div>
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Thông tin cá nhân
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="candidate-info">
                    <div class="candidate-info_avatar">
                        @include('enterprise.partials.show-image-avatar-user', ['member' => $candidate])
                    </div>
                    <div class="candidate-info_table">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="component-table-info">
                                    <tr>
                                        <td>
                                            Ứng viên:
                                        </td>
                                        <td>
                                            <span class="text-green-300">
                                                {{ optional($candidate->user)->name }}
                                            </span>
                                        </td>
                                    </tr>
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            Ngày sinh:--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            02/03/1994--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            Giới tính:--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            Nữ--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="component-table-info">
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>
                                            <em><a href="mailto:{{ optional($candidate->user)->email }}">{{ optional($candidate->user)->email }}</a></em>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Số điện thoại:
                                        </td>
                                        <td>
                                            <a href="tel:{{ optional($candidate->user)->mobile }}">
                                                {{ optional($candidate->user)->mobile }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
{{--                                        <td>--}}
{{--                                            Địa chỉ:--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            Số 2, ngõ 82, Duy Tân, Cầu Giấy, Hà Nội--}}
{{--                                        </td>--}}
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Hồ sơ ứng viên
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="candidate-show-file">
                    <iframe src="https://docs.google.com/gview?url={{ asset($candidate->file_path) }}&embedded=true"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('pages/enterprise/candidates/detail.js') }}"></script>
@stop
