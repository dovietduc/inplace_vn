function InvalidMsgEmail() {
    if (this.value == '') {
        this.setCustomValidity('Xin hãy nhập email');
    }
    else if(this.validity.typeMismatch){
        this.setCustomValidity('Xin nhập đúng định dạng email');
    }
    else {
        this.setCustomValidity('');
    }
    return true;
}



function InvalidMsgPassWord() {
    if (this.value == '') {
        this.setCustomValidity('Xin hãy nhập password');
    }
    else if(this.validity.tooShort){
        this.setCustomValidity('Xin nhập tối thiểu 8 kí tự');
    }
    else {
        this.setCustomValidity('');
    }
    return true;
}

document.getElementById("email").addEventListener("invalid", InvalidMsgEmail);
document.getElementById("email").addEventListener("change", InvalidMsgEmail);
document.getElementById("password").addEventListener("invalid", InvalidMsgPassWord);
document.getElementById("password").addEventListener("change", InvalidMsgPassWord);
