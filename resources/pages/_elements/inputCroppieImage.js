let uploadCropDemo;

function validateFileImage(that) {
    let fileExtension = ['png', 'jpg', 'jpeg'];
    let fileExtentionCurrent = $(that).val().split('.').pop().toLowerCase();
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        swal({
            icon: "error",
            title: "Sai định dạng file!",
            text: "Vui lòng upload hình ảnh có định dạng *.png, *.jpg hoặc *.jpeg",
            button: {
                text: "Đóng",
                className: "btn-custom",
                closeModal: true,
            }
        });
        $(that).val('');
        uploadCropDemo.croppie('destroy');

    }
}

function showImageBeforeCrop(that) {
    let heightCrop = $('.inputCroppieImage').height();
    uploadCropDemo = $('.upload-crop-demo').croppie({
        viewport: {
            width: '100%',
            height: '100%',
        },
        enableExif: true,
        boundary: {
            height: heightCrop
        },
        mouseWheelZoom: false,
    });
    validateFileImage(that);
    if (that.files && that.files[0]) {
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(that.files[0]);
        reader.onload = function (e) {
            image.src = e.target.result;
            image.onload = function () {
                if (this.width < 600 || this.height < 250) {
                    uploadCropDemo.croppie('destroy');
                    swal({
                        icon: "error",
                        title: "Ảnh quá nhỏ!",
                        text: "Vui lòng chọn hình ảnh rộng ít nhất 600 pixel và cao ít nhất 250 pixel.",
                        button: {
                            text: "Đóng",
                            className: "btn-custom",
                            closeModal: true,
                        }
                    });
                    $('.croppieImage_input').val('');
                } else {
                    uploadCropDemo.croppie('bind', {
                        url: e.target.result
                    });
                    $('.inputCroppieImage_inpPr').hide();
                    $('.inputCroppieImage_crop').show();
                }
            }
        };
    }
}

function submitDataAfterCrop(event) {
    let type = event.data.type;
    if (uploadCropDemo) {
        uploadCropDemo.croppie('result', {
            type: 'canvas',
            format: 'png',
            size: {width: 1110, height: 435},
            circle: false
        }).then(function (resp) {
            $('.image_crop_data').val(resp);
            $('.type_submit').val(type);
            $('.formEditNewsDetail').submit();
        });
    } else {
        $('.type_submit').val(type);
        $('.formEditNewsDetail').submit();
    }
}

function jobSubmitDataAfterCrop(event) {
    let type = event.data.type;
    let showFormSalaryCheck = $(".job-salary-select option:selected").data('show_form_salary');
    if (uploadCropDemo) {
        uploadCropDemo.croppie('result', {
            type: 'canvas',
            format: 'png',
            size: {width: 1110, height: 435},
            circle: false
        }).then(function (resp) {
            $('.image_crop_data').val(resp);
            $('.type_submit').val(type);
            $('.show_form_salary').val(showFormSalaryCheck);
            $('.formJobAdd').submit();
        });
    } else {
        $('.type_submit').val(type);
        $('.formJobAdd').submit();
    }
}

function removeCrop() {
    $('.inputCroppieImage_crop').hide();
    $('.inputCroppieImage_inpPr').show();
    uploadCropDemo.croppie('destroy');
    $('.croppieImage_input').val('');
}

function dragoverImage(event) {
    event.preventDefault();
    event.stopPropagation();
    $('.inputCroppieImage_inpPr').addClass('dragover');
}

function dragleaveImage(event) {
    event.preventDefault();
    event.stopPropagation();
    $('.inputCroppieImage_inpPr').removeClass('dragover');
}


export function readFileImageCroppie() {
    $('.croppieImage_input').on('change', function () {
        showImageBeforeCrop(this);
    });

    $('.button-public-news').on('click', {type: 'public'}, submitDataAfterCrop);
    $('.button-draft-news').on('click', {type: 'draft'}, submitDataAfterCrop);
    $('.button-public-job').on('click', {type: 'public'}, jobSubmitDataAfterCrop);
    $('.button-draft-job').on('click', {type: 'draft'}, jobSubmitDataAfterCrop);


    //dragover
    $('.croppieImage_input').on('dragover', dragoverImage);
    $('.croppieImage_input').on('dragleave', dragleaveImage);
    // Remove image
    $('.btnRemoveCrop').on('click', removeCrop);
}





