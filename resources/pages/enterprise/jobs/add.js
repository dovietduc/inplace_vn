import swal from 'sweetalert';
import {dashboardLayout} from '../dashboard.js';
import {textareaAutoHeight} from '../../_elements/textareaAutoHeight.js';
import {readFileImageCroppie} from '../../_elements/inputCroppieImage.js';
import {textareaFroalaEditorInline} from '../../_elements/textareaFroalaEditorInline.js';
import {addMembersToJob} from '../../_elements/addMembersToJob.js';
import {textareaCouterLength} from '../../_elements/textareaCouterLength.js';
import {selectOccupation} from '../../_elements/selectOccupation.js';


function selectSalary() {
    let showFormSalaryCheck = $(".job-salary-select option:selected").data('show_form_salary');
    if (!showFormSalaryCheck) {
        $('.job-salary-pending').addClass('d-none');
        $('.job-salary-from').removeAttr('name');
        $('.job-salary-to').removeAttr('name');
    } else {
        $('.job-salary-pending').removeClass('d-none');
        $('.job-salary-from').attr('name', 'salary_min');
        $('.job-salary-to').attr('name', 'salary_max');
    }
}
function validateEmail(email) {
    var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return re.test(email);
}
function emailNotificationCheck(event) {
    event.preventDefault();
    let email_inp = $(this).val();
    let email_err = $(this).parent().children('.err-inp');
    if (!validateEmail(email_inp)){
        $(this).addClass('error');
        email_err.text('Sai định dạng email!').show();
    }else{
        $(this).removeClass('error');
        email_err.text('').hide();
    }
    if (!email_inp){
        $(this).removeClass('error');
        email_err.text('').hide();
    }
}
function checkEmptyField() {
    let flagCheck = true;
    if (!$('.inputCroppieImage').hasClass('hasImg') && !$('.upload-crop-demo').html().trim().length){
        flagCheck = false;
    }

    $('.check-value-submit-disable').each(function () {
        if (!$(this).val()) {
            flagCheck = false;
            return flagCheck;
        }
    });

    let showFormSalaryCheck = $(".job-salary-select option:selected").data('show_form_salary');
    if ( showFormSalaryCheck ) {
        if (!$('.job-salary-from').val() || !$('.job-salary-to').val()) {
            flagCheck = false;
        }
    };

    let EmailNotify = $('.email-notification-input');
    let EmailNotifyVal = EmailNotify.val();
    if(EmailNotifyVal){
        if(!validateEmail(EmailNotifyVal)) {
            flagCheck = false;
        }
    }else {
        flagCheck = false;
        return flagCheck;
    }

    return flagCheck;
}

function checkValueSubmitDisable() {
    let flagCheck = checkEmptyField();

    if ( flagCheck ) {
        $('.button-public-job').removeAttr('disabled');
    } else {
        $('.button-public-job').attr('disabled', true);
    }
}
$(function () {
    //Prevent users from submitting a form by hitting Enter
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    dashboardLayout();
    // on change show preview image and move crop before upload
    readFileImageCroppie();
    textareaCouterLength();
    textareaAutoHeight();
    selectOccupation();
    $('.fixHeight').each(function () {
        var a = $(this);
        var h = a.width() * 435 / 1110;
        a.height(h);
        $(window).resize(function () {
            a.height(a.width() * 435 / 1110);
        })
    });
    $('.select2-noSearch').select2({
        minimumResultsForSearch: Infinity,
    });
    $('.select2-mutiple-3item').select2({
        maximumSelectionLength: 3,
        language: {
            maximumSelected: function (e) {
                return "Bạn chỉ có thể lựa chọn tối đa " + e.maximum + " lựa chọn";
            }
        }
    });
    $('.select-working-parts').select2({
        containerCssClass: "your"
    });
    $('.select2-mutiple-tags-3item').select2({
        maximumSelectionLength: 3,
        language: {
            maximumSelected: function (e) {
                return "Bạn chỉ có thể lựa chọn tối đa " + e.maximum + " lựa chọn";
            }
        },
        tags: true,
        ajax: {
            url: $('.select2-mutiple-tags-3item').data('url'),
            data: function (params) {
                return {
                    search: params.term // search term
                };
            },
            delay: 250,
            processResults: function (data) {
                var myResults = [];
                for (var item in data) {
                    myResults.push({
                        'id': data[item].name,
                        'text': data[item].name
                    });
                }
                return {
                    results: myResults
                };
            },
        },
    });
    //froala editor
    textareaFroalaEditorInline();
    //END: froala editor
    checkValueSubmitDisable();
    //select salary
    $(document).on('change', '.job-salary-select', selectSalary);
    $(document).on('keyup change', '.email-notification-input', emailNotificationCheck);
    $(document).on('click', '.btnRemoveCrop, .select-occupation__option', checkValueSubmitDisable);
    $(document).on('keyup change', '.check-value-submit-disable, .job-salary-select, .job-salary-from, .job-salary-to, .email-notification-input, .croppieImage_input', checkValueSubmitDisable);
    addMembersToJob();


});
