<div class="modal d-block" id="modalChooseIndustry" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form
                    data-url="{{ route('jobs.ajax.occupation-update-user') }}"
                    action="" method="POST" class="occupation_update_user_form">
                    <h3>Lựa chọn công việc hiện tại của bạn</h3>
                    <div class="list-choose-industry">
                        @foreach($occupations as $key => $occupationItem )
                            <div class="form-group">
                                <label class="d-block">
                                    <input name="occupation_id"
                                           type="radio" class="d-none" value="{{ $occupationItem->id }}">
                                    <span class="form-control">{{ $occupationItem->name }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="err-inp mb-2 text-center">Vui lòng chọn một công việc mà bạn quan tâm!</div>
                    <button type="submit"
                            class="btn btn-custom w-100 update_occupation">
                        Xác nhận
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal-backdrop show"></div>
