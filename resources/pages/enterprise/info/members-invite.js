function showHidePassword(){
    let typeInp =  $('#input-password').attr('type');
    $(this).toggleClass('open');
    if (typeInp == 'password'){
        $('#input-password').attr('type','text');
    }else {
        $('#input-password').attr('type','password');
    }

}
function emptyValidateInput(event){
    event.preventDefault();

    let err =  $(this).parent().find('.err-inp');
    let attrInput = $(this).attr('placeholder');
    $(this).removeClass('error');
    err.text('').hide();
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        err.text(attrInput + " không được để trống!").show();
    }
}
$(function () {
    $('.label-input-password').on('click',showHidePassword);
    $('.empty-validate').on('change', emptyValidateInput);
    $('.empty-validate').on('invalid', emptyValidateInput);
});



