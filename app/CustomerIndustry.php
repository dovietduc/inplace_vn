<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class CustomerIndustry extends Model
{
    protected $table = 'jjob_industries';
    protected $guarded = [];
    use BaseEloquent;
}
