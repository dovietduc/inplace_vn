<div class="txt-format">
    <h1>Tìm kiếm Tin tức</h1>
        <p>Người
            dùng có thể đọc tin tức được người dùng/ doanh nghiệp khác chia sẻ bằng cách nhấp vào <strong>Khám
                phá</strong> trên thanh công cụ góc trên bên trái màn hình.&nbsp;</p>
        <p>Để tìm kiếm một tin tức nhất định, hãy nhập tên hoặc từ khóa liên quan đến bài viết trên thanh công cụ . Bạn
            có thể tìm kiếm các bài viết có cùng chủ đề bằng cách nhấn vào <strong>Chủ đề</strong> bên dưới mỗi bài viết
            hoặc theo các chuyên mục gợi ý của chúng tôi bên dưới thanh công cụ như<strong> Công ty, Văn hóa, Phỏng vấn
                nhân viên, Workstyle, Chuyện nghề, .... </strong></p>
        <p>Ngoài ra, hãy cập nhật các tin tức hot nhất và được quan tâm nhiều nhất tại <strong>Đang Hot</strong> và
            <strong>Xem Nhiều</strong>.</p>
</div>
