@extends('layouts.dashboard')
@section('meta')
    <title>News List Dashboard</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/news/list.css') }}">
@stop
@section('content')

    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Bài viết của <strong>
                        {{ !empty($customerCurrent) ? $customerCurrent->name : '' }}
                    </strong></h1>
                <div class="button-group">
                    <form class="form_filter_news" action="{{ route('enterprise.news.list') }}" method="get">
                        <select class="dashboard-header_filter select_news_choose" name="status_news">
                            <option {{ empty(request('status_news')) ? 'selected' : '' }} value="">Tất cả</option>
                            <option
                                {{ !empty(request('status_news')) && request('status_news') == \App\News::statusPublic ? 'selected' : '' }}
                                value="{{ \App\News::statusPublic }}">Đã đăng
                            </option>
                            <option
                                {{ !empty(request('status_news')) && request('status_news') == \App\News::statusDraft ? 'selected' : '' }}
                                value="{{ \App\News::statusDraft }}">Đã ẩn
                            </option>
                        </select>
                    </form>
                    <a href="{{ route('enterprise.news.add') }}"
                       class="btn btn-custom">
                        <i class="fas fa-plus mr-1"></i>
                        Đăng bài viết
                    </a>
                </div>
            </div>
        </div>
    </div>
    @if (session('message'))
        <div class="dashboard-news-alert">
            <div class="container">
                <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
            <span>
               {{ session('message') }}
            </span>
                    <button type="button" class="close py-2" data-dismiss="alert" aria-label="Close"><i
                            class="fal fa-times"></i></button>
                </div>
            </div>
        </div>
    @endif
    <div class="dashboard-news-list">
        <div class="container">
            <div class="dashboard-news-list_th">
                <div class="row">
                    <div class="col-5">
                        Tiêu đề
                    </div>
                    <div class="col-7">
                        <div class="row">
                            <div class="col-4">
                                Danh mục
                            </div>
                            <div class="col-3">
                                Người viết bài
                            </div>
                            <div class="col-5">
                                <div class="row">
                                    <div class="col-6">
                                        Trạng thái
                                    </div>
                                    <div class="col-6">
                                        Active
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @foreach($news as $newItem)
                <div class="dashboard-news-item">
                    <div class="row">
                        <div class="col-lg-5 dashboard-news-item_col">
                            <div class="dashboard-news-item_title">
                                <div class="row">
                                    <div class="col-sm-4 col-md-3 col-lg-4">
                                        <a href="#" class="thumb">
                                            @if( jjobCheckImageExit($newItem->thumbnail) )
                                                <img src="{{asset($newItem->thumbnail)}}" alt="">
                                            @else
                                                <img src="{{asset('images/no_img_180x71.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="col-sm-8 col-md-9 col-lg-8">
                                        <h4 class="title">
                                            <a href="#">
                                                {{ $newItem->title }}
                                            </a>
                                        </h4>
                                        <div class="d-flex align-items-center justify-content-between mt-2">
                                            <div class="time">
                                                <i class="far fa-clock"></i>
                                                {{  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $newItem->created_at)
                                                ->format('d-m-Y') }}

                                            </div>
                                            <div class="action">
                                                @if(!empty($newItem->status) && $newItem->status == \App\Job::statusPublic)
                                                    <a href="{{ route('news.show', ['slugCompany' => optional($newItem->customer)->slug, 'id' => $newItem->id]) }}"
                                                       class="action_view"
                                                       title="Xem"
                                                       target="_blank"><i
                                                            class="far fa-eye"></i></a>
                                                @endif
                                                <a href="{{ route('enterprise.news.edit',
                                                 ['id' => $newItem->id]) }}" class="action_edit" title="Sửa"><i
                                                        class="far fa-edit"></i></a>
                                                <a href=""
                                                   data-url="{{ route('enterprise.news.delete', ['id' => $newItem->id]) }}"
                                                   class="action_delete" title="Xóa"
                                                   data-id=""><i
                                                        class="far fa-trash-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 dashboard-news-item_col">
                            <div class="dashboard-news-item_info">
                                <div class="row">
                                    <div class="col-sm-7 col-md-4 col_cate">
                                        <span class="info-txt">
                                            {{ optional($newItem->category)->name }}
                                        </span>
                                    </div>
                                    <div class="col-8 col-sm-5 col-md-3 col_auth">
                                        <span class="info-txt">
                                            {{ optional($newItem->user)->name }}
                                        </span>
                                    </div>
                                    <div class="col-md-5 mt-2 mt-md-0">
                                        <div class="row">
                                            <div class="col-6 col_status">
                                                <span class="status-txt active">
                                                    {{ $newItem->status == \App\News::statusPublic ? 'Đã đăng' : 'Nháp' }}
                                                </span>
                                            </div>
                                            <div class="col-6">
                                                @if($newItem->status == \App\News::statusPublic)
                                                    @if($newItem->active == \App\News::active)
                                                        <span class="status-active active">Active</span>
                                                    @elseif($newItem->active == \App\News::rejectActive)
                                                        <span class="status-active deactive">Deactive</span>
                                                    @elseif($newItem->active == \App\News::waitting)
                                                        <span class="status-active">Waiting</span>
                                                    @else
                                                        <span class="status-active"></span>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                {{ $news->appends($_GET)->links('vendor.pagination.default') }}
            </div>

        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/news/list.js')}}"></script>
@stop
