<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaveJob extends Model
{
    use SoftDeletes, BaseEloquent;
    protected $guarded = [];
    protected $table = 'jjob_user_save_jobs';
}
