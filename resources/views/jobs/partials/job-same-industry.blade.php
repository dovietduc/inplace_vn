<div class="list-job">
    <div class="container">
        <h3 class="list-job_title">Công việc tương tự</h3>
        <div class="list-job_slick">
            @foreach($jobsFollowIndustry as $jobFollowIndustryItem)
                <div class="item">
                    <div class="list-job_item">
                        <div class="company">
                            <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}" class="logo" title="{{ optional($jobFollowIndustryItem->customer)->name }}">
                                @if(!empty($jobFollowIndustryItem->customer))
                                    @if( jjobCheckImageExit(optional($jobFollowIndustryItem->customer)->logo) )
                                        <img src="{{ asset(optional($jobFollowIndustryItem->customer)->logo) }}"
                                             alt="">
                                    @else
                                        <img src="{{ asset('images/no_avatar_company_30x30.jpg') }}"
                                             alt="">
                                    @endif
                                @endif
                            </a>
                            <h4 class="title">
                                <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}" title="{{ optional($jobFollowIndustryItem->customer)->name }}">
                                    {{ optional($jobFollowIndustryItem->customer)->name }}
                                </a>
                            </h4>
                        </div>
                        <div class="thumb">
                            <a href="{{ route('jobs.show', ['slug' => $jobFollowIndustryItem->slug]) }}" title="{{ $jobFollowIndustryItem->title }}">
                                @if( jjobCheckImageExit($jobFollowIndustryItem->feature_image) )
                                    <img src="{{ asset($jobFollowIndustryItem->feature_image) }}" alt="">
                                @else
                                    <img src="{{ asset('images/no_img_255x100.jpg') }}" alt="">
                                @endif
                            </a>
                        </div>
                        <h3 class="title">
                            <a href="{{ route('jobs.show', ['slug' => $jobFollowIndustryItem->slug]) }}" title="{{ $jobFollowIndustryItem->title }}">
                                {{ $jobFollowIndustryItem->title }}
                            </a>
                        </h3>
                        <div class="job-item-tag">
                            @if(!empty($jobFollowIndustryItem->hot))
                                <div class="tag-hot-job">
                                    <span>Hot Job</span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
