<a href="" class="button-filter-block-mobile btn btn-custom_light w-100 mb-3 text-uppercase d-md-none">
    <i class="fas fa-filter"></i> Tìm kiếm công việc
</a>
<div class="block-filter">
    <form class="search-form" name="search" id="search-complete"
          action="{{route('home')}}" method="GET">
        <div class="filter--keywords">
            <p class="title">
                <i class="fas fa-filter"></i>
                <span>Tìm kiếm theo</span>
                <button type="reset" class="reset-form-filter"><i class="fal fa-times"></i></button>
            </p>
            <div class="search-input">
                <label class="search-input_label">
                    <input type="text"
                           data-url="{{route('frontend.jobs.filter.keywords')}}"
                           class="filter--keywords--textbox"
                           autocomplete="off" placeholder="Search keywords">
                </label>
                <div class="filter--keywords--dropdown"></div>
            </div>
        </div>
        <div class="filter--occupation">
            <div class="filter-control">
                <div class="filter-control__text"><span>Tìm theo ngành nghề</span></div>
                <ul class="filter-control__dropdown">
                    @foreach($occupations as $occupationItem)
                        <li>
                            <label class="filter-checkbox-item">
                                <input
                                        data-id="{{ $occupationItem->id }}"
                                        data-name="{{ $occupationItem->name }}"
                                        class="filter--occupation--checkbox"
                                        type="checkbox"
                                        value="{{ $occupationItem->id }}"
                                        name="occupation_main[]">
                                <span>{{ $occupationItem->name }}</span>
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="filter--job-level">
            <div class="filter-control">
                <div class="filter-control__text"><span>Tìm theo cấp bậc</span></div>
                <ul class="filter-control__dropdown">
                    @foreach($levelJobs as $levelJobItem)
                        <li>
                            <label class="filter-checkbox-item">
                                <input
                                        data-id="{{ $levelJobItem->id }}"
                                        data-name="{{ $levelJobItem->name }}"
                                        class="filter--job-level--checkbox"
                                        type="checkbox"
                                        value="{{ $levelJobItem->id }}"
                                        name="job_level_id[]">
                                <span>{{ $levelJobItem->name }}</span>
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="selected-base selected-base--occupations"></div>
        <div class="selected-base selected-base--cities"></div>
        <div class="selected-base selected-base--occupations-detail"></div>
        <div class="filter--occupation-detail"></div>
        <div class="filter--occupation-supporting">
            <p class="title">Vị trí nghề nghiệp</p>
            @foreach($occupations as $occupationItem)
                <ul class="filter--occupation-group--{{ $occupationItem->id }}">
                    @foreach($occupationItem->occupationChildrent as $occupationChildItem)
                        <li>
                            <label class="filter-checkbox-item">
                                <input
                                        data-id="{{ $occupationChildItem->id }}"
                                        data-name="{{ $occupationChildItem->name }}"
                                        class="filter--occupation-supporting--checkbox"
                                        type="checkbox"
                                        id="checkbox-occupationSub-{{ $occupationChildItem->id }}"
                                        value="{{ $occupationChildItem->id }}"
                                        name="occupation_supporting[]">
                                <span>{{ $occupationChildItem->name }}</span>
                            </label>
                        </li>
                    @endforeach
                </ul>
            @endforeach
        </div>
        <div class="filter--city">
            <p class="title">Khu vực thành phố</p>
            <input type="checkbox" id="filter-city-showmore" class="d-none">
            <ul>
                @foreach($citys as $cityItem)
                    <li class="city-item-{{ $cityItem->id }}">
                        <label class="filter-checkbox-item">
                            <input
                                    class="filter--city--checkbox"
                                    data-id="{{ $cityItem->id }}"
                                    data-name="{{ $cityItem->name }}"
                                    type="checkbox"
                                    value="{{ $cityItem->id }}"
                                    id="checkbox-city-{{ $cityItem->id }}"
                                    name="city_id[]">
                            <span>{{ $cityItem->name }}</span>
                        </label>
                    </li>
                @endforeach
            </ul>
            <label for="filter-city-showmore" class="show-more">Hiển thị thêm</label>
        </div>
    </form>
</div>
