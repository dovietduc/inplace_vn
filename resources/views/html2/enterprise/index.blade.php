@extends('html2.enterprise.layout')
@section('content_page')
<div class="container">
    <div class="row company-caption justify-content-center">
        <div class="col-12 col-lg-8">
            <h1 class="company-slogan font-weight-black text-black-400 mb-3">
                Chúng ta làm việc, chúng ta hạnh phúc
            </h1>
            <div class="text-size-15 text-grey-800 company-slogan-caption">
                A social commerce blockchain platform that incentivises content creators, helps brand owners understand their customers, provides
                a
                personalised experience for shoppers and empowers marketers with concrete data to quantify influence, all in one place.
            </div>
        </div>
    </div>

    <div class="company-top-post card card-line border-0">
        <div class="card-header px-0">
            <span class="card-name">
                Bài viết nổi bật
            </span>
            <div class="card-read-more">
                <a href="#" title="">
                    Xem tất cả (6)
                </a>
            </div>
        </div>
        <div class="card-body px-0 py-30">
            @include('html2.enterprise.partials.news_top')
        </div>
    </div>

    <div class="card card-line company-quote-section border-0">
        <div class="card-header px-0">
            <span class="card-name">
                Who
            </span>
            <div class="card-caption">
                Ai có thể sẽ trở thành động đội trong tương lai của bạn?
            </div>
            <div class="card-read-more">
                <a href="#" title="">
                    Xem tất cả (6)
                </a>
            </div>
        </div>
        <div class="card-body px-0 pt-30">
            @include('html2.enterprise.partials.quote_master')
        </div>
    </div>

    <div class="row post-who-box">
        <div class="col-12 col-lg-3 mb-3 mb-lg-0">
            <a href="#" title="" class="">
                <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
            </a>
        </div>
        <div class="col-12 col-lg-7">
            <div class="post-name">
                <a href="#" title="" class="text-size-16 font-weight-bold">
                    Lê Diệp Kiều Trang trả lời câu hỏi: Tại sao phải học hành vất vả 16 năm rồi đi làm cũng chẳng liên quan
                </a>
                <span class="post-time text-grey-200 font-weight-regular text-size-13">
                    (26/05/2017)
                </span>
            </div>
            <div class="post-desc">
                <span class="text-black-400">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum elit tortor, tempus in consectetur sit amet, mollis sit amet diam. Estibulum sit amet malesuada libero, commodo posuere sapien.
                Aliquam porttitor consequat nisi...
                </span>
                <a href="#" title="" class="">
                    Xem thêm
                </a>
            </div>
        </div>
    </div>
    <!--WHAT section--->
    <div class="card card-line border-0 box-choice-intro">
        <div class="card-header px-0">
            <span class="card-name">
                What
            </span>
            <div class="card-caption">
                Sứ mệnh của công ty là gì?
            </div>
        </div>
        <div class="card-body px-0 py-30">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <p class="">
                        Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực HR&Tech. HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh nghiệp.
                    </p>
                    <p>
                        Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi phí nhân lực tối ưu nhất. Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt nhất. Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc cursus feugiat molestie. Maecenas suscipit fermentum augue, at pellentesque ligula blandit sit amet. Sed ornare metus justo, ut laoreet ligula hendrerit quis.
                    </p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END: WHAT section---->
    <!--WHY section--->
    <div class="card card-line border-0 box-choice-intro">
        <div class="card-header px-0">
            <span class="card-name">
                Why
            </span>
            <div class="card-caption">
                Tại sao sứ mệnh đó tồn tại?
            </div>
        </div>
        <div class="card-body px-0 py-30">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <p>
                        Chúng tôi hoạt động với phương châm: "Lấy khách hàng là trọng tâm"
                    </p>
                    <p>
                        JJob không phải là công ty lâu đời nhất hoặc lớn nhất trong lĩnh vực head-hunting, nhưng chúng tôi hướng đến trở thành Nhà tư vấn khiến khách hàng hài lòng nhất.
                    </p>
                    <p>
                        Với mỗi Khách hàng khi làm việc JJob luôn gắn bó như một đối tác lâu dài và bám sát cùng sự phát triển của họ, giúp các doanh nghiệp giải quyết những thách thức về nhân lực khi mở rộng hoạt động kinh doanh.
                    </p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END: WHY section---->
    <!--WHY section--->
    <div class="card card-line border-0 box-choice-intro">
        <div class="card-header px-0">
            <span class="card-name">
                How
            </span>
            <div class="card-caption">
                Làm thế nào để hoàn thành sứ mệnh?
            </div>
        </div>
        <div class="card-body px-0 py-30">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <p>
                        Chúng tôi chờ đón các bạn gia nhập đội ngũ Tư vấn tuyển dụng (ESS Consultant) của JJob với tinh thần:
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp; +  Đam mê với nghề nhân sự và không ngừng nghỉ "săn" những nhân lực giỏi cho cách doanh nghiệp Khách hàng, cảm nhận
                        được
                        niềm vui khi là một đối tác đồng hành với sự phát triển của Khách hàng.
                    </p>
                    <p >
                        &nbsp;&nbsp;&nbsp;+  Am hiểu về các ngành nghề kinh doanh lĩnh vực IT- Phần mềm, Sản xuất, Dịch vụ, Tài chính..
                    </p>
                    <p>
                        Đội ngũ Head-hunter của J-Job tốt nghiệp những trường khối nhân lực, kinh doanh hoặc Khoa học, như ĐH Ngoại thương, ĐH Thương mại, ĐH Khoa học tự nhiên.., và có nhiều năm kinh nghiệm ở các công ty nước ngoài trong lĩnh vực nhân sự.
                    </p>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-4 mb-md-0">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <a href="#" title="" class="">
                        <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                    </a>
                    <div class="mt-10">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            J-JOB Executive Search mang đến một Giải pháp nhân sự đầu tiên tại Việt Nam.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END: WHY section---->
</div>
<div class="job-list">
        <div class="container">
            <div class="card card-line border-0 bg-transparent">
                <div class="card-header px-0">
                <span class="card-name">
                    <span class="text-green-300">Job</span> now
                </span>
                    <div class="card-caption">
                        Chúng tôi đang cần bạn
                    </div>
                </div>
                <div class="card-body px-0 pt-40">
                    <div class="js-slide-horizontal owl-carousel owl-theme">
                        @for ($i = 0; $i < 9; $i++)
                            @include('html2.enterprise.partials.job_item')
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="company-info-bottom mb-5 mb-lg-0">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card card-line border-0 box-choice-intro">
                    <div class="card-header px-0">
                        <span class="card-name">
                            Thông tin công ty
                        </span>
                    </div>
                    <div class="card-body detail-info px-0 py-30">
                        <div class="company-brand">
                            <div class="media">
                                <img src="https://via.placeholder.com/50x50" class="rounded-circle mr-20" alt="...">
                                <div class="media-body align-self-center">
                                    <h5 class="my-0 font-weight-bold text-black-400">J-JOB Executive Search</h5>
                                </div>
                            </div>
                        </div>
                        @include('html2.enterprise.partials.company_info_list')
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="company-map shadow-sm">
                    <img src="https://via.placeholder.com/560x418" alt="..." class="p-1 w-100">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
