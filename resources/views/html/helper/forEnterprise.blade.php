@extends('layouts.app')
@section('meta')
    <title>Dành cho Doanh nghiệp</title>
@stop
@section('css')
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/forEnterprise.css')}}">
@stop
@section('content')
    <div class="for-enterprise-head">
        <div class="container">
            <div class="for-enterprise-head_content">
                <h3 class="title"><small>Dành cho</small>Doanh nghiệp</h3>
            </div>
        </div>
    </div>
    <div class="for-enterprise-body">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 box_sidebar">
                    <div class="nav nav-pills flex-column">
                        <a id="tao-trang-doanh-nghiep-moi-tab" class="active" data-toggle="pill"
                           href="#tao-trang-doanh-nghiep-moi" role="tab">Tạo trang doanh nghiệp mới</a>
                        <a id="dang-tuyen-moi-tab" data-toggle="pill" href="#dang-tuyen-moi" role="tab">Đăng tuyển
                            mới</a>
                        <a id="dang-noi-dung-moi-tab" data-toggle="pill" href="#dang-noi-dung-moi" role="tab">Đăng nội
                            dung mới</a>
                        <a id="tim-kiem-ung-vien-tab" data-toggle="pill" href="#tim-kiem-ung-vien" role="tab">Tìm kiếm
                            ứng viên</a>
                    </div>
                </div>
                <div class="col-lg-9 box_content">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tao-trang-doanh-nghiep-moi" role="tabpanel"
                             aria-labelledby="tao-trang-doanh-nghiep-moi-tab">
                            @include('html.helper.forEnterpriseTabs.tao-trang-doanh-nghiep-moi')
                        </div>
                        <div class="tab-pane fade" id="dang-tuyen-moi" role="tabpanel"
                             aria-labelledby="dang-tuyen-moi-tab">
                            @include('html.helper.forEnterpriseTabs.dang-tuyen-moi')
                        </div>
                        <div class="tab-pane fade" id="dang-noi-dung-moi" role="tabpanel"
                             aria-labelledby="dang-noi-dung-moi-tab">
                            @include('html.helper.forEnterpriseTabs.dang-noi-dung-moi')
                        </div>
                        <div class="tab-pane fade" id="tim-kiem-ung-vien" role="tabpanel"
                             aria-labelledby="tim-kiem-ung-vien-tab">
                            @include('html.helper.forEnterpriseTabs.tim-kiem-ung-vien')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/forEnterprise.js')}}"></script>
@stop
