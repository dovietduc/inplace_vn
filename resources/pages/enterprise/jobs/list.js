import {dashboardLayout} from '../dashboard.js';
import swal from "sweetalert";
const axios = require('axios');

function submitFormFilter() {
    $('.form_filter_jobs').submit();
}


function deleteJob(e) {
    e.preventDefault();
    let url = $(this).data('url');
    let that = $(this);
    swal({
        icon: "warning",
        title: "Bạn chắc chắn xóa không?",
        text: "Dữ liệu sẽ bị xóa hoàn toàn, bạn sẽ không thể lấy lại!",
        buttons: {
            cancel: {
                text: "Hủy",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Xóa",
                value: true,
                visible: true,
                className: "btn-custom",
                closeModal: true
            }
        }
    }).then(function (result) {
        if (result) {
            axios.post(url)
                .then(function (response) {
                    if (response.status) {
                        that.parents('.dashboard-jobs-item').remove();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    });
}
$(function () {
    dashboardLayout();
    window.setTimeout(function() {
        $(".dashboard-jobs-alert .alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);
    $(".dashboard-header_filter").select2({
        minimumResultsForSearch: Infinity,
    });
    $('.action_delete').on('click', deleteJob);
    $(".select_jobs_choose").on('change', submitFormFilter);
});
