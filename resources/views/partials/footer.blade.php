<footer class="footer">
    <div class="info">
        <div class="container">
            <div class="list">
                <div class="item company">
                    <div class="wrap">
                        <h2 class="logo">
                            <a href="{{ route('wellcome') }}"><img src="{{ asset('images/logo-footer.png') }}"
                                                                   alt="logo-footer"></a></h2>
                        <p>Match values to place right people to right seats</p>
                        <p>Hotline: 028 6275 5586<br>Email: services@inplace.vn</p>
                        <ul class="clearfix social">
                            <li class="linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li class="instagram"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Về chúng tôi</h2>
                        <ul class="clearfix menu">
                            <li><a href="/">Trang chủ</a></li>
                            <li><a href="/frontend/helper/gioi-thieu">Giới thiệu</a></li>
                            <li><a href="/frontend/helper/cham-soc-khach-hang">Chăm sóc khác hàng</a></li>
                            <li><a href="/frontend/helper/tro-giup">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Dành cho doanh nghiệp</h2>
                        <ul class="clearfix menu">
                            <li>
                                <a href="/frontend/helper/danh-cho-doanh-nghiep?k=tao-trang-doanh-nghiep-moi">Tạo trang Doanh nghiệp mới</a>
                            </li>
                            <li><a href="/frontend/helper/danh-cho-doanh-nghiep?k=dang-tuyen-moi">Đăng tuyển mới</a></li>
                            <li><a href="/frontend/helper/danh-cho-doanh-nghiep?k=dang-noi-dung-moi">Đăng nội dung mới</a></li>
                            <li><a href="/frontend/helper/danh-cho-doanh-nghiep?k=tim-kiem-ung-vien">Tìm kiếm ứng viên</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <div class="wrap">
                        <h2 class="title">Dành cho cá nhân</h2>
                        <ul class="clearfix menu">
                            <li><a href="/frontend/helper/danh-cho-ca-nhan?k=tao-trang-ca-nhan-moi">Tạo trang cá nhân mới</a></li>
                            <li><a href="/frontend/helper/danh-cho-ca-nhan?k=tao-bai-viet-moi">Tạo bài viết mới</a></li>
                            <li><a href="/frontend/helper/danh-cho-ca-nhan?k=upload-cv">Upload CV</a></li>
                            <li><a href="/frontend/helper/danh-cho-ca-nhan?k=viec-lam-cua-toi">Việc làm của tôi</a></li>
                            <li><a href="/frontend/helper/danh-cho-ca-nhan?k=tim-kiem-doanh-nghiep">Tìm kiếm doanh nghiệp</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="container">
            <div class="text-center d-md-flex justify-content-between">
                <div class="left mb-2 mb-md-0">InPlace © 2019. All Rights Reserved.</div>
                <div class="right">
                    <a href="/frontend/helper/quy-dinh-bao-mat">Quy định bảo mật</a>
                    <span>・</span>
                    <a href="/frontend/helper/dieu-khoan-su-dung">Điều kiện và Thoả thuận sử dụng</a>
                    <span>・</span>
                    <a href="/frontend/helper/quy-che-hoat-dong">Quy chế hoạt động</a>
                </div>
            </div>
        </div>
    </div>
</footer>

