<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class NewsLike extends Model
{
    use BaseEloquent;
    protected $guarded = [];
    protected $table = 'jjob_news_likes';
}
