function showButtonReset() {
    let btnReset = $('button.reset-form-filter');
    let keywords = $('.filter--keywords--textbox').val();
    let checkbox = $('#search-complete input[type="checkbox"]:checked').length;
    let occDetail = $('.filter--occupation-detail').children().length;
    let keySearched = $('.filter--keywords--searched').length;
    let checker = ($(this).is(':checked') || keywords || checkbox || occDetail || keySearched) ? true : false;
    if (checker){
        btnReset.addClass('d-block');
    }else {
        btnReset.removeClass('d-block');
    }
}
export function showButtonResetForm(){
    $(document).on('change', '#search-complete input[type="checkbox"]', showButtonReset);
    $(document).on('keyup', '#search-complete input[type="text"]', showButtonReset);
    $(document).on('click', '.filter--keywords--searched', showButtonReset);
}