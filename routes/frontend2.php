<?php

Route::group(['prefix' => 'html-2'], function () {
    
    Route::group(['prefix' => 'news'], function () {
        Route::get('/', ['as' => 'html.news.list', 'uses' => 'Frontend2Controller@news']);
        Route::get('/detail', ['uses' => 'Frontend2Controller@detailNews']);
    });
    
    Route::group(['prefix' => 'enterprise'], function () {
        Route::get('/', ['uses' => 'Frontend2Controller@enterprise']);
        Route::get('/job', ['uses' => 'Frontend2Controller@jobEnterprise']);
        Route::get('/news', ['uses' => 'Frontend2Controller@newsEnterprise']);
        Route::get('/building', ['uses' => 'Frontend2Controller@buildingEnterprise']);
    });
});

