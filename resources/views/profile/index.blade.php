@extends('layouts.app')
@section('meta')
    <title>Home Profile</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('pages/profile/home/home.css') }}">
@stop
@section('content')
    <div class="profile-pages">
        @include('profile.partials.profile-header-pages')
        <div class="profile-homePage">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 box_content mb-5 mb-md-0">
                        <div class="profile-homePage_box">
                            <div class="component-title">
                                <h3 class="title">
                                    Form ứng tuyển Job
                                </h3>
                                <div class="desc"></div>
                            </div>
                            @include ('profile.partials.form-apply-job')
                        </div>

                        @include('profile.partials.profile-show-contact')

                    </div>
                    <div class="col-md-4 box_sidebar">
{{--                        @include ('profile.partials.profile-sidebar')--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/moment-with-locales.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('vendor/croppie/croppie.js') }}"></script>
    <script src="{{ asset('vendor/jquery-form/jquery-form.js') }}"></script>
    <script src="{{ asset('pages/profile/profile.js') }}"></script>
    <script src="{{asset('pages/profile/home/home.js')}}"></script>
@stop
