@extends('layouts.app')
@section('meta')
    <title>Công ty đang theo dõi</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/companies/following/following.css') }}">
@stop
@section('content')
    <div class="companies-following-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="component-title">
                        <h3 class="title">Công ty bạn đang theo dõi</h3>
                        <div class="desc"></div>
                    </div>
                    <div class="companies-following-list">
                        @foreach($customersFollow as $customersFollowItem)
                            <div class="companies-following-item">
                                <a href="#" class="avatar">
                                    @if( jjobCheckImageExit($customersFollowItem->feature_image) )
                                        <img src="{{ asset($customersFollowItem->feature_image) }}" alt=""/>
                                    @else
                                        <img src="{{ asset('images/no_avatar_company_80x80.jpg') }}" alt=""/>
                                    @endif
                                </a>
                                <div class="content">
                                    <h4 class="title"><a href="#">
                                            {{ $customersFollowItem->name }}
                                        </a></h4>
                                    <div class="desc">
                                        {{ $customersFollowItem->mission }}
                                    </div>
                                </div>
                                <div class="follow">
{{--                                    @include('partials.follows-customer.button-follow-customer',--}}
{{--                                     ['customer' => $customersFollowItem])--}}

                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{--add component pagination--}}
                    {{ $customersFollow->links('vendor.pagination.default') }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{ asset('pages/companies/following/following.js') }}"></script>
@stop
