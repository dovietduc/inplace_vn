@extends('layouts.app')
@section('meta')
    <title>Công việc đã lưu</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/jobs/save-jobs/save-jobs.css')}}">
@stop
@section('content')
    <div class="save-jobs-page">
        <div class="container">
            <div class="component-title">
                <h3 class="title">Công việc đã lưu</h3>
                <div class="desc">Bạn có {{ auth()->user()->saveJobs()
                ->whereNull('jjob_user_save_jobs.deleted_at')
                ->whereNull('jjob_jobs.deleted_at')->count() }} công việc đã lưu</div>
            </div>
            <div class="row">
                @include('jobs.partials.job-save-item')
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/jobs/save-jobs/save-jobs.js')}}"></script>
@stop
