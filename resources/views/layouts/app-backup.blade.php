<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('partials.favicon-for-website')
    @yield('meta')
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=vietnamese"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('user/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('user/css/app.css') }}">
    @yield('stylesheetAddon')
</head>
<body>
@include('partials.header')

<div class="main-content">
    <div class="page-enterprise">
        <div class="cover-page position-relative">
            @if( jjobCheckImageExit($customer->feature_image) )
                <img src="{{ url($customer->feature_image) }}" alt="" class="w-100"/>
            @else
                <img src="{{ asset('images/no_img_1920_400.jpg') }}" alt="" class="w-100"/>
            @endif
        </div>

        @include('company.partials.company-head-menu')
        @yield('content')
    </div>
</div>

@include('partials.footer')
<script src="{{ mix('user/js/vendor.js') }}"></script>
<script src="{{ mix('user/js/app.js') }}"></script>

@yield('scriptAddon')

</body>
</html>
