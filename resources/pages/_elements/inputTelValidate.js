let inpTel = $('.inp-tel');
let errTel = inpTel.parent().find('.err-inp');
function telValid(event) {
    event.preventDefault();
    $(this).removeClass('error');
    errTel.text('').hide();
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        errTel.text("Số điện thoại không được để trống!").show();
    }
    if (event.target.validity.patternMismatch) {
        $(this).addClass("error");
        errTel.text("Vui lòng nhập đúng định dạng số điện thoại!").show();
    }
}
export function inputTelValidate(){
    inpTel.on('change',telValid);
    inpTel.on('invalid',telValid);
}
