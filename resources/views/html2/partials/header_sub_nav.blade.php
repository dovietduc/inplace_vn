<div class="header-sub-menu">
    <ul class="header-sub-menu_ul">
        @foreach([
        'Gợi ý của InPlace', 'Công ty', 'Văn hóa', 'Phỏng vấn nhân viên', 'WorkStyle', 'Chuyện nghề', 'Nhà Tuyển dụng', 'Sự kiện', 'Tech',
        'Nhà sáng lập', 'Startup', 'Gallery', 'Video', 'Văn phòng ảo', 'Quiz', 'Thông báo'
        ] as $nav)
        <li class="{{ $loop->index == 0 ? 'active' : '' }}">
            <a href="#">
                {{ $nav }}
            </a>
        </li>
        @endforeach
    </ul>
</div>
