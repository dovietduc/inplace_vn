<div class="txt-format">
    <h1>Q&A và Quiz</h1>
    <h3>Q&A</h3>
    <p>Các Doanh Nghiệp có một không gian riêng để trả lời các câu hỏi của ứng viên, người tìm việc, người quan tâm trong mục Q&A (Question & Answers). Các câu hỏi và câu trả lời này do Doanh Nghiệp tự tuỳ chỉnh, lựa chọn và đăng tải. Thông quan việc trả lời các câu hỏi thường gặp về công việc, công ty, môi trường làm việc v.v.., doanh nghiệp có thể tương tác với ứng viên, cho họ một cái nhìn thân thiện và toàn diện hơn về công ty.
        </p><p>Ngoài ra, đây cũng là môt công cụ làm Employer Branding rất tốt, bởi qua ngôn ngữ, phong cách trả lời câu hỏi mà doanh nghiệp có thể bộc lộ văn hoá làm việc của mình rõ nét hơn. Q&A do đó không chỉ là chỗ cung cấp thông tin, giải đáp thắc mắc đơn thuần, mà còn là nơi sự tiếp xúc và tương tác giữa ứng viên – doanh nghiệp, công chúng – doanh nghiệp trở nên thiết thực hơn bao giờ hết.
    </p><p>Tính năng Q&A chỉ hiển thị trên thanh công cụ của Trang Doanh Nghiệp, người sử dụng có thể truy cập vào từ dãy menu tại trang Profile công ty của mình để thêm và chỉnh sửa nội dung tương ứng.
    </p>
    <h3>Quiz</h3>
    <p>
        Quiz là các bài trắc nhiệm nhỏ mà hệ thống đưa ra cho các thành viên thử sức mình dưới dạng Minigame. Các câu trả lời của các bài Quiz sẽ không có đúng hay sai. Mục đích của các bài Quiz là trắc nghiệm tính cách, phong cách, quan điểm, thói quen v.v. của người thực hiện. Kết quả các bài Quiz có tác dụng giúp hệ thống tự động tìm ra các điểm tương thích về văn hoá, tính cách của cá nhân với một doanh nghiệp hay một công việc nào đó, thực hiện tính năng Matching tư động của website.
    </p><p>Các cá nhân có thể gia tăng điểm Inplace Score của mình bằng cách thực hiện càng nhiều bài Quiz do hệ thống đưa ra càng tốt. Inplace sẽ có những món quà hoặc những đãi ngộ đặc biệt cho các các bạn đạt điểm Inplace Score cao xuất sắc.
    </p><p>Để tìm các bài Quiz hệ thống đưa ra và theo dõi các bài Quiz mình đã thực hiện, cá nhân có thể vào mục Quiz tại Profile trang cá nhân của mình.
    </p><p>Ngoài ra, doanh nghiệp có thêm tính năng Tạo Quiz để thử tài những ứng viên đang quan tâm tới vị trí công việc nào đó tại doanh nghiệp. Những Quiz do Doanh Nghiệp tạo ra sẽ không đóng vai trò vào điểm số Inplace Score của hệ thống. Tuy nhiên nếu ứng viên muốn ghi điểm đối với nhà tuyển dụng thì nên làm các bài Quiz do doanh nghiệp đưa ra.
    </p><p>Để tạo Quiz cho mình, Doanh Nghiệp có thể vào thư mục Quiz để xem, quản lý và tạo Quiz mới cho mình từ Profile trang Doanh Nghiệp.
    </p>
</div>
