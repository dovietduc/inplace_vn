<div id="popupErrorUploadImage" class="popup-error-upload alert-form-success modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
                <div class="icon-success"><i class="fas fa-exclamation"></i></div>
                <h2 class="title mb-1"></h2>
                <div class="desc"></div>
            </div>
        </div>
    </div>
</div>
