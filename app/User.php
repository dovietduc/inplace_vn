<?php

namespace App;

use App\Notifications\CustomResetPasswordNotification;
use App\Notifications\CustomVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function sendEmailVerificationNotification()
    {
        $this->notify(new CustomVerifyEmail());
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    public function candidatecv()
    {
        return $this->hasMany(CandidateCv::class, 'user_id');

    }
    public function saveJobs()
    {
        return $this->belongsToMany(Job::class, 'jjob_user_save_jobs', 'user_id', 'job_id');
    }

    public function applyJobs()
    {
        return $this->belongsToMany(Job::class, 'jjob_user_apply_jobs', 'user_id', 'job_id');
    }
    public function followCustomers()
    {
        return $this->belongsToMany(Customer::class, 'jjob_customer_follows', 'user_id', 'customer_id');
    }
}
