@extends('layouts.master')

@section('meta')
    <title>Jobs detail - inplace.vn</title>
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{ $job->title }}"/>
    <meta property="og:description" content="{{ strip_tags($job->description) }}"/>
    <meta property="og:image"
          content="{{ jjobCheckImageExit($job->feature_image) ? url($job->feature_image) : asset('images/no_img_730x286.jpg') }}"/>
    <meta property="og:image:width" content="715"/>
    <meta property="og:image:height" content="280"/>
    <meta property="fb:app_id" content="2226038500857645"/>

@stop

@section('stylesheetAddon')
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset('vendor/froala-editor/css/froala_style.css')}}">
    <link href="{{ asset('pages/jobs/detail/detail.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @php
        $customer = optional($job->customer);
    @endphp
    @include('partials.header')
    <div class="main">

        @include('jobs.partials.job-detail-head')

        <div class="job-detail_content">
            <div class="container">
                <div class="box">
                    <div class="row">
                        <div class="col-lg-8 box_content">
                            @include('jobs.partials.who-is-us')
                            @include('jobs.partials.why-is-us')
                            @include('jobs.partials.job-description')
                            @include('jobs.partials.hot-new')
                            @include('jobs.partials.infor-company')

                        </div>
                        @include('jobs.partials.box-siderbar')
                    </div>
                </div>
            </div>
        </div>
        <div class="job-detail_footer">
            @include('jobs.partials.job-same-industry')
            @include('jobs.partials.job-viewed')
        </div>
    </div>
    @include('jobs.partials.popup-applyJob')
    @include('partials.footer')

@stop
@section('scriptAddon')
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-form/jquery-form.js') }}"></script>
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/jobs/detail/detail.js') }}"></script>
@stop
