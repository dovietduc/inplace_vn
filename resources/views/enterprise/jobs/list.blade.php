@extends('layouts.dashboard')
@section('meta')
    <title>Jobs List Dashboard</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/jobs/list.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
                <div class="button-group">
                    <form
                        class="form_filter_jobs"
                        action="{{ route('enterprise.jobs.list') }}"
                        method="get">
                        <select
                            name="status_jobs"
                            class="dashboard-header_filter select_jobs_choose">
                            <option {{ empty(request('status_jobs')) ? 'selected' : '' }} value="">
                                Tất cả
                            </option>
                            <option
                                value="{{ \App\Job::statusPublic }}"
                                {{ !empty(request('status_jobs')) && request('status_jobs') == \App\Job::statusPublic ? 'selected' : '' }}>
                                Đã đăng
                            </option>
                            <option
                                value="{{ \App\Job::statusDraft }}"
                                {{ !empty(request('status_jobs')) && request('status_jobs') == \App\Job::statusDraft ? 'selected' : '' }}>
                                Nháp
                            </option>
                        </select>
                    </form>

                    <a href="{{ route('enterprise.jobs.add') }}"
                       class="btn btn-custom"><i class="fas fa-plus mr-1"></i>Đăng tuyển dụng</a>
                </div>
            </div>
        </div>
    </div>
    @if (session('message'))
        <div class="dashboard-jobs-alert">
            <div class="container">

                <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                <span>
                        {{ session('message') }}
                </span>
                    <button type="button" class="close py-2" data-dismiss="alert" aria-label="Close"><i
                            class="fal fa-times"></i></button>
                </div>
            </div>
        </div>
    @endif
    <div class="dashboard-jobs-list">
        <div class="container">
            <div class="dashboard-jobs-list_th">
                <div class="row">
                    <div class="col-5">
                        Tiêu đề
                    </div>
                    <div class="col-7">
                        <div class="row">
                            <div class="col-3">
                                Lượt xem
                            </div>
                            {{--                            <div class="col-3">--}}
                            {{--                                Ứng tuyển--}}
                            {{--                            </div>--}}
                            <div class="col-3">
                                Thời gian
                            </div>
                            <div class="col-3">
                                Trạng thái
                            </div>
                            <div class="col-3">
                                Active
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($jobs as $job)
                <div class="dashboard-jobs-item">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="dashboard-jobs-item_title">
                                <h4 class="title">
                                    <a href="{{ route('enterprise.jobs.edit', ['id' => $job->id]) }}">
                                        {{ $job->title }}
                                    </a>
                                </h4>
                                <div class="d-flex align-items-center justify-content-between mt-2 flex-wrap">
                                    <div class="location">
                                        <span>{{$job->citys->implode('name', ', ') }}</span>
                                    </div>
                                    <div class="action">
                                        @if(!empty($job->status) && $job->status == \App\Job::statusPublic)
                                            <a href="{{ route('jobs.show', ['slug' => $job->slug]) }}"
                                               class="action_view" title="Xem" target="_blank"><i
                                                    class="far fa-eye"></i></a>
                                        @endif
                                        <a href="{{ route('enterprise.jobs.edit', ['id' => $job->id]) }}"
                                           class="action_edit" title="Sửa">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a
                                            data-url="{{ route('enterprise.jobs.delete', ['id' => $job->id]) }}"
                                            href=""
                                            class="action_delete" title="Xóa"><i
                                                class="far fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-3 d-none d-sm-block">
                                    <p class="info-txt">{{ $job->view_count }}</p>
                                </div>
                                {{--                                <div class="col-3">--}}
                                {{--                                    <p class="info-txt"><strong>9</strong> ứng viên</p>--}}
                                {{--                                    <a href="#" class="btn btn-custom_light">Xem</a>--}}
                                {{--                                </div>--}}
                                <div class="col-4 col-sm-3">
                                    <p class="info-txt"><small>Ngày đăng:</small><br>
                                        {{ Carbon::createFromFormat('Y-m-d H:i:s', $job->created_at)->format('d-m-Y') }}
                                    </p>
                                </div>
                                <div class="col-4 col-sm-3">
                                    @if($job->status == \App\Job::statusPublic)
                                        <span class="status-txt active">Đã đăng</span>
                                    @else
                                        <span class="status-txt draft">Nháp</span>
                                    @endif
                                </div>
                                <div class="col-4 col-sm-3 text-right text-md-left">
                                    @if($job->status == \App\Job::statusPublic)
                                        @if($job->active == \App\Job::active)
                                            <span class="status-active active">Active</span>
                                        @elseif($job->active == \App\Job::rejectActive)
                                            <span class="status-active deactive">Deactive</span>
                                        @elseif($job->active == \App\Job::waitting)
                                            <span class="status-active">Waiting</span>
                                        @else
                                            <span class="status-active"></span>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                {{ $jobs->appends($_GET)->links('vendor.pagination.default') }}
            </div>

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/jobs/list.js')}}"></script>
@stop
