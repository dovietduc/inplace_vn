<div class="txt-format">
        <h1>Đăng nhập</h1>
        <p>Khi xem website ở trạng thái chưa đăng nhập, bạn sẽ không thể sử dụng rất nhiều các tính năng hay ho của
            website ví dụ như Lưu tin vài, Ứng tuyển Job, Gửi tin nhắn, tương tác với các thành viên khác v.v….</p>
        <p>Bạn có thể đăng nhập nếu như trước đó bạn đã tạo tài khoản tại <a
                href="http://www.inplace.vn" target="_blank">www.inplace.vn</a>. Hãy click vào biểu tượng <strong>Đăng Nhập </strong>ở
            góc trên bên phải của màn hình, điền địa chỉ email bạn đã dùng để đăng ký và mật khẩu, sau đó click Enter
            hoặc “<strong>Đăng Nhập</strong>” là xong. Nếu trước đó bạn đã dùng Facebook hoặc Linkedin để đăng ký tài
            khoản tại Inplace thì hãy click vào “<strong>Đăng nhập với Facebook</strong>” hoặc “ <strong>Đăng nhập với
                Linkedin</strong>” – website sẽ tự động giúp bạn đăng nhập sau đó.</p>
        <p>Nếu bạn chưa có tài khoản thì hãy <a href="{{route('register')}}" target="_blank">Đăng Ký tạo tài khoản</a> trước nhé. Tất cả chỉ vài click
            chuột là xong thôi mà.</p>
        <p>&nbsp;</p>
        <h1>Đăng xuất</h1>
        <p>Khi không có nhu cầu sử dụng Inplace và bạn muốn bảo mật các thông tin tại tài khoản Inplace của mình thì tốt
            nhất bạn nên <strong>Đăng Xuất</strong> khỏi Inplace sau khi dùng xong.</p>
        <p>Để đăng xuất khỏi Inplace, bạn hãy click vào biểu tượng avatar của mình ở góc trên cùng bên phải của màn
            hình.<br>Chọn <strong>Thoát</strong>&nbsp; từ dãy menu dọc là bạn đã đăng xuất khỏi tài khoản Inplace của
            mình rồi.</p>
</div>
