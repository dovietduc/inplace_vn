<div class="txt-format">
    <h1>Hướng dẫn tạo trang Cá nhân</h1>
    <p>Trước hết, để tạo được trang Profile cá nhân, bạn cần tạo tài
        khoản trên INPLACE</p>
    <h3><strong>TẠO TÀI KHOẢN</strong></h3>
    <p>Tất cả mọi người đều có thể đăng ký sử dụng Inplace và điều này là hoàn toàn miễn
        phí. Sau khi đăng ký sử dụng, người dùng có thể dùng được tất cả các tiện ích mà
        trang web mang lại bao gồm tự do đăng tải CV, đăng tải nội dung yêu thích, kết
        nối với người dùng khác và doanh nghiệp, tìm kiếm việc làm, ứng tuyển và trao
        đổi thông tin với nhà tuyển dụng về việc làm bất kỳ, chia sẻ thông tin và được
        hệ thống Inplace tự động gợi ý và giới thiệu tới công ty hoặc việc làm phù
        hợp.</p>
    <p>Có 3 cách để đăng ký tài khoản tại Inplace:<br>- Dùng tài khoản Facebook<br>-
        Dùng tài khoản Linkedin<br>- &nbsp;Dùng email cá nhân</p>
    <h4><strong>Đăng ký
            nhanh chóng bằng tài khoản Facebook:</strong>&nbsp;</h4>
    <p>Click vào biểu tượng <img src="{{asset('images/icon-login-facebook.jpg')}}" alt="">
        trên trang chủ của <a href="http://www.inplace.vn"
                              target="_blank">www.inplace.vn</a>.
        Hệ thống sẽ tự liên kết với tài khoản Facebook của bạn.&nbsp;</p>
    <p>Lưu ý, sau khi đăng ký bằng tài khoản Facebook xong, hệ thống sẽ yêu cầu bạn nhập
        email và xác thực địa chỉ email một lần nữa.<br></p><h4><strong>Đăng ký bằng tài
            khoản Linkedin:</strong>&nbsp;</h4>
    <p>Click vào biểu tượng <img src="{{asset('images/icon-login-linkedin.jpg')}}" alt="">
        trên trang chủ của <a href="http://www.inplace.vn">www.inplace.vn</a>. Hệ thống sẽ
        tự liên kết
        với tài khoản Linkedin của bạn.</p><h4><strong>Đăng ký bằng email cá
            nhân:</strong>&nbsp;</h4>
    <p>Bạn cần thực hiện các bước sau:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_trang_ca_nhan.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Truy cập
                    <a href="http://www.inplace.vn" target="_blank">www.inplace.vn</a>
                </li>
                <li><strong>Bước 2:</strong> Nhập thông tin vào trường như
                    <strong>Họ và Tên</strong>(có
                    thể dùng tiếng Việt có dấu và dấu cách giữa các từ),
                    <strong>Email</strong>
                    cá nhân (1 email chỉ đăng ký được 1 tài khoản cá nhân duy nhất) và
                    <strong>Mật
                        Khẩu</strong> (tối thiểu 6 ký tự).
                </li>
                <li><strong>Bước 3:</strong> Nhấp vào Đăng ký</li>
                <li><strong>Bước 4:</strong> Để hoàn tất quá trình tạo tài khoản, hệ thống
                    sẽ
                    yêu cầu xác thực tài khoản qua email đã đăng ký.
                </li>
            </ul>
        </div>
    </div>

    <p>Sau khi tài khoản được tạo thành công, bây giờ bạn có thể tuỳ ý chính sửa Profile
        hoặc tìm kiếm và tương tác không giới hạn với các nội dung trên Inplace rồi.</p>
    <p>&nbsp;</p>
    <h3>TẠO TRANG PROFILE CÁ NHÂN</h3>
    <p>Sau khi đã đăng ký tài khoản, hãy đăng nhập ngay và bắt đầu tạo trang cá nhân của
        &nbsp;bạn nhé:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_trang_ca_nhan_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Click vào biểu tượng Profile <!--[if !vml]-->
                    <!--[endif]-->của bạn ở góc phải màn hình, chọn <strong>Chỉnh sửa trang
                        cá
                        nhân</strong></li>
                <li><strong>Bước 2:</strong> Clich vào biểu tượng Chỉnh sửa trang cá nhân để
                    sửa
                    hàng loạt thông tin cùng một lúc hoặc biểu tượng <!--[if !vml]-->
                    <!--[endif]--> và để điền vào từng trường thông tin trong form Profile
                    như
                    các thông tin cá nhân, kinh nghiệm làm việc, sở trường sở đoản, kỹ năng
                    mềm
                    v.v..
                </li>
                <li><strong>Bước 3:</strong> Hãy upload CV của bạn để dùng những lúc cần
                    thiết
                    nhé
                </li>
                <li><strong>Bước 4:</strong> Đừng quên upload hình ảnh đại diện và ảnh Cover
                    để
                    làm Profile của bạn thêm sinh động nhé
                </li>
            </ul>
        </div>
    </div>

</div>
