<div class="dashboard-sidebar">
    <button class="btn-dashboard-sidebar-toggle"><i class="fas fa-list"></i></button>
    <div class="dashboard-sidebar_box">
        <div class="dashboard-sidebar_content">
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Menu</h3>
                <ul class="dashboard-sidebar__menu">
                    <li class="">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon-analytics.png')}}" alt="">
                            <span>Tổng quan</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon_job.png')}}" alt="">
                            <span>Việc làm</span>
                            <small class="count">12</small>
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon_news.png')}}" alt="">
                            <span>Bài viết</span></a>
                    </li>
                    <li class="">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon_users.png')}}" alt="">
                            <span>Users</span></a>
                    </li>
                    <li class="">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon-candidate.png')}}" alt="">
                            <span>Ứng viên</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <img class="icon" src="{{asset('images/icon_company.png')}}" alt="">
                            <span>Công ty</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Hỗ trợ khách hàng</h3>
                <div class="dashboard-sidebar__support">
                    <p>Email: <a href="mailto:services@inplace.vn"><span class="color-theme">services@inplace.vn</span></a><br>
                        Hotline: <a href="tel:028 6275 5586"><strong>028 6275 5586</strong></a></p>
                    <a href="#"><i class="far fa-life-ring mr-1"></i><span class="color-theme">Help Center</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
