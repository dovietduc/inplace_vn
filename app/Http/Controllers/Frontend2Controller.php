<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Frontend2Controller extends Controller
{
    
    public function news()
    {
        return view('html2.news.index');
    }
    
    public function detailNews()
    {
        return view('html2.news.detail');
    }
    
    public function enterprise()
    {
        return view('html2.enterprise.index');
    }
    
    public function jobEnterprise()
    {
        return view('html2.enterprise.job');
    }
    
    public function newsEnterprise()
    {
        return view('html2.enterprise.news');
    }
    
    public function buildingEnterprise()
    {
        return view('html2.enterprise.building');
    }
}
