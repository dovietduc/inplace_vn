<?php

namespace App;

use App\Scopes\JobPublicScope;
use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use BaseEloquent, SoftDeletes;
    protected $table = 'jjob_jobs';
    protected $guarded = [];
    const paginations = 15;
    const statusDraft = 'draft';
    const statusPublic = 'public';
    const active = 1;
    const rejectActive = 2;
    const waitting = 3;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new JobPublicScope());
    }

    public function scopePublic($query)
    {
        return $query->where('jjob_jobs.status', Job::statusPublic);
    }

    public function scopeActive($query)
    {
        return $query->where('jjob_jobs.active', Job::active);
    }

    public function scopePopular($query)
    {
        return $query->where('jjob_jobs.status', Job::statusPublic)
            ->where('jjob_jobs.active', Job::active);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function citys()
    {
        return $this->belongsToMany(City::class, 'jjob_job_cities', 'job_id', 'city_id');
    }

    public function userSaveJob()
    {
        return $this->hasMany(SaveJob::class, 'job_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function customerIndustry()
    {
        return $this->belongsToMany(Industry::class, 'jjob_job_industries',
            'job_id', 'industry_id');
    }

    public function jobLevel()
    {
        return $this->belongsTo(LevelJob::class, 'job_level_id');
    }

    public function salary()
    {
        return $this->belongsTo(Salary::class, 'salary_id');
    }

    public function jobViews()
    {
        return $this->hasMany(JobView::class, 'job_id');
    }

    public function tagsJob()
    {
        return $this->belongsToMany(TagsOfJob::class, 'jjob_job_tags', 'job_id', 'tag_id');
    }

    public function membersCompany()
    {
        return $this->belongsToMany(MemberCompany::class, 'job_middle_member_company', 'job_id', 'user_add_member_id');
    }

    public function levelEnglish()
    {
        return $this->belongsTo(LanguageLevelCommon::class, 'english_level_id');
    }

    public function emailApplyJobs()
    {
        return $this->hasMany(EmailApplyJobs::class, 'job_id');
    }

    public function listJobFilterAjax($data)
    {
        $jobs = new Job();
        if (!empty(auth()->user()->occupation_id)) {
            $jobs = $jobs->where('jjob_jobs.occupation_main', auth()->user()->occupation_id);
        }
        if (!empty($data ['city_id'])) {
            $jobs = $jobs
                ->join('jjob_job_cities', 'jjob_jobs.id', '=', 'jjob_job_cities.job_id')
                ->whereIn('jjob_job_cities.city_id', $data['city_id']);
        }
        if (!empty($data ['job_level_id'])) {
            $jobs = $jobs
                ->whereIn('jjob_jobs.job_level_id', $data['job_level_id']);
        }
        if (!empty($data ['occupation_main'])) {
            $jobs = $jobs
                ->whereIn('jjob_jobs.occupation_main', $data['occupation_main']);
        }
        if (!empty($data ['occupation_supporting'])) {
            $jobs = $jobs
                ->whereIn('jjob_jobs.occupation_supporting', $data['occupation_supporting']);
        }
        if (!empty($data ['occupation_supporting_detail'])) {
            $jobs = $jobs
                ->whereIn('jjob_jobs.occupation_supporting_detail', $data['occupation_supporting_detail']);
        }
        if (!empty($data ['title'])) {
            $jobs = $jobs
                ->where('jjob_jobs.title', 'like', '%' . implode(",", $data['title']) . '%');
        }

        $jobs = $jobs
            ->latest('jjob_jobs.created_at')
            ->groupBy('jjob_jobs.id')
            ->select('jjob_jobs.*')
            ->with(['customer', 'citys'])
            ->paginate(Job::paginations);

        return $jobs;
    }

    public function searchTypeHeadFollowNameCustomerOrNameJobAjax($inputSearch)
    {
        $jobInstance = new Job();
        return $jobInstance
            ->leftJoin('jjob_job_tags', 'jjob_jobs.id', '=', 'jjob_job_tags.job_id')
            ->leftJoin('jjob_tags', 'jjob_job_tags.tag_id', '=', 'jjob_tags.id')
            ->leftJoin('jjob_customers', 'jjob_jobs.customer_id', '=', 'jjob_customers.id')
            ->where(function ($query) use ($inputSearch) {
                $query->where('jjob_jobs.title', 'like', '%' . $inputSearch . '%')
                    ->where('jjob_jobs.status', Job::statusPublic)
                    ->where('jjob_jobs.active', Job::active)
                    ->orWhere('jjob_tags.name', 'like', '%' . $inputSearch . '%');

            })
            ->orWhere('jjob_customers.name', 'like', '%' . $inputSearch . '%')
            ->groupBy('jjob_jobs.title')
            ->latest('jjob_jobs.created_at')
            ->select('jjob_jobs.title as name')
            ->get();

    }

    public function getJobsFollowIndustry($idJob, $industryJob)
    {
        return Job::join('jjob_job_industries', 'jjob_jobs.id', '=', 'jjob_job_industries.job_id')
            ->join('jjob_industries', 'jjob_job_industries.industry_id', '=', 'jjob_industries.id')
            ->whereIn('jjob_industries.id', $industryJob)
            ->where('jjob_jobs.id', '<>', $idJob)
            ->select('jjob_jobs.*')
            ->limit(20)->latest()
            ->groupBy('jjob_jobs.id')
            ->with('customer')->get();

    }


    // Model for admin
    public function searchJobsAdmin($request)
    {
        $dataReturn = new Job();
        $dataReturn = $dataReturn
            ->join('jjob_customers', 'jjob_jobs.customer_id', 'jjob_customers.id');
        if (!empty($request->id_job)) {
            $dataReturn = $dataReturn->where('jjob_jobs.id', $request->id_job);
        }
        if (!empty($request->name_job)) {
            $dataReturn = $dataReturn->where('jjob_jobs.title', 'like', '%' . $request->name_job . '%');
        }
        if (!empty($request->id_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.id', $request->id_customer);
        }
        if (!empty($request->name_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.name', 'like', '%' . $request->name_customer . '%');
        }
        $dataReturn = $dataReturn
            ->latest('jjob_jobs.created_at')
            ->select('jjob_jobs.*')
            ->paginate(Job::paginations);

        return $dataReturn;
    }

}
