<div class="txt-format">
    <h1>Tìm kiếm Công ty</h1>
        <p>Để tìm
            kiếm công ty, bạn cần thực hiện theo các bước sau:</p>
        <ul>
            <li><strong>Bước 1:&nbsp;</strong>Tạo tài khoản cá nhân và tiến hành đăng nhập vào tài khoản. (Xem <a
                    href="javascript:;" onclick="$('#bat-dau_tao-tai-khoan_tab').click();">Hướng dẫn tạo tài khoản mới</a>)
            </li>
            <li><strong>Bước 2:</strong> Nhập thông tin tên doanh nghiệp mà bạn cần tìm và nhấp vào biểu tượng tìm kiếm.
                Hoặc: Vào phần Việc Làm từ &nbsp;Head Menu và tìm tên Doanh Nghiệp từ công cụ tìm kiếm Job
            </li>
            <li><strong>Bước 3: </strong>Click vào logo của công ty để tới được trang Doanh Nghiệp để đọc các thông tin
                về doanh nghiệp. Trở thành Fan (<strong>Become a fan</strong>) của doanh nghiệp đó nếu bạn muốn được ưu
                tiên cập nhật các thông tin của doanh nghiệp trên Inplace
            </li>
        </ul>
</div>
