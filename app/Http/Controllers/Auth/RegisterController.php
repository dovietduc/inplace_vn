<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\SendEmailActiveAcount;
use App\Occupation;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Auth;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $user;
    protected $occupation;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Occupation $occupation)
    {
        $this->user = $user;
        $this->occupation = $occupation;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        $occupations = $this->occupation->where('parent_id', 0)->get();
        return view('auth.register', compact('occupations'));
    }

    public function register(RegisterRequest $request)
    {
        try {
            DB::beginTransaction();
            if (!empty($request->join_company)) {
                session(['join_company_not_have_acount' => $request->join_company]);
            }
            $userInstance = $this->user->create([
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'email' => $request->email,
                'occupation_id' => $request->occupation_id
            ]);
            $details['email'] = $request->email;
            $details['id'] = $userInstance->id;
            $details['name'] = $userInstance->name;
            dispatch(new SendEmailActiveAcount($details));
            Auth::login($userInstance);
            DB::commit();
            return redirect()->route('home');
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
            return redirect()->back()->with('message', 'Xảy ra lỗi trong quá trình update')->withInput();
        }
    }

    public function showRegistrationFormHome() {
        $occupations = $this->occupation->where('parent_id', 0)->get();
        return view('auth.register', compact('occupations'));
    }
}
