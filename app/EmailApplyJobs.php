<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class EmailApplyJobs extends Model
{
    protected $guarded = [];
    use BaseEloquent;
}
