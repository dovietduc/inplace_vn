<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'jjob_customer_address';
    protected $guarded = [];
    use BaseEloquent;

    public function city() {
        return $this->belongsTo(City::class, 'city_id');
    }
}
