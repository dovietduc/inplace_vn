<div class="section-info_form">
    <form class="formEditCompanyInfoDashboard" data-city="{{ json_encode($citys) }}"
          action="{{ route('enterprise.companies.edit.customer-basic', ['id' => $customer->id]) }}"
          method="POST">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Slogan</label>
                </div>
                <div class="col-md-8">
                    <input
                        value="{{ $customer->slogan }}"
                        type="text"
                        class="form-control"
                        placeholder="Khẩu hiệu công ty"
                        name="slogan"
                    >
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Giới thiệu chung</label>
                </div>
                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="mission"
                                        maxlength="1000"
                                    >{!! $customer->mission !!}</textarea>
                    <span class="note"><span
                            class="textarea-counter-result">1000</span> ký tự còn lại</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Website</label>
                </div>
                <div class="col-md-8">
                    <input
                        type="text"
                        value="{{ $customer->website }}"
                        class="form-control inp-url"
                        placeholder="http://..."
                        name="website">
                    <div class="err-inp"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Điện thoại</label>
                </div>
                <div class="col-md-8">
                    <input type="text"
                           value="{{ $customer->phone }}"
                           class="form-control inp-tel"
                           placeholder="Số điện thoại"
                           pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+"
                           name="phone"
                           required>
                    <div class="err-inp"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Giám đốc / Nhà sáng lập</label>
                </div>
                <div class="col-md-8">
                    <input
                        value="{{ $customer->director }}"
                        type="text"
                        class="form-control"
                        placeholder="Tên Giám đốc/ Nhà sáng lập"
                        name="director">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Số Lượng Nhân Sự</label>
                </div>
                <div class="col-md-8">
                    <select name="company_size_id" class="select2-noSearch">
                        <option value=""></option>
                        @foreach($companySizes as $size)
                            <option
                                {{ $customer->company_size_id == $size->id ? 'selected' : '' }}
                                value="{{ $size->id }}">
                                {{ $size->size }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Năm thành lập</label>
                </div>
                <div class="col-md-8">
                    <div class="reset-semantic-calendar">
                        <label for="founded-year"><i class="far fa-calendar-alt"></i></label>
                        <input
                            type="text"
                            value="{{ $customer->founded_year }}"
                            name="founded_year"
                            placeholder="Năm thành lập"
                            class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Lĩnh vực hoạt động</label>
                </div>
                <div class="col-md-8">
                    <select class="select-2-field-activity" name="industry[]" multiple>
                        <option value=""></option>
                        @foreach($customerIndustrys as $customerIndustryItem)
                            <option
                                {{ $industrySelected->contains($customerIndustryItem->id) ? 'selected' : '' }}
                                value="{{ $customerIndustryItem->id }}">
                                {{ $customerIndustryItem->name }}
                            </option>
                        @endforeach

                    </select>
                    <span class="note">Tối đa 3 ngành nghề</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Bản đồ</label>
                </div>
                <div class="col-md-8">
                    <div class="form-group_map">
                        <textarea type="text"
                               id="input-map"
                               name="place_location"
                               class="form-control" rows="5"
                               placeholder="Chia sẻ địa điểm Google Map">{{ $customer->place_location }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Địa chỉ</label>
                </div>
                <div class="col-md-8">
                    <div class="dynamicallRowsAddress form-group_address">
                        <div class="dynamicallRowsAddress_result">


                            @if($customer->customerAddress()->count() == 0)
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group_address_city">
                                            <select
                                                required
                                                name="city[]"
                                                class="dynamicallRowsAddress_city select2-city">
                                                <option value=""></option>
                                                @foreach($citys as $cityItem)
                                                    <option value="{{ $cityItem->id }}">
                                                        {{ $cityItem->name }}
                                                    </option>
                                                @endforeach

                                            </select>
                                            <label><i class="fas fa-map-marker-alt"></i></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group_address_branch">
                                            <input
                                                name="address[]"
                                                type="text" value=""
                                                class="dynamicallRowsAddress_branch form-control"
                                                placeholder="Nhập đia chỉ văn phòng / chi nhánh của bạn">
                                            <label><i class="fas fa-map-signs"></i></label>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @foreach($customer->customerAddress as $addressItem)
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group_address_city">
                                                <select
                                                    required
                                                    name="city[]"
                                                    class="dynamicallRowsAddress_city select2-city">
                                                    <option value=""></option>
                                                    @foreach($citys as $cityItem)
                                                        <option
                                                            {{ $addressItem->city_id == $cityItem->id ? 'selected' : '' }}
                                                            value="{{ $cityItem->id }}">
                                                            {{ $cityItem->name }}
                                                        </option>
                                                    @endforeach

                                                </select>
                                                <label><i class="fas fa-map-marker-alt"></i></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group_address_branch">
                                                <input
                                                    name="address[]"
                                                    type="text" value="{{  $addressItem->address }}"
                                                    class="dynamicallRowsAddress_branch form-control"
                                                    placeholder="Nhập đia chỉ văn phòng / chi nhánh của bạn">
                                                <label><i class="fas fa-map-signs"></i></label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif


                        </div>
                        <div class="form-group_address_btnAdd">
                            <button type="button"
                                    class="dynamicallRowsAddress_btnAdd btn btn-custom_light"><i
                                    class="fal fa-plus"></i> Thêm địa chỉ
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 offset-md-4">
                    <div class="form-group_submit">
                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
