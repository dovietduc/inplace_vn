<div class="company-info-list">
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Lĩnh vực hoạt động:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            HR&Technology
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Website:
        </div>
        <div class="col-12 col-md-8">
            <a href="#" title="" class="text-green-300">
                https://www.timcongsu.vn
            </a>
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Quy mô doanh nghiệp:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            201 - 500 nhân viên
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Giám đốc:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            Huyen Nguyen
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Văn hóa doanh nghiệp:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            <span class="badge badge-pill badge-white-500 text-size-14 font-weight-regular">
                <span class="text-green-300 px-1">Trẻ trung</span>
            </span>
            <span class="badge badge-pill badge-white-500 text-size-14 font-weight-regular">
                <span class="text-green-300 px-1">Năng động</span>
            </span>
            <span class="badge badge-pill badge-white-500 text-size-14 font-weight-regular">
                <span class="text-green-300 px-1">Sáng tạo</span>
            </span>
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Năm thành lập:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            2003
        </div>
    </div>
    <div class="item row">
        <div class="col-12 col-md-4 text-black-500">
            Trụ sở chính:
        </div>
        <div class="col-12 col-md-8 text-black-400">
            2F Mekong Tower, 249 Cong Hoa Street, Tan Binh District, HCMC
        </div>
    </div>
</div>
