@extends('layouts.app')
@section('meta')
    <title>Members Invite</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/enterprise/info/members-invite.css')}}">
@stop
@section('content')
    @if(empty($checkUserHaveAcount))
        <div class="members-invite-page" data-url="{{ url()->current() }}">
            <div class="container">
                <a href="#" class="avatar">
                    <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                </a>
                <p class="text-welcome">
                    {{ optional($member->user)->name }} has visited you to become a member of {{ optional($member->customer)->name }} on Inplace Admin
                </p>
                <a
                    href="{{ url('/auth/redirect/facebook') }}"
                    class="btn btn-custom join_company">
                    Join HRE width Facebook
                </a>
                <p class="text-login">
                    <a href="{{ route('login') }}" class="join_company">Đăng nhập vào tài khoản của bạn</a><br>Hoặc,
                    <a href="{{ route('register') }}" class="join_company">Đăng ký bằng tài khoản email của bạn</a>
                </p>
            </div>
        </div>

    @else
        <div class="members-invite-page">
            <div class="container">
                <a href="#" class="avatar">
                    $member
                    @include('enterprise.partials.show-image-avatar-user', ['member' => $checkUserHaveAcount])
                </a>
                <p class="text-welcome">
                    {{ optional($member->user)->name }} has visited you to become a member of {{ optional($member->customer)->name }} on Inplace Admin
                </p>
                <a
                    href="{{ route('enterprise.member.acceptJoinCompany', ['customerId' => optional($member->customer)->id, 'token' => $token]) }}"
                    class="btn btn-custom">
                    Tham gia
                </a>
            </div>
        </div>
    @endif
@stop
@section('js')
    <script src="{{asset('pages/enterprise/info/members-invite.js')}}"></script>
@stop
