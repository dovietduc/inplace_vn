@extends('layouts.login')

@section('meta')
    <title>Login - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link href="{{ asset('pages/users/forgot/forgot.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @include('partials.header-signin')
        <div class="forgot">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="sologan">
                            <h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
                            <div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
                        </div>
                    </div>
                    <div class="col-lg-5 d-flex justify-content-center justify-content-lg-end">
                        <div class="form">
                            <div class="title text-center">Oops!<br>Có vẻ như bạn đã quên mật khẩu?</div>
                            <div class="txt text-center">Không sao, chỉ cần nhập địa chỉ email vào ô bên dưới để đặt lại mật khẩu
                                nhé, sẽ nhanh thôi.
                            </div>

                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    @error('email')
                                    <div class="alert alert-warning" role="alert">
                                        {{ $message }}
                                    </div>
                                    @enderror

                                    <input id="email"
                                           type="email" class="form-control form-control-gray @error('email') is-invalid @enderror"
                                           name="email"
                                           placeholder="Nhập email của bạn"
                                           value="{{ old('email') }}"
                                           required
                                           autocomplete="email" autofocus
                                    >
                                </div>

                                <div class="form-group button">
                                    <button type="submit" class="btn btn-custom">GỬI</button>
                                </div>

                                <div class="back">
                                    <a href="{{ route('login') }}"><i class="fas fa-chevron-left"></i> Quay lại đăng nhập</a>
                                </div>

                                <div class="regiter text-center">Bạn chưa có tài khoản? <a href="{{ route('register') }}">Đăng ký ngay</a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .forgot -->

    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/users/forgot/forgot.js') }}"></script>
@stop

