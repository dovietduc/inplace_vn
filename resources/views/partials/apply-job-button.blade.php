<a href="#"
   class="btn btn-custom font-weight-normal ml-2"
   data-toggle="modal"
   data-target="{{ auth()->check() ? '#frm-apply' : '#frm-apply_signin' }}">
    <i class="far fa-paper-plane"></i>
    Ứng tuyển
</a>