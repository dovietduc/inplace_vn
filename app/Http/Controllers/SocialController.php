<?php

namespace App\Http\Controllers;

use App\Http\Requests\FillEmailRequest;
use App\Jobs\SendEmailActiveAcount;
use App\Occupation;
use App\UserProvider;
use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use DB;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;

class SocialController extends Controller
{
    private $userProvider;
    private $user;
    private $occupation;

    public function __construct(UserProvider $userProvider, User $user, Occupation $occupation)
    {
        $this->userProvider = $userProvider;
        $this->user = $user;
        $this->occupation = $occupation;

    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $providerUser = Socialite::driver($provider)->user();
        $account = $this->userProvider->where([
            'sns_id' => $providerUser->getId(),
            'sns_type' => $provider
        ])->first();

        if (!empty($account)) {
            auth()->login($account->user);
            return redirect()->route('home');
        } else {
            session(['provider_user' => $providerUser, 'provider' => $provider]);
            return redirect()->route('socialGetEmail');
        }


    }

    public function fillEmail()
    {
        $occupations = $this->occupation->where('parent_id', 0)->get();
        return view('auth.fill-email', compact('occupations'));
    }

    public function createOrGetUser($request)
    {
        try {
            DB::beginTransaction();
            $providerUser = $request->session()->get('provider_user');
            $account = new UserProvider([
                'sns_id' => $providerUser->getId(),
                'sns_type' => $request->session()->get('provider')
            ]);
            // check user exit or not exit
            $user = $this->user->whereEmail($providerUser->getEmail())->first();
            if (empty($user)) {
                $user = $this->user->create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                    'email_verified_at' => Carbon::now()->toDateTimeString(),
                    'occupation_id' => $request->occupation_id
                ]);
            } else {
                // email have facebook validate
                $user->email_verified_at = Carbon::now()->toDateTimeString();
                $user->save();
            }
            $account->user()->associate($user);
            $account->save();
            DB::commit();
            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
        }
    }

    public function socialGetEmail(FillEmailRequest $request)
    {
        $providerUser = $request->session()->get('provider_user');
        $provider = $request->session()->get('provider');
        if ( !empty( session('provider_user')->getEmail()) ) {
            $user = $this->createOrGetUser($request);
            auth()->login($user);

        } else {
            $user = $this->createOrGetUserNotHaveEmail($request);
            $details['email'] = $user->email;
            $details['id'] = $user->id;
            dispatch(new SendEmailActiveAcount($details));
        }
        return redirect()->route('home');

    }

    public function createOrGetUserNotHaveEmail($request)
    {
        try {
            DB::beginTransaction();
            $providerUser = $request->session()->get('provider_user');

            $account = new UserProvider([
                'sns_id' => $providerUser->getId(),
                'sns_type' => $request->session()->get('provider')
            ]);
            $user = $this->user->whereEmail($request->email)->first();
            if (empty($user)) {
                $user = $this->user->create([
                    'email' => $request->email,
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                    'occupation_id' => $request->occupation_id
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            $request->session()->forget(['provider_user', 'provider']);
            DB::commit();
            return $user;

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
        }
    }
}
