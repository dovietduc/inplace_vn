@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('pages/companies/home/home.css') }}">
@stop

@section('content')
    @include('company.partials.company-head-menu')

    <div class="container my-3 py-3 my-lg-5 py-lg-5">
        <div class="row">
            <div class="col-12 col-lg-8 offset-lg-2">
                <div class="text-center">
                    <img src="{{ url('/user/image/coming_soon.png') }}" class="w-100" style="max-width: 670px"/>
                </div>
            </div>
        </div>
    </div>
@endsection
