<?php

namespace App\Console\Commands;

use App\Job;
use App\StatusJobs;
use Illuminate\Console\Command;

class CreateStatusActiveJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:status-active-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create status active jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('status_jobs')->truncate();
        $data = [
            'active',
            'reject',
            'waitting...'
        ];
        foreach ($data as $item) {
            StatusJobs::create(
                ['name' => $item]
            );
        }
        $this->info('Create status jobs complete');

    }
}
