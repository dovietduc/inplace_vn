@extends('layouts.dashboard')
@section('meta')
    <title>Jobs Candidates</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/enterprise/jobs/candidates.css')}}">
@stop
@section('content')
    <div class="container">
        <div class="dashboard-candidates-header">
            <h1>Account Executive (Vietnamese/English)</h1>
            <div class="row">
                <div class="col-5 col-md-3">
                    <label>Trạng thái</label>
                    <p class="color-theme">Đang tuyển</p>
                </div>
                <div class="col-7 col-md-3">
                    <label>Đợt tuyển dụng</label>
                    <p>30/09/2019 - 30/10/2019</p>
                </div>
                <div class="col-md-6 mt-3 mt-md-0">
                    <label>Tin tuyển dụng được liên kết</label>
                    <p><a class="color-theme" href="#">Are you trying to find your place in the creative industry? Be our next 2D Creative Graphic
                        Designer!</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-candidates-list">
        <div class="container">
            <div class="component-title">
                <h3 class="title">Danh sách ứng viên</h3>
                <div class="desc"></div>
            </div>
            <div class="dashboard-candidates-list_th">
                <div class="row">
                    <div class="col-sm-6 col-xl-4">
                        Thông tin ứng viên
                    </div>
                    <div class="col-sm-6 col-xl-4">
                        Thông tin liên hệ
                    </div>
                    <div class="col-sm-7 col-xl-2">
                        Ngày gửi
                    </div>
                    <div class="col-sm-5 col-xl-2 text-right text-xl-center">
                        Thao tác
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-candidate-item" data-member="1">
                <div class="row align-items-center">
                    <div class="col-sm-6 col-xl-4">
                        <div class="info">
                            <a href="" class="avatar">
                                <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                            </a>
                            <div class="title">
                                <h3 class="name">Đào Mai Lê</h3>
                                <div class="desc"><i class="fas fa-map-marker-alt"></i> TP.Hồ Chí Minh</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                        <div class="about">
                            0912 345 6789 <br>
                            votranghoaithanh@gmail.com
                        </div>
                    </div>
                    <div class="col-7 col-xl-2">
                        <div class="authority">24/10/2019</div>
                    </div>
                    <div class="col-5 col-xl-2 text-right text-xl-center">
                        <div class="control">
                            <button class="btn-delete-member btn-control"><i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>
        </div>
    </div>

    {{--    modal Delete member--}}
    <div class="modal-delete-member modal fade" data-member="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header align-items-center">
                    <h5 class="modal-title">Xóa ứng viên</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                            class="far fa-times-circle"></i></button>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xóa ứng viên này?</p>
                    <img class="member-avatar" src="" alt="">
                    <h4 class="member-name"></h4>
                    <p class="member-desc"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-grey" data-dismiss="modal" aria-label="Close">Bỏ qua</button>
                    <button type="button" class="btn-delete-member-confirm btn btn-custom">Xóa</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/enterprise/jobs/candidates.js')}}"></script>
@stop
