const axios = require('axios');

function renderHtmlFollow(data) {
    return `<span class="${data.message == 'Đã là fan' ? 'active' : ''}">${data.message}</span>`;
}

function followCustomerNewsList() {
    let url = $(this).data('url');
    let that = $(this);
    axios.get(url, {
        params: {
            follow_type: 'news_list_screen'
        }
    })
        .then(function (response) {
            that.html(renderHtmlFollow(response.data));
            if ($(".label_count_user_follow").length) {
                that.parent().find('.label_count_user_follow').html(response.data.user_follow_count + ' Fan');
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}

function toogleLike() {
    let url = $(this).data('url');
    let that = $(this)
    axios.get(url, {
        params: {
            news_like_type: 'news_list_screen'
        }
    })
        .then(function (response) {
            that.parent('.posts-item-auth_like').html(response.data.like_html);
        })
        .catch(function (error) {
            console.log(error);
        });

};


$(function () {
    $('.follow_customer_news_list').on('click', followCustomerNewsList);
    // Like news
    $(document).on('click', '.toogle_like_button', toogleLike);
});
