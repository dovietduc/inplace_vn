function toggleSelectBoxOccupation() {
    event.stopPropagation();
    $('.filter--keywords--dropdown').html('').removeClass('d-block');
    $('.filter-control__text').not(this).removeClass('open');
    $(this).toggleClass('open');
}

function hideOptionsOfSelectBox() {
    $('.filter-control__text').removeClass('open');
    $('.filter--keywords--dropdown').html('').removeClass('d-block');
}


export function toogleSelectBoxOccupation() {
    $(document).on('click', '.filter-control__text', toggleSelectBoxOccupation);
    // hide selectbox occupation when click outsite
    $(document).on('click', hideOptionsOfSelectBox);
    $(document).on('click', '.filter-control', (event) => {event.stopPropagation()});
}
