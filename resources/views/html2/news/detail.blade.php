@extends('html2.layout')
@section('content')
    <div class="container page-detail-news">
        <div class="row top-page-list-news">
            <div class="col-lg mb-3 mb-lg-0">
                <div class="article-cat media">
                    <img src="https://via.placeholder.com/100x100" class="cat-avatar border rounded-circle mr-2 mr-lg-20" alt="...">
                    <div class="media-body align-self-center ">
                        <h3 class="cat-name mt-0 font-weight-bold text-black-400">
                            J-JOB Executive Search
                        </h3>
                        <div class="d-flex">
                            <a href="" title=""
                               class="btn-follow btn btn-outline-green-300 rounded-pill text-size-13 mr-10">
                                <span class="px-lg-1">Trở thành Fan</span>
                            </a>
                            <div class="quick-post-info text-grey-200 text-size-12 align-self-center">
                                <span>2899 Fan</span>
                                <span class="dot-space"></span>
                                <span>Hanoi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-auto ml-auto align-self-lg-center">
                <div class="company-menu-page">
                    <div class="nav nav-line nav-link-padding-y-0">
                        @foreach([
                        'Trang chủ', 'Bài viết', 'Thư viện ảnh', 'Video', 'Văn phòng ảo', 'Q&A', 'Quiz'
                        ] as $nav)
                            <div class="nav-item">
                                <a class="nav-link {{ $loop->index == 1 ? 'active' : '' }}" href="#">
                                    {{ $nav }}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
        <div class="cover-article w-100">
            <img src="https://via.placeholder.com/1170x400" class="w-100" alt="">
        </div>

        <div class="row content-page-article">
            <div class="col-12 col-lg-8 offset-lg-2">
                <h1 class="article-title text-black-400">
                    Niềm tin “có ích” như thế nào đối với sự thăng tiến trong sự nghiệp và sự lớn mạnh của tổ chức?
                </h1>
                <div class="article-auth row mb-15 d-none d-lg-block">
                    <div class="col-lg-auto">
                        <div class="auth-info media">
                            <img src="https://via.placeholder.com/48x48" class="auth-avatar rounded-circle mr-15" alt="...">
                            <div class="media-body align-self-center ">
                                <div class="auth-name mt-0 font-weight-bold text-black-400 line-height-sm">
                                    <a href="#" title="">
                                        John Doe
                                    </a>
                                </div>
                                <div class="d-flex">
                                    <div class="auth-role-name text-size-12 text-grey-200">
                                        <span class="">Director/manager</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-auto ml-auto align-self-lg-center">
                        <div class="auth-activity-info">
                            <div class="counter d-inline-block">
                                <span class="text-size-13 text-grey-200">
                                    128k Thích
                                </span>
                            </div>
                            <div class="like-action d-inline-block pl-10 pl-lg-15">
                                <button class="btn btn-link p-0 line-height-sm">
                                    <i class="fas fa-heart text-size-14 text-green-300"></i>
                                </button>
                                <a href="#" title="" class="btn btn-link p-0 line-height-sm text-grey-200 ml-10">
                                    <i class="far fa-share-square"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <hr class="m-0 p-0 d-none d-lg-block"/>
                <div class="article-content mt-lg-4 pt-lg-1 pb-lg-5">
                    @include('html2.news.partials.content_article_detail')
                </div>
                <div class="clearfix mt-lg-5"></div>
                <div class="company-top-jobs px-30 rounded-sm mb-4">
                    <h5 class="title-box text-black-400 mb-10">
                        J-JOB Executive Search đang tuyển dụng các vị trí:
                    </h5>
                    <ul class="list-jobs list-unstyled p-0 mb-1">
                        <li class="item">
                            <a href="#" title="" class="text-green-300">
                                PHP Developer
                            </a>
                        </li>
                        <li class="item">
                            <a href="#" title="" class="text-green-300">
                                HR
                            </a>
                        </li>
                        <li class="item">
                            <a href="#" title="" class="text-green-300">
                                Kế toán trưởng
                            </a>
                        </li>
                    </ul>
                    <div class="read-more">
                        <a href="#" title="" class="text-size-13 text-grey-200">
                            Xem thêm  <i class="fas fa-angle-down"></i>
                        </a>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-12 col-lg-auto mb-3 mb-lg-0">
                        <div class="article-tags">
                            <div class="tags">
                                @foreach(['Culture', 'Career', 'Spotlight'] as $tag)
                                    <a href="#" title="" class="btn btn-outline-grey rounded-pill text-grey-200 py-0 tag-item mr-1 px-3">
                                        {{ $tag }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-auto ml-auto align-self-lg-center">
                        <div class="auth-activity-info">
                            <div class="counter d-inline-block">
                                <span class="text-size-13 text-grey-200">
                                    128k Thích
                                </span>
                            </div>
                            <div class="like-action d-inline-block pl-10 pl-lg-15">
                                <button class="btn btn-link p-0 line-height-sm">
                                    <i class="fas fa-heart text-size-14 text-green-300"></i>
                                </button>
                                <a href="#" title="" class="btn btn-link p-0 line-height-sm text-grey-200 ml-10">
                                    <i class="far fa-share-square"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mb-30 mt-0 p-0"/>
                <div class="article-auth-intro">
                    <div class="item row">
                        <div class="col-lg-9">
                            <div class="media">
                                <img src="https://via.placeholder.com/80x80" class="cat-avatar border rounded-circle mr-15" alt="...">
                                <div class="media-body mt-15">
                                    <div class="">
                                        <a href="#" title="" class="font-weight-bold text-size-16">
                                            John Doe
                                        </a>
                                        <span class="text-grey-200 text-size-12">
                                            /  Marketing Officer
                                        </span>
                                    </div>
                                    <p class="text-black-500">
                                        I am creative and resourceful with over 2 years of writing experience.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto ml-auto align-self-center">
                            <a href="" title=""
                               class="btn-follow btn btn-outline-green-300 rounded-pill text-size-13">
                                <span class="px-lg-1 font-weight-bold">Kết bạn</span>
                            </a>
                        </div>
                    </div>
                    <div class="item row">
                        <div class="col-lg-9">
                            <div class="media">
                                <img src="https://via.placeholder.com/80x80" class="cat-avatar border rounded-circle mr-15" alt="...">
                                <div class="media-body mt-15">
                                    <div class="">
                                        <a href="#" title="" class="font-weight-bold text-size-16">
                                            J-JOB Executive Search
                                        </a>
                                    </div>
                                    <p class="text-black-500">
                                        J-Job là công ty hoạt động trong lĩnh vực Tư vấn nhân sự Head hunting cho
                                        các khách hàng Nhật Bản, Âu Mỹ, Việt Nam. J-Job thành lập từ năm 2012
                                        với đội ngũ gần 20 Tư vấn ...
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto ml-auto align-self-center">
                            <a href="" title=""
                               class="btn-follow btn btn-outline-green-300 rounded-pill text-size-13 mb-2">
                                <span class="px-lg-1 font-weight-bold">Trở thành Fan</span>
                            </a>
                            <div class="text-grey-200 text-size-12 align-self-center text-right">
                                <span>2899 Fan</span>
                                <span class="dot-space"></span>
                                <span>Hanoi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="m-0 p-0"/>
        <div class="related-posts pt-30">
            <div class="box-title mb-30">
                <span class="text-uppercase font-weight-black">
                    Bài viết liên quan
                </span>
            </div>
            <div class="row">
                @for ($i = 0; $i < 3; $i++)
                    @include('html2.news.partials.related_item')
                @endfor
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ mix('user/css/news.css') }}">
@stop
