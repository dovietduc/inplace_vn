@foreach($newsHot as $newsHotItem)
    <div class="component-posts-item-sidebar">
        <div class="posts-item-thumb">
            <a href="{{ route('news.show', ['slugCompany' => optional($newsHotItem->customer)->slug, 'id' => $newsHotItem->id]) }}"
               title="" class="">
                @if( jjobCheckImageExit($newsHotItem->thumbnail) )
                    <img src="{{ asset($newsHotItem->thumbnail) }}" alt="..." class="rounded-circle">
                @else
                    <img src="{{ asset('images/no_img_50x50.jpg') }}" alt="..." class="rounded-circle">
                @endif
                <div class="post-order">
                    {{ $loop->index + 1 }}
                </div>
            </a>
        </div>
        <div class="posts-item-content">
            <h6 class="posts-item-title">
                <a href="{{ route('news.show', ['slugCompany' => optional($newsHotItem->customer)->slug, 'id' => $newsHotItem->id]) }}"
                   title="" class="">
                    {{ $newsHotItem->title }}
                </a>
            </h6>
            <div class="posts-item-auth">
                <a href="" class="name">
                    {{ optional($newsHotItem->user)->name }}
                </a>
                <span class="job">
                {{  optional($newsHotItem->category)->name }}
            </span>
            </div>
        </div>
    </div>
@endforeach
