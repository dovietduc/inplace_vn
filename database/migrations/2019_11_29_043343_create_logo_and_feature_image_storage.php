<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogoAndFeatureImageStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jjob_customers', function (Blueprint $table) {
           $table->string('logo_storage');
           $table->string('feature_image_storage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jjob_customers', function (Blueprint $table) {
            $table->dropColumn('logo_storage');
            $table->dropColumn('feature_image_storage');
        });
    }
}
