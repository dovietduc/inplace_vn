<div class="modal fade" id="popupEditAvatar"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cập nhật ảnh đại diện</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div id="upload-demo-avatar"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-custom_light" data-dismiss="modal">Hủy</button>
                <button type="submit"
                        data-url="{{ route('croppie.upload-image-avatar') }}"
                        class="btn-crop-avatar btn btn-custom">Cập nhật</button>
            </div>
        </div>
    </div>
</div>
