<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    private $customer;
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function update()
    {
        $customers = $this->customer->getAll();
        return view('admin.companies.form-update', compact('customers'));
    }

    public function postUpdate(Request $request)
    {
        $dataUpdate = [];
        if (!empty(trim($request->email))) {
            $dataUpdate['email'] = trim($request->email);
        }
        if (!empty(trim($request->name))) {
            $dataUpdate['name'] = trim($request->name);
        }
        $this->customer->updateData($request->customer, $dataUpdate);
        return redirect()->back()->with('message', 'Update thành công');
    }
}
