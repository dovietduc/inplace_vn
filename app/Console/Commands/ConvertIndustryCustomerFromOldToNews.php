<?php

namespace App\Console\Commands;

use App\CustomerIndustry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class ConvertIndustryCustomerFromOldToNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:industry-customer-from-old-to-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert industry customer from old to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataIndustryMore = [
            'An ninh mạng',
            'An Ninh/Quốc Phòng',
            'App gọi xe',
            'Bảo hiểm',
            'Bảo tàng',
            'Bảo tồn thiên nhiên',
            'Bảo vệ động vật',
            'Bảo vệ môi trường',
            'Bất động sản',
            'BPO',
            'Chăm sóc sắc đẹp',
            'Chăm sóc sức khỏe',
            'Chứng khoán',
            'Cơ khí',
            'Cơ quan nhà nước',
            'Công nghệ sinh học',
            'Công ty cổ phần',
            'Công ty có vốn nhà nước',
            'Công ty có vốn nước ngoài',
            'Công ty đa quốc gia',
            'Công ty tư nhân',
            'Công viên / Công trình văn hoá',
            'Dầu khí',
            'Dệt may',
            'Dịch Thuật',
            'Dịch vụ Bảo Hộ / Bảo Vệ',
            'Dịch vụ Bảo trì / Sửa chữa',
            'Dịch vụ Kế Toán / Thuế',
            'Tư Vấn Tài Chính',
            'Điện',
            'Điện tử / Điện lạnh',
            'Dược Phẩm',
            'E - Commerce',
            'Ed - Tech',
            'Event',
            'Fin - Tech',
            'FMCG',
            'Food App',
            'Giải trí / Phim',
            'Giáo dục / Đào tạo',
            'Hàng hải',
            'Hàng không',
            'Headhunt / Tuyển dụng',
            'Health - Tech',
            'Hoá chất',
            'HR Services',
            'HR - Tech',
            'In ấn / Xuất bản',
            'Internet / Online Media',
            'IT - Big Data / Cơ Sở Dữ Liệu',
            'IT Phần Cứng',
            'ITO',
            'IT - Phát triển App / Phần Mềm',
            'Logistics / Kho vận',
            'Khoáng sản',
            'Kiến Trúc',
            'Lâm nghiệp',
            'Marketing Agency',
            'Media Agency',
            'Môi trường / Xử lý chất thải',
            'Mỹ phẩm',
            'Mỹ thuật / Nghệ thuật / Thiết kế',
            'Ngân Hàng',
            'Ngành nghề truyền thống',
            'Nghiên cứu thị trường',
            'Nhà hàng / Dịch vụ ăn uống',
            'Nông nghiệp',
            'Nước',
            'Phân phối / Bán lẻ',
            'Phi chính phủ / Phi lợi nhuận',
            'Photography Studio',
            'Quân Đội',
            'Quảng Cáo / Tiếp Thị',
            'Quỹ Đầu Tư',
            'Real - Tech',
            'Sản phẩm thân thiện môi trường',
            'Sản xuất Âm Nhạc',
            'Sản xuất công nghiệp',
            'Sản xuất ô tô',
            'Sản xuất Phim',
            'Social Media',
            'Tập Đoàn Đa Ngành Nghề',
            'Tech Startup',
            'QA / QC',
            'Thảo Cầm Viên / Sở Thú',
            'Thiết bị điện công nghiệp',
            'Thiết Kế Nội Thất',
            'Thời Trang',
            'Thử nghiệm / Thí nghiệm',
            'Thực phẩm & Đồ uống',
            'Trang sức / Thủ công / Mỹ Nghệ',
            'Truyền thông / Báo chí / Truyền hình',
            'Tự động hóa',
            'Tư vấn Luật',
            'Tư Vấn Quản Lý',
            'Tư vấn quản lý dự án',
            'Vận chuyển / Giao hàng công nghệ',
            'Vận chuyển / Giao hàng',
            'Vật tư / Thu mua / Cung ứng',
            'Viễn Thông',
            'Xuất nhập khẩu',
            'Y tế / Bệnh Viện',
            'Khác'
        ];
        try {
            DB::beginTransaction();

            foreach ($dataIndustryMore as $industryItem) {
                CustomerIndustry::create([
                    'name' => $industryItem
                ]);
            }

            DB::commit();
            $this->info('Convert data industry customer complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
