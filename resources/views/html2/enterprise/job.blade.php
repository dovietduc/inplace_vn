@extends('html2.enterprise.layout')
@section('content_page')
<div class="container">
    <div class="row enterprise-job-page mt-15">
    <div class="col-lg-3">
        <div class="thread-list">
            <div class="box-name text-grey-200 font-weight-bold mb-15">
                <span>
                    Chủ đề/Từ khóa
                </span>
            </div>
            <ul class="list-unstyled">
                <li>
                    <a href="#" title="" class="font-weight-medium active">
                        Marketing
                    </a>
                </li>
                <li>
                    <a href="#" title="" class="font-weight-medium">
                        Designer
                    </a>
                </li>
                <li>
                    <a href="#" title="" class="font-weight-medium">
                        Sales
                    </a>
                </li>
                <li>
                    <a href="#" title="" class="font-weight-medium">
                        PHP Developer
                    </a>
                </li>
                <li>
                    <a href="#" title="" class="font-weight-medium">
                        Mobile Developer
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="card card-line border-0 box-choice-intro">
            <div class="card-header px-0">
                <span class="card-name">
                    Việc làm cho Marketing
                </span>
            </div>
            <div class="card-body px-0 py-30">
                <div class="post-list mt-2">
                    @for ($i = 0; $i < 3; $i++)
                        @include('html2.enterprise.partials.post_item_job_page')
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('html2.enterprise.partials.related_company')
@endsection
