<?php

namespace App\Http\Middleware;

use App\MemberCompany;
use Closure;

class AuthenticationCustomerBasic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $customerIdAuth = $request->route('customerId');
        if(auth()->check()) {
            $checkUserIsMemberCompany = MemberCompany::where([
            'user_id' => auth()->id(),
            'customer_id' => $customerIdAuth
        ])->first();
            if (!empty($checkUserIsMemberCompany)) {
                return $next($request);
            }
            abort('404');
        }
        else {
            abort('404');
        }
    }
}
