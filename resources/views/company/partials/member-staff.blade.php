<div class="job-detail_content__who">
    <div class="component-title">
        <h3 class="title">Who</h3>
        <div class="desc">Ai có thể trở thành đồng đội của bạn</div>
    </div>
    <a href="" class="view-more">Xem tất cả (<span class="count">6</span>)</a>
    <div class="component-who">
        <ul class="tab-who nav" role="tablist">
            <?php for ($i = 0; $i < 4; $i++){ ?>
            <li class="col-3">
                <a class="item {{ $i == 0 ?  'active' : ''}}" data-toggle="tab"
                   href="#tab-<?php echo $i;?>" role="tab" aria-controls="home"
                   aria-selected="true">
                    <img class="avatar" src="/images/job-detail_who.png" alt="">
                    <h5 class="title">Francis Sim</h5>
                    <div class="desc">Content Creator</div>
                </a>
            </li>
            <?php } ?>
        </ul>
        <div class="tab-content">
            <?php for ($i = 0; $i < 4; $i++){ ?>
            <div class="item_content tab-pane {{ $i == 0 ?  'active' : ''}}"
                 id="tab-<?php echo $i;?>" role="tabpanel" aria-labelledby="home-tab">
                <?php echo $i;?>-John Doe phụ trách Customer Service với các Khách hàng
                Doanh
                nghiệp của dự án Inplace.vn Lorem ipsum dolor sit amet, consectetur
                adipiscing elit.
            </div>
            <?php } ?>
        </div>
    </div>
</div>