
$(function () {
    $('.account-setting-navigation-scroll a').on('click', function(e) {
        e.preventDefault();
        let target = $(this).attr("href");
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, 600, function() {
            location.hash = target;
        });
    });
    $(window).scroll(function() {
        var scrollDistance = $(window).scrollTop();
        $('.account-setting-section').each(function(i) {
            if ($(this).position().top <= scrollDistance) {
                $('.account-setting-navigation-scroll a.active').removeClass('active');
                $('.account-setting-navigation-scroll a').eq(i).addClass('active');
            }
        });
    }).scroll();
})

