<div class="job-detail_content__detail">
    <div class="component-title">
        <h3 class="title">Mô tả công việc</h3>
        <div class="desc"></div>
    </div>
    <div class="content txt-format fr-view">
        {!! $job->description !!}
    </div>
    <div class="button-group">
        @include('partials.apply-job-button')
        @if(auth()->check())
            @if( empty($job->userSaveJob()->where('user_id', auth()->id())->count()) )
                <button
                    data-url="{{ route('jobs.save', ['id' => $job->id]) }}"
                    class="btn-save-job save_job btn btn-custom_light">
                    <i class="far fa-bookmark"></i>
                </button>
            @else
                <button
                    disabled
                    class="btn-save-job btn btn-custom_light saved">
                    <i class="far fa-bookmark"></i>
                </button>
            @endif

            @else
            <a href="{{ route('register') }}"
                class="btn-save-job btn btn-custom_light">
                <i class="far fa-bookmark"></i>
        @endif


        <a
            target="_blank"
            href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}"
            class="btn btn-custom_light share_facebook_social">
            <i class="far fa-share-square"></i> Chia sẻ
        </a>
    </div>
</div>
