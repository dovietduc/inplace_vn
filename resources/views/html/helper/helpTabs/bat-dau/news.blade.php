<div class="txt-format">
    <h1>News (Tin tức)</h1>
    <p>
        Người dùng có thể đọc tin tức được người dùng/ doanh nghiệp khác chia sẻ bằng cách nhấp vào <strong>Khám phá</strong> trên thanh công cụ góc trên bên trái màn hình. Trang tin <strong>Khám phá</strong> là nơi người dùng sẽ chia sẻ những nội dung liên quan tới công việc và văn hoá  tại nơi làm việc của họ, nơi các nhà tuyển dụng chia sẻ kinh nghiệm và kiến thức góp phần xây dựng thương hiệu cá nhân và thương hiệu nhà tuyển dụng hiệu quả hơn. Mỗi một nội dung được thêm vào sẽ làm tăng thêm sự phong phú cho kho kiến thức chung của INPLACE. Ngoài ra, các Photo Gallery, Video hay các bài Quiz mới cũng sẽ hiển thị trên trang <strong>Khám phá</strong> này.
        </p><p>Để tạo nội dung mới, người dùng có thể vào phần <strong>Tạo bài viết mới</strong>, bằng cách nhấp vào <strong>Viết bài</strong> và làm theo hướng dẫn.
    </p><p>Các nội dung trong trang cũng được tổ chức theo các nhóm chuyên biệt để người xem tiện theo dõi như Công Ty, Văn Hoá, Phỏng Vấn, Chuyện nghề,  Tech,  Startup  v.v.. .
    </p>
</div>
