@extends('layouts.dashboard')
@section('meta')
    <title>Jobs List Dashboard</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/jobs/list.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
                <div class="button-group">
                    <select class="dashboard-header_filter">
                        <option>Tất cả (10)</option>
                        <option>Đã đăng (8)</option>
                        <option>Nháp (12)</option>
                        <option>Hết hạn (12)</option>
                        <option>Tạm ngưng (12)</option>
                    </select>
                    <a href="{{ route('enterprise.jobs.add') }}"
                       class="btn btn-custom"><i class="fas fa-plus mr-1"></i>Đăng tuyển dụng</a>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-jobs-list">
        <div class="container">
            <div class="dashboard-jobs-list_th">
                <div class="row">
                    <div class="col-5">
                        Tiêu đề
                    </div>
                    <div class="col-7">
                        <div class="row">
                            <div class="col-3">
                                Lượt xem
                            </div>
                            <div class="col-3">
                                Ứng tuyển
                            </div>
                            <div class="col-3">
                                thời gian
                            </div>
                            <div class="col-3">
                                Trạng thái
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-jobs-item">
                <div class="row">
                    <div class="col-md-5">
                        <div class="dashboard-jobs-item_title">
                            <h4 class="title">
                                <a href="#">
                                    {{ $job->title }}
                                </a>
                            </h4>
                            <div class="d-flex align-items-center justify-content-between mt-2">
                                <div class="location"><i class="fas fa-map-marker-alt"></i> TP.Hồ Chí Minh</div>
                                <div class="action">
                                    <a href="" class="action_view" title="Xem" target="_blank"><i class="far fa-eye"></i></a>
                                    <a href="" class="action_edit" title="Sửa"><i class="far fa-edit"></i></a>
                                    <a href="" class="action_delete" title="Xóa" data-id = "<?php echo $i ?>"><i class="far fa-trash-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-3">
                                <p class="info-txt">10</p>
                            </div>
                            <div class="col-3">
                                <p class="info-txt"><strong>9</strong> ứng viên</p>
                                <a href="#" class="btn btn-custom_light">Xem</a>
                            </div>
                            <div class="col-3">
                                <p class="info-txt"><small>Ngày đăng:</small><br>10/9/2019</p>
                            </div>
                            <div class="col-3 text-right text-md-left">
                                <span class="status-txt active">Đã đăng</span>
{{--                                <span class="status-txt draft">Nháp</span>--}}
{{--                                <span class="status-txt">Đã đóng</span>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/jobs/list.js')}}"></script>
@stop
