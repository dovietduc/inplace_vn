import {addDataCityToFormSubmit} from "../jobs/components/add-data-city-to-form-submit";
import {addDataOccupationToFormSubmit} from "../jobs/components/add-data-occupation-to-form-submit";
import {toogleSelectBoxOccupation} from "../jobs/components/toogle-select-box-occupation";
import {addDataLevelJobToFormSubmit} from "../jobs/components/add-data-level-job-to-submit-form";
import {resetFormData} from "../jobs/components/reset-form-data";
import {addInputFilterToFormSubmit} from "../jobs/components/add-input-filter-to-form-submit";
import {submitFormData} from "../jobs/components/submit-form-data";
export function sideBarFilterJob() {
    // click city submit form
    addDataCityToFormSubmit();
    // click occupation submit form
    addDataOccupationToFormSubmit();
    // Toogle select box occupation
    toogleSelectBoxOccupation();
    // click select job level
    addDataLevelJobToFormSubmit();
    // show button and reset form data
    resetFormData();
    // input filter search keyword add to form submit
    addInputFilterToFormSubmit();
    //submit form
    submitFormData();
    //preventDefault
    $(document).on('click', '.filter--keywords .search-input', keywordStopPropagation);
    $(document).on('click','.button-filter-block-mobile', blockFilterToggle);
}

function blockFilterToggle(event) {
    event.preventDefault();
    $('.block-filter').slideToggle();
}

function keywordStopPropagation(){
    event.stopPropagation();
    $('.filter-control__text').removeClass('open');
}
