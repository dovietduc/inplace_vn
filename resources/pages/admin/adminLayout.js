//layout sidebar dashboard
export function adminLayout() {
    if(window.innerWidth > 991){
        $('.dashboard-sidebar, .dashboard-main').theiaStickySidebar({
            additionalMarginTop: 64,
        });
    }
    if (window.innerWidth <= 991){
        $('body').click(function () {
            $(this).removeClass('dashboard-sidebar-toggle');
        })
    }

    $('.btn-dashboard-sidebar-toggle').click(function (e) {
        e.stopPropagation();
        $('body').toggleClass('dashboard-sidebar-toggle');
    })
    $('.dashboard-sidebar').click(function (e) {
        e.stopPropagation();
    })
//end: layout sidebar dashboard
}

