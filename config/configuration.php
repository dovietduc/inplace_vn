<?php
return [
    'category' => [
        1 => 'Chúng ta làm việc- Chúng ta hạnh phúc',
        2 => 'Câu chuyện Startup',
        3 => 'Work Style',
        4 => 'Discover Company',
        5 => 'Blog Nhật bản',
        6 => 'Tech Talk'
    ],
    'job_apply_status' => [
        1 => 'Ứng viên muốn ứng tuyển ngay',
        2 => 'Ứng viên muốn nghe thêm thông tin về Job',
        3 => 'Ứng viên mới quan tâm đến Job'
    ],
    'job_languages_id' => [
        'en' => 1,
        'jp' => 2
    ],
    'industry_type' => [
        'customer' => 2,
        'user' => 1
    ],
    'job_salary' => [
        1 => 'You\'ll love it',
        2 => 'Negotiable',
        3 => 'Trong khoảng'
    ],
    'category_news' => [
        'all' => 10,
        'page' => 20
    ],


    // Config ngày trong tuần theo tiếng việt
    'day_of_week_map' => [
        0 => 'Chủ nhật',
        1 => 'Thứ 2',
        2 => 'Thứ 3',
        3 => 'Thứ 4',
        4 => 'Thứ 5',
        5 => 'Thứ 6',
        6 => 'Thứ 7',
    ],

    /*
     * BACKEND SECTION
     */
    // dia chi van phong chinh
    'address_type_customer' => [
        'head_office_main' => 1,
        'head_office_not_main' => 0
    ],
    // phân quyền trong ứng dụng
    'permission' => [
        'normal' => 1,
        'customer' => 2,
        'super' => 3,
        'hr' => 4
    ],
    // Trạng thái ẩn hiện jobs, new..
    'status' => [
        'show' => 1,
        'hide' => 0
    ],

    // jobs, new có lưu nháp hay không?
    'draft' => [
        'not' => 0,
        'have' => 1
    ],

    // TRạng thái delete
    'deleted_flag' => [
        'not' => 0,
        'have' => 1
    ],
    'permission_list' => [
        'vacancy',
        'customer',
        'jobs',
        'news',
        'user',
        'staff',
        'permission',
        'hradmin'
    ],

    'show_always' => [
        1 => "Luôn luôn hiển thị",
        2 => "Chọn ngày hết hạn"
    ],

    // option cho job có ngày hết hạn hay không?


    // option cho job status pust top?
    'push_top' => [
        'not' => 0,
        'pussing' => 1,
        'pusded' => 2
    ],

    'search_type_jobs' => [
        'Tất cả',
        'Job hots'
    ],

    // trạng thái lấy theo giá trị mới event 1/4/2018
    'status_flag' => [
        'show' => 0,
        'hide' => 1
    ],

    // Trạng thái user tham gia event với trạng thái gì?
    'event_user_status' => [
        'care' => 1, // quan tâm đến sự kiện
        'attend' => 2, // sẽ tham gia sự kiện,
        'no_care' => ''
    ],
    //kiểu thành viên
    'staff_type' => [
        1 => 'Thành viên',
        2 => 'Diễn giả'
    ],
    //quyền hạn thành viên trong công ty
    'staff_role' => [
        2 => 'Admin',
        5 => 'Nội dung'
    ],
    // config user profile for delete
    'user_profile' => [
        2 => 'userExperience',
        3 => 'userEducation',
        4 => 'userLanguage',
        5 => 'userSocialActivity',
        6 => 'userPrize',
        7 => 'userSkill',
        8 => 'userPortfolio'
    ],

    'user_apply_type' => [
        'user' => 1,
        'customer' => 2
    ],

    'list_tags_job' => [
        'Marketing',
        'Designer',
        'Sales',
        'PHP Developer',
        'Mobile Developer'
    ],
    'company' => [
        'pagination' => 1
    ]

];
