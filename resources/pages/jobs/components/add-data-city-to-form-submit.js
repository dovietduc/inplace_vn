function  getCity(){
    let cityID = $(this).attr('id');
    let cityBase = $('.selected-base--cities');
    if ($(this).is(':checked')) {
        let htmlBase = '<label for="' + cityID + '">' + $(this).data('name') + '</label>';
        cityBase.append(htmlBase);
    } else {
        let thisBase = cityBase.find('label[for="' + cityID + '"]');
        if (thisBase.length) {
            thisBase.remove();
        }
    }
    $('#search-complete').submit();
}

export function addDataCityToFormSubmit() {
    $(document).on('change', '.filter--city--checkbox', getCity);
}
