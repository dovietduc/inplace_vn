<div class="company-top-jobs">
    <h5 class="company-top-jobs_title">
        {{ optional($newItem->customer)->name }} đang tuyển dụng các vị trí:
    </h5>
    <ul class="top-jobs-list">
        @foreach($jobRelatest as $jobRelatestItem)
            <li class="item">
                <a href="{{ route('jobs.show', ['slug' => $jobRelatestItem->slug]) }}" title="" class="text-green-300">
                    {{ $jobRelatestItem->short_title }}
                </a>
            </li>
        @endforeach
    </ul>
    <a href="{{ route('home') }}" class="viewmore">
        Xem thêm <i class="fas fa-angle-right"></i>
    </a>
</div>
