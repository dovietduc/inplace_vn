<div class="component-title">
    <h3 class="title">
        Why we do
    </h3>
    <div class="desc"></div>
</div>
<a href="" class="view-more section-info_toggle">Thu gọn</a>
<div class="section-info_form">
    <form
        action="{{ route('enterprise.companies.edit.why-we-do',  ['id' => $customer->id]) }}"
        enctype="multipart/form-data"
        method="POST">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Giới thiệu chung</label>
                </div>
                <div class="col-md-8">
                                    <textarea
                                        rows="6"
                                        class="form-control textarea-counter"
                                        placeholder="Giới thiệu điểm nổi bật về Công ty và Sứ mệnh - Tầm nhìn"
                                        name="why_we_do"
                                        maxlength="1000"
                                    >{!! $customer->why_we_do !!}</textarea>
                    <span class="note"><span
                            class="textarea-counter-result">1000</span> ký tự còn lại</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Ảnh số 1</label>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8 col-lg-7">
                            <div class="form-group_upload-image">
                                <img
                                    src="{{ !empty($customer->why_feature_image_1) ? asset($customer->why_feature_image_1) : '' }}">
                                <input
                                    name="why_feature_image_1"
                                    type="file"
                                    class="inp-file-image"
                                    accept=".png,.jpg,.jpeg"/>
                            </div>
                            <textarea
                                name="why_quote_image_1"
                                class="form-control autoheight disenter mt-3"
                                placeholder="Viết chú thích...">{{ $customer->why_quote_image_1 }}</textarea>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Ảnh số 2</label>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8 col-lg-7">
                            <div class="form-group_upload-image">
                                <img
                                    src="{{ !empty($customer->why_feature_image_2) ? asset($customer->why_feature_image_2) : '' }}">
                                <input
                                    name="why_feature_image_2"
                                    type="file"
                                    class="inp-file-image" accept=".png,.jpg,.jpeg"/>
                            </div>
                            <textarea
                                name="why_quote_image_2"
                                class="form-control autoheight disenter mt-3"
                                placeholder="Viết chú thích...">{{ $customer->why_quote_image_2 }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 offset-md-4">
                    <div class="form-group_submit">
                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
