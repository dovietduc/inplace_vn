<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job detail</title>
    <link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/select2/select2.min.css" rel="stylesheet"/>
    <link href="/vendor/slick/slick.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/slick/slick-theme.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
    @yield('css')
</head>
<body>
@include('html.element.header')
@include('html2.partials.header_sub_nav')
@yield('content')
@include('html.element.footer')

<script src="/vendor/slick/slick.min.js"></script>
<script src="/vendor/select2/select2.min.js"></script>
<script src="/vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js"></script>
<script src="/pages/common/main.js"></script>
@yield('js')
</body>
</html>
