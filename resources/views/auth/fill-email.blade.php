@extends('layouts.login')

@section('meta')
    <title>Fill Email - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    <link href="{{ asset('/pages/users/fill-email/fill-email.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @include('partials.header-forgot')
    <div class="fill-email">
        <h3 class="title">Chào mừng bạn</h3>
        <p class="txt">{{ empty( session('provider_user')->getEmail() ) ? 'Vui lòng nhập địa chỉ email của bạn để xác thực'  : ''}}</p>

        <form class="form" method="post" action="{{ route('socialGetEmail') }}">
            @csrf
            <div class="form-group">
                <label>Email</label>
                <input
                    name="email"
                    type="email"
                    class="form-control email @error('email') error @enderror"
                    value="{{ !empty( session('provider_user')->getEmail() ) ? session('provider_user')->getEmail() : '' }}"
                    required
                    placeholder="Nhập email của bạn"
                    required
                    {{ !empty( session('provider_user')->getEmail() ) ? 'readonly' : '' }}
                >
                @error('email')
                <p class="form-alert">
                    <i class="fas fa-exclamation-circle"></i>
                    {{ $message }}
                </p>
                @enderror
            </div>
            <div class="form-group">
                <label>Nghề nghiệp bạn quan tâm</label>
                <select class="select-industry" required name="occupation_id">
                    <option></option>
                    @foreach($occupations as $occupationItem)
                        <option value="{{ $occupationItem->id }}">{{ $occupationItem->name }}</option>
                    @endforeach
                </select>
            </div>


            <button class="btn-submit btn btn-custom" type="submit">Gửi</button>
        </form>
    </div>
    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/pages/users/fill-email/fill-email.js') }}"></script>
@stop
