@extends('layouts.app')
@section('meta')
    <title>Posts Profile</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('pages/profile/posts/posts.css') }}">
@stop
@section('content')
    <div class="profile-pages">
        @include('html.profile.partials.profile-header-pages')
        <div class="profile-postsPage">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 box_content mb-5 mb-md-0">
                        <div class="component-title">
                            <h3 class="title">Bài viết của tôi</h3>
                            <div class="desc"></div>
                        </div>
                        <button class="btn btn-open-form-post"><i class="far fa-edit"></i> Đăng bài viết</button>
                        <div class="row">
                            <?php for($i = 0; $i < 6; $i++) { ?>
                            <div class="col-lg-6">
                                <div class="component-posts-item-box">
                                    <a href="#" class="posts-item-thumb">
                                        <img src="{{asset('images/no_img_350x137.jpg')}}" alt="">
                                    </a>
                                    <div class="posts-item-content">
                                        <h3 class="posts-item-title">
                                            <a href="#">15 Companies Embracing Transparency & Being Truly “Open”</a>
                                        </h3>
                                        <div class="posts-item-desc">
                                            Nếu như trước đây, nhân viên chỉ cần vững chuyên môn thì ngày nay điều đó là chưa đủ. Có lẽ thời thế đã đổi thay, ngoại hình dần trở thành một tiêu chuẩn bắt buộc để...
                                        </div>
                                    </div>
                                    <div class="posts-item-activities">
                                        <div class="times">
                                            <span><i class="far fa-clock"></i> 1 giờ trước</span>
                                        </div>
                                        <div class="like">
                                            <span class="like-count">12k thích</span>
                                            <button class="btn-like-heart toogle_like_button">
                                                <i class="far fa-heart"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4 box_sidebar">
                        @include ('html.profile.partials.profile-sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/moment-with-locales.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('vendor/croppie/croppie.js') }}"></script>
    <script src="{{asset('pages/profile/profile.js')}}"></script>
    <script src="{{asset('pages/profile/posts/posts.js')}}"></script>
@stop
