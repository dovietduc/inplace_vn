{{--Note:--}}
{{--- Nếu đã có ảnh thì khối ".inputCroppieImage" thêm class 'hasImg' và style="background-image: url('{{asset('images/img_profile_cover.png')}}')"--}}
{{--- Nếu không có ảnh thì bỏ class 'hasImg' và bỏ style="background-image: url('{{asset('images/img_profile_cover.png')}}')" đi--}}
<div class="inputCroppieImage fixHeight hasImg" style="background-image: url('{{asset('images/img_profile_cover.png')}}')">
    <div class="inputCroppieImage_inpPr">
        <label><i class="fal fa-image"></i> Kéo hoặc nhấp để sửa ảnh</label>
        <input type="file" name="" class="croppieImage_input" accept=".png, .jpg, .jpeg">
    </div>
    <div class="inputCroppieImage_crop">
        <div class="upload-crop-demo"></div>
        <div class="upload-crop-button">
            <button type="button" class="btnRemoveCrop btn btn-grey">Gỡ</button>
        </div>
    </div>
</div>
