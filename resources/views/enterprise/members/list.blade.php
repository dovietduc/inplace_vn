@extends('layouts.dashboard')
@section('meta')
    <title>Members Dashboard</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/info/members.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Thành viên của <strong>{{ !empty($customerCurrent) ? $customerCurrent->name : '' }}</strong>
                </h1>

                <div class="button-group">
                    <button class="btn btn-custom" data-toggle="modal" data-target="#addMember"><i
                            class="fas fa-plus mr-1"></i> Thêm thành viên
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-members-list">
        <div class="container">
            <div class="dashboard-members-list_th">
                <div class="row">
                    <div class="col-sm-7 col-xl-4">
                        Thành viên
                    </div>
                    <div class="col-sm 3 col-xl-2">
                        Quyền hạn
                    </div>
                    <div class="d-none d-xl-block col-xl-4">
                        Giới thiệu
                    </div>
                    <div class="col-sm-2 col-xl-2">
                        Thao tác
                    </div>
                </div>
            </div>
            @foreach($members as $member)
                <div class="dashboard-member-item member_item_{{ $member->id }}" data-id="{{ $member->id }}">
                    <div class="row align-items-center">
                        <div class="col-sm-7 col-xl-4">
                            <div class="info">
                                <a href="" class="avatar">
                                    @include('enterprise.partials.show-image-avatar-user')
                                </a>
                                <div class="title">
                                    <h3 class="name">{{ optional($member->user)->name }}</h3>
                                    <div class="desc position">{{ $member->position }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-8 col-sm-3 col-xl-2">
                            <div class="authority">Job Admin</div>
                        </div>
                        <div class="d-none d-xl-block col-xl-4">
                            <div class="about introduction">
                                {!! nl2br($member->introduction) !!}
                            </div>
                        </div>
                        <div class="col-4 col-sm-2 col-xl-2">
                            <div class="control">
                                <button
                                    data-url="{{ route('enterprise.member.showMemberEdit', ['id' => $member->id]) }}"
                                    class="btn-edit-member btn-control"
                                    >
                                    <i class="far fa-edit"></i>
                                </button>
                                <button class="btn-delete-member btn-control"><i class="far fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                {{ $members->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>

    {{--    modal Add member--}}
    @include('enterprise.modal.add-member')
    {{--    modal Delete member--}}
    @include('enterprise.modal.delete-member')
    {{--    modal Edit member--}}
    <div class="modal-edit-member modal fade" data-member="" tabindex="-1" role="dialog" aria-hidden="true">
    </div>

@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{ asset('pages/enterprise/info/members.js') }}"></script>
@stop
