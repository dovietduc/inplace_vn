import {dashboardLayout} from '../dashboard.js';
import swal from 'sweetalert';
import {inputTelValidate} from '../../_elements/inputTelValidate.js';
import {textareaAutoHeight} from '../../_elements/textareaAutoHeight.js';
import * as inputImage from '../../_elements/inputBasicImage.js';
import {textareaCouterLength} from '../../_elements/textareaCouterLength.js';
import {dynamicallyRowsAddress} from '../../_elements/dynamicallyRowsAddress.js';


function sectionInfo_toggle(e){
    e.preventDefault();
    $(this).parent().toggleClass('hide');
    $(this).text($(this).text() == 'Thu gọn' ? 'Chỉnh sửa' : 'Thu gọn');
}

$(function () {
    dashboardLayout();
    textareaCouterLength();
    inputTelValidate();
    textareaAutoHeight();
    inputImage.inputBasicImage();
    inputImage.inputBasicImageBig();
    $('.section-info_toggle').on('click', sectionInfo_toggle);

    $('.select2-noSearch').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Chọn số lượng nhân sự'
    });
    $('.select-2-field-activity').select2({
        placeholder: 'Chọn lĩnh vực hoạt động',
        maximumSelectionLength: 3,
        language: {
            maximumSelected: function (e) {
                return  "Bạn chỉ có thể lựa chọn tối đa " + e.maximum + " ngành nghề";
            }
        }
    });
    dynamicallyRowsAddress();

});
