@extends('layouts.admin')
@section('meta')
    <title>Admin - Candidate Detail</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/candidates/detail.css')}}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Ứng viên <strong>Võ Trần Hoài Thanh</strong>
                </h1>
            </div>
        </div>
    </div>
    <div class="dashboard-info-candidate">
        <div class="container">
            <div class="candidate-info_job-apply">
                <div class="mb-2">
                    <span class="job-apply-label">Vị trí ứng tuyển:</span>
                    <div class="job-item-tag">
                        <div class="tag-list">
                            <a href="#" class="tag-item" target="_blank">
                                .Net Deverloper
                            </a>
                        </div>
                    </div>
                    <div class="d-inline-block my-2"><span class="job-apply-label">tại</span><a href="#"><strong>Tinh Vân Oursourcing</strong></a></div>
                </div>
                <div class="mb-2">
                    <span class="job-apply-label">Thời gian ứng tuyển:</span><span>
                        01/01/2020
                    </span>
                </div>
            </div>
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Thông tin cá nhân
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="candidate-info">
                    <div class="candidate-info_avatar">
                        <img class="member-avatar" src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                    </div>
                    <div class="candidate-info_table">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="component-table-info">
                                    <tr>
                                        <td>
                                            Ứng viên:
                                        </td>
                                        <td>
                                            <span class="text-green-300">
                                                Võ Trần Hoài Thanh
                                            </span>
                                        </td>
                                    </tr>
                                    {{--                                    <tr>--}}
                                    {{--                                        <td>--}}
                                    {{--                                            Ngày sinh:--}}
                                    {{--                                        </td>--}}
                                    {{--                                        <td>--}}
                                    {{--                                            02/03/1994--}}
                                    {{--                                        </td>--}}
                                    {{--                                    </tr>--}}
                                    {{--                                    <tr>--}}
                                    {{--                                        <td>--}}
                                    {{--                                            Giới tính:--}}
                                    {{--                                        </td>--}}
                                    {{--                                        <td>--}}
                                    {{--                                            Nữ--}}
                                    {{--                                        </td>--}}
                                    {{--                                    </tr>--}}
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="component-table-info">
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>
                                            <em><a href="mailto:Votranhoaithanh@gmail.com">Votranhoaithanh@gmail.com</a></em>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Số điện thoại:
                                        </td>
                                        <td>
                                            <a href="tel:0912 345 6789">
                                                0912 345 6789
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        {{--                                        <td>--}}
                                        {{--                                            Địa chỉ:--}}
                                        {{--                                        </td>--}}
                                        {{--                                        <td>--}}
                                        {{--                                            Số 2, ngõ 82, Duy Tân, Cầu Giấy, Hà Nội--}}
                                        {{--                                        </td>--}}
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Hồ sơ ứng viên
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="candidate-show-file">
                    <iframe src="https://docs.google.com/gview?url=https://url...&embedded=true"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/admin/candidates/detail.js')}}"></script>
@stop
