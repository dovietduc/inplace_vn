<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = 'jjob_salaries';
    use BaseEloquent;
}
