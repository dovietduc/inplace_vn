<?php

namespace App\Components;


use App\NewsLike;
use App\SaveJob;
use App\UserFollowCustomer;

class Job
{
    public static function countUserFollowCustomer($customerId)
    {
        return UserFollowCustomer::whereCustomerId($customerId)
            ->where('status', false)
            ->count();

    }

    public static function jjobCheckSaveJob($idJob)
    {
        return SaveJob::whereUserIdAndJobId(auth()->id(), $idJob)->count();
    }

    public static function countNewsLike($newId)
    {
        return NewsLike::where([
            'news_id' => $newId,
            'deleted_flag' => false
        ])
            ->count();
    }


}
