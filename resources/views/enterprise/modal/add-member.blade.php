<div class="modal-add-member modal fade" id="addMember" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('enterprise.member.employee-invitations') }}">
                @csrf
                <input type="hidden" name="customer_id" value="{{ $customerCurrent->id }}">
                <div class="modal-header align-items-center">
                    <h5 class="modal-title">Thêm thành viên mới</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                            class="far fa-times-circle"></i></button>
                </div>
                <div class="modal-body">
                    <p>Bạn có thể thêm thành viên mới vào nhóm tuyển dụng của mình bằng cách gửi email mời.<br>Nhập địa
                        chỉ email của họ bên dưới và nhấp vào "Gửi email".</p>
                    <h4>Mời theo địa chỉ email</h4>
                    <textarea class="form-control"
                              rows="5"
                              name="employee_invitations"
                              placeholder="hovaten@gmail.com&#10;nickname@gmail.com&#10;nguoidung@gmail.com"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-custom">
                        <i class="far fa-envelope"></i> Gửi email
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
