@extends('html2.layout')
@section('content')
<div class="page-enterprise">
    @include('html2.enterprise._top_page')
    @yield('content_page')
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ mix('pages/company/company.css') }}">
@stop
