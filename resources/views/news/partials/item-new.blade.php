@foreach($news as $newItem)
    @php
        $customer = optional($newItem->customer);
    @endphp

    <div class="component-posts-item-large">

        <div class="posts-item-company justify-content-between">
            <div class="d-flex align-items-center">
                <a href="{{ route('companies.index', ['slug' => optional($newItem->customer)->slug]) }}" title="">
                    @if( jjobCheckImageExit(optional($newItem->customer)->logo) )
                        <img src="{{ url(optional($newItem->customer)->logo) }}"
                             alt="" class="avatar">
                    @else
                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" alt="" class="avatar">
                    @endif

                </a>
                <div class="name">
                    <h5>
                        <a href="{{ route('companies.index', ['slug' => optional($newItem->customer)->slug]) }}">
                            {{ optional($newItem->customer)->name }}
                        </a>
                    </h5>
                    <p>
                        <span class="label_count_user_follow"
                              data-count_follows="{{ jobHelper::countUserFollowCustomer(optional($newItem->customer)->id) }}">
                            {{ jobHelper::countUserFollowCustomer(optional($newItem->customer)->id) }} Fan
                        </span> •
                        <span>{{ $newItem->created_at->diffForHumans() }}</span>
                    </p>
                </div>
            </div>
            @if(auth()->check())
                @include('partials.follows-customer.news-list')
            @else
                <a href="{{ route('register') }}" class="btn-follow-customer">
                    <span>Trở thành fan</span>
                </a>
            @endif
        </div>

        <div class="posts-item-thumb">
            <a href="{{ route('news.show', ['slugCompany' => $customer->slug, 'id' => $newItem->id]) }}"
               title="">
                @if( jjobCheckImageExit($newItem->thumbnail) )
                    <img src="{{ asset($newItem->thumbnail) }}" alt="" class="rounded">
                @else
                    <img src="{{ asset('images/no_img_730x286.jpg') }}" alt="" class="rounded">
                @endif
            </a>
        </div>
        <h3 class="posts-item-title">
            <a href="{{ route('news.show', ['slugCompany' => $customer->slug, 'id' => $newItem->id]) }}" title="">
                {{ $newItem->title }}
            </a>
        </h3>
        <div class="posts-item-desc txt-format">
            {!! str_limit( strip_tags($newItem->content), $limit = 400, $end = '...') !!}
        </div>

        <div class="posts-item-auth">
            <div class="posts-item-auth_info">
                <a href="#" title="" class="auth-avatar">
                    @if( jjobCheckImageExit(optional($newItem->user)->avatar) )
                        <img src="{{ asset(optional($newItem->user)->avatar) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_user_30x30.jpg') }}" alt="">
                    @endif
                </a>
                <div class="auth-name">
                    <h3>
                        <a href="#" title="">
                            {{ optional($newItem->user)->name }}
                        </a>
                    </h3>
                    <span><i class="far fa-clock"></i>
                        {{ $newItem->created_at->diffForHumans() }}
                    </span>
                </div>
            </div>
            <div class="posts-item-auth_like">
                @if(auth()->check())
                    @include('partials.like-news.button-like-heart')
                @else
                    <a href="{{ route('register') }}"
                       class="btn-like-heart">
                        <i class="far fa-heart"></i>
                    </a>
                @endif
            </div>
        </div>

    </div>

@endforeach
