@extends('layouts.dashboard')
@section('meta')
    <title>Company Info Dashboard</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/info/company.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Xin chào <strong>
                        {{ !empty($customerCurrent) ? $customerCurrent->name : '' }}
                    </strong>
                    <span class="title-sub">Giới thiệu về doanh nghiệp của bạn với các ứng viên tiềm năng!</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="dashboard-info-company">
        <div class="container">
            <div class="section-info">
                <div class="component-title">
                    <h3 class="title">
                        Thông tin công ty
                    </h3>
                    <div class="desc"></div>
                </div>

                @include('enterprise.company.partials.basic-information-customer')
            </div>

            <div class="section-info">
                @include('enterprise.company.partials.image-feature-customer')
            </div>

            <div class="section-info">
                @include('enterprise.company.partials.what-is-customer')

            </div>

            <div class="section-info">
                @include('enterprise.company.partials.why-we-do')
            </div>
            <div class="section-info">
                @include('enterprise.company.partials.how-we-do')
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/info/company.js')}}"></script>
@stop
