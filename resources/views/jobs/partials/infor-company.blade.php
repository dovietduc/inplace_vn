<div class="job-detail_content__company">
    <div class="component-title">
        <h3 class="title">Thông tin công ty</h3>
        <div class="desc"></div>
    </div>
    <table class="component-table-info">
        <tr>
            <td>Lĩnh vực hoạt động:</td>
            <td>
                <span>
                    {{ $job->customerIndustry->implode('name', ', ') }}
                </span>
            </td>
        </tr>
        <tr>
            <td>Quy mô doanh nghiệp:</td>
            <td>
                @if(!empty($job->customer) && !empty($job->customer->companySize))
                    {{ $job->customer->companySize->size }}
                @endif
            </td>
        </tr>
        <tr>
            <td>Đặc tính văn hóa:</td>
            <td>
                <span class="tab">Trẻ trung</span>
                <span class="tab">Năng động</span>
                <span class="tab">Sáng tạo</span>
            </td>
        </tr>
        <tr>
            <td>Năm thành lập:</td>
            <td>{{ optional($job->customer)->founded_year }}</td>
        </tr>
        <tr>
            <td>Trụ sở chính:</td>
            <td>
                @if(!empty($job->customer) && !empty($job->customer->customerAddress))
                    @foreach($job->customer->customerAddress as $address)
                        @if($address->head_office)
                            {{ $address->address }}
                        @endif
                    @endforeach
                @endif
            </td>
        </tr>
    </table>
</div>
