<div class="txt-format">
    <h1>Chia sẻ nội dung</h1>
    <p>Người dùng tại Inplace có thể tự do kết nối và chia sẻ thông tin từ Inplace sang các nền tảng mạng xã hội khác như Linkedin hoặc Facebook.</p>
    <p>Tại mỗi bài viết, tin tức hay tin đăng tuyển v.v.. đều có các biểu tượng chia sẻ <img src="{{asset('images/img_button_chia_se.jpg')}}" alt="">
        <br>Ở đó, bạn có thể chọn nơi chia sẻ bài viết hay nội dung yêu thích qua Facebook hoặc Linkedin <img src="{{asset('images/img_icon_chia_se_mxh.jpg')}}" alt="">
        </p><p>Tính năng chia sẻ có tác dụng nâng cao hiệu quả của hoạt động branding cho người sử dụng, để số lượng người tiếp cận nội dung được cao hơn, tăng độ phủ của hoạt động truyền thông thương hiệu cá nhân hoặc thương hiệu nhà tuyển dụng. Ngoài ra, việc chia sẻ các nội dung hay, có ích cũng là một trong những cách đóng góp phát triển kho kiến thức chung và gia tăng tính kết nối cộng đồng. </p>
</div>
