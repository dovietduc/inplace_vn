@extends('layouts.dashboard')
@section('meta')
    <title>News List Dashboard</title>
@stop
@section('css')
{{--CSS Froala editor--}}
    @include('html.dashboard.partials._css-Froala')
{{--END: Froala Editor--}}
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/news/add.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Bài viết của <strong>J-JOB Executive Search</strong></h1>
            </div>
        </div>
    </div>
    <div class="dashboard-news-detail">
        <div class="container">
            <form action="" method="POST" class="formEditNewsDetail">
                @csrf
                <input type="hidden" value="" name="image_crop_data" class="image_crop_data d-none">
                <div class="dashboard-news-detail_head">
                    <div class="news-auth">
                        <a href="#" class="news-auth_avatar">
                            <img src="{{asset("images/no_user_35x35.jpg")}}" alt="">
                        </a>
                        <div class="news-auth_info">
                            <h5 class="news-auth_name"><a href="#">Võ Trần Hoài Thanh</a></h5>
                        </div>
                    </div>
                    <div class="news-category">
                        <select name="category" class="js-select2">
                            <option value="">Công ty</option>
                            <option value="">Văn hóa</option>
                            <option value="">Phỏng vấn ứng viên</option>
                            <option value="">WorkStyle</option>
                            <option value="">Chuyện nghề</option>
                            <option value="">Nhà tuyển dụng</option>
                            <option value="">Sự kiện</option>
                        </select>
                    </div>
                </div>
                <div class="dashboard-news-detail_box">
                    <div class="dashboard-news-detail_thumb">
                        @include('html.dashboard.partials.imageCroppie')
                    </div>
                    <div class="row px-3 px-md-0">
                        <div class="col-md-10 offset-md-1">
                            <div class="box-width">
                                <div class="dashboard-news-detail_title">
                                    <textarea class="autoheight disenter" name="title" placeholder="Tiêu đề"></textarea>
                                </div>
                                <div class="dashboard-news-detail_head">
                                    <div class="news-auth">
                                        <a href="#" class="news-auth_avatar">
                                            <img src="{{asset("images/no_user_35x35.jpg")}}" alt="">
                                        </a>
                                        <div class="news-auth_info">
                                            <h5 class="news-auth_name"><a href="#">Võ Trần Hoài Thanh</a></h5>
                                            <div class="news-auth_desc">Account Manager</div>
                                        </div>
                                    </div>
                                    <div class="news-time">
                                        <i class="far fa-clock"></i> Thứ Năm, 31 tháng 10, 2019
                                    </div>
                                </div>
                                <div class="dashboard-news-detail_content txt-format">
                                    <textarea class="inp-froala-editor" name="content" placeholder="Nhập nội dung bài viết"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-news-detail_submit">
                        <a href="/frontend/dashboard/news/list" class="btn btn-grey">Hủy</a>
                        <button type="submit" class="btn btn-custom_light">Lưu nháp</button>
                        <button type="submit" class="button-public-news btn btn-custom">Đăng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('enterprise.partials.popup-error')
@stop
@section('js')
    {{--JS Froala editor--}}
    @include('html.dashboard.partials._js-Froala')
    {{--END: Froala editor--}}
    <script src="{{asset('vendor/croppie/croppie.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/news/add.js')}}"></script>

@stop
