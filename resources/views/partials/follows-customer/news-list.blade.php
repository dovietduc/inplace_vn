@php
    $checkUserFollowCustomer = '';
    if (!empty($customer)) {
     $checkUserFollowCustomer = $customer->userFollow()->where([
            'user_id' => auth()->id(),
            'status' => false
        ])->count();
    }
@endphp
<button class="btn-follow-customer follow_customer_news_list"
        data-url="{{ route('companies.follow', ['id' => $customer->id]) }}">
    @if(!empty($checkUserFollowCustomer) )
        <span class="active">Đã là fan</span>
    @else
        <span>Trở thành fan</span>
    @endif
</button>
