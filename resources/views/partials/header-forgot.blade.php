<header class="header-forgot">
	<a href="{{ route('wellcome') }}" class="logo">
		<img src="{{ asset('images/logo_short.png') }}" alt="">
	</a>
	<ul class="menu clearfix">
		<li><a href="#"><span>Giới thiệu</span></a></li>
		<li><a href="#"><span>Tìm kiếm</span></a></li>
		<li><a href="#"><span>Góc doanh nghiệp</span></a></li>
	</ul>
	<div class="right">
		<div class="register ">
			<span class="txt">Bạn chưa có tài khoản? </span>
			@include('partials.button-register')
		</div>
	</div>
</header><!-- /header -->
