<div class="related-posts">
    <div class="component-title">
        <h3 class="title">
            Bài viết liên quan
        </h3>
        <div class="desc"></div>
    </div>
    <div class="row">
        @foreach($newsRelatest as $newRelatestItem)
            <div class="col-sm-6 col-md-4">
                <div class="component-posts-item">
                    <a href="{{ route('news.show', ['slugCompany' => $customer->slug, 'id' => $newRelatestItem->id]) }}" title="" class="posts-item-thumb">
                        @if( jjobCheckImageExit($newRelatestItem->thumbnail) )
                            <img src="{{ url($newRelatestItem->thumbnail) }}" alt=""/>
                        @else
                            <img src="{{ asset('images/no_img_350x137.jpg') }}" alt=""/>
                        @endif
                    </a>
                    <h3 class="posts-item-title">
                        <a href="{{ route('news.show', ['slugCompany' => $customer->slug, 'id' => $newRelatestItem->id]) }}" title="">
                            {{ $newRelatestItem->title }}
                        </a>
                    </h3>
                </div>
            </div>
        @endforeach

    </div>
</div>
