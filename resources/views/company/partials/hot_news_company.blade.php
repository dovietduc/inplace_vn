<div class="row">
    @foreach ($hotNews as $hotNewItem)
        <div class="col-sm-6 col-md-12">
            <div class="component-posts-item-small">
                <div class="posts-item-thumb">
                    <a href="{{ route('news.show', ['slugCompany' => optional($hotNewItem->customer)->slug, 'id' => $hotNewItem->id]) }}" title="{{ $hotNewItem->title }}">
                        @if( jjobCheckImageExit($hotNewItem->thumbnail) )
                            <img src="{{ url($hotNewItem->thumbnail) }}">
                        @else
                            <img src="{{ asset('images/no_img_320x125.jpg') }}">
                        @endif
                    </a>
                </div>
                <h3 class="posts-item-title">
                    <a href="{{ route('news.show', ['slugCompany' => optional($hotNewItem->customer)->slug, 'id' => $hotNewItem->id]) }}" title="{{ $hotNewItem->title }}">
                        {{ $hotNewItem->title }}
                    </a>
                </h3>
                <p class="posts-item-like">
                    {{ $hotNewItem->newLikes()->where([
                       'deleted_flag' => false
                    ])->count() }} Thích
                </p>
            </div>
        </div>
    @endforeach
</div>
