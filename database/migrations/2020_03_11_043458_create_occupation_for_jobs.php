<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccupationForJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jjob_jobs', function (Blueprint $table) {
            $table->integer('occupation_main')->nullable();
            $table->integer('occupation_supporting')->nullable();
            $table->string('occupation_supporting_detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jjob_jobs', function (Blueprint $table) {
            $table->dropColumn('occupation_main');
            $table->dropColumn('occupation_supporting');
            $table->dropColumn('occupation_supporting_detail');
        });
    }
}
