<?php

namespace App\Console\Commands;

use App\Customer;
use App\MemberCompany;
use Illuminate\Console\Command;
use DB;
use Log;

class ChangeDataCreateCompanyFromOldToNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:data-create-company-from-old-to-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change data create company from old to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            Customer::orderBy('id')->chunk(100, function ($customers) {
                foreach ($customers as $customer) {
                    MemberCompany::create([
                        'user_id' => $customer->user_id,
                        'customer_id' => $customer->id,
                        'active' => $customer->active
                    ]);
                }
            });
            DB::commit();
            $this->info('Convert news complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
