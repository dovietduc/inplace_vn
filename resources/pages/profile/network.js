function addFriend() {
    $(this).addClass('added').prop('disibale');
}
function refuseFriend() {
    $(this).parents('.network-item').remove();
}
$(function () {
    $(document).on('click','.btn-accept-friend',addFriend);
    $(document).on('click','.btn-refuse-friend',refuseFriend)
})
