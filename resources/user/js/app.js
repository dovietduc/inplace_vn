$(document).ready(function () {
    let screenWidth = $(window).width();
    
    $(window).scroll(function () {
        if ($(document).scrollTop() > 0 && screenWidth >= 700) {
            $('header').addClass('animated fadeInDown sticky-top');
        } else {
            $('header').removeClass('animated fadeInDown sticky-top');
        }
    });
    
    $('.js-slide-horizontal').owlCarousel({
        loop       : true,
        margin     : 30,
        nav        : true,
        dots       : false,
        responsive : {
            0    : {
                items : 1
            },
            600  : {
                items : 2
            },
            1000 : {
                items : 3
            }
        }
    });
    
    shortContentLink();
});


function shortContentLink() {
    $('.js-short-content').each(function () {
        let showChar     = $(this).data('msg-length'),
            moreText     = $(this).data('msg-more'),
            lessText     = $(this).data('msg-less'),
            ellipsesText = "...",
            content      = $(this).html();
        if (content.length <= showChar) {
            return true;
        }
        
        let c = content.substr(0, showChar),
            h = content.substr(showChar - 1, content.length - showChar);
        
        let html = c + '<span class="more-ellipses">' + ellipsesText + '</span>&nbsp;<span class="more-content"><span>' + h + '</span>&nbsp;&nbsp;<a' +
            ' href="javascript:;" class="js-more-link" data-mgs-less="' + lessText + '" data-mgs-more="' + moreText + '">' + moreText + '</a></span>';
        
        $(this).html(html);
    });
    
    
    $(".js-more-link").click(function () {
        let settingAttr = $(this).closest(".js-short-content");
        let moreText    = settingAttr.data('msg-more'),
            lessText    = settingAttr.data('msg-less');
        
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moreText);
        } else {
            $(this).addClass("less");
            $(this).html(lessText);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
}
