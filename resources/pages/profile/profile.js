import swal from 'sweetalert';

//check validate
function nameValid(event) {
    event.preventDefault();
    $(this).removeClass('error');
    $(this).parent().find('.err-name').text('');
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        $(this).parent().find('.err-name').text("Họ tên không được để trống!");
        $(this).focus();
    }
}

function telValid(event) {
    event.preventDefault();
    $(this).removeClass('error');
    $(this).parent().find('.err-tel').text('');
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        $(this).parent().find('.err-tel').text("Bạn chưa nhập số điện thoại!");
    }
    if (event.target.validity.patternMismatch) {
        $(this).addClass("error");
        $(this).parent().find('.err-tel').text("Vui lòng nhập đúng định dạng số điện thoại!");
    }
}

function frmProfileSubmit(event) {
    event.preventDefault();
    //...
    $('#popupEditProfile').modal('hide');
    $('#popupSaveSuccess').modal('show');
}

function frmContactInfoSubmit(event) {
    event.preventDefault();
    //...
    $('#popupEditContactInfo').modal('hide');
    $('#popupSaveSuccess').modal('show');
}

function resetForm() {
    var pr = $(this).find('form');
    pr[0].reset();
    pr.find('input.error').removeClass('error');
    pr.find('.err-inp').text('');
}

$(function () {
    $('.js-select2-multi').select2();
    $('.input-datetimepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        icons: {
            next: "far fa-chevron-right",
            previous: "far fa-chevron-left",
        },
    });
    //form edit profile and contact info popup
    $('.inp-tel').on('invalid', telValid);
    $('.inp-tel').on('change', telValid);
    $('.inp-name').on('invalid', nameValid);
    $('.inp-name').on('change', nameValid);


    $('.btn-open-popup-edit-contact-info').click(function (e) {
        e.preventDefault();
        $('#popupEditProfile').modal('hide');
        $('#popupEditContactInfo').modal('show');
    });
    $('.formEditProfile').on('submit', frmProfileSubmit);
    $('.formEditContactInfo').on('submit', frmContactInfoSubmit);
    $('.popup-edit-profile').on('hidden.bs.modal', resetForm);

});
//===============================================================================================================

// Start upload avatar image
let popupCropAvatar = $('#popupEditAvatar'),
    rawImg,
    inpAvatar = $('.inp-avatar'),
    avatarOutput = $('#profile-avatar-output');

let $uploadCropAvatar = $('#upload-demo-avatar').croppie({
    viewport: {
        width: 250,
        height: 250,
        type: 'circle'
    },
    enableExif: true,
    boundary: {height: 300 }
});
function validateFileImage(that) {
    let fileExtension = ['png', 'jpg', 'jpeg'];
    let fileExtentionCurrent = $(that).val().split('.').pop().toLowerCase();
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        swal({
            icon: "error",
            title: "Sai định dạng file!",
            text: "Vui lòng upload hình ảnh có định dạng *.png, *.jpg hoặc *.jpeg",
            button: {
                text: "Đóng",
                className: "btn-custom",
                closeModal: true,
            }
        });
        $(that).val('');
    }
}
function readFilePopup(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            popupCropAvatar.modal('show');
            rawImg = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
    } else {
        console.log("Sorry - you're browser doesn't support the FileReader API");
    };
}


function btnCropAvatar() {
    let url = $(this).data('url');
    $uploadCropAvatar.croppie('result', {
        type: 'canvas',
        format: 'png',
        size: {width: 320, height: 320},
        circle: false
    }).then(function (resp) {
        avatarOutput.attr('src', resp);
        popupCropAvatar.modal('hide');
        $.ajax({
            url: url,
            type: "POST",
            data: {"image": resp},
            success: function (data) {
                if (data.status) {
                    location.reload();
                }

            }
        });
    });
}

function showImageCrop() {
    $uploadCropAvatar.croppie('bind', {
        url: rawImg
    })
}
///
inpAvatar.on('change', function () {
    validateFileImage(this);
    readFilePopup(this);
});
popupCropAvatar.on('shown.bs.modal', showImageCrop);
popupCropAvatar.on('hidden.bs.modal', function () {
    inpAvatar.val('');
});
$('.btn-crop-avatar').on('click', btnCropAvatar);
// END: upload avatar image
//===============================================================================================================



// Start upload cover image
let $uploadCropCover,
    coverBlur = $('.profile-header-pages_cover .cover-blur'),
    coverImage = $('.cover-image_result img'),
    viewCoverWidth = coverImage.width(),
    viewCoverHeight = coverImage.height();
$uploadCropCover = $('#upload-demo-cover').croppie({
    viewport: {
        width: '100%',
        height: viewCoverWidth * 320 / 1110,
    },
    enableExif: true,
    boundary: {
        height: viewCoverHeight
    },
    mouseWheelZoom: false,
});
function coverImageOutput(result) {
    coverImage.attr('src', result);
    coverBlur.css('background-image', 'url("' + result + '")');
}
function readFileCover(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var image = new Image();
        reader.onload = function(e) {
            image.src = e.target.result;
            image.onload=function(){
                if(this.width < 600 || this.height < 250){
                    $('#popupErrorUploadImage').find('.modal-body .desc').text('Vui lòng chọn hình ảnh rộng ít nhất 600 pixel và cao ít nhất 250 pixel.')
                    $('#popupErrorUploadImage').modal('show');
                    $(input).val('');
                }else{
                    $uploadCropCover.croppie("bind", {
                        url: e.target.result
                    });
                    $uploadCropCover.find('.cr-viewport').append(
                        '<button class="btn-move-cover"><i class="far fa-arrows"></i> Kéo để di chuyển ảnh bìa</button>'
                    );
                    coverBlur.css('background-image', 'url("' + e.target.result + '")');
                    $('.cover-image_result').hide();
                    $('.cover-image_croppie').show();
                }
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function btnCropCover(){
    let url = $(this).data('url');
    $uploadCropCover.croppie('result', {
        type: 'canvas',
        format: 'png',
        size: {width: 1110, height: 320},
        circle: false
    }).then(function (resp) {
        coverImageOutput(resp);
        removeCropCover();
        $.ajax({
            url: url,
            type: "POST",
            data: {"image": resp},
            success: function (data) {
               location.reload();
            }
        });
    });
}
function removeCropCover(){
    $('.cover-image_result').show();
    $('.cover-image_croppie').hide();
    $('.inp-cover-image').val('');
    coverBlur.css('background-image', 'url("' + coverImage.attr('src') + '")');
}
///
coverBlur.css('background-image', 'url("' + coverImage.attr('src') + '")');
$('.inp-cover-image').on('change', function (){
    validateFileImage(this);
    readFileCover(this)
});
$('.btn-crop-cover').on('click', btnCropCover);
$('.btn-remove-crop-cover').on('click', removeCropCover);
// END: upload cover image


