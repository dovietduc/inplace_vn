<?php

namespace App\Http\Controllers;

use App\City;
use App\Customer;
use App\Industry;
use App\Job;
use App\LevelJob;
use App\MemberCompany;
use App\Occupation;
use App\TagsMainSideBar;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $city;
    private $industry;
    private $levelJob;
    private $job;
    private $customer;
    private $memberCompany;
    private $tagsMainSideBar;
    private $occupation;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(City $city, Industry $industry, LevelJob $levelJob,
                                Job $job, Customer $customer, MemberCompany $memberCompany,
                                TagsMainSideBar $tagsMainSideBar, Occupation $occupation)
    {
        $this->middleware(['verified']);
        $this->city = $city;
        $this->industry = $industry;
        $this->levelJob = $levelJob;
        $this->job = $job;
        $this->customer = $customer;
        $this->memberCompany = $memberCompany;
        $this->tagsMainSideBar = $tagsMainSideBar;
        $this->occupation = $occupation;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $jobs = $this->job->listJobFilterAjax($request->input());
        $tagsSideBar = $this->tagsMainSideBar->getAll();
        if ($request->ajax()) {
            $listJob = view('jobs.partials.list-job', compact('jobs'))->render();
            return response()->json([
                'list_job' => $listJob,
                'code' => 200
            ]);
        } else {
            session()->forget('customer_id');
            if (session('join_company_not_have_acount')) {
                return redirect(session('join_company_not_have_acount'));
            }
            $citys = $this->city->all();
            $occupations = $this->occupation->where('parent_id', 0)->get();
            $levelJobs = $this->levelJob->all();
            $JobsSliderHome = $this->job->where('slider_home', true)
                ->limit(10)->with('customer')->get();
            return view('home', compact('citys', 'occupations',
                'levelJobs', 'JobsSliderHome', 'jobs', 'tagsSideBar'));
        }
    }

    public function listJob(Request $request)
    {
        $jobs = $this->job->listJobFilterAjax($request->input());
        if ($request->ajax()) {
            $listJob = view('jobs.partials.list-job', compact('jobs'))->render();
            return response()->json([
                'list_job' => $listJob,
                'code' => 200
            ]);
        } else {
            session()->forget('customer_id');
            $citys = $this->city->all();
            $industrys = $this->industry->all();
            $levelJobs = $this->levelJob->all();
            $hotJobs = $this->job->whereHot(true)->limit(10)->with('customer')->get();

            return view('home', compact('citys', 'industrys',
                'levelJobs', 'hotJobs', 'jobs'));
        }
    }


}
