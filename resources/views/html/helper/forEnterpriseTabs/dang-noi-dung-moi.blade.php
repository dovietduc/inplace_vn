<div class="txt-format">
    <h1>Hướng dẫn Tạo bài viết mới</h1>
    <p>Để tạo bài viết, bạn vui lòng tạo tài khoản doanh nghiệp theo
        <a href="javascript:;" onclick="$('#tao-trang-doanh-nghiep-moi-tab').click();">Hướng dẫn tạo trang doanh nghiệp mới</a>.</p>
    <p>Sau khi tạo trang doanh nghiệp thành công, di chuyển đến trang doanh nghiệp mới
        và thao tác theo 1 trong 2 cách dưới đây để tạo bài viết mới:</p>
    <h3>Cách 1: Thao tác với mục bài viết trên thanh công cụ tại trang Profile Doanh
        Nghiệp của mình</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào mục <strong>Bài Viết</strong></li>
                <li><strong>Bước 2:</strong> Chọn <strong>Tạo Bài Viết Mới</strong>. Sau đó,
                    điền đầy đủ các nội dung liên quan đến bài viết như tiêu đề, nội dung,
                    mô tả tính chất bài viết
                    v.v..
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất bài viết bằng cách nhấp vào ô <strong>Đăng
                        Bài</strong>.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 2: Thao tác trực tiếp tại công cụ <strong>Quản Lý Bài Viết</strong> trong
        công cụ <strong>Admin</strong> của Doanh Nghiệp</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Truy cập vào công cụ Admin trang Doanh Nghiệp
                </li>
                <li><strong>Bước 2:</strong> Chọn tab Quản Lý <strong>Bài Viết</strong> ở
                    cột
                    menu bên tay trái
                </li>
                <li><strong>Bước 3:</strong> Click vào <strong>Tạo Bài Viết Mới</strong> ở
                    góc
                    phải màn hình
                </li>
            </ul>
        </div>
    </div>

    <blockquote>Việc xây dựng nội dung có chất lượng cao, hiểu được đối tượng mà bạn đang
        hướng
        tới cùng với lối viết tự nhiên là những kim chỉ nam giúp bạn tạo nên một bài
        viết EB hiệu quả. Bạn cũng đừng quên đính kèm các hình ảnh rõ nét, chất lượng
        cao để minh hoạ cho nội dung và &nbsp;mang lại hiệu ứng trong việc thu hút người
        xem nhé.
    </blockquote>
</div>
