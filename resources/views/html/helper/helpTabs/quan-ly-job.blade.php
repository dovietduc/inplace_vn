<div class="txt-format">
    <h1>Quản lý Job</h1>
    <h3>Quản lý việc làm - Với cá nhân</h3>
    <p>Để quản lí công việc đã ứng tuyển hay đang theo dõi, trước hết bạn cần đăng nhập vào tài khoản của mình:</p>
    <ul>
        <li><strong>Bước 1:</strong> Nhấp vào biểu tượng<strong> Tài khoản cá nhân</strong> ở góc trên bên phải trang
            chủ Inplace. <img src="{{asset('images/img_quan_ly_job_icon_user.jpg')}}" alt="">
        </li>
        <li><strong>Bước 2: </strong><br>Để theo dõi việc làm đã lưu, nhấp vào mục <strong>Công việc đã
                lưu.</strong><img
                src="{{asset('images/img_quan_ly_job_icon_cong_viec_da_luu.jpg')}}" alt="">
            <br>Để theo dõi việc làm đã ứng tuyển, nhấp vào mục <strong>Việc làm đã ứng tuyển.</strong></li>
        <img src="{{asset('images/img_quan_ly_job_icon_cong_viec_ung_tuyen.jpg')}}" alt="">
    </ul>
    <h3>Quản lý việc làm - Với doanh nghiệp</h3>
    <p>Doanh nghiệp sẽ có công cụ riêng để quản lý Job của ông ty mình. Công cụ Quản Lý Việc Làm tại trang Admin của
        Doanh Nghiệp:</p>
    <ul>
        <li><strong>Bước 1:</strong> Truy cập phần <strong>Admin</strong> của trang Doanh Nghiệp <img src="{{asset('images/img_quan_ly_job_icon_ten_cong_ty_cua_ban.jpg')}}" alt=""></li>
        <li><strong>Bước 2:</strong> Truy cập vào phần <strong>Quản Lý Việc Làm</strong> <img src="{{asset('images/img_quan_ly_job_icon_viec_lam.jpg')}}" alt=""></li>
        <li><strong>Bước 3:</strong> Thực hiện tuỳ chỉnh, Tạo mới, Sửa, Xoá, Xem số lượng hồ sư ứng tuyển, Ngưng đăng
            tuyển, Gắn Hot Job, Đẩy Slider v.v..
        </li>
    </ul>
</div>
