@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pages/companies/jobs/jobs.css') }}">
@stop
@section('content')
    <div class="page-enterprise">
        @include('company.partials.company-head-menu')
        <div class="jobs-enterprise-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 box_sidebar">
                        <div class="block-filter">
                            <div class="keyword">
                                <p class="title">Chủ đề/Từ khóa</p>
                                <ul class="keyword-list tag_content">
                                    @foreach($tags as $tag)
                                        <li class="item tag_content_detail">
                                            <a href="{{ route('companies.jobs.tag', ['slug' => $customer->slug, 'slug1' => $tag->slug]) }}"
                                               title=""
                                               class="font-weight-medium {{ !empty($jobTag) && $tag->id  == $jobTag->id ? 'active' : '' }}">
                                                {{ $tag->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 box_content">
                        @include('company.partials.job_list_company')
                    </div>
                </div>
            </div>
        </div>
        {{--    @include('html2.enterprise.partials.related_company')--}}
    </div>

@endsection

@section('js')
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('pages/companies/jobs/jobs.js') }}"></script>
@stop
