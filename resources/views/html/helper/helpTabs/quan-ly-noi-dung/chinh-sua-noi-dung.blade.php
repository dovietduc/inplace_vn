<div class="txt-format">
    <h1>Hướng dẫn Chỉnh sửa Nội dung - Trang Cá nhân</h1>
    <p>Với cá
        nhân, bạn có thể chỉnh sửa bài viết của bạn dễ dàng bằng cách sau:</p>
    <ul>
        <li>Tại Profile cá nhân, click vào phần “<strong>Bài Viết</strong>”</li>
        <li>Tại danh sách bài viết, chọn biểu tượng “<strong>Sửa</strong>” tại tiêu đề mỗi bài viết</li>
        <li>Sau khi chỉnh sửa các nội dung cần thiết, click vào “<strong>Lưu</strong>” &nbsp;thay đổi.</li>
    </ul>
    <p><br></p>
    <h1>Hướng dẫn Chỉnh sửa Nội dung - Trang Doanh nghiệp</h1>
        <p>Với
            trang doanh nghiệp, bạn có thể dùng công cụ Quản Lý Bài Viết trong phần Admin trang doanh nghiệp</p>
        <ul>
            <li>Truy cập vào phần Admin trang Doanh Nghiệp</li>
            <li>Chọn mục Quản Lý “<strong>Bài Viết</strong>” từ dãy menu bên tay trái màn hình</li>
            <li>Từ danh sách bài viết hiện ra, bạn có thể chọn biểu tượng “<strong>Chỉnh sửa</strong>” xuất hiện ở mỗi
                tiêu đề bài viết.
            </li>
            <li>Sau khi chỉnh sửa các nội dung cần thiết, click vào “<strong>Lưu</strong>”</li>
        </ul>
</div>
