<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $guarded = [];
    use BaseEloquent;

    public function occupationChildrent()
    {
        return $this->hasMany(Occupation::class, 'parent_id');
    }
}
