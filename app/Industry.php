<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $guarded = [];
    use BaseEloquent;
}
