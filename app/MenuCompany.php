<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class MenuCompany extends Model
{
    use BaseEloquent;
    protected $table = 'menu_enterprise';
    protected $guarded = [];
}
