<?php

namespace App\Http\Controllers;

use App\CandidateCv;
use App\Traits\StoreImageTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Validator;

class ProfileController extends Controller
{
    private $user;
    private $candidateCv;
    use StoreImageTrait;

    public function __construct(User $user, CandidateCv $candidateCv)
    {
        $this->user = $user;
        $this->candidateCv = $candidateCv;
    }

    public function index($id = null)
    {
        if (!empty($id)) {
            $user = $this->user->find($id);
        } else {
            $user = auth()->user();
        }
        return view('profile.index', compact('user'));
    }

    public function uploadImageAvatar(Request $request)
    {

        $dirUpload = $this->verifyAndStoreImageJson($request, 'image', 'profile');
        if ($dirUpload) {
            $this->user->find(auth()->id())->update([
                'avatar' => 'storage' . $dirUpload,
                'avatar_asset' => 'public' . $dirUpload
            ]);
            return response()->json([
                'status' => true
            ], 200);
        }
        return response()->json([
            'status' => false
        ], 200);

    }

    public function uploadImageCover(Request $request)
    {
        $dirUpload = $this->verifyAndStoreImageJson($request, 'image', 'profile-cover');
        if ($dirUpload) {
            $this->user->find(auth()->id())->update([
                'feature_image' => 'storage' . $dirUpload,
                'feature_image_asset' => 'public' . $dirUpload
            ]);
            return response()->json([
                'status' => true
            ], 200);
        }
        return response()->json([
            'status' => false
        ], 200);
    }

    public function applyJob(Request $request)
    {
        $messages = [
            'tel.required' => 'Số điện thoại là bắt buộc',
            'tel.max' => 'Số điện thoại tối đa 25 kí tự',
            'file_upload.file' => 'File upload không thể để trống',
            'file_upload.mimes' => 'File cv chỉ chấp nhận doc,docx,pdf,xls,xlsx',
            'file_upload.max' => 'Dung lượng file cv upload tối đa 2mb',
        ];
        $validator = Validator::make($request->all(), [
            'tel' => 'bail|required|max:25',
            'file_upload' => 'bail|file|mimes:pdf,doc,docx,xls,xlsx|max:2000'
        ], $messages);

        if ($validator->passes()) {
            // Insert apply job to database
            $dataInsert = $this->verifyAndStoreImage($request, 'file_upload', 'profile');
            if ($dataInsert) {
                $this->user->find(auth()->id())->update([
                    'mobile' => $request->tel,
                    'name' => $request->name
                ]);
                $dataInsert['user_id'] = auth()->id();
                $this->candidateCv->create($dataInsert);
                return response()->json([
                    'status' => 200
                ]);
            }

        } else {
            return response()->json([
                'responseJSON' => $validator->getMessageBag()->toArray(),
                'status' => 422
            ]);
        }

    }
}
