<div class="component-job-item-save">
    <a href="#" class="job-item-thumb">
        <img src="{{asset('images/no_img_350x137.jpg')}}" alt="">
    </a>
    <div class="job-item-content">
        <h3 class="title"><a href="">Tìm kiếm các PHP Developer làm việc cho các dự án tại Nhật</a></h3>
        <div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium asperiores aut dignissimos dolores ea eos et eum laudantium, molestias nam natus odio perferendis quas quia reprehenderit sed sequi tenetur. Nobis?</div>
    </div>
    <div class="job-item-company">
        <a class="avatar" href="#">
            <img src="{{asset('images/no_avatar_company_50x50.jpg')}}" alt="">
        </a>
        <h4 class="name">NTQ Solution</h4>
    </div>
    <button class="job-item_btn-delete"><i class="fal fa-times"></i></button>
</div>
