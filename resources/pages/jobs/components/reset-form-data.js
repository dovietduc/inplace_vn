import {showButtonResetForm} from "./show-button-reset-form";

function resetForm() {
    event.preventDefault();
    $('.filter-control__text').find('.txt').remove();
    $('.filter-control__text').find('span').removeClass('d-none');
    $('.selected-base').html('');
    $('.filter--occupation-detail').html('');
    $('.filter--occupation-supporting').removeClass('d-block');
    $('.filter--occupation-supporting ul').slideUp();
    $(this).removeClass('d-block');
    $('#search-complete').trigger("reset").submit();
}

export function resetFormData() {
    showButtonResetForm();
    $(document).on('click', 'button.reset-form-filter', resetForm);
}
