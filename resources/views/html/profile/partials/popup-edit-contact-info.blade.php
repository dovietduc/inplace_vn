<div id="popupEditContactInfo" class="popup-edit-profile modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chỉnh sửa Thông tin liên hệ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <form action="" method="POST" class="formEditContactInfo">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Profile URL</label>
                        <a href="inplace.com/in/vo-tran-hoai-thanh-924852171" target="_blank"><span
                                class="color-theme font-weight-bold">inplace.com/in/vo-tran-hoai-thanh-924852171</span></a>
                    </div>
                    <div class="form-group">
                        <label for="">Số điện thoại</label>
                        <input class="inp-tel form-control" type="tel" name=""
                               pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+" required>
                        <span class="err-tel err-inp"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input class="form-control" type="email" name="" value="votranhoaithanh@inplace.vn"
                               disabled="disabled">
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Ngày sinh</label>
                                <div class="input-datetimepicker input-group">
                                    <input type="text" class="form-control" placeholder="dd-MM-yyyy"/>
                                    <span class="input-group-addon">
                                             <i class="fad fa-calendar-alt"></i>
                                         </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Location</label>
                                <select class="js-select2-multi">
                                    <option>Select A</option>
                                    <option>Select B</option>
                                    <option>Select C</option>
                                    <option>Select D</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="">Địa chỉ</label>
                            <input class="form-control" type="tel" name="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-custom">Lưu thay đổi</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
