<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inplace - @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('pages/email/email-layout.css') }}">
    @yield('css')
</head>
<body>

<section class="inplace-email-layout">
    <header>
        <a href="https://inplace.vn/">
            <img src="https://inplace.vn/images/logo.png" alt="">
        </a>
        <span><a href="https://inplace.vn" target="_blank">WWW.INPLACE.VN</a></span>
    </header>
    <article>
        @yield('content')
        <p>Nếu có yêu cầu nào khác cần hỗ trợ, hãy liên hệ hotline: <strong style="font-size: 1.1em">028 6275 5586</strong> của Inplace hoặc
            email: <a href="mailto:support@inplace.vn">support@inplace.vn</a> nhé.
        </p>
    </article>
    <footer>
        <hr>
        <strong>INPLACE VIETNAM TEAM</strong> <br>
        Match values to place right people to right seats.
    </footer>
</section>
@yield('js')
</body>
</html>
