<div class="profile-homePage_box">
    <div class="component-title">
        <h3 class="title">
            Thông tin liên hệ
        </h3>
        <div class="desc"></div>
    </div>
{{--    <a href="#" class="view-more" data-toggle="modal" data-target="#popupEditContactInfo">--}}
{{--        <i class="fas fa-pencil-alt"></i>--}}
{{--        Chỉnh sửa--}}
{{--    </a>--}}
    <table class="component-table-info">
        <tbody>
        <tr>
            <td>Số điện thoại:</td>
            <td>{{ auth()->user()->mobile }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ auth()->user()->email }}</td>
        </tr>
        <tr>
            <td>Ngày sinh:</td>
            <td>{{ auth()->user()->birthday }}</td>
        </tr>
        <tr>
            <td>Địa chỉ:</td>
            <td>{{ auth()->user()->address }}</td>
        </tr>
        <tr>
            <td>Tỉnh/Thành phố:</td>
            <td>Hà Nội</td>
        </tr>
        </tbody>
    </table>
</div>
