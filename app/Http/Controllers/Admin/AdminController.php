<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function login()
    {
        $password = bcrypt('inplacE@19#7*uyhnd4879FGJNjsd');
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    {

        $checkLogin = [
            'email' => $request->email,
            'password' => $request->password,
            'type' => 'admin'
        ];
        if (auth()->attempt($checkLogin)) {
            // login success

            return redirect()->route('admin.home');
        }

    }


    public function home()
    {
        return view('admin.home');
    }

}
