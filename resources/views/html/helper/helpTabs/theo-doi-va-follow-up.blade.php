<div class="txt-format">
    <h1>Theo dõi và Follow up</h1>
    <p>Tại Inplace, các thành viên có thể kết nối với nhau qua tính năng <img src="{{asset('images/img_icon_ket_ban.jpg')}}" alt="">, khi đã trở thành bạn bè, họ có thể tương tác như gửi tin nhắn, like, share, comment các nội dung bài viết dễ dàng hơn.
    </p>
    <p>Ngoài ra, cá nhân có thể follow Doanh Nghiệp qua tính năng “Become a fan” - <img src="{{asset('images/img_icon_tro_thanh_fan.jpg')}}" alt=""> để có thể theo dõi công ty mình yêu thích thường xuyên hơn. Công ty có nhiều Fan sẽ là những doanh nghiệp có tầm ảnh hưởng trong cộng đồng Inplace, do đó những nội dung họ đăng tải sẽ được nhiều người chú ý và quan tâm.
    </p>
</div>
