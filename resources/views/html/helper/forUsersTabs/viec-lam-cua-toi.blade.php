<div class="txt-format">
    <h1>Hướng dẫn Quản lý việc làm</h1>
    <p>Để quản lí công việc đã ứng tuyển hay đang theo dõi, trước hết bạn cần đăng
        nhập vào tài khoản của mình:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_viec_lam_cua_toi.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào biểu tượng tài khoản cá nhân ở góc trên bên phải trang chủ Inplace.</li>
                <li><strong>Bước 2:</strong> Để theo dõi việc làm đã lưu, nhấp vào mục Công việc đã lưu, để theo dõi việc làm đã ứng tuyển, nhấp vào mục Công viêc đã
                    ứng tuyển</li>
            </ul>
        </div>
    </div>

</div>
