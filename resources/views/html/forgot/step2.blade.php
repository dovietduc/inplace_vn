<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forgot step 2</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/forgot/forgot.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('html/element/header-signin')
	@endcomponent

	<div class="forgot">
		<div class="holder">
			<div class="sologan">
				<h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
				<div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
			</div><!-- sologan -->

			<div class="form">
				<div class="title text-center">Kiểm tra email ngay nào!</div>

				<div class="mgs">
					<div class="icon"><i class="far fa-check-circle"></i></div>
					Chúng tôi đã gửi hướng dẫn đặt lại mật khẩu vào<br>email của bạn. Hẹn gặp bạn bên trong nhé.
				</div>

				<div class="resend">
					<p>Bạn chưa nhận được email?</p>
					<button>Gửi lại email</button>
				</div>

				<div class="regiter text-center">Bạn chưa có tài khoản? <a href="#">Đăng ký ngay</a></div>
			</div><!-- form -->
		</div>
	</div><!-- .forgot -->
	
	@component('html/element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/common/main.js"></script>
	<script src="/pages/forgot/forgot.js"></script>
</body>
</html>