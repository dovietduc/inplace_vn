<div class="col-lg-4 box_sidebar">
    <div class="around">
        <h4 class="title">Box 360</h4>
        <ul>
            <li>
                <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}"><i
                        class="far fa-building"></i>
                    Company Home
                </a>
            </li>
            <li>
                <a href="{{ route('companies.news', ['slug' => $customer->slug]) }}">
                    <i class="far fa-newspaper"></i>
                    Bài viết
                </a>
            </li>
            <li>
                <a href="{{ route('companies.media', ['slug' => $customer->slug]) }}">
                    <i class="far fa-images"></i>
                    Gallery
                </a>
            </li>
            <li>
                <a href="{{ route('companies.media', ['slug' => $customer->slug]) }}">
                    <i class="fas fa-video"></i>Video
                </a>
            </li>
            <li>
                <a href="{{ route('companies.media', ['slug' => $customer->slug]) }}">
                    <i class="fas fa-cube"></i>
                    Thăm văn phòng ảo
                </a>
            </li>
            <li>
                <a href="{{ route('companies.media', ['slug' => $customer->slug]) }}">
                    <i class="fas fa-info-circle"></i>
                    Q&A
                </a>
            </li>
            <li>
                <a href="{{ route('companies.media', ['slug' => $customer->slug]) }}">
                    <i class="far fa-question-circle"></i>
                    Quiz
                </a>
            </li>
        </ul>
    </div>
    <div class="around">
        <h4 class="title">Bản đồ</h4>
        @if($customer->place_location)
            <div class="map">
                {!!  $customer->place_location !!}
            </div>
        @endif

    </div>
    <div class="around">
        <h3>Gia nhập {{ $customer->name }} ngay hôm nay để trở
            thành {{ $job->short_title }}</h3>
        <ul>
            <li>
                <span href="">
                    <i class="far fa-tag"></i>
                    {{ !empty($job->jobLevel) ? optional($job->jobLevel)->name : '' }}
                </span>
            </li>
            <li>
                <span href=""><i class="far fa-id-card"></i>
                    {{ $job->customerIndustry->implode('name', ', ') }}
                </span>
            </li>
            <li>
                <span href="">
                    <i class="fas fa-map-marker-alt"></i>
                    {{ $job->citys->implode('name', ', ') }}
                </span>
            </li>
            <li>
                <span href="">
                    <i class="far fa-comment-dots"></i>
                    Tiếng Anh
                    <small>
                        {{ optional($job->levelEnglish)->name }}
                    </small>
{{--                    <br>Tiếng nhật--}}
                    {{--                    <small>(N3)</small>--}}
                </span></li>
            <li>
                <span href="">
                    <i class="far fa-credit-card"></i>
                    {{ !empty($job->salary) ? optional($job->salary)->name : '' }}
                </span>
            </li>
        </ul>
    </div>
    <div class="around other">
        <h3>Vị trí khác công ty đang tuyển dụng</h3>
        @foreach($jobsRelative as $jobRelativeItem)
            <div class="item">
                <div class="thumb">
                    <a href="{{ route('jobs.show', ['slug' => $jobRelativeItem->slug]) }}">
                        @if( jjobCheckImageExit($jobRelativeItem->feature_image) )
                            <img src="{{ asset($jobRelativeItem->feature_image) }}" alt="" class="">
                        @else
                            <img src="{{ asset('images/no_img_50x50.jpg') }}" alt="" class="">
                        @endif
                    </a>
                </div>
                <div class="content">
                    <div class="item_date">
                        {{ Carbon::createFromFormat('Y-m-d H:i:s', $jobRelativeItem->updated_at)->format('d-m-Y') }}
                    </div>
                    <h4 class="item_title">
                        <a href="{{ route('jobs.show', ['slug' => $jobRelativeItem->slug]) }}">
                            {{ $jobRelativeItem->title }}
                        </a>
                    </h4>
                    <div class="item_address">
                        {{ $jobRelativeItem->citys->implode('name', ', ') }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
