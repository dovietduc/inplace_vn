let jme_memberFilter = $('.job-members-editor_filter');
let jme_memberList = $('.job-members-editor_result');
let jme_memberContentList = $('.job-members-editor_content');

function jobMemberAdd_append() {
    $('.job-members-editor_add').append('<div class="job-members-editor_add_item">\n' +
        '    <i class="fad fa-user-circle"></i>\n' +
        '</div>');
}

function jobMemberAdd_click() {
    jme_memberFilter.toggle();
}


function getMemberOrigin() {
    let jobMemberNumber = 4;
    let memberOriginNumber =  jme_memberList.children().length;
    let memberAddOriginNumber = jobMemberNumber - memberOriginNumber;
    if (memberAddOriginNumber > 0){
        for (var i = 0; i<memberAddOriginNumber; i++){
            jobMemberAdd_append();
        }
    };
    if($('.job-members-editor_result_item').length){
        $('.job-members-editor_result_item').each(function () {
            let idMember = $(this).find('input[type="hidden"]').val();
            $('.job-members-editor_filter_member[data-idmember="'+idMember+'"]').addClass('d-none');

        })
    }
}

function jobMemberHover() {
    jme_memberFilter.hide();
    $('.job-members-editor_add_item:first-child').removeClass('hover');
    $('.job-members-editor_result_item').removeClass('active');
    $(this).addClass('active');
    $('.job-members-editor_content .content').hide();
    $('.job-members-editor_content .content[data-contentmember="'+ $(this).find('input[type="hidden"]').val() +'"]').show();
}


function jobMemberAddResult() {
    let that = $(this);
    let memberAdd = {
        id:  that.attr('data-idmember'),
        avatar: that.find('.member-avatar').attr('src'),
        name: that.find('.name').text(),
        station: that.find('.station').text(),
        desc: that.find('.desc').html(),
    };
    let memberResultNumber =  jme_memberList.children().length;
    if (memberResultNumber < 4){
        $('.job-members-editor_add_item:last-child').remove();
        jme_memberList.append('<div class="job-members-editor_result_item">\n' +
            '<input type="hidden" name="members_job[]" value="'+ memberAdd.id +'">\n' +
            '<img class="member-avatar" src="'+ memberAdd.avatar +'" alt="">\n' +
            '<i class="btn-delete fal fa-times"></i>\n' +
            '</div>');
        jme_memberContentList.append('<div class="job-members-editor_content">\n' +
            '    <div class="content" data-contentmember="'+ memberAdd.id +'">\n' +
            '        <h5 class="name">'+ memberAdd.name +'</h5>\n' +
            '        <p class="station">'+ memberAdd.station +'</p>\n' +
            '        <div class="desc">'+ memberAdd.desc +'</div>\n' +
            '    </div>\n' +
            '</div>');
        that.addClass('d-none');
    };
    if (memberResultNumber === 3){
        jme_memberFilter.hide();
    };
}

function jobMemberDelete() {
    let idMemberDel = $(this).parent().find('input[type="hidden"]').val();
    $(this).parent().remove();
    jobMemberAdd_append();
    $('.job-members-editor_filter_member[data-idmember="'+idMemberDel+'"]').removeClass('d-none');
    $('.job-members-editor_content [data-contentmember="'+idMemberDel+'"]').remove();
}

function removeAccents(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}

function jobMemberFilterSearch(e) {
    let input = $(this);
    let filter = removeAccents(input.val().toUpperCase());
    let li = $('.job-members-editor_filter_member');

    li.each(function () {
        let a = $(this).find('.findname');
        let txtValue = removeAccents(a.text().toUpperCase());
        if (txtValue.indexOf(filter) <= -1) {
            $(this).hide();
        }else {
            $(this).removeAttr('style');
        }
    });

}


export function addMembersToJob() {

    getMemberOrigin();
    $(document).on('mouseover', '.job-members-editor_result_item', jobMemberHover);

    $(document).on('click','.job-members-editor_add_item:first-child', jobMemberAdd_click);
    $(document).on('click','.job-members-editor_result_item .btn-delete',jobMemberDelete);
    $(document).on('click', '.job-members-editor_filter_member', jobMemberAddResult);
    $(document).on('keyup', '#job-members-editor_filter_input', jobMemberFilterSearch);
}
