@extends('layouts.app')
@section('meta')
    <title>Giới thiệu</title>
@stop
@section('css')
    <link
        href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/aboutUs.css')}}">
@stop
@section('content')
    <div class="aboutUs-page">
        <section class="aboutUs-welcome">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Hãy đến với INPLACE <br>
                            để trải nghiệm những tính năng <br>
                            vô cùng Ưu việt – Độc đáo – Mới lạ!</h1>

                    </div>
                    <div class="col-lg-5">
                        <div class="txt-format mb-4">
                            <ul>
                                <li><strong>Bạn là người tìm việc?</strong> Một công việc khiến bạn thực sự cảm thấy
                                    hạnh phúc và gắn bó lâu dài?
                                </li>
                                <li><strong>Bạn là doanh nghiệp</strong> đang có nhu phát triển thương hiệu nhà tuyển
                                    dụng nhằm thu hút và gìn giữ nhân tài cho doanh nghiệp đồng thời muốn tìm một giải
                                    pháp công nghệ hỗ trợ quá trình tuyển dụng của mình?
                                </li>
                                <li><strong>Đến với INPLACE</strong> để tìm đúng người và đưa về đúng chỗ!</li>
                            </ul>
                        </div>
                        <a href="www.inplace.vn" class="btn btn-custom">THAM GIA INPLACE</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="aboutUs-introduce">
            <div class="container">
                <h2 class="aboutUs-title">Về Inplace</h2>
                <div class="row mb-5">
                    <div class="col-md-8 col-lg-7">
                        <div class="txt-format">
                            <strong>INPLACE</strong> là một nền tảng công nghệ cho phép người
                            dùng là doanh nghiệp hay người tìm việc được kết nối và tương tác sâu trên góc độ
                            văn hóa. Kết quả của sự tương tác này là người dùng sẽ tìm và được tự động kết nối
                            với những đối tượng tương thích với mình hay nhóm của mình không những ở góc độ
                            chuyên môn mà còn ở góc độ văn hóa và môi trường làm việc.</p>
                            <p>Website <a href="http://www.inplace.vn" target="_blank" rel="noopener noreferrer">www.inplace.vn</a>
                                sở hữu bởi INPLACE CO.,LTD - thành viên thuộc J-Job Executive Search, công ty có hơn
                                6 năm kinh nghiệm trên thị trường trong lĩnh vực tư vấn tuyển dụng nhân sự cấp cao.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 offset-lg-1">
                        <img src="{{asset('images/img_aboutus_logo.png')}}" alt="">
                    </div>
                </div>

            </div>
            <div class="aboutUs-introduce-brand">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 offset-lg-5 bg-white">
                            <div class="txt-format">
                                <p>Trong bối cảnh công nghệ 4.0 và sự thay đổi về thói quen và nhu
                                    cầu tìm kiếm việc làm trong thị trường nhân lực trẻ, nhân sự chất lượng cao ở các
                                    ngành nghề năng động như Công nghệ, Công nghiệp, Dịch vụ.. các nhà tuyển dụng phải
                                    đối mặt với thách thức từ cuộc cạnh tranh “thu hút nhân tài” (Talent Acquisition)
                                    đang ngày càng gay gắt.&nbsp;</p>
                                <p><strong>INPLACE</strong> ra đời với mong muốn là một giải pháp hữu hiệu cho Doanh
                                    nghiệp xây dựng Thương Hiệu Nhà tuyển dụng và giúp quá trình tuyển dụng được tự động
                                    hoá trong một quy trình khép kín. Tại <a href="http://www.inplace.vn"
                                                                             target="_blank" rel="noopener noreferrer">www.inplace.vn</a>,
                                    doanh nghiệp có thể liên tục chia sẻ và thu hút các Ứng viên tiềm năng thông qua
                                    những Câu chuyện Doanh nghiệp, Văn hóa và Con người tại Công ty với mục đích thu hút
                                    được những Ứng viên tài năng và phù hợp nhất với Văn hóa của Công ty, với một chi
                                    phí hết sức hợp lý.</p>
                                <p><strong>INPLACE</strong> nỗ lực trong sứ mệnh hỗ trợ Doanh nghiệp tuyển dụng được
                                    những nhân sự phù hợp về văn hóa của Doanh nghiệp. Chúng tôi mong muốn kết nối Ứng
                                    viên và Nhà tuyển dụng không chỉ dựa trên các yếu tố như Thu nhập và Chế độ, mà dựa
                                    trên sự phù hợp và đồng cảm về <strong>Sứ mệnh - Tầm nhìn</strong> và <strong>Văn
                                        hóa</strong> của Doanh nghiệp.</p>
                                <p><a href="http://www.inplace.vn" target="_blank" rel="noopener noreferrer">www.inplace.vn</a>
                                    tiền thân là <a href="http://www.timcongsu.vn" target="_blank"
                                                    rel="noopener noreferrer">www.timcongsu.vn</a> ra đời vào năm 2017.
                                    Trải qua hơn 1 năm thử nghiệm và tự hoàn thiện, <strong>INPLACE</strong> làm nên một
                                    cú chuyển mình ngoạn mục với diện mạo mới vô cùng thân thiện, đem đến những tính
                                    năng tiện ích tuyệt vời cho người dùng cá nhân cũng như doanh nghiệp.</p>
                                <p><strong>INPLACE</strong> sẽ không ngừng cải tiến, nâng cao chất lượng dịch vụ và hoàn
                                    thiện sản phẩm để người dùng có được những trải nghiệm tuyệt vời nhất.
                                    <strong>J-JOB</strong> và <strong>INPLACE</strong> sẽ cùng đồng hành vì mục tiêu
                                    luôn thỏa mãn nhu cầu của khách hàng một cách tối đa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="aboutUs-utilities">
            <div class="container">
                <h2 class="aboutUs-title">Tiện ích mà INPLACE mang lại</h2>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="aboutUs-utilities-box">
                            <h3 class="title"><img src="{{asset('images/icon-employer.png')}}" alt=""><span>Đối với cá nhân</span>
                            </h3>
                            <div class="txt-format">
                                <ul>
                                    <li>Công cụ làm Personal Branding chuyên nghiệp</li>
                                    <li>Dễ dàng kết nối và tìm hiểu văn hoá doanh nghiệp mình quan tâm</li>
                                    <li>Tham gia và đóng góp xây dựng cộng đồng.</li>
                                    <li>Tìm kiếm “đúng chỗ” – tức là một công việc phù hợp không những về chuyên môn,
                                        tính
                                        cách mà còn định hướng cá nhân của mình
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="aboutUs-utilities-box">
                            <h3 class="title"><img src="{{asset('images/icon-building.png')}}" alt="">Đối với doanh
                                nghiệp</h3>
                            <div class="txt-format">
                                <ul>
                                    <li>Có Không gian và công cụ để xây dựng thương hiệu nhà tuyển dụng hiệu quả.</li>
                                    <li>Kết nối nhanh chóng với ứng viên phù hợp về mọi mặt với doanh nghiệp mình.</li>
                                    <li>Cơ hội thể hiện chiều sâu về văn hóa doanh nghiệp.</li>
                                    <li>Thực hiện quy trình tuyển dụng khép kín online</li>
                                    <li>Tối ưu hoá chi phí tuyển dụng.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/aboutUs.js')}}"></script>
@stop
