@extends('layouts.dashboard')
@section('meta')
    <title>Get started</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/enterprise/home/home.css')}}">
@stop
@section('content')
    <div class="dashboard-home-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                    <div class="dashboard-home-page_title">
                        <h1 class="title">Hãy bắt đầu nhé!</h1>
                        <a href="#" class="btn btn-custom"><i class="far fa-plus"></i> Đăng tuyển dụng</a>
                    </div>
                    <ul class="dashboard-home-page_list-step">
                        <li>
                            <div class="step-title">
                                <h3>Tạo tài khoản</h3>
                            </div>
                            <a href="" class="step-btn">Chỉnh sửa</a>
                        </li>
                        <li class="active">
                            <div class="step-title">
                                <h3>Viết câu chuyện của công ty bạn</h3>
                                <span>Share the What, Why, and How behind your company. <a href="">Learn more</a></span>
                            </div>
                            <a href="" class="step-btn">Thêm</a>
                        </li>
                        <li>
                            <div class="step-title">
                                <h3>Các thành viên công ty của bạn đã tham gia!</h3>
                                <span>You and 1 more members are in. You can invite more at any time. <a href="">Learn more</a></span>
                            </div>
                            <a href="" class="step-btn">Mời thêm</a>
                        </li>
                        <li>
                            <div class="step-title">
                                <h3>Bài viết công việc đầu tiên của bạn được xuất bản!</h3>
                                <span>You're on your way to finding your future team member! <a
                                        href="">Learn more</a></span>
                            </div>
                            <a href="" class="step-btn">Chỉnh sửa</a>
                        </li>
                        <li>
                            <div class="step-title">
                                <h3>Khán giả của bạn đang tăng lên!</h3>
                                <span>Kiểm tra bài viết công việc của bạn đang làm như thế nào. <a
                                        href="">Tìm hiểu thêm</a></span>
                            </div>
                            <a href="" class="step-btn">Xem thống kê</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/enterprise/home/home.js')}}"></script>
@stop
