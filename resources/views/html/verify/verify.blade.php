<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verify email</title>
    <link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/pages/verify/verify.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

@component('html/element/header-forgot')
@endcomponent

<div class="verify-content">
    <h3 class="title">Chào mừng bạn</h3>
    <p class="txt">Vui lòng nhập địa chỉ email của bạn để xác thực.</p>

    <form class="form">
        <input type="text" class="form-control error" value="" placeholder="Nhập email của bạn">

        <p class="form-alert"><i class="fas fa-exclamation-circle"></i>Email này đã được sử dụng</p>

        <button class="btn-submit" type="submit">Gửi</button>
    </form>
</div>

@component('html/element/footer')
@endcomponent

<script src="/pages/common/main.js"></script>
</body>
</html>