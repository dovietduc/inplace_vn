<?php

namespace App\Http\Controllers\Enterprise;

use App\City;
use App\CustomerIndustry;
use App\EmailApplyJobs;
use App\Events\CreatedCompany;
use App\Industry;
use App\Job;
use App\JobAdmin;
use App\JobWorkTypes;
use App\LanguageLevelCommon;
use App\LevelJob;
use App\MemberCompany;
use App\Occupation;
use App\PositionOccupation;
use App\Salary;
use App\Scopes\JobPublicScope;
use App\TagsJob;
use App\TagsOfJob;
use App\Traits\StoreImageTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class JobController extends Controller
{
    use StoreImageTrait;
    private $memberCompany;
    private $levelJob;
    private $jobWorkTypes;
    private $city;
    private $industry;
    private $languageLevelCommon;
    private $salary;
    private $job;
    private $tagsOfJob;
    private $emailApplyJobs;
    private $occupation;
    private $positionOccupation;
    public function __construct(MemberCompany $memberCompany, LevelJob $levelJob,
                                JobWorkTypes $jobWorkTypes, City $city,
                                Industry $industry, LanguageLevelCommon $languageLevelCommon,
                                Salary $salary, JobAdmin $job, TagsOfJob $tagsOfJob, EmailApplyJobs $emailApplyJobs,
                                Occupation $occupation, PositionOccupation $positionOccupation)
    {
        $this->memberCompany = $memberCompany;
        $this->levelJob = $levelJob;
        $this->jobWorkTypes = $jobWorkTypes;
        $this->city = $city;
        $this->industry = $industry;
        $this->languageLevelCommon = $languageLevelCommon;
        $this->salary = $salary;
        $this->job = $job;
        $this->tagsOfJob = $tagsOfJob;
        $this->emailApplyJobs = $emailApplyJobs;
        $this->occupation = $occupation;
        $this->positionOccupation = $positionOccupation;

    }

    public function list(Request $request)
    {
        $customerId = session('customer_id');
        $jobs = $this->job->getManyWhereOrderPagination([
            'customer_id' =>$customerId
        ]);
        if (!empty($request->status_jobs)) {
            $jobs = $this->job->getManyWhereOrderPagination([
                'customer_id' => $customerId,
                'status' => $request->status_jobs
            ]);
        }
        return view('enterprise.jobs.list', compact('jobs'));
    }

    public function create()
    {
        $customerId = session('customer_id');
        $listUsersFollowCustomer = $this->memberCompany->getByMultiConditionsModel(['customer_id' => $customerId]);
        $levelsJob = $this->levelJob->orderBy('order_id', 'asc')->get();
        $typesWork = $this->jobWorkTypes->getAll();
        $citys = $this->city->latest()->get();
        $occupations = $this->occupation->where('parent_id', 0)->get();
        $englishLevels = $this->languageLevelCommon->getAll();
        $salarysOffer = $this->salary->getAll();

        return view('enterprise.jobs.add', compact('listUsersFollowCustomer', 'levelsJob',
            'typesWork', 'citys', 'occupations', 'englishLevels', 'salarysOffer'));
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $dirUpload = $this->verifyAndStoreImageJson($request, 'image_crop_data', 'jobs');
            $dataJob = [
                'title' => $request->title,
                'short_title' => $request->occupation_supporting_detail,
                'description' => $request->description,
                'job_level_id' => $request->job_level_id,
//                'work_types_id' => $request->work_types_id,
                'english_level_id' => $request->english_level_id,
                'salary_id' => $request->salary_id,
                'user_id' => auth()->id(),
                'slug' => str_slug($request->title),
                'customer_id' => session('customer_id'),
                'occupation_main' => $request->occupation_main,
                'occupation_supporting' => $request->occupation_supporting,
                'status' => $request->type_submit == Job::statusPublic ? Job::statusPublic : Job::statusDraft
            ];
            if (!empty($dirUpload)) {
                $dataJob['feature_image'] = 'storage' . $dirUpload;
            }
            $salaryChoose = $this->salary->findById($request->salary_id);
            if (!empty($salaryChoose) && !empty($salaryChoose->show_form_salary)) {
                $dataJob['salary_min'] = $request->salary_min;
                $dataJob['salary_max'] = $request->salary_max;
            }
            // Add position occupation
            if (!empty($request->occupation_supporting_detail)) {
                $positionOccupationInstance = $this->positionOccupation->firstOrCreate([
                    'name' => $request->occupation_supporting_detail
                ]);
                if ($positionOccupationInstance) {
                    $dataJob['occupation_supporting_detail'] = $positionOccupationInstance->id;
                }

            }
            // Insert to jjob_jobs table
            $job = $this->job->createData($dataJob);
            // Insert city to jobs
            if(!empty($request->citys)) {
                $job->citys()->attach($request->citys);
            }
            // Insert industry to jobs
//            if(!empty($request->industrys)) {
//                $job->customerIndustry()->attach($request->industrys);
//            }
            // Add tags to table
//            $this->InsertOrUpdateTags($request, $job);

            // add member_company to job
            if(!empty($request->members_job)) {
                $job->membersCompany()->attach($request->members_job);
            }
            // Create email apply jobs
            if ( !empty(request()->email) ) {
                $job->emailApplyJobs()->create(['email' => request()->email]);
            }

            event(new CreatedCompany($job));
            DB::commit();
            return redirect()->route('enterprise.jobs.list')->with('message',
                'Bạn vừa đăng một Công việc mới! Nội dung đã đăng của bạn sẽ được xét duyệt trong vòng 24h hoặc sớm hơn');
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error('Xảy ra lỗi trong quá trình insert: ' . $exception->getMessage() . 'Line: ' . $exception->getLine());
        }
    }

    public function InsertOrUpdateTags($request, $job)
    {
        if ( !empty($request->tags) && count($request->tags) > 0 ) {
            $tagIds = [];
            foreach ($request->tags as $tagItem) {
                $tag =  $this->tagsOfJob->firstOrCreate([
                    'name' => $tagItem
                ]);
                if($tag)
                {
                    $tagIds[] = $tag->id;
                }
            }
            $job->tagsJob()->sync($tagIds);
        }
    }

    public function searchTags(Request $request) {
        $search = $request->search;
        if($search == ''){
            $tags = $this->tagsOfJob->getManyWhereOrderLimit([], 20, ['id', 'name'],'name asc');
        }else{
            $tags = $this->tagsOfJob->getManyWhereOrderLimit([], 20, ['id', 'name'],'name asc');
        }
        return response()->json($tags);
    }

    public function edit(Request $request, $id)
    {
        $customerId = session('customer_id');
        $listUsersFollowCustomer = $this->memberCompany->getByMultiConditionsModel(['customer_id' => $customerId]);
        $levelsJob = $this->levelJob->orderBy('order_id', 'asc')->get();
        $typesWork = $this->jobWorkTypes->getAll();
        $citys = $this->city->latest()->get();
        $occupations = $this->occupation->where('parent_id', 0)->get();
        $englishLevels = $this->languageLevelCommon->getAll();
        $salarysOffer = $this->salary->getAll();
        $job = $this->job->findById($id);
        $citySelectedJob = $job->citys()->pluck('jjob_cities.id');
        $industrySelectedJob = $job->customerIndustry()->pluck('industries.id');


        return view('enterprise.jobs.edit', compact('listUsersFollowCustomer', 'levelsJob',
            'typesWork', 'citys', 'occupations', 'englishLevels',
            'salarysOffer', 'job', 'citySelectedJob', 'industrySelectedJob'));
    }

    public function postEdit(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $job = $this->job->findById($id);
            $dirUpload = $this->verifyAndStoreImageJson($request, 'image_crop_data', 'jobs');
            $dataJob = [
                'title' => $request->title,
                'short_title' => $request->occupation_supporting_detail,
                'description' => $request->description,
                'job_level_id' => $request->job_level_id,
                'work_types_id' => $request->work_types_id,
                'english_level_id' => $request->english_level_id,
                'salary_id' => $request->salary_id,
                'user_id' => auth()->id(),
                'customer_id' => session('customer_id'),
                'occupation_main' => $request->occupation_main,
                'occupation_supporting' => $request->occupation_supporting,
                'status' => $request->type_submit == Job::statusPublic ? Job::statusPublic : Job::statusDraft,
                'slug' => str_slug($request->title)
            ];
            if(!empty($dirUpload)) {
                $dataJob['feature_image'] = 'storage' . $dirUpload;
            }
            $salaryChoose = $this->salary->findById($request->salary_id);
            if (!empty($salaryChoose) && !empty($salaryChoose->show_form_salary)) {
                $dataJob['salary_min'] = $request->salary_min;
                $dataJob['salary_max'] = $request->salary_max;
            } else {
                $dataJob['salary_min'] = null;
                $dataJob['salary_max'] = null;
            }
            // Add position occupation
            if (!empty($request->occupation_supporting_detail)) {
                $positionOccupationInstance = $this->positionOccupation->firstOrCreate([
                    'name' => $request->occupation_supporting_detail
                ]);
                if ($positionOccupationInstance) {
                    $dataJob['occupation_supporting_detail'] = $positionOccupationInstance->id;
                }

            }
            // Update to jjob_jobs table
            $job->update($dataJob);
            // Update city to jobs
            if(!empty($request->citys)) {
                $job->citys()->sync($request->citys);
            }
            // Update industry to jobs
//            if(!empty($request->industrys)) {
//                $job->customerIndustry()->sync($request->industrys);
//            }
//            // Update tags to table
//            $this->InsertOrUpdateTags($request, $job);
            // Update member_company to job
            if(!empty($request->members_job)) {
                $job->membersCompany()->sync($request->members_job);
            }
            // Create email apply jobs
            if (!empty(request()->email)) {
                $this->emailApplyJobs->deleteDataWhere(['job_id' => $job->id]);
                $job->emailApplyJobs()->create(['email' => request()->email]);
            }

            event(new CreatedCompany( $this->job->findById($id) ));
            DB::commit();
            return redirect()->route('enterprise.jobs.list');
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error('Xảy ra lỗi trong quá trình insert: ' . $exception->getMessage() . 'Line: ' . $exception->getLine());
        }
    }

    public function delete($id)
    {
        $this->job->deleteData($id);
        return response()->json(['status' => true], 200);
    }

    public function searchOccupationSelect(Request $request)
    {
        $occupationChild = $this->occupation->where('parent_id', $request->search)->get();
        $optionsHtml = view('enterprise.jobs.partials.option-occupation-select', compact('occupationChild'))->render();
        return response()->json([
            'code' => 200,
            'message' => 'success',
            'option_html' => $optionsHtml
        ], 200);

    }
}
