<div class="txt-format">
    <h1>Liên hệ</h1>
        <p>Mọi thông tin liên hệ xin vui lòng liên hệ</p><h4>CÔNG TY TNHH INPLACE VIETNAM</h4>
        <p><i class="fas fa-map-marker-alt" style="color: #3ea37d"></i>&nbsp;<strong>Địa chỉ liên hệ: </strong><br>&nbsp; &nbsp; &nbsp; <i class="fas fa-circle" style="color: #868686; font-size: 5px"></i>&nbsp;Tại Hà Nội: Tầng 3, tòa nhà Đại Phát, ngõ 82 Duy
            Tân, Cầu Giấy, Hà Nội – 024 6328 3156<br>&nbsp; &nbsp; &nbsp; <i class="fas fa-circle" style="color: #868686; font-size: 5px"></i>&nbsp;Tại TP Hồ Chí Minh: Tầng 2, tòa nhà Mekong, số
            235-241 Cộng Hòa, Tân Bình, TP Hồ Chí Minh</p>
        <p><i class="fas fa-phone" style="color: #3ea37d"></i>&nbsp;<strong>Hotline:</strong> 0866 872 554</p>
        <p><i class="fas fa-envelope" style="color: #3ea37d"></i>&nbsp;<strong>Email:</strong> <a href="mailto:services@inplace.vn">services@inplace.vn</a></p>
</div>