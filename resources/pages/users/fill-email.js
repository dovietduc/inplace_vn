
function InvalidMsgEmail() {
    if (this.value == '') {
        this.setCustomValidity('Xin hãy nhập email');
    }
    else if(this.validity.typeMismatch){
        this.setCustomValidity('Xin nhập đúng định dạng email');
    }
    else {
        this.setCustomValidity('');
    }
    return true;
}


$(function () {
    $(".email").on("invalid", InvalidMsgEmail);
    $(".email").on("change", InvalidMsgEmail);
    $('.select-industry').select2({
        placeholder: 'Nghề nghiệp bạn quan tâm',
    })
})
