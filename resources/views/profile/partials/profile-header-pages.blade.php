<div class="profile-header-pages">
    <div class="profile-header-pages_cover">
        <div class="cover-blur"></div>
        <div class="container">
            <div class="cover-image">
                <div class="cover-image_result">
                    @if( jjobCheckImageExit($user->feature_image) )
                        <img src="{{ asset($user->feature_image) }}" id="profile-cover-image-output" alt="">
                    @else
                        <img src="{{ asset('images/img_profile_cover.png') }}" id="profile-cover-image-output" alt="">
                    @endif

                    <div class="btn-upload-cover">
                        <label for="inp-cover-image-profile"><i class="fas fa-camera-alt"></i><span class="ml-1">Cập nhật ảnh bìa</span></label>
                        <input type="file" id="inp-cover-image-profile" class="inp-cover-image" accept=".png, .jpg, .jpeg"/>
                    </div>
                </div>
                <div class="cover-image_croppie">
                    <div id="upload-demo-cover"></div>
                    <div class="btn-group-cover">
                        <button type="button" class="btn-remove-crop-cover btn btn-custom_light mr-1">Hủy</button>
                        <button
                            data-url="{{ route('croppie.upload-image-cover') }}"
                            type="submit"
                            class="btn-crop-cover btn btn-custom">
                            Lưu thay đổi
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="profile-header-pages_nav">

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="profile-box-left">
                        <label class="profile-avatar">
                            @if( jjobCheckImageExit($user->avatar) )
                                <img src="{{asset($user->avatar)}}" id="profile-avatar-output"/>
                            @else
                                <img src="{{asset('images/no_user_125x125.jpg')}}" id="profile-avatar-output"/>
                            @endif

                            <label for="inp-avatar-profile" class="profile-avatar-upload">
                                <i class="fas fa-camera-alt"></i>
                                <input type="file" id="inp-avatar-profile" class="d-none inp-avatar" accept=".png, .jpg, .jpeg"/>
                            </label>
                        </label>
                        <div class="profile-info">
                            <h3 class="profile-name">
                                <a href="#">
                                    {{ $user->name }}
                                </a>
                            </h3>
{{--                                                        <div class="profile-desc">Account Manager tại J-Job Executive Search • </div>--}}
                            {{--                                    class="d-inline-block">Hanoi</span></div>--}}
{{--                            <ul class="profile-menu">--}}
{{--                                <li class="active"><a href="/frontend/profile/home">Trang chủ</a></li>--}}
{{--                                <li><a href="/frontend/profile/network">Network <small>(128)</small></a></li>--}}
{{--                                <li><a href="/frontend/profile/posts">Bài viết <small>(6)</small></a></li>--}}
{{--                                <li><a href="/frontend/profile/media">Media</a></li>--}}
{{--                                <li><a href="/frontend/profile/quiz">Quiz</a></li>--}}
{{--                            </ul>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 position-static">
                    <div class="profile-box-right text-right">
                        <a href="#" class="btn btn-custom_light"><i class="far fa-eye"></i> Xem trang cá nhân</a>
{{--                        <a href="#" class="btn btn-custom" data-toggle="modal" data-target="#popupEditProfile"><i--}}
{{--                                class="fas fa-edit"></i> Chỉnh--}}
{{--                            sửa</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('profile.partials.popup-edit-avatar')
@include('profile.partials.popup-edit-profile')
@include('profile.partials.popup-edit-contact-info')
@include('html.profile.partials.popup-save-success')

