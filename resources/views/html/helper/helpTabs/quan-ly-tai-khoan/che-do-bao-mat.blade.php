<div class="txt-format">
    <h1>Chế độ bảo mật</h1>
    <p>Khi được hỏi về các thông tin cá nhân trên website của Inplace, có nghĩa là bạn đang chia sẻ thông tin đó với riêng Website www.inplace.vn, trừ phi có thông báo cụ thể khác. Tuy nhiên, một số hoạt động do đặc trưng của chúng, sẽ dẫn đến việc thông tin cá nhân của bạn được tiết lộ cho những người sử dụng khác của Website www.inplace.vn biết. Ví dụ, khi bạn điền thông tin cá nhân lên trang profile của bạn hoặc bản đăng quảng cáo tuyển dụng, thông tin này nói chung sẽ được gộp trong nội dung của bạn, trừ phi có thông báo cụ thể khác.
        </p><p>Inplace bảo đảm không tiết lộ cho bên thứ ba thông tin cá nhân của bạn, cũng như thông tin cá nhân và nhân khẩu học kết hợp, hoặc thông tin về việc sử dụng Website www.inplace.vn của bạn (chẳng hạn các khu vực bạn ghé thăm, hay các dịch vụ mà bạn truy cập), trừ năm mục sau:
    </p>
    <ol>
        <li>
            Chúng tôi được quyền để lộ thông tin cho các nhóm thứ ba nếu có sự đồng ý của bạn.
        </li>
        <li>
            Chúng tôi có thể tiết lộ thông tin cho các công ty và cá nhân thay mặt chúng tôi thực hiện các chức năng của công ty. Ví dụ, việc lưu giữ các máy chủ web, phân tích dữ liệu, cung cấp các trợ giúp về marketing, xử lý thẻ tín dụng hoặc các hình thức thanh toán khác, và dịch vụ cho khách hàng.
        </li>
        <li>Chúng tôi có thể tiết lộ thông tin nếu có yêu cầu pháp lý, hay từ một cơ quan chính phủ.</li>
        <li>Chúng tôi có thể tiết lộ và chuyển thông tin tới một nhóm thứ ba, đối tượng mua lại toàn bộ hay phần lớn công việc kinh doanh của công ty Inplace Vietnam, Ltd, bằng cách liên kết, hợp nhất hoặc mua toàn bộ hay phần lớn các tài sản của chúng tôi.</li>
        <li>Chúng tôi có thể dùng tên bạn, tên hay logo của công ty bạn, hay thông tin khác về hoặc từ các quảng cáo tuyển dụng hoặc tài khoản xem hồ sơ ứng viên của bạn cho bất kỳ hay tất cả các mục đích tiếp thị của Inplace Vietnam (hay Website www.inplace.vn).</li>
    </ol>
    <p>Ngoài người quản trị Website www.inplace.vn hoặc cá nhân được uỷ quyền khác của Website www.inplace.vn ra, bạn là người duy nhất được truy nhập đến và thay đổi thông tin cá nhân của mình. Đăng ký sử dụng của bạn được bảo vệ bằng mật khẩu để ngăn chặn sự truy nhập trái phép.
    </p><p>Người dùng có trách nhiệm tự bảo mật tài khoản Inplace của mình. Để đảm bảo tài khoản của bạn được an toàn, hãy chắc chắn rằng mật khẩu bạn dùng để bảo mật là mật khẩu khó bị dò đoán. Hãy dùng các dãy ký tự dài (từ 6 ký tự trở lên), kết hợp chữ và số, in hoa và in thường, có khi thêm cả các ký tự đặc biệt.
    </p><p>Khi có nhu cầu đổi mật khẩu, bạn có thể làm các cách sau:
    </p>
    <ul><li><strong>Bước 1:</strong> Click vào Avatar bên góc phải của màn hình</li><li><strong>Bước 2:</strong> Trên Drop Down chọn <strong>Cài Đặt Tài Khoản.</strong></li><li><strong>Bước 3:</strong> Chọn <strong>Đổi mật khẩu</strong></li><li><strong>Bước 4:</strong> Điền mật khẩu mới vào hai ô trống theo yêu cầu của website</li></ul>
    <p>Sau khi đổi mật khẩu thành công, bạn sẽ nhận được email thông báo về việc đổi mật khẩu đã được thực hiện.</p>
</div>
