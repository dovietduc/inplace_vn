<div class="txt-format">
    <h1>Quản lý Tuyển dụng</h1>
    <p>Để quản lý việc tuyển dụng của doanh nghiệp, bạn cần quản lý Tin Tuyển Dụng và theo dõi tình hình ứng viên của
        các tin đăng tuyển đó.
    </p>
    <div class="row">
        <div class="col-sm-5 mt-3">
            <img src="{{asset('images/img_quan_ly_tuyen_dung.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mt-3">
            <ul>
                <li>Để quản lý tin đăng tuyển dụng, bạn có thể sử dụng công cụ <strong>Quản Lý Việc Làm</strong> trong &nbsp;công
                    cụ <strong>Admin</strong> của trang <strong>Doanh Nghiệp</strong>. . Tại đây, bạn có thể Tạo tin đăng mới,
                    Sửa, xoá tin, gắn Hot Job stamp, đẩy Slider trang chủ v.v…
                </li>
                <li>Để quản lý tình hình ứng tuyển của tin đăng, bạn cùng công cụ <strong>Quản Lý Ứng Viên</strong> trong &nbsp;công
                    cụ <strong>Admin</strong> của trang <strong>Doanh Nghiệp</strong>. . Tại đây bạn có thể xem tổng số ứng viên
                    ứng tuyển vào việc làm, xem Hồ sơ ứng tuyển và thực hiện các thao tác khác liên quan tới quá trình tuyển
                    dụng.
                </li>
            </ul>
        </div>
    </div>

</div>