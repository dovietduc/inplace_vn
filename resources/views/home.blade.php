@extends('layouts.master')
@section('meta')
    <title>Jobs list - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link href="{{ asset('vendor/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('pages/jobs/list/list.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <script>
        if (localStorage.getItem("url_back_join_company")) {
            localStorage.removeItem("url_back_join_company");
        }
    </script>
@stop

@section('content')
    @include('partials.header')
    <div class="block-slider">
        @include('jobs.partials.job-slider-home')
    </div>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-4 box_sidebar">
                    @include('jobs.partials.side-bar')
                </div>
                <div class="col-md-8 box_content">
                    <div class="block-main">
                        <div class="block-top">
                            <div class="job-found">
                                <b>1-10</b>
                                <span>/</span>
                                <span>{{ $jobs->total() }}</span>
                            </div>
                            <div class="job-tag ml-auto">
                                <a href="#" class="tag-item active">Đề xuất cho bạn</a>
                                <a href="#" class="tag-item">Mới nhất</a>
                                <a href="#" class="tag-item">Phổ biển nhất</a>
                            </div>
                        </div>
                        <div class="list-job-content" id="list_job_content">
                            @include('jobs.partials.list-job')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if( empty(auth()->user()->occupation_id) )
        @include('jobs.partials.modal-industry')
    @endif
    @include('partials.footer')
@stop
@section('scriptAddon')
    <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/jobs/list/list.js') }}"></script>
@stop
