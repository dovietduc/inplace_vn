<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnUserApplyJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jjob_user_apply_jobs', function (Blueprint $table) {
            $table->string('file_name');
            $table->string('file_path');
            $table->string('file_display_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jjob_user_apply_jobs', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('file_path');
            $table->dropColumn('file_display_name');
        });
    }
}
