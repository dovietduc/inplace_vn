<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait StoreImageTrait
{

    /**
     * Does very basic image validity checking and stores it. Redirects back if somethings wrong.
     * @Notice: This is not an alternative to the model validation for this field.
     *
     * @param Request $request
     * @return $this|false|string
     */
    public function verifyAndStoreImageJson($request, $fieldname = 'image', $directory = 'unknown')
    {
        if ($request->$fieldname) {
            $now = Carbon::now();
            $image = $request->$fieldname;
            list($type, $image) = explode(';', $image);
            list(, $image) = explode(',', $image);
            $image = base64_decode($image);
            $image_name = str_random(20) . '.png';
            $dirUpload = '/upload/inplace/' . $now->year . '/' . $now->month . '/' . $now->day
                . '/' . auth()->id() . '/' . $directory . '/' . $image_name;
            Storage::put('public' . $dirUpload, $image, 'public');
            return $dirUpload;
        }
        return null;

    }

    public function verifyAndStoreImage($request, $fieldname = 'image', $directory = 'unknown')
    {
        if ($request->hasFile($fieldname)) {
            $now = Carbon::now();
            $file = $request->file($fieldname);
            $imageName = str_random(20) . '.' . $file->getClientOriginalExtension();
            $dirUpload = '/upload/inplace/' . $now->year . '/' . $now->month . '/' . $now->day
                . '/' . auth()->id() . '/';
            $file->storeAs('public' . $dirUpload, $imageName);
            return [
                'file_name' => $imageName,
                'file_path' => 'storage' . $dirUpload . $imageName,
                'file_path_asset' => 'public' . $dirUpload . $imageName,
                'file_name_display' => $file->getClientOriginalName()
            ];

        }
        return null;
    }

    public function uploadImage($request, $fieldname = 'image', $directory = 'unknown')
    {
        if ($request->hasFile($fieldname)) {
            $now = Carbon::now();
            $file = $request->file($fieldname);
            $imageName = str_random(20) . '.' . $file->getClientOriginalExtension();
            $dirUpload = '/upload/inplace/' . $now->year . '/' . $now->month . '/' . $now->day
                . '/' . auth()->id() . '/' . $directory . '/';
            $file->storeAs('public' . $dirUpload, $imageName);
            return 'storage' . $dirUpload . $imageName;

        }
        return null;
    }

    public function uploadImageBase($request, $fieldname = 'image', $directory = 'unknown')
    {
        if ($request->hasFile($fieldname)) {
            $now = Carbon::now();
            $file = $request->file($fieldname);
            $imageName = str_random(20) . '.' . $file->getClientOriginalExtension();
            $dirUpload = '/upload/inplace/' . $now->year . '/' . $now->month . '/' . $now->day
                . '/' . auth()->id() . '/' . $directory . '/';
            $file->storeAs('public' . $dirUpload, $imageName);
            return $dirUpload . $imageName;

        }
        return null;
    }

}

