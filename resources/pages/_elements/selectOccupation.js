const axios = require('axios');

function selectOccupationClick(e) {
    e.stopPropagation();
    $(this).toggleClass('select-open').next().toggle();
}

function selectOccupationSelected(e) {
    e.stopPropagation();
    let optionThis = $(this);
    let lstOption = $('.select-occupation__list-options');
    let options = $('.select-occupation__option');
    let select2_occupation = $('.select2__occupation-sup');
    options.removeClass('selected');
    optionThis.addClass('selected');
    lstOption.toggle().prev().toggleClass('select-open').addClass('hasValue').text(optionThis.text());
    $('.select-occupation__value').val(optionThis.data('id'));
    $('.textarea-occupation-detail').removeAttr('readonly');
    let url = $(this).data('url');
    axios.post(url, {
        search: optionThis.data('id')
    })
        .then(function (response) {

            if (response.status == 200) {
                if(response.data.option_html){
                    select2_occupation.html('<option></option>' + response.data.option_html);
                    select2_occupation.select2({
                        minimumResultsForSearch: Infinity,
                        placeholder: optionThis.text(),
                    });
                    $('.select-occupation-sup').slideDown();
                }else {
                    select2_occupation.html('');
                    $('.select-occupation-sup').slideUp();
                }

            }

        })
        .catch(function (error) {
            console.log(error);
        });
}


function closeSelectOccupation() {
    $('.select-occupation__list-options').hide();
    $('.select-occupation__result').removeClass('select-open');
}


export function selectOccupation() {
    $(document).on('click', '.select-occupation__result', selectOccupationClick);
    $(document).on('click', '.select-occupation__option', selectOccupationSelected);
    $(document).on('click', 'body', closeSelectOccupation);
}
