<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="" method="POST">
            <div class="modal-header align-items-center">
                <h5 class="modal-title">Sửa thành viên</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                        class="far fa-times-circle"></i></button>
            </div>
            <div class="modal-body">
                @include('enterprise.partials.show-image-avatar-user', ['member' => $memberItem])
                <div class="info">
                    <h3 class="member-name">{{ optional($memberItem->user)->name }}</h3>
                    <input type="text"
                           value="{{ $memberItem->position }}"
                           name="position" class="member-desc form-control"
                           placeholder="Nhập chức danh thành viên">
                </div>
                <div class="about">
                            <textarea
                                class="member-about form-control autoheight"
                                name="introduction"
                                placeholder="Giới thiệu về thành viên này">{!! $memberItem->introduction !!}</textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-grey" data-dismiss="modal" aria-label="Close">Bỏ qua
                </button>
                <button
                    data-url="{{ route('enterprise.member.postMemberEdit', ['id' => $memberItem->id]) }}"
                    type="button"
                    class="btn btn-custom edit_member_update">
                    Cập nhật
                </button>
            </div>
        </form>
    </div>
</div>







