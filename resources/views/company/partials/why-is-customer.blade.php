<div class="company-why">
    <div class="component-title">
        <h3 class="title">
            Why
        </h3>
        <div class="desc">
            Tại sao sứ mệnh đó tồn tại?
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 mb-4 mb-sm-0">
            <div class="component-how-item">
                <a href="">
                    @if( jjobCheckImageExit($customer->why_feature_image_1) )
                        <img src="{{ url($customer->why_feature_image_1) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_270x150.jpg') }}" alt="">
                    @endif
                    <h5 class="title">{{ $customer->why_quote_image_1 }}</h5>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="component-how-item">
                <a href="">
                    @if( jjobCheckImageExit($customer->why_feature_image_2) )
                        <img src="{{ url($customer->why_feature_image_2) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_270x150.jpg') }}" alt="">
                    @endif
                    <h5 class="title">{{ $customer->why_quote_image_2 }}</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="txt-format mt-3">
        {!! $customer->why_we_do  !!}
    </div>
</div>
