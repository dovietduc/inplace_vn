@extends('layouts.app')
@section('meta')
    <title>Dành cho Cá nhân</title>
@stop
@section('css')
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/forUsers.css')}}">
@stop
@section('content')
    <div class="for-enterprise-head">
        <div class="container">
            <div class="for-enterprise-head_content">
                <h3 class="title">
                    <small>Dành cho</small>
                    Cá nhân
                </h3>
            </div>
        </div>
    </div>
    <div class="for-enterprise-body">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 box_sidebar">
                    <div class="nav nav-pills flex-column">
                        <a id="tao-trang-ca-nhan-moi-tab" class="active" data-toggle="pill"
                           href="#tao-trang-ca-nhan-moi" role="tab">Tạo trang cá nhân mới</a>
                        <a id="tao-bai-viet-moi-tab" data-toggle="pill" href="#tao-bai-viet-moi" role="tab">Tạo bài viết
                            mới</a>
                        <a id="upload-cv-tab" data-toggle="pill" href="#upload-cv" role="tab">Upload CV</a>
                        <a id="viec-lam-cua-toi-tab" data-toggle="pill" href="#viec-lam-cua-toi" role="tab">Việc làm của
                            tôi</a>
                        <a id="tim-kiem-doanh-nghiep-tab" data-toggle="pill" href="#tim-kiem-doanh-nghiep" role="tab">Tìm
                            kiếm doanh nghiệp</a>
                    </div>
                </div>
                <div class="col-lg-9 box_content">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tao-trang-ca-nhan-moi" role="tabpanel"
                             aria-labelledby="tao-trang-ca-nhan-moi-tab">
                            @include('html.helper.forUsersTabs.tao-trang-ca-nhan-moi')
                        </div>
                        <div class="tab-pane fade" id="tao-bai-viet-moi" role="tabpanel"
                             aria-labelledby="tai-bai-viet-moi-tab">
                            @include('html.helper.forUsersTabs.tao-bai-viet-moi')
                        </div>
                        <div class="tab-pane fade" id="upload-cv" role="tabpanel"
                             aria-labelledby="upload-cv-tab">
                            @include('html.helper.forUsersTabs.upload-cv')
                        </div>
                        <div class="tab-pane fade" id="viec-lam-cua-toi" role="tabpanel"
                             aria-labelledby="viec-lam-cua-toi-tab">
                            @include('html.helper.forUsersTabs.viec-lam-cua-toi')
                        </div>
                        <div class="tab-pane fade" id="tim-kiem-doanh-nghiep" role="tabpanel"
                             aria-labelledby="tim-kiem-doanh-nghiep-tab">
                            @include('html.helper.forUsersTabs.tim-kiem-doanh-nghiep')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/forUsers.js')}}"></script>
@stop
