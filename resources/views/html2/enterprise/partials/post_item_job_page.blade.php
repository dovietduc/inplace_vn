<div class="item border-bottom mb-40 pb-10">
    <div class="image-post mb-20">
        <a href="#" title="">
            <img src="https://via.placeholder.com/670x274" alt="" class="w-100 rounded-sm">
        </a>
    </div>
    <div class="tag-post mb-4">
        <a href="#" title="" class="btn btn-outline-green-300 rounded-sm text-size-12 py-0 px-10">
            Marketing Executive
        </a>
        <a href="#" title="" class="btn btn-outline-green-300 rounded-sm text-size-12 py-0 py-0 px-10">
            Digital
        </a>
    </div>
    <h3 class="post-name font-weight-bold mb-3">
        <a href="#" title="">
            Digital Marketing Executive WANTED!
        </a>
    </h3>
    <div class="post-desc text-black-500 mb-4">
        We’re looking for a thoughtful, extremely talented Product Designer to join our team. Our aim is to deliver a fun, no-pressure dating experience that leads the dating scenes in Asia and beyond.
        Responsibilities:  - Brainstorm for new features with our Product Manager and Head of Design ...
    </div>
    <div class="post-actions d-flex">
        <div class="post-info text-grey-200 d-flex align-self-center">
            <span class="mr-25">
                <i class="far fa-map"></i>
                Hà Nội
            </span>
            <span>
                <i class="far fa-clock"></i>
                1 giờ trước
            </span>
        </div>
        <div class="post-share-tools ml-auto d-flex ">
            <a href="#" class="item item-share d-flex align-self-center justify-content-center mr-1">
                <i class="far fa-share-square text-grey-200"></i>
            </a>
            <a href="#" class="item item-bookmark d-flex align-self-center justify-content-center mr-1">
                <i class="far fa-bookmark text-grey-200"></i>
            </a>
            <a href="#" class="item item-sent d-flex align-self-center justify-content-center mr-1">
                <i class="fas fa-paper-plane text-green-300"></i>
            </a>
        </div>
    </div>
</div>
