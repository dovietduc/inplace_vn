@extends('layouts.dashboard')
@section('meta')
    <title>News List Dashboard</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/news/list.css') }}">
@stop
@section('content')

    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Bài viết của <strong>J-JOB Executive Search</strong></h1>
                <div class="button-group">
                    <select class="dashboard-header_filter">
                        <option>Tất cả (10)</option>
                        <option>Đã đăng (8)</option>
                        <option>Đã ẩn (12)</option>
                    </select>
                    <a href="#" class="btn btn-custom"><i class="fas fa-plus mr-1"></i>Đăng bài viết</a>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-news-list">
        <div class="container">
            <div class="dashboard-news-list_th">
                <div class="row">
                    <div class="col-6">
                        Tiêu đề
                    </div>
                    <div class="col-2">
                        Danh mục
                    </div>
                    <div class="col-2">
                        Người viết bài
                    </div>
                    <div class="col-2">
                        Trạng thái
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-news-item">
                <div class="row">
                    <div class="col-lg-6 dashboard-news-item_col">
                        <div class="dashboard-news-item_title">
                            <div class="row">
                                <div class="col-sm-4 col-md-3 col-lg-4">
                                    <a href="#" class="thumb">
                                        <img src="{{asset('images/no_img_180x71.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="col-sm-8 col-md-9 col-lg-8">
                                    <h4 class="title"><a href="#">Lead, grow, and create impact with our team as our
                                            next Business Developer!</a></h4>
                                    <div class="d-flex align-items-center justify-content-between mt-2">
                                        <div class="time"><i class="far fa-clock"></i> 2019/10/22 - 7h35</div>
                                        <div class="action">
                                            <a href="" class="action_view" title="Xem" target="_blank"><i class="far fa-eye"></i></a>
                                            <a href="" class="action_edit" title="Sửa"><i class="far fa-edit"></i></a>
                                            <a href="" class="action_delete" title="Xóa" data-id = "<?php echo $i ?>"><i class="far fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 dashboard-news-item_col">
                        <div class="dashboard-news-item_info">
                            <div class="row">
                                <div class="col-sm-5 col-lg-4 col_cate">
                                    <span class="info-txt">Phỏng vấn nhân viên</span>
                                </div>
                                <div class="col-8 col-sm-4 col-lg-4 col_auth">
                                    <span class="info-txt">Nguyễn Hoài An</span>
                                </div>
                                <div class="col-4 col-sm-3 col-lg-4 col_status">
                                    <span class="status-txt active">Đã đăng</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>

            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>

        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/news/list.js')}}"></script>
@stop
