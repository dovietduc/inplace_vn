<div class="row top-page-list-news">
    <div class="col-lg mb-3 mb-lg-0">
        <div class="article-cat media">
            @if( jjobCheckImageExit(optional($newItem->customer)->logo) )
                <img src="{{ url(optional($newItem->customer)->logo) }}" alt=""
                     class="cat-avatar border rounded-circle mr-2 mr-lg-20">
            @else
                <img src="{{ asset('images/no-avatar-company.jpg') }}" alt=""
                     class="cat-avatar border rounded-circle mr-2 mr-lg-20">
            @endif
            <div class="media-body align-self-center ">
                <h3 class="cat-name mt-0 font-weight-bold text-black-400">
                    {{ optional($newItem->customer)->name }}
                </h3>
                <div class="d-flex">
                    <a href="" title=""
                       data-url="{{ route('companies.follow', ['id' => $customer->id]) }}"
                       class="btn-follow btn btn-outline-green-300 rounded-pill text-size-13 mr-10 follow_customer">
                        @if(!empty($checkUserFollowCustomer))
                            <span class="px-lg-1"> <i class="far fa-heart"></i>Đã là Fan</span>
                        @else
                            <span class="px-lg-1"> <i class="far fa-heart"></i>Trở thành Fan</span>
                        @endif
                    </a>
                    <div class="quick-post-info text-grey-200 text-size-12 align-self-center">
                        <span>{{ isset($countUserFollowCustomer) ? $countUserFollowCustomer : 0 }} Fan</span>
                        @if(!empty($cityCustomer))
                            <span class="dot-space"></span>
                        @endif
                        <span>
                                    {{ $cityCustomer }}
                                </span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $currentRouteName = Route::currentRouteName();
    @endphp
    <div class="col-lg-auto ml-auto align-self-lg-center">
        <div class="company-menu-page">
            <div class="nav nav-line nav-link-padding-y-0">
                @foreach($menus as $menu)
                    <div class="nav-item">
                        <a class="nav-link {{ $currentRouteName == $menu->route ? 'active' : ''}}"
                           href="{{ url('companies/' . $customer->slug) . '/' . $menu->slug }}">
                            {{ $menu->name }}
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</div>