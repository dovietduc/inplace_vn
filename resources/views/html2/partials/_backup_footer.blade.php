<hr class="m-0 shadow-sm position-relative">
<footer class="container-fluid bg-white-600 py-30 py-lg-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 company-info">
                <div class="logo-footer">
                    <a href="#" title="">
                        <img src="{{ asset('user/image/logo-footer.png') }}"/>
                    </a>
                </div>
                <div class="slogan-footer mb-lg-4">
                    Match values to place right people to right seats
                </div>
                <p class="line-height-sm footer-hot-line">
                    Hotline: <span class="font-weight-medium text-black-400">+ 123 456 789</span>
                </p>
                <p class="line-height-sm mb-0">
                    Email: <a href="mailto:hello@inplace.work" title="" class="text-green-300">hello@inplace.work</a>
                </p>
                <div class="footer-social-links mt-4 mt-lg-30">
                    <div class="item">
                        <a href="#" title="" class=" hexagon">
                            <i class="fab fa-linkedin-in text-grey-200"></i>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" title="" class=" hexagon">
                            <i class="fab fa-facebook-f text-grey-200"></i>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" title="" class=" hexagon">
                            <i class="fab fa-youtube text-grey-200"></i>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" title="" class=" hexagon">
                            <i class="fab fa-instagram text-grey-200"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 offset-lg-1">
                <div class="row footer-nav-links">
                    <div class="item col-12 col-md-6 col-lg-4">
                        <h5 class="title-box text-capitalize font-weight-bold mb-lg-40 text-black-400">
                            Về chúng tôi
                        </h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" title="">
                                    Trang chủ
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Giới thiệu
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Chăm sóc khách hàng
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Liên hệ
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="item col-12 col-md-6 col-lg-4">
                        <h5 class="title-box text-capitalize font-weight-bold mb-lg-40 text-black-400">
                            Dành cho doanh nghiệp
                        </h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" title="">
                                    Tạo trang Doanh nghiệp mới
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Đăng tuyển mới
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Đăng nội dung mới
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Tìm kiếm ưng viên
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="item col-12 col-md-6 col-lg-4">
                        <h5 class="title-box text-capitalize font-weight-bold mb-lg-40 text-black-400">
                            Dành cho cá nhân
                        </h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" title="">
                                    Tạo trang cá nhân mới
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Update CV
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Việc làm của tôi
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    Tìm kiếm doanh nghiệp
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="sub-footer bg-white-800 text-size-13">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg order-1 order-lg-0">
                <div class="copyright">
                    <span class="text-grey-200">
                    InPlace © 2019. All Rights Reserved.
                </span>
                </div>
            </div>
            <div class="col-12 col-lg ml-lg-auto mb-3 mb-lg-0 order-0 order-lg-1">
                <div class="footer-helper-links text-right">
                    <a href="#" title="" class="text-grey-200">
                        Quy định bảo mật
                    </a>
                    <a href="#" title="" class="text-grey-200">
                        Điều kiện và Thoả thuận sử dụng
                    </a>
                    <a href="#" title="" class="text-grey-200">
                        Quy chế hoạt động
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
