<div class="txt-format">
    <h1>Tạo Profile</h1>
        <p>Sau khi
            đã đăng ký tài khoản, hãy đăng nhập ngay và bắt đầu tạo trang cá nhân của &nbsp;bạn nhé:</p>
        <ul>
            <li><strong>Bước 1:</strong> Click vào biểu tượng Profile của bạn ở góc phải màn hình, chọn <strong>Chỉnh
                    sửa trang cá nhân</strong></li>
            <li><strong>Bước 2:</strong> Click vào biểu tượng <strong>Chỉnh sửa Trang cá nhân</strong> để sửa hàng loạt
                thông tin cùng một lúc hoặc biểu tượng &nbsp;<strong>Chỉnh sửa</strong> và <strong>Thêm</strong> để điền
                vào từng trường thông tin trong form Profile như các thông tin cá nhân, kinh nghiệm làm việc, sở trường
                sở đoản, kỹ năng mềm v.v..
            </li>
            <li><strong>Bước 3:</strong> Hãy upload CV của bạn để dùng những lúc cần thiết nhé</li>
            <li><strong>Bước 4:</strong> Đừng quên upload hình ảnh đại diện và ảnh Cover để làm Profile của bạn thêm
                sinh động nhé.
            </li>
            <li><strong>Bước 5:</strong> Để tăng tính đa dạng về nội dung của trang Profile, bạn nên bổ sung thêm hình
                ảnh và các bài viết để chứng tỏ khả năng chuyên môn hay kỹ năng về một lĩnh vực gì đó để tạo ấn tượng
                tốt với nhà tuyển dụng và các thành viên khác nhé.
            </li>
        </ul>
</div>
