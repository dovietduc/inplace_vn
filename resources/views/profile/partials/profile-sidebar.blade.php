<div class="profile-sidebar">
    <div class="profile-sidebar_box">
        <h4 class="title">Chia sẻ CV của bạn</h4>
        <form action="" method="post">
            <div class="form-share-cv">
                <input type="text" placeholder="CV của bạn" class="form-control" value="">
                <button type="button" class="btn-share-cv btn btn-custom"><i class="fas fa-share"></i></button>
            </div>
        </form>
    </div>
    <div class="profile-scores">
        <div class="bg-score">
            <span class="score-number">12</span>
            <small>InPlace score</small>
        </div>
        <a href="#">Điểm InPlace là gì?</a>
    </div>
    <div class="related-users">
        <h5 class="title-sidebar">Bạn có thể biết</h5>
        <?php for ($i = 0; $i<5; $i++ ){?>
        <div class="component-user-sidebar">
            <a href="#" title="" class="user-avatar">
                <img src="/images/no_user_65x65.jpg" alt="">
            </a>
            <div class="user-content">
                <h6 class="user-name">
                    <a href="#" title="">
                        Jasmine Seow
                    </a>
                </h6>
                <div class="user-desc">
                    Purchasing staff at Panasonic Appliances
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>
