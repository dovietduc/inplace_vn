@extends('layouts.dashboard')
@section('meta')
    <title>Danh sách ứng viên</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/enterprise/candidates/list.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Danh sách <strong>Ứng viên</strong>
                </h1>
                <div class="button-group">
                    <div class="btn"><strong>{{ $candidateApplyCount }} hồ sơ</strong> đã ứng tuyển</div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-candidates-list">
        <div class="container">
            <div class="dashboard-candidates-list_th">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        Ứng viên
                    </div>
                    <div class="col-sm-6 col-md-4">
                        Thông tin liên hệ
                    </div>
                    <div class="col-sm-7 col-md-3">
                        Ngày ứng tuyển
                    </div>
                </div>
            </div>
            @foreach($candidates as $candidateItem)
                <div class="dashboard-candidate-item" data-member="1">
                    <div class="row align-items-center">
                        <div class="col-sm-6 col-md-5">
                            <div class="info">
                                <a href="{{ route('enterprise.candidates.show', ['id' => $candidateItem->id]) }}" class="avatar">
                                    @include('enterprise.partials.show-image-avatar-user', ['member' => $candidateItem])
                                </a>
                                <div class="title">
                                    <h3 class="name">
                                        <a href="{{ route('enterprise.candidates.show', ['id' => $candidateItem->id]) }}">
                                            {{ optional($candidateItem->user)->name }}
                                        </a>
                                    </h3>
                                    {{--                                <div class="desc"><i class="fas fa-map-marker-alt"></i> TP.Hồ Chí Minh</div>--}}
                                    <div class="job-item-tag">
                                        <div class="tag-list">
                                            <a href="{{ route('jobs.show', ['slug' => optional($candidateItem->job)->slug]) }}" class="tag-item" target="_blank">
                                               {{ optional($candidateItem->job)->short_title }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="contact">
                                <i class="fal fa-envelope"></i>
                                <em>
                                    {{ optional($candidateItem->user)->email }}
                                </em> <br>
                                <i class="fal fa-phone-alt"></i>
                                <strong>
                                    {{ $candidateItem->mobile }}
                                </strong>
                            </div>
                        </div>
                        <div class="col-7 col-md-3 offset-sm-6 offset-md-0">
                            <div class="date-apply"><i class="fal fa-calendar-alt d-md-none mr-2"></i>
                                {{  Carbon::createFromFormat('Y-m-d H:i:s', $candidateItem->created_at)
                                                ->format('d-m-Y') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                {{ $candidates->appends($_GET)->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>


@stop
@section('js')
    <script src="{{ asset('pages/enterprise/candidates/list.js') }}"></script>
@stop
