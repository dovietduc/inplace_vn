<div class="txt-format">
    <h1>Hướng dẫn Tìm kiếm Ứng viên</h1>
    <p>Để tìm kiếm ứng viên, bạn cần thực hiện theo các cách sau:</p>
    <h3>Cách 1: Tìm kiếm ứng viên theo tên, vị trí, chức vụ, địa phương v.v…</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tim_kiem_ung_vien.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Tạo tài khoản tại Inplace và tiến hành đăng nhập
                    vào tài khoản. (Xem hướng đẫn tại đây: Đưa đường link tới phần hướng dẫn tạo
                    tài khoản mới)
                </li>
                <li><strong>Bước 2: </strong>Nhập thông tin theo tên, chức danh hay địa điểm nơi
                    đối tượng mà bạn hướng đến và nhấp vào biểu tượng tìm kiếm.
                </li>
                <li><strong>Bước 3:</strong> Click vào biểu tượng Avatar từ kết quả tìm kiếm để
                    tìm tới và đọc Profile tóm gọn của ứng viên đó.
                </li>
                <li><strong>Bước 4:</strong> Hãy Connect với ứng viên bạn quan tâm. Khi bạn và
                    ứng viên đã trở thành bạn bè thì bạn có thể sử dụng công cụ Nhắn Tin để trao
                    đổi thêm thông tin với ứng viên theo cách bạn muốn.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 2: Tìm kiếm ứng viên theo vị trí công việc bạn đã đăng tuyển (sau khi bạn
        đã có tài khoản doanh nghiệp và đăng tuyển thành công)</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tim_kiem_ung_vien_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> truy cập vào công cụ Admin của Trang Doanh Nghiệp.&nbsp;
                </li>
                <li><strong>Bước 2:</strong> Tìm tới phần Quản Lý Ứng Viên từ Menu bên tay trái
                    màn hình. Trong danh sách ứng viên, bạn có thể tìm kiếm ứng viên theo Job
                    Title do mình đã đăng tuyển từ trước.
                </li>
                <li><strong>Bước 3:</strong> Click vào biểu tượng Avatar từ kết quả tìm kiếm để
                    tìm tới và đọc Profile tóm gọn của ứng viên đó.
                </li>
                <li><strong>Bước 4: </strong>Hãy Connect và Nhắn Tin với Ứng Viên theo cách bạn
                    muốn
                </li>
            </ul>
        </div>
    </div>

</div>
