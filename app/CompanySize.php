<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class CompanySize extends Model
{
    protected $table = 'jjob_company_size';
    protected $guarded = [];
    use BaseEloquent;
}
