<?php

namespace App\Jobs;

use App\Mail\SendMailToCustomerManagementJobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendEmailToUserManagementJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailCustomer = new SendMailToCustomerManagementJobs($this->data);
        Mail::to($this->data['email_user_admin_job'])
            ->bcc('support@inplace.vn')
            ->send($emailCustomer);
    }
}
