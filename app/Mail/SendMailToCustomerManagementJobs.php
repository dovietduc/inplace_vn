<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailToCustomerManagementJobs extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tittle = $this->data['name'] . ' vừa ứng tuyển vào vị trí ' . $this->data['job_short_title'] . ' của ' . $this->data['name_customer'] . ' thông qua Inplace';
        $objectMail = $this->subject($tittle)
            ->view('emails.apply-jobs.customer-get-email');
        if ( !empty($this->data['file_cv_attach']) && \Storage::exists($this->data['file_cv_attach']) ) {
            return $objectMail
                ->attach(storage_path('app/' . $this->data['file_cv_attach']), [
                    'as' => $this->data['file_name_attach']
                ]);
        } else {
            return $objectMail;
        }
        return $objectMail;
    }
}
