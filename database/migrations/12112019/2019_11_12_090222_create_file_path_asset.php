<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilePathAsset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jjob_user_apply_jobs', function (Blueprint $table) {
            $table->string('file_path_asset');
            $table->renameColumn('file_display_name', 'file_name_display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jjob_user_apply_jobs', function (Blueprint $table) {
            //
        });
    }
}
