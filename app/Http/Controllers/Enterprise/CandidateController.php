<?php

namespace App\Http\Controllers\Enterprise;

use App\UserApplyJob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CandidateController extends Controller
{
    private $userApplyJob;

    public function __construct(UserApplyJob $userApplyJob)
    {
        $this->userApplyJob = $userApplyJob;
    }

    public function index()
    {
        $candidates = $this->userApplyJob->getManyWhereOrderPagination([
            'customer_id' => session('customer_id')
        ]);
        $candidateApplyCount = $this->userApplyJob->where([
            'customer_id' => session('customer_id')
        ])->count();
        return view('enterprise.candidates.list', compact('candidates', 'candidateApplyCount'));
    }

    public function show($id)
    {
        $candidate = $this->userApplyJob->findById($id);
        return view('enterprise.candidates.detail', compact('candidate'));
    }
}
