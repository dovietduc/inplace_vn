<div class="txt-format">
    <h1>Các gói Dịch vụ & Thanh toán</h1>
    <p>Đối với doanh nghiệp, Inplace cung cấp Dịch vụ Hỗ Trợ Employer Branding và Dịch vụ Hỗ Trợ Tuyển Dụng.
    </p>
    <h4>Dịch vụ hỗ trợ Employer Branding:</h4>
    <p>Doanh nghiệp có thể chọn sử dụng một trong 4 gói dịch vụ bao gồm Dùng Thử (Trial), Gói Cơ Bản (Standard), gói
        Doanh Nghiệp (Enterprise) và gói Cao Cấp(Premium). Các chuyên viên chăm sóc khách hàng của chúng tôi sẽ căn cứ
        vào nhu cầu, ngân sách của doanh nghiệp để đưa ra các tư vấn phù hợp.</p>
    <img class="mb-3" src="{{asset('images/img_cac_goi_dich_vu_va_thanh_toan.jpg')}}" alt="">
    <h4>Dịch vụ hỗ trợ tuyển dụng: </h4>
    <p><em><span style="color: rgb(134, 134, 134);">* INPLACE cung cấp các công cụ hỗ trợ Doanh nghiệp trong việc theo sát các Ứng viên ứng tuyển trong suốt quá trình Tuyển dụng, Hỗ trợ screen ứng viên, Đặt lịch phỏng vấn, chăm sóc sau phỏng vấn và gửi Offer. Hãy liên hệ với Hotline CSKH của chúng tôi để nhận được tư vấn dịch vụ tốt nhất.</span></em>
    </p>
    <p>Với sự trợ giúp của website, quá trình tuyển dụng của doanh nghiệp sẽ được thực hiện nhanh chóng và hiệu quả.
        Khi hệ thống ghi nhận trường hợp doanh nghiệp tuyển dụng thành công từ website, doanh nghiệp mới phải thanh
        toán phí dịch vụ với mức chi phí hết sức tiết kiệm.</p>
    <h4>Thanh toán</h4>
    <ul>
        <li>Hình thức thanh toán trả trước sẽ được áp dụng cho Dịch vụ hỗ trợ Employer Branding.</li>
        <li>Hình thức thành toán trả sau (dựa trên kết quả ghi nhận trường hợp thành công của website) được áp dụng cho
            Dịch vụ hỗ trợ tuyển dụng.
        </li>
        <li>Bộ phận CKSH của Inplace sẽ liên hệ và thông báo tới khách hàng các khoản thanh toán tới hạn &nbsp;</li>
    </ul>
</div>