<div class="component-who">
    <ul class="tab-who nav" role="tablist">
        @foreach($members as $key => $member)
            <li class="col-3">
                <a class="item {{ $key == 0 ?  'active' : ''}}" data-toggle="tab"
                   href="#tab-{{ $key }}" role="tab" aria-controls="home"
                   aria-selected="true">
                    @if(empty(optional($member->user)->avatar))
                        <img class="avatar" src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                    @else
                        @if(str_contains(optional($member->user)->avatar, 'upload') && jjobCheckImageExit(optional($member->user)->avatar))
                            <img class="avatar" src="{{ asset(optional($member->user)->avatar) }}" alt="">
                        @else
                            <img class="avatar" src="{{ optional($member->user)->avatar }}" alt="">
                        @endif
                    @endif
                    <h5 class="name">
                        {{ optional($member->user)->name }}
                    </h5>
                    <div class="position">
                        {{ $member->position }}
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($members as $key => $member)
            <div class="item_content tab-pane {{ $key == 0 ?  'active' : ''}}"
                 id="tab-{{ $key }}" role="tabpanel" aria-labelledby="home-tab">
                <div class="info-member">
{{--                    <a href="#">--}}
                        <h5 class="name">
                            <span class="mr-3">{{ optional($member->user)->name }}</span>
                            <span class="position"> {{ $member->position }}</span>
                        </h5>
                        <div class="introduction">
                            {{ $member->introduction }}
                        </div>
{{--                    </a>--}}

                </div>
                @php
                    $news = $member['news'];
                @endphp
                @if(!empty($news->title))
                    <div class="post-member">
                        <div class="thumb">
                            <a href="{{ route('news.show', ['slugCompany' => optional($news->customer)->slug, 'id' => $news->id]) }}">
                                @if( jjobCheckImageExit($news->thumbnail) )
                                    <img src="{{ asset($news->thumbnail) }}" alt="">
                                @else
                                    <img src="{{asset('images/no_img_255x100.jpg')}}"
                                         alt="">
                                @endif

                            </a>
                        </div>
                        <div class="content">
                            <h4 class="title">
                                <a href="{{ route('news.show', ['slugCompany' => optional($news->customer)->slug, 'id' => $news->id]) }}">
                                    {{ $news->title }}
                                </a>
                            </h4>
                            <span class="time">
                                ({{ Carbon::createFromFormat('Y-m-d H:i:s', $news->created_at)->format('d-m-Y') }})
                            </span>
                            <div class="desc">
                                {!! str_limit( preg_replace('/([-\s])+/i', ' ', strip_tags($news->content)), $limit = 300, $end = '...') !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</div>
