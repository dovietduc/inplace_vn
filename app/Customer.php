<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use BaseEloquent;
    protected $table = 'jjob_customers';
    protected $guarded = [];
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function industrys()
    {
        return $this->belongsToMany(CustomerIndustry::class, 'jjob_customer_industries', 'customer_id', 'industry_id');
    }
    public function CompanySize()
    {
        return $this->belongsTo(CompanySize::class, 'company_size_id');
    }

    public function customerAddress()
    {
        return $this->hasMany(CustomerAddress::class, 'customer_id');
    }
    public function userFollow()
    {
        return $this->belongsToMany(User::class, 'jjob_customer_follows');
    }
    public function membersCompany()
    {
        return $this->belongsToMany(User::class, 'user_add_members', 'customer_id', 'user_id');
    }

}
