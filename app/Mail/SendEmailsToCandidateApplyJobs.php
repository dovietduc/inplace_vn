<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailsToCandidateApplyJobs extends Mailable
{
    use Queueable, SerializesModels;
    public $candidate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($candidate)
    {
        $this->candidate = $candidate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tittle = 'Bạn vừa ứng tuyển thành công vào vị trí ' . $this->candidate['job_short_title'] . ' tại ' . $this->candidate['name_customer'] . ' thông qua Inplace';
        return $this->subject($tittle)
            ->view('emails.apply-jobs.candidate-apply');
    }
}
