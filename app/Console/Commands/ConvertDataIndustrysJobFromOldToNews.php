<?php

namespace App\Console\Commands;

use App\Industry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class ConvertDataIndustrysJobFromOldToNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:data-job-industry-from-old-to-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Covert data job industry from old to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataIndustryMore = [
            'Bảo vệ/An Ninh',
            'Sản xuất',
            'Nhà máy',
            'Bảo trì/ Bảo Dưỡng',
            'Phát triển sản phẩm',
            'Nghiên cứu phát triển/ R&D',
            'Phòng thí nghiệm',
            'Giám sát/Thanh tra',
            'Quản lý chất lượng/ QA-QC',
            'IT Quản lý hệ thống',
            'IT Lập trình',
            'IT Testing',
            'IT Front End',
            'IT Back End',
            'IT Full Stack',
            'IT BrSE/Kỹ sư cầu nối',
            'IT An ninh mạng',
            'IT khác',
            'Logistics',
            'Vận chuyển/Giao nhận',
            'Supply Chain/ Chuỗi cung ứng',
            'Creative Content',
            'PR',
            'Media/ Digital',
            'Nghiên cứu thị trường',
            'Truyền thông/quảng cáo',
            'Event',
            'Branding',
            'Phát triển thị trường mới',
            'Chăm sóc khách hàng',
            'Call Center',
            'Quỹ / Công nợ',
            'Định giá / thẩm định',
            'Pháp chế',
            'Licensing / Franchising',
            'Hành chính',
            'Trợ lý / Thư ký',
            'Đối ngoại / Ngoại giao',
            'HR / Nhân sự',
            'Employer Branding',
            'Giảng dạy / Đào tạo',
            'Tuyển dụng',
            'Overseas / thị trường nước ngoài',
            'Xuất nhập khẩu',
            'Tư vấn / Cố vấn',
            'Quản lý rủi do',
            'Lập kế hoạch',
            'Điều hành cấp cao'
        ];
        try {
            DB::beginTransaction();

            // Update industry
            Industry::orderBy('id')->chunk(100, function ($industrys) {
                foreach ($industrys as $industry) {
                    if ($industry->id == 90) {
                        $industry->update(['name' => 'Kỹ Thuật']);
                    }
                    if ($industry->id == 94) {
                        $industry->update(['name' => 'Kinh Doanh / Bán hàng']);
                    }
                    if ($industry->id == 96) {
                        $industry->update(['name' => 'Tài chính / Kế toán']);
                    }

                }
            });
            // insert more industry

            foreach ($dataIndustryMore as $industryItem) {
                Industry::create([
                    'name' => $industryItem
                ]);
            }

            DB::commit();
            $this->info('Convert data industry job complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
