import {dashboardLayout} from '../dashboard.js';
function deleteMember(){
    let parentM = $(this).parents('.dashboard-candidate-item');
    let idMember = parentM.attr('data-member');
    let avatar = parentM.find('.avatar img').attr('src');
    let name = parentM.find('.name').text();
    let desc = parentM.find('.desc').text();
    $('.modal-delete-member').attr('data-member',idMember);
    $('.modal-delete-member .member-avatar').attr('src', avatar);
    $('.modal-delete-member .member-name').text(name);
    $('.modal-delete-member .member-desc').text(desc);
    $('.modal-delete-member').modal('show');
}
function deleteMemberConfirm(){
    let idMember = $(this).parents('.modal-delete-member').attr('data-member');
    $.ajax({
        url: '/delete',
        type: "DELETE",
        data: {
            "ID-member": idMember
        },
        success: function (data) {
            if (data.status) {
                location.reload();
            }
        }
    });
}
$(function () {
    dashboardLayout();
    $(document).on('click','.btn-delete-member',deleteMember);
    $(document).on('click','.btn-delete-member-confirm',deleteMemberConfirm);
})
