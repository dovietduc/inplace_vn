<div class="txt-format">
    <h2>PERSONAL BRANDING</h2>
    <p>Inplace cung cấp công cụ để người dùng có thể tự làm Branding cho cá nhân. Các bạn có thể sử dụng trang Profile
        cá nhân, các bài viết , photo hay video để minh hoạ cho tính cách, kỹ năng chuyên môn, tư duy, ý kiến và quan
        điểm cá nhân của mình. Hãy tận dụng các tính năng của Inplace để tạo cho mình một hồ sơ cá nhân ấn tượng, điều
        đó sẽ tạo được thiện cảm với nhà tuyển dụng hay bất cứ ai ghé thăm trang cá nhân của mình nhé các bạn.
    </p>
    <p><br></p>
    <h2>EMPLOYER BRANDING</h2>
    <p>Với Doanh nghiệp, ngoài các công cụ branding cơ bản như Profile, công vụ viết bài, hiển thị thành viên, Media
        library, Quiz trắc nghiệm v.v..Inplace sẽ cung cấp nhiều hơn tài nguyên, công cụ tính năng và dung lượng theo
        các gói dịch vụ Hỗ Trợ Employer Branding tuỳ thuộc vào nhu cầu, ngân sách của doanh nghiệp.
    </p>
    <ul>
        <li>Gói <strong>Trial</strong>: dành cho các doanh nghiệp trải nghiệm thử tính năng của Inplace</li>
        <li>Gói <strong>Standard</strong>: dành cho các doanh nghiệp SME, startup&nbsp;</li>
        <li>Gói <strong>Enterprise</strong>: dành cho các doanh nghiệp tầm trung trở lên, có chiến lược phát triển EB
            lâu dài
        </li>
        <li>Gói <strong>Premium</strong>: dành cho các doanh nghiệp dẫn đầu thị trường, các tập đoàn lớn thực sự cần sự
            tối ưu trong chiến lược phát triển Thương Hiệu Nhà Tuyển Dụng.&nbsp;
        </li>
    </ul>
        <p>Khi bạn
            Tạo trang Doanh Nghiệp lần đầu tiên, bạn sẽ ở mặc định gói Trial. Nếu có nhu cầu nâng cấp gói, hãy liên hệ
            Hotline CSKH của Inplace hoặc email <a href="mailto:services@inplace.vn">services@inplace.vn</a>. Inplace
            luôn sẵn sàng hỗ trợ bất cứ lúc nào.</p>
        <p>Ngoài ra, Inplace sẽ luôn đồng hành cùng các khách hàng doanh nghiệp của mình trong quá trình branding. Team
            EB của Inplace sẽ là những trợ thủ đắc lực của doanh nghiệp trong việc xây dựng content EB, tư vấn cách thực
            hiện chiến lược EB của mình. Tất cả những điều trên đều được bao gồm trong các gói hỗ trợ
            Standard/Enterprise và Premium của Inplace.</p>
</div>
