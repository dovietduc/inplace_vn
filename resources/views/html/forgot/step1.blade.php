<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forgot step 1</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/forgot/forgot.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('html/element/header-signin')
	@endcomponent

	<div class="forgot">
		<div class="holder">
			<div class="sologan">
				<h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
				<div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
			</div><!-- sologan -->

			<div class="form">
				<div class="title text-center">Oops!<br>Có vẻ như bạn đã quên mật khẩu?</div>
				<div class="txt text-center">Không sao, chỉ cần nhập địa chỉ email vào ô bên<br>dưới để đặt lại mật khẩu nhé, sẽ nhanh thôi.</div>

				<form action="#" method="POST">
					<div class="form-group">
						<div class="messages error">Email không hợp lệ.</div>
						<input type="email" name="email" placeholder="Nhập email của bạn" class="form-control form-control-gray">
					</div>

					<div class="form-group button">
						<button type="submit" class="btn btn-custom">GỬI</button>
					</div>

					<div class="back">
						<a href="#"><i class="fas fa-chevron-left"></i> Quay lại đăng nhập</a>
					</div>

					<div class="regiter text-center">Bạn chưa có tài khoản? <a href="#">Đăng ký ngay</a></div>
				</form>
			</div><!-- form -->
		</div>
	</div><!-- .forgot -->
	
	@component('html/element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/common/main.js"></script>
	<script src="/pages/forgot/forgot.js"></script>
</body>
</html>