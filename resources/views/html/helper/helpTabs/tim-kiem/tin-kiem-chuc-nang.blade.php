<div class="txt-format">
    <h1>Tìm kiếm chức năng</h1>
        <p>Để tìm
            kiếm chức năng, bạn cần thực hiện theo các bước sau:</p>
        <ul>
            <li><strong>Bước 1:</strong> Sau khi đăng nhập, thanh Search bar sẽ luôn hiện trên Head Menu của website.&nbsp;
            </li>
            <li><strong>Bước 2:</strong> Nhập thông tin tên từ khoá chức năng mà bạn cần tìm và nhấp vào biểu tượng tìm
                kiếm.
            </li>
        </ul>
</div>
