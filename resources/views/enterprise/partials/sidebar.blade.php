<div class="dashboard-sidebar">
    <button class="btn-dashboard-sidebar-toggle"><i class="fas fa-list"></i></button>
    <div class="dashboard-sidebar_box">
        <div class="dashboard-sidebar_content">
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Menu</h3>
                @php
                $currentRouteSiderBar = \Request::route()->getName();
                @endphp
                <ul class="dashboard-sidebar__menu">
                    <li class="{{ $currentRouteSiderBar == 'enterprise.index'  ? 'active' : ''}}">
                        <a href="{{ route('enterprise.index', ['customerId' => session('customer_id')]) }}">
                            <img class="icon" src="{{asset('images/icon-getstarted.png')}}" alt="">
                            <span>Get started</span>
                        </a>
                    </li>
                    <li class="{{ $currentRouteSiderBar == 'enterprise.jobs.list' ||
                     $currentRouteSiderBar == 'enterprise.jobs.add' ||  $currentRouteSiderBar == 'enterprise.jobs.edit' ? 'active' : ''}}">
                        <a href="{{ route('enterprise.jobs.list') }}">
                            <img class="icon" src="{{asset('images/icon_job.png')}}" alt="">
                            <span>Việc làm</span>
{{--                            <small class="count">12</small>--}}
                        </a>
                    </li>
                    <li class="{{ $currentRouteSiderBar == 'enterprise.candidates.index' ? 'active' : ''}}">
                        <a href="{{ route('enterprise.candidates.index') }}">
                            <img class="icon" src="{{asset('images/icon-candidate.png')}}" alt="">
                            <span>Ứng viên</span>
                        </a>
                    </li>
                    <li class="{{ $currentRouteSiderBar == 'enterprise.news.list' ||
                     $currentRouteSiderBar == 'enterprise.news.add' ||  $currentRouteSiderBar == 'enterprise.news.edit' ? 'active' : ''}}">
                        <a href="{{ route('enterprise.news.list') }}">
                            <img class="icon" src="{{asset('images/icon_news.png')}}" alt="">
                            <span>Bài viết</span></a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="#">--}}
{{--                            <img class="icon" src="{{asset('images/icon-mes.png')}}" alt="">--}}
{{--                            <span>Tin nhắn</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="#">--}}
{{--                            <img class="icon" src="{{asset('images/icon-analytics.png')}}" alt="">--}}
{{--                            <span>Thống kê</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div>
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Thông tin</h3>
                <ul class="dashboard-sidebar__info">
                    <li class="{{ $currentRouteSiderBar == 'enterprise.companies.edit'  ? 'active' : ''}}">
                        <a

                            href="{{ route('enterprise.companies.edit', ['id' => session('customer_id')]) }}">
                            Công ty
                        </a>
                    </li>
                    <li class="{{ $currentRouteSiderBar == 'enterprise.member.users'  ? 'active' : ''}}">
                        <a href="{{ route('enterprise.member.users') }}">Thành viên</a></li>
                </ul>
            </div>
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Tài khoản</h3>
                <div class="dashboard-sidebar__account">
                    <h4>Phiên bản dùng thử</h4>
                    <p>Bạn có thể đăng 10 Job và gắn tối đa 12 Hot Job Stamp</p>
                    <a href="#" class="btn btn-custom">Nâng cấp</a>
                </div>
            </div>
            <div class="dashboard-sidebar_widget">
                <h3 class="title-sidebar">Hỗ trợ khách hàng</h3>
                <div class="dashboard-sidebar__support">
                    <p>Email: <a href="mailto:services@inplace.vn"><span class="color-theme">services@inplace.vn</span></a><br>
                        Hotline: <a href="tel:028 6275 5586"><strong>028 6275 5586</strong></a></p>
                    <a href="#"><i class="far fa-life-ring mr-1"></i><span class="color-theme">Help Center</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
