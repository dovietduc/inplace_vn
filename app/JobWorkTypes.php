<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class JobWorkTypes extends Model
{
    use BaseEloquent;
    protected $table = 'jjob_work_types';
    protected $guarded = [];
}
