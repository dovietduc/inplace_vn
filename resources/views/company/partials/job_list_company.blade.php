<div class="component-title">
    <h3 class="title">Việc làm {{ !empty($jobTag) ? 'cho ' . $jobTag->name : '' }}</h3>
    <div class="desc"></div>
</div>
<div class="company-list-jobs">
    @foreach($jobs as $jobItem)
        <div class="component-job-item-large">
            <div class="job-item-thumb">
                <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="">
                    @if( jjobCheckImageExit($jobItem->feature_image) )
                        <img src="{{ url($jobItem->feature_image) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_730x286.jpg') }}" alt="">
                    @endif
                </a>
            </div>
            <div class="job-item-tag">
                @if($jobItem->hot)
                    <div class="tag-hot-job">
                        <span>Hot Job</span>
                    </div>
                @endif
                <div class="tag-list">
                    @if(!empty($jobItem->short_title))
                        <a href="" title="" class="tag-item">
                            {{ $jobItem->short_title }}
                        </a>
                    @endif
                </div>
            </div>

            <h3 class="job-item-title">
                <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="">
                    {{ $jobItem->title }}
                </a>
            </h3>
            <div class="job-item-desc txt-format">
                {!! str_limit( strip_tags($jobItem->description), $limit = 400, $end = '...') !!}
            </div>
            <div class="job-item-action">
                <div class="job-item-locate">
                    <span class="mr-4">
                        <i class="far fa-map"></i>
                        {{ !empty($jobItem->citys) ? optional($jobItem->citys()->first())->name : '' }}
                    </span>
                    <span>
                        <i class="far fa-clock"></i> {{ $jobItem->created_at->diffForHumans() }}
                    </span>
                </div>
                <div class="job-item-share">
                    @include('partials.button-save-job-icon')
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="col-md-12 block-pagination pagination-page-search">
    {{ $jobs->links('vendor.pagination.default') }}
</div>

