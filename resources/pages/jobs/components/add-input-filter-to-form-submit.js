const axios = require('axios');
let counting = 0;

function changeKeywords(event) {
    let search_dropdown = $('.filter--keywords--dropdown');
    let url = $(this).data('url');
    let keySearch = $(this).val();
    let keyCode = event.keyCode;
    if (keySearch.trim()) {
        if (keyCode != 38 && keyCode != 40 && keyCode != 13) {
            axios.post(url, {search: keySearch})
                .then(function (response) {
                    let data_search = response.data.data_search;
                    if (data_search) {
                        if (keyCode != 13) {
                            search_dropdown.html(data_search).addClass('d-block');
                            counting = 0;
                        }
                    } else {
                        search_dropdown.html('').removeClass('d-block');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            selectedKeywordSuggest(keyCode);
            if (keyCode == 13) {
                getKeywords();
            }
        }
    } else {
        search_dropdown.html('').removeClass('d-block');
    }
}

function selectedKeywordSuggest(keyCode) {
    let search_dropdown = $('.filter--keywords--dropdown');
    let totalItemKeywords = search_dropdown.children().length;
    if (totalItemKeywords && keyCode == 40) {
        if (counting < totalItemKeywords) {
            counting++;
        } else {
            counting = 1;
        }
        search_dropdown.find('label').removeClass('focus').eq(counting - 1).addClass('focus');
    }
    if (totalItemKeywords && keyCode == 38) {
        if (counting <= 1) {
            counting = totalItemKeywords + 1;
        }
        if (counting > 1) {
            counting--;
        }
        search_dropdown.find('label').removeClass('focus').eq(counting - 1).addClass('focus');
    }
}

function getKeywords() {
    let itemKeywordFocus = $('.filter--keywords--dropdown').find('.focus');
    if (itemKeywordFocus.length) {
        itemKeywordFocus.click();
        destroyKeywordsSuggestDropdown();
    } else {
        let search_keyword = $('.filter--keywords--textbox');
        let keySearch = search_keyword.val();
        let keySearchDuplicate = $('.filter--keywords--searched[data-search="' + keySearch + '"]');
        if (keySearch && !keySearchDuplicate.length) {
            let labelTextSearched = '<label class="filter--keywords--searched" data-search="' + keySearch + '">' +
                '<input type="checkbox" name="title[]" value="' + keySearch + '" checked class="d-none">' +
                '<span>' + keySearch + '</span>' +
                '</label>';
            $('.selected-base--occupations-detail').append(labelTextSearched);
            destroyKeywordsSuggestDropdown();
            $('#search-complete').submit();
        } else {
            destroyKeywordsSuggestDropdown();
        }
    }
}

function destroyKeywordsSuggestDropdown() {
    if ($('.filter--keywords--textbox').val() || $('.filter--keywords--dropdown').is(':visible')) {
        $('.filter--keywords--textbox').val('');
        $('.filter--keywords--dropdown').html('').removeClass('d-block');
    }
}

function getKeywordsSuggest(event) {
    event.preventDefault();
    let item = $(this);
    let itemName = item.data('name');
    let itemId = item.data('id');
    let itemFor = item.attr('for');
    let itemCheckbox = $('#' + itemFor);

    if (itemCheckbox.length) {
        destroyKeywordsSuggestDropdown();
        if (!itemCheckbox.is(':checked')) {
            itemCheckbox.click();
        }
    } else {
        let labelOccupationDetailSelected = '<label for="' + itemFor + '">' + itemName + '</label>';
        let filterOccupationDetailItem = createHtmlForPositionDetailOccupation(itemId, itemName);
        $('.selected-base--occupations-detail').append(labelOccupationDetailSelected);
        $('.filter--occupation-detail').append(filterOccupationDetailItem);
        destroyKeywordsSuggestDropdown();
        $('#search-complete').submit();
    }
}

function createHtmlForPositionDetailOccupation(itemId, itemName) {
    let $positionDetailOccupation =
        `<label class="filter-checkbox-item">
            <input class="filter--occupation-detail--checkbox" data-id="${itemId}" data-name="${itemName}"
             type="checkbox" value="${itemId}" id="checkbox-occupationDetail-${itemId}" name="occupation_supporting_detail[]" checked>
            <span>${itemName}</span>
        </label>`;
    return $positionDetailOccupation;
}


function getOccupationDetail() {
    let occupationDetailThis = $(this);
    let occupationDetailName = $(this).data('name');
    let occupationDetailID = $(this).attr('id');
    let occupationDetailBase = $('.selected-base--occupations-detail');
    if (occupationDetailThis.is(':checked')) {
        let htmlBase = '<label for="' + occupationDetailID + '">' + occupationDetailName + '</label>'
        occupationDetailBase.append(htmlBase);
    } else {
        let thisBase = occupationDetailBase.find('label[for="' + occupationDetailID + '"]');
        if (thisBase.length) {
            thisBase.remove();
        }

    }
    $('#search-complete').submit();

}

function removeKeywordSearched() {
    $(this).remove();
    destroyKeywordsSuggestDropdown();
    $('#search-complete').submit();
}


export function addInputFilterToFormSubmit() {
    $(document).on('keypress', '.filter--keywords--textbox', (event) => {
        event.keyCode === 13 ? event.preventDefault() : ''
    });
    $(document).on('keyup', '.filter--keywords--textbox', changeKeywords);
    $(document).on('click', '.filter--keywords--dropdown label', getKeywordsSuggest);
    $(document).on('change', '.filter--occupation-detail--checkbox', getOccupationDetail);
    $(document).on('click', '.filter--keywords--searched', removeKeywordSearched);

}
