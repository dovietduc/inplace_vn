<?php

namespace App\Listeners;

use App\Events\CreatedCompany;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSlugCompanyUnique
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedCompany  $event
     * @return void
     */
    public function handle(CreatedCompany $event)
    {
        $customer = $event->customer;
        $customer->update(['slug' => $customer->slug . '-' . $customer->id]);
    }
}
