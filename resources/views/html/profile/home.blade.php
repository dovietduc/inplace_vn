@extends('layouts.app')
@section('meta')
    <title>Home Profile</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('pages/profile/home/home.css') }}">
@stop
@section('content')
    <div class="profile-pages">
        @include('html.profile.partials.profile-header-pages')
        <div class="profile-homePage">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 box_content mb-5 mb-md-0">
                        <div class="profile-homePage_box">
                            <div class="component-title">
                                <h3 class="title">
                                    Form ứng tuyển Job
                                </h3>
                                <div class="desc"></div>
                            </div>
                            @include ('html.profile.partials.form-applyJob')
                        </div>
                        <div class="profile-homePage_box">
                            <div class="component-title">
                                <h3 class="title">
                                    Thông tin liên hệ
                                </h3>
                                <div class="desc"></div>
                            </div>
                            <a href="#" class="view-more" data-toggle="modal" data-target="#popupEditContactInfo"><i class="fas fa-pencil-alt"></i> Chỉnh sửa</a>
                            <table class="component-info-company">
                                <tbody>
                                <tr>
                                    <td>Số điện thoại:</td>
                                    <td>092 345 6789</td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td>votranthoaithanh@inplace.vn</td>
                                </tr>
                                <tr>
                                    <td>Ngày sinh:</td>
                                    <td>05/09/1982</td>
                                </tr>
                                <tr>
                                    <td>Địa chỉ:</td>
                                    <td>82 Duy Tân, Cầu Giấy</td>
                                </tr>
                                <tr>
                                    <td>Tỉnh/Thành phố:</td>
                                    <td>Hà Nội</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4 box_sidebar">
                        @include ('html.profile.partials.profile-sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/moment-with-locales.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('vendor/croppie/croppie.js') }}"></script>
    <script src="{{ asset('pages/profile/profile.js') }}"></script>
    <script src="{{asset('pages/profile/home/home.js')}}"></script>
@stop
