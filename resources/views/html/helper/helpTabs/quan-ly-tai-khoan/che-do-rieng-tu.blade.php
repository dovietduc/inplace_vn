<div class="txt-format">
    <h1>Chế độ riêng tư</h1>
        <p>Bạn có
            thể cài đặt cho tài khoản của mình với các chế độ riêng tư như sau:</p>
        <ul>
            <li>Cho phép người lạ tìm kiếm hồ sơ của bạn hay không.</li>
            <li>Cho phép hệ thống thông báo cập nhật về bạn bè/công việc hay không.</li>
            <li>Có hiển thị các thông tin cá nhân hay không.</li>
            <li>Những ai có thể xem Profile của bạn.</li>
            <li>Những ai có thể gửi tin nhắn cho bạn.</li>
            <li>Block/chặn các thành viên khác.</li>
        </ul>
        <p><br>Để điều chỉnh chế độ riêng tư như mình muốn, bạn có thể làm các cách sau:</p>
        <ul>
            <li>Bước 1: Click vào Avatar bên góc phải của màn hình</li>
            <li>Bước 2: Trên Drop Down chọn <strong>Cài Đặt Tài Khoản</strong>.</li>
            <li>Bước 3: Chọn “<strong>Cài đặt riêng tư</strong>” ở menu dọc bên tay trái</li>
            <li>Bước 4: Tuỳ chỉnh chế độ riêng tư như bạn mong muốn</li>
            <li>Bước 5: Click vào “<strong>Lưu cài đặt</strong>”</li>
        </ul>
</div>
