import {dashboardLayout} from '../dashboard.js';
import {textareaAutoHeight} from '../../_elements/textareaAutoHeight.js';

const axios = require('axios');

function deleteMember() {
    let $modalDeleteWrapper = $('.modal-delete-member');
    let $memberItemWrapper = $(this).parents('.dashboard-member-item');
    $modalDeleteWrapper.find('.member-avatar').attr('src', $memberItemWrapper.find('.avatar img').attr('src'));
    $modalDeleteWrapper.find('.member-name').text($memberItemWrapper.find('.name').text());
    $modalDeleteWrapper.find('.position').text($memberItemWrapper.find('.desc').text());
    $modalDeleteWrapper.find('.member_delete_id').val($memberItemWrapper.data('id'));
    $modalDeleteWrapper.modal('show');
}


function editMember() {
    let url = $(this).data('url');
    axios.get(url)
        .then(function (response) {
            $('.modal-edit-member').html(response.data.member).modal('show');
            // handle success
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })

}

function updateMemberData() {
    let url = $(this).data('url');
    axios.post(url, {
        position: $("input[name='position']").val(),
        introduction: $("textarea[name='introduction']").val()
    })
        .then(function (response) {
            if (response.data.code == 200) {
                let meberItem = $('.member_item_' + response.data.member.id);
                meberItem.find('.position').text(response.data.member.position);
                meberItem.find('.introduction').html(response.data.member.introduction.replace(/\n/g, "<br />"));
                $('.modal-edit-member').modal('hide');
            }
        })
        .catch(function (error) {
            console.log(error);
        })
}


function deleteMemberConfirm() {
    let url = $(this).data('url');
    let that = $(this);
    let idMember = $("input[class='member_delete_id']").val();
    axios.post(url, {
        id: idMember
    })
        .then(function (response) {
            if (response.data.code == 200) {
                $('.member_item_' + idMember).remove();
                $('.modal-delete-member').modal('hide');
            }
            if (response.data.code == 400) {
               alert('Không thể delete thành viên này')
            }
        })
        .catch(function (error) {
            console.log(error);
        })
}

$(function () {
    dashboardLayout();
    textareaAutoHeight();

    $(document).on('click', '.btn-edit-member', editMember);
    $(document).on('click', '.edit_member_update', updateMemberData);
    $(document).on('click', '.btn-delete-member', deleteMember);
    $(document).on('click', '.btn-delete-member-confirm', deleteMemberConfirm);
});
