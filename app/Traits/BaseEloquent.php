<?php

namespace App\Traits;


trait BaseEloquent
{
    public function getAll()
    {
        return $this->resolveEntity()->all();
    }

    public function resolveEntity()
    {
        return app()->make(get_class($this));
    }

    public function findById($id)
    {
        return $this->resolveEntity()->find($id);
    }

    public function findWhere($column, $value)
    {
        return $this->resolveEntity()->where($column, $value)->get();
    }

    public function findWhereFirst($column, $value)
    {
        return $this->resolveEntity()->where($column, $value)->first();
    }

    public function pagination($perPage = 10)
    {
        return $this->resolveEntity()->paginate($perPage);
    }

    public function createData($data)
    {
        return $this->resolveEntity()->create($data);
    }

    public function updateData($id, array $data)
    {
        return $this->resolveEntity()->find($id)->update($data);
    }

    public function deleteData($id)
    {
        return $this->resolveEntity()->find($id)->delete();
    }
    public function deleteDataWhere($data = array())
    {
        return $this->resolveEntity()->where($data)->delete();
    }

    public function getByMultiConditionsModel($whereData = array(), $select = array("*"))
    {
        return $this->resolveEntity()
            ->select($select)
            ->where($whereData)
            ->latest()
            ->get();
    }

    public function getManyWhereOrderFirst($whereData = array(), $select = array("*"))
    {

        return $this->resolveEntity()
            ->where($whereData)
            ->select($select)
            ->first();
    }

    public function getManyWhereOrderLimitNumber($whereData = array(), $limit = 5, $select = array("*"))
    {

        return $this->resolveEntity()
            ->where($whereData)
            ->select($select)
            ->latest()
            ->limit($limit)
            ->get();
    }
    public function getManyWhereOrderPagination($whereData = array(), $select = array("*"), $perpage = 10)
    {
        return $this->resolveEntity()
            ->where($whereData)
            ->select($select)
            ->latest()
            ->paginate($perpage);
    }
    public function countByMultiConditionsModel($whereData = array())
    {
        return $this->resolveEntity()
            ->where($whereData)
            ->count();
    }

    public function getManyWhereOrderRandomNumber($whereData = array(), $limit = 5, $select = array("*"))
    {

        return $this->resolveEntity()
            ->where($whereData)
            ->inRandomOrder()
            ->limit($limit)
            ->select($select)
            ->get();
    }

    public function getManyWhereOrderLimit($whereData = array(), $limit = 5, $select = array("*"), $orderBy = "id desc")
    {

        return $this->resolveEntity()
            ->where($whereData)
            ->select($select)
            ->orderByRaw($orderBy)
            ->limit($limit)
            ->get();
    }



}
