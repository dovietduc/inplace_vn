@extends('layouts.app')
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/companies/create/create.css') }}">
@stop
@section('content')
    <div class="company-create-page">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                    <div class="company-create-content">
                        <div class="company-create-head">
                            <div class="company-create-head_icon">
                                <img
                                    src="{{asset('images/icon-building.png')}}"
                                    alt="">
                            </div>
                            <div class="company-create-head_content">
                                <h1 class="title">Tạo trang doanh nghiệp</h1>
                                <p class="desc">Hãy bắt đầu với một vài chi tiết về doanh nghiệp của bạn.</p>
                            </div>
                        </div>
                        <div class="company-create-form">
                            <form class="formCreateCompany" action="{{ route('company.postCreate') }}"
                                  method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="">Tên công ty <span class="color-theme">*</span></label>
                                    <input
                                        type="text"
                                        placeholder="Tên công ty"
                                        class="inputName form-control"
                                        name="name"
                                        required>
                                    <span class="err-name err-inp"></span>
                                </div>
                                <div class="form-group">
                                    <label for="">Website</label>
                                    <input
                                        type="text"
                                        value=""
                                        placeholder="Website công ty"
                                        name="website"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Email <span class="color-theme">*</span></label>
                                    <input
                                        type="email"
                                        value=""
                                        placeholder="Email Công ty"
                                        class="inputEmail form-control"
                                        name="email"
                                        required>
                                    <span class="err-email err-inp"></span>
                                </div>
                                <div class="form-group">
                                    <label for="">Số điện thoại <span class="color-theme">*</span></label>
                                    <input
                                        type="tel"
                                        value=""
                                        placeholder="Số điện thoại"
                                        class="inputTel form-control"
                                        name="phone"
                                        pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+" required>
                                    <span class="err-tel err-inp"></span>
                                </div>
                                <div class="form-group">
                                    <label for="">Quy mô doanh nghiệp</label>
                                    <select class="select-2-nosearch" name="company_size_id">
                                        @foreach($companySizes as $size)
                                            <option value="{{ $size->id }}">
                                                {{ $size->size }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Lĩnh vực hoạt động</label>
                                    <select class="select-2-multiple" name="industry[]" multiple>
                                        @foreach($industrys as $industry)
                                            <option value="{{ $industry->id }}">
                                                {{ $industry->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Khẩu hiệu</label>
                                    <textarea
                                        class="form-control"
                                        row="5"
                                        name="slogan"
                                        placeholder="Example: A family-run accounting firm that promises you won’t lose sleep over filing your taxes."
                                    ></textarea>
                                    <div class="form-policy">
                                        <span class="color-theme">*</span> Bằng việc click vào nút Tạo trang, bạn xác
                                        minh rằng bạn là đại diện được ủy quyền của tổ chức này và có quyền hành động
                                        thay mặt tổ chức trong việc tạo, quản lý trang này. Bạn và tổ chức đồng ý với
                                        các
                                        <a href="#" target="_blank"><span class="color-theme">Điều khoản sử dụng</span></a>
                                        và <a href="#" target="_blank"><span class="color-theme">Chính sách quyền riêng tư</span></a>.
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-custom">Tạo trang</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade remove-modal" tabindex="-1" role="dialog" id="cropperModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-positioner">
                    <h1>Crop Photo</h1>
                    <hr>
                    <img style="width: 271px; height: 271px;" class="js-avatar-preview" src="">
                    <button class="btn btn-primary js-save-cropped-avatar">Save</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('pages/companies/create/create.js') }}"></script>

@stop
