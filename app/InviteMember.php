<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class InviteMember extends Model
{
    protected $table = 'invite_member_by_email';
    protected $guarded = [];
    use BaseEloquent;

    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
