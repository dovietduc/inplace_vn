<!DOCTYPE html>
<html lang="vi" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job lists</title>
    <link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/select2/select2.min.css" rel="stylesheet"/>
    <link href="/vendor/slick/slick.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/slick/slick-theme.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/pages/job/job.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

@component('html/element/header')
@endcomponent

<div class="main">
    <div class="block-slider">
        <section class="js-slideJob job-slider">
            <div class="job-item">
                <div class="detail">
                    <h2>
                        <a href="#">Gia nhập J-Job để trở thành Recruitment Consultant chuyên nghiệp!</a>
                    </h2>
                    <div class="company">
                        <a href="#">
                            {{-- Default image when image's source is wrong --}}
                            <div class="avatar">
                                <img src="/images/img_no_avatar.png" alt="">
                            </div>
                            <h5>J-JOB Executive Search</h5>
                        </a>
                    </div>
                </div>
                <img class="img-bg" src="/images/img_slider_1.png" alt="">
            </div>
            <div class="job-item">
                <div class="detail">
                    <h2>
                        <a href="#">Gia nhập J-Job để trở thành Recruitment Consultant chuyên nghiệp!</a>
                    </h2>
                    <div class="company">
                        <a href="#">
                            <div class="avatar">
                                <img src="/images/img_avatar_jjob.png" alt="">
                            </div>
                            <h5>J-JOB Executive Search</h5>
                        </a>
                    </div>
                </div>
                <img class="img-bg" src="/images/img_slider_2.png" alt="">
            </div>
            <div class="job-item">
                <div class="detail">
                    <h2>
                        <a href="#">Gia nhập J-Job để trở thành Recruitment Consultant chuyên nghiệp!</a>
                    </h2>
                    <div class="company">
                        <a href="#">
                            <div class="avatar">
                                <img src="/images/img_avatar_jjob.png" alt="">
                            </div>
                            <h5>J-JOB Executive Search</h5>
                        </a>
                    </div>
                </div>
                <img class="img-bg" src="/images/img_slider_3.png" alt>
            </div>
            <div class="job-item">
                <div class="detail">
                    <h2>
                        <a href="#">Gia nhập J-Job để trở thành Recruitment Consultant chuyên nghiệp!</a>
                    </h2>
                    <div class="company">
                        <a href="#">
                            <div class="avatar">
                                <img src="/images/img_avatar_jjob.png" alt="">
                            </div>
                            <h5>J-JOB Executive Search</h5>
                        </a>
                    </div>
                </div>
                <img class="img-bg" src="/images/img_slider_1.png" alt="">
            </div>
            <div class="job-item">
                <div class="detail">
                    <h2>
                        <a href="#">Gia nhập J-Job để trở thành Recruitment Consultant chuyên nghiệp!</a>
                    </h2>
                    <div class="company">
                        <a href="#">
                            <div class="avatar">
                                <img src="/images/img_avatar_jjob.png" alt="">
                            </div>
                            <h5>J-JOB Executive Search</h5>
                        </a>
                    </div>
                </div>
                <img class="img-bg" src="/images/img_slider_2.png" alt="">
            </div>
        </section>
    </div>
    <div class="block-list">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="block-side">
                        <div class="block-filter">
                            <div class="search">
                                <p class="title">
                                    <i class="fas fa-filter"></i>
                                    <span>Tìm kiếm theo</span>
                                </p>

                                <form class="search-form">
                                    <div class="search-item">
                                        <div class="search-input">
                                            <button><i class="fas fa-search"></i></button>
                                            <input type="text" placeholder="Nhập tên job, tên công ty">
                                        </div>
                                    </div>

                                    <label class="select-box">
                                        <select class="js-select-box">
                                            <option value="">Thành phố</option>
                                            <option value="67">Hà Nội</option>
                                            <option value="66">Hồ Chí Minh</option>
                                            <option value="65">Đà Nẵng</option>
                                            <option value="57">Đồng Nai</option>
                                            <option value="54">Yên Bái</option>
                                            <option value="56">Đắk Lắk</option>
                                            <option value="58">Đồng Tháp</option>
                                            <option value="59">Bạc Liêu</option>
                                            <option value="60">Sóc Trăng</option>
                                            <option value="61">Hậu Giang</option>
                                        </select>
                                    </label>

                                    <label class="select-box">
                                        <select class="js-select-box">
                                            <option value="">Ngành nghề</option>
                                            <option value="90">Kỹ sư</option>
                                            <option value="91">Thiết kế</option>
                                            <option value="92">Hành chính nhân sự</option>
                                            <option value="93">Quản lý</option>
                                            <option value="94">Sales</option>
                                            <option value="95">Marketing</option>
                                            <option value="96">Kế toán / Tài chính</option>
                                            <option value="97">Khác</option>
                                        </select>
                                    </label>

                                    <label class="select-box">
                                        <select class="js-select-box">
                                            <option value="">Cấp bậc</option>
                                            <option value="1">Nhân viên</option>
                                            <option value="2">Trưởng phòng</option>
                                            <option value="3">Giám đốc hoặc cấp cao hơn</option>
                                        </select>
                                    </label>
                                </form>
                            </div>
                            <div class="keyword">
                                <p class="title">Chủ đề/Từ khóa</p>
                                <ul class="keyword-list">
                                    <li class="item"><a href="#">Marketing</a></li>
                                    <li class="item"><a href="#">Designer</a></li>
                                    <li class="item"><a href="#">Sales</a></li>
                                    <li class="item"><a href="#">PHP Developer</a></li>
                                    <li class="item"><a href="#">Mobile Developer</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="block-main list-job-content ml-auto" id="list_job_content">
                        <div class="block-top">
                            <div class="job-found">
                                <b>1-10</b>
                                <span>/</span>
                                <span>6829</span>
                            </div>
                            <div class="job-tag ml-auto">
                                <a href="#" class="tag-item active">Đề xuất cho bạn</a>
                                <a href="#" class="tag-item">Mới nhất</a>
                                <a href="#" class="tag-item">Phổ biển nhất</a>
                            </div>
                        </div>
                        <div class="block-post">
                            <div class="post-item">
                                <div class="post-company">
                                    <a href="#" class="d-inline-block">
                                        <div class="wrapper">
                                            {{-- Default image when image's source is wrong --}}
                                            <div class="avatar">
                                                <img src="/images/img_no_avatar.png" alt="">
                                            </div>
                                        </div>
                                        <div class="info">
                                            <h5>J-JOB Executive Search</h5>
                                            <p>2899 Fan</p>
                                        </div>
                                    </a>
                                </div>
                                {{-- Default image when image's source is wrong --}}
                                <div class="post-thumb">
                                    <a href="#" class="thumb-img">
                                        <img src="/images/img_thumb_noimage.png" alt="">
                                    </a>
                                </div>
                                <div class="tag-list">
                                    <span class="tag-item">Marketing Executive</span>
                                    <span class="tag-item">Digital</span>
                                </div>
                                <div class="post-content">
                                    <h2 class="title">
                                        <a href="#">Digital Marketing Executive WANTED!</a>
                                    </h2>
                                    <p class="content">We’re looking for a thoughtful, extremely talented Product
                                        Designer to join our team. Our aim is to deliver a fun, no-pressure dating
                                        experience that leads the dating scenes in Asia and beyond.
                                        Responsibilities: - Brainstorm for new features with our Product Manager and
                                        Head of Design ...</p>
                                </div>
                                <div class="post-option">
                                    <div class="post-info">
                                        <span class="item item-location"><i class="far fa-map"></i>Hà Nội</span>
                                        <span class="item item-time"><i class="far fa-clock"></i>1 giờ trước</span>
                                    </div>
                                    <div class="post-social ml-auto">
                                        <a href="#" class="item item-share">
                                            <i class="fas fa-share"></i>
                                        </a>
                                        <a href="#" class="item item-bookmark">
                                            <i class="fas fa-bookmark"></i>
                                        </a>
                                        <a href="#" class="item item-sent activ">
                                            <i class="fas fa-paper-plane"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!-- .post-item -->

                            <div class="post-item">
                                <div class="post-company">
                                    <a href="#" class="d-inline-block">
                                        <div class="wrapper">
                                            <div class="avatar">
                                                <img src="/images/img_avatar_2.png" alt="">
                                            </div>
                                        </div>
                                        <div class="info">
                                            <h5>FLAP Inc</h5>
                                            <p>2899 Fan</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-thumb">
                                    <a href="#" class="thumb-img">
                                        <img src="/images/img_thumb_2.png" alt="">
                                    </a>
                                </div>
                                <div class="tag-list">
                                    <span class="tag-item">Designer</span>
                                    <span class="tag-item">Branding</span>
                                </div>
                                <div class="post-content">
                                    <h2 class="title">
                                        <a href="#">Tuyển dụng Nhân viên thiết kế làm branding cho khách hàng lớn thị
                                            trường châu Âu</a>
                                    </h2>
                                    <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những
                                        khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có
                                        thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh
                                        mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức ...</p>
                                </div>
                                <div class="post-option">
                                    <div class="post-info">
                                        <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>
                                        <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>
                                    </div>
                                    <div class="post-social ml-auto">
                                        <a href="#" class="item item-share">
                                            <i class="fas fa-share"></i>
                                        </a>
                                        <a href="#" class="item item-bookmark">
                                            <i class="fas fa-bookmark"></i>
                                        </a>
                                        <a href="#" class="item item-sent">
                                            <i class="fas fa-paper-plane"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!-- .post-item -->

                            <div class="post-item">
                                <div class="post-company">
                                    <a href="#" class="d-inline-block">
                                        <div class="wrapper">
                                            <div class="avatar">
                                                <img src="/images/img_avatar_3.png" alt="">
                                            </div>
                                        </div>
                                        <div class="info">
                                            <h5>SGAG Studio</h5>
                                            <p>2899 Fan</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-thumb">
                                    <a href="#" class="thumb-img">
                                        <img src="/images/img_thumb_5.png" alt="">
                                    </a>
                                </div>
                                <div class="tag-list">
                                    <span class="tag-item">Photography</span>
                                    <span class="tag-item">Graphic Designer</span>
                                </div>
                                <div class="post-content">
                                    <h2 class="title">
                                        <a href="#">Bạn đam mê nhiếp ảnh? Đây là cơ hội để bạn chạm vào niềm đam mê của
                                            mình với vị trí Photography Intern</a>
                                    </h2>
                                    <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những
                                        khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có
                                        thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh
                                        mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức ...</p>
                                </div>
                                <div class="post-option">
                                    <div class="post-info">
                                        <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>
                                        <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>
                                    </div>
                                    <div class="post-social ml-auto">
                                        <a href="#" class="item item-share">
                                            <i class="fas fa-share"></i>
                                        </a>
                                        <a href="#" class="item item-bookmark">
                                            <i class="fas fa-bookmark"></i>
                                        </a>
                                        <a href="#" class="item item-sent">
                                            <i class="fas fa-paper-plane"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!-- .post-item -->

                            <div class="post-item">
                                <div class="post-company">
                                    <a href="#">
                                        <div class="wrapper">
                                            <div class="avatar">
                                                <img src="/images/img_avatar_2.png" alt="">
                                            </div>
                                        </div>
                                        <div class="info">
                                            <h5>FLAP Inc</h5>
                                            <p>2899 Fan</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="post-thumb">
                                    <a href="#" class="thumb-img">
                                        <img src="/images/img_thumb_4.png" alt="">
                                    </a>
                                </div>
                                <div class="tag-list">
                                    <span class="tag-item">Designer</span>
                                    <span class="tag-item">Branding</span>
                                </div>
                                <div class="post-content">
                                    <h2 class="title">
                                        <a href="#">Tuyển dụng Nhân viên thiết kế làm branding cho khách hàng lớn thị
                                            trường châu Âu</a>
                                    </h2>
                                    <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những
                                        khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có
                                        thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh
                                        mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức ...</p>
                                </div>
                                <div class="post-option">
                                    <div class="post-info">
                                        <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>
                                        <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>
                                    </div>
                                    <div class="post-social ml-auto">
                                        <a href="#" class="item item-share">
                                            <i class="fas fa-share"></i>
                                        </a>
                                        <a href="#" class="item item-bookmark">
                                            <i class="fas fa-bookmark"></i>
                                        </a>
                                        <a href="#" class="item item-sent">
                                            <i class="fas fa-paper-plane"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!-- .post-item -->

                            <div class="block-pagination">
                                <ul class="pagination">
                                    <li class="pagination-item pre"><a href="#"><i class="far fa-arrow-left"></i></a>
                                    </li>
                                    <li class="pagination-item"><a href="#">1</a></li>
                                    <li class="pagination-item active"><a href="#">2</a></li>
                                    <li class="pagination-item"><a href="#">3</a></li>
                                    <li class="pagination-item"><a href="#">4</a></li>
                                    <li class="pagination-item"><a href="#">...</a></li>
                                    <li class="pagination-item next"><a href="#"><i class="far fa-arrow-right"></i></a>
                                    </li>
                                </ul>
                            </div><!-- .block-pagination -->
                        </div><!-- .block-post -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="main-wrapper">--}}
    {{--        <div class="block-side">--}}
    {{--            <div class="js-filter fixed-when-scroll">--}}
    {{--                <div class="block-filter">--}}
    {{--                    <div class="search">--}}
    {{--                        <p class="title">--}}
    {{--                            <i class="fas fa-filter"></i>--}}
    {{--                            <span>Tìm kiếm theo</span>--}}
    {{--                        </p>--}}

    {{--                        <form class="search-form">--}}
    {{--                            <div class="search-item">--}}
    {{--                                <div class="search-input">--}}
    {{--                                    <button><i class="fas fa-search"></i></button>--}}
    {{--                                    <input type="text" placeholder="Nhập tên job, tên công ty">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <label class="select-box">--}}
    {{--                                <select class="js-select-box">--}}
    {{--                                    <option value="">Thành phố</option>--}}
    {{--                                    <option value="67">Hà Nội</option>--}}
    {{--                                    <option value="66">Hồ Chí Minh</option>--}}
    {{--                                    <option value="65">Đà Nẵng</option>--}}
    {{--                                    <option value="57">Đồng Nai</option>--}}
    {{--                                    <option value="54">Yên Bái</option>--}}
    {{--                                    <option value="56">Đắk Lắk</option>--}}
    {{--                                    <option value="58">Đồng Tháp</option>--}}
    {{--                                    <option value="59">Bạc Liêu</option>--}}
    {{--                                    <option value="60">Sóc Trăng</option>--}}
    {{--                                    <option value="61">Hậu Giang</option>--}}
    {{--                                </select>--}}
    {{--                            </label>--}}

    {{--                            <label class="select-box">--}}
    {{--                                <select class="js-select-box">--}}
    {{--                                    <option value="">Ngành nghề</option>--}}
    {{--                                    <option value="90">Kỹ sư</option>--}}
    {{--                                    <option value="91">Thiết kế</option>--}}
    {{--                                    <option value="92">Hành chính nhân sự</option>--}}
    {{--                                    <option value="93">Quản lý</option>--}}
    {{--                                    <option value="94">Sales</option>--}}
    {{--                                    <option value="95">Marketing</option>--}}
    {{--                                    <option value="96">Kế toán / Tài chính</option>--}}
    {{--                                    <option value="97">Khác</option>--}}
    {{--                                </select>--}}
    {{--                            </label>--}}

    {{--                            <label class="select-box">--}}
    {{--                                <select class="js-select-box">--}}
    {{--                                    <option value="" >Cấp bậc</option>--}}
    {{--                                    <option value="1">Nhân viên</option>--}}
    {{--                                    <option value="2">Trưởng phòng</option>--}}
    {{--                                    <option value="3">Giám đốc hoặc cấp cao hơn</option>--}}
    {{--                                </select>--}}
    {{--                            </label>--}}
    {{--                        </form>--}}
    {{--                    </div>--}}
    {{--                    <div class="keyword">--}}
    {{--                        <p class="title">Chủ đề/Từ khóa</p>--}}
    {{--                        <ul class="keyword-list">--}}
    {{--                            <li class="item"><a href="#">Marketing</a></li>--}}
    {{--                            <li class="item"><a href="#">Designer</a></li>--}}
    {{--                            <li class="item"><a href="#">Sales</a></li>--}}
    {{--                            <li class="item"><a href="#">PHP Developer</a></li>--}}
    {{--                            <li class="item"><a href="#">Mobile Developer</a></li>--}}
    {{--                        </ul>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}

    {{--        </div><!-- .block-side -->--}}
    {{--        <div class="block-main list-job-content ml-auto" id="list_job_content">--}}
    {{--            <div class="block-top">--}}
    {{--                <div class="job-found">--}}
    {{--                    <b>1-10</b>--}}
    {{--                    <span>/</span>--}}
    {{--                    <span>6829</span>--}}
    {{--                </div>--}}
    {{--                <div class="job-tag ml-auto">--}}
    {{--                    <a href="#" class="tag-item active">Đề xuất cho bạn</a>--}}
    {{--                    <a href="#" class="tag-item">Mới nhất</a>--}}
    {{--                    <a href="#" class="tag-item">Phổ biển nhất</a>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="block-post">--}}
    {{--                <div class="post-item">--}}
    {{--                    <div class="post-company">--}}
    {{--                        <a href="#" class="d-inline-block">--}}
    {{--                            <div class="wrapper">--}}
    {{--                                --}}{{-- Default image when image's source is wrong --}}
    {{--                                <div class="avatar">--}}
    {{--                                    <img src="/images/img_no_avatar.png" alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="info">--}}
    {{--                                <h5>J-JOB Executive Search</h5>--}}
    {{--                                <p>2899 Fan</p>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    --}}{{-- Default image when image's source is wrong --}}
    {{--                    <div class="post-thumb">--}}
    {{--                        <a href="#" class="thumb-img">--}}
    {{--                            <img src="/images/img_thumb_noimage.png" alt="">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="tag-list">--}}
    {{--                        <span class="tag-item">Marketing Executive</span>--}}
    {{--                        <span class="tag-item">Digital</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-content">--}}
    {{--                        <h2 class="title">--}}
    {{--                            <a href="#">Digital Marketing Executive WANTED!</a>--}}
    {{--                        </h2>--}}
    {{--                        <p class="content">We’re looking for a thoughtful, extremely talented Product Designer to join our team. Our aim is to deliver a fun, no-pressure dating experience that leads the dating scenes in Asia and beyond.--}}
    {{--                            Responsibilities:  - Brainstorm for new features with our Product Manager and Head of Design ...</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-option">--}}
    {{--                        <div class="post-info">--}}
    {{--                            <span class="item item-location"><i class="far fa-map"></i>Hà Nội</span>--}}
    {{--                            <span class="item item-time"><i class="far fa-clock"></i>1 giờ trước</span>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-social ml-auto">--}}
    {{--                            <a href="#" class="item item-share">--}}
    {{--                                <i class="fas fa-share"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-bookmark">--}}
    {{--                                <i class="fas fa-bookmark"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-sent activ">--}}
    {{--                                <i class="fas fa-paper-plane"></i>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div><!-- .post-item -->--}}

    {{--                <div class="post-item">--}}
    {{--                    <div class="post-company">--}}
    {{--                        <a href="#" class="d-inline-block">--}}
    {{--                            <div class="wrapper">--}}
    {{--                                <div class="avatar">--}}
    {{--                                    <img src="/images/img_avatar_2.png" alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="info">--}}
    {{--                                <h5>FLAP Inc</h5>--}}
    {{--                                <p>2899 Fan</p>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-thumb">--}}
    {{--                        <a href="#" class="thumb-img">--}}
    {{--                            <img src="/images/img_thumb_2.png" alt="">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="tag-list">--}}
    {{--                        <span class="tag-item">Designer</span>--}}
    {{--                        <span class="tag-item">Branding</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-content">--}}
    {{--                        <h2 class="title">--}}
    {{--                            <a href="#">Tuyển dụng Nhân viên thiết kế làm branding cho khách hàng lớn thị trường châu Âu</a>--}}
    {{--                        </h2>--}}
    {{--                        <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức  ...</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-option">--}}
    {{--                        <div class="post-info">--}}
    {{--                            <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>--}}
    {{--                            <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-social ml-auto">--}}
    {{--                            <a href="#" class="item item-share">--}}
    {{--                                <i class="fas fa-share"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-bookmark">--}}
    {{--                                <i class="fas fa-bookmark"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-sent">--}}
    {{--                                <i class="fas fa-paper-plane"></i>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div><!-- .post-item -->--}}

    {{--                <div class="post-item">--}}
    {{--                    <div class="post-company">--}}
    {{--                        <a href="#" class="d-inline-block">--}}
    {{--                            <div class="wrapper">--}}
    {{--                                <div class="avatar">--}}
    {{--                                    <img src="/images/img_avatar_3.png" alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="info">--}}
    {{--                                <h5>SGAG Studio</h5>--}}
    {{--                                <p>2899 Fan</p>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-thumb">--}}
    {{--                        <a href="#" class="thumb-img">--}}
    {{--                            <img src="/images/img_thumb_5.png" alt="">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="tag-list">--}}
    {{--                        <span class="tag-item">Photography</span>--}}
    {{--                        <span class="tag-item">Graphic Designer</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-content">--}}
    {{--                        <h2 class="title">--}}
    {{--                            <a href="#">Bạn đam mê nhiếp ảnh? Đây là cơ hội để bạn chạm vào niềm đam mê của mình với vị trí Photography Intern</a>--}}
    {{--                        </h2>--}}
    {{--                        <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức  ...</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-option">--}}
    {{--                        <div class="post-info">--}}
    {{--                            <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>--}}
    {{--                            <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-social ml-auto">--}}
    {{--                            <a href="#" class="item item-share">--}}
    {{--                                <i class="fas fa-share"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-bookmark">--}}
    {{--                                <i class="fas fa-bookmark"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-sent">--}}
    {{--                                <i class="fas fa-paper-plane"></i>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div><!-- .post-item -->--}}

    {{--                <div class="post-item">--}}
    {{--                    <div class="post-company">--}}
    {{--                        <a href="#">--}}
    {{--                            <div class="wrapper">--}}
    {{--                                <div class="avatar">--}}
    {{--                                    <img src="/images/img_avatar_2.png" alt="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="info">--}}
    {{--                                <h5>FLAP Inc</h5>--}}
    {{--                                <p>2899 Fan</p>--}}
    {{--                            </div>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-thumb">--}}
    {{--                        <a href="#" class="thumb-img">--}}
    {{--                            <img src="/images/img_thumb_4.png" alt="">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="tag-list">--}}
    {{--                        <span class="tag-item">Designer</span>--}}
    {{--                        <span class="tag-item">Branding</span>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-content">--}}
    {{--                        <h2 class="title">--}}
    {{--                            <a href="#">Tuyển dụng Nhân viên thiết kế làm branding cho khách hàng lớn thị trường châu Âu</a>--}}
    {{--                        </h2>--}}
    {{--                        <p class="content">Chúng tôi đang tìm kiếm một nhiếp ảnh gia đam mê để ghi lại những khoảnh khắc trên phim và sử dụng hình ảnh để kể một câu chuyện. Yêu cầu: - Có thể cam kết trong thời gian tối thiểu 3 tháng - Đam mê chụp ảnh chân dung - Danh mục bắt mắt - Thành thạo với thiết bị chụp ảnh hiện đại - Kiến thức  ...</p>--}}
    {{--                    </div>--}}
    {{--                    <div class="post-option">--}}
    {{--                        <div class="post-info">--}}
    {{--                            <span class="item item-location"><i class="far fa-map"></i>Đà Nẵng</span>--}}
    {{--                            <span class="item item-time"><i class="far fa-clock"></i>2 giờ trước</span>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-social ml-auto">--}}
    {{--                            <a href="#" class="item item-share">--}}
    {{--                                <i class="fas fa-share"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-bookmark">--}}
    {{--                                <i class="fas fa-bookmark"></i>--}}
    {{--                            </a>--}}
    {{--                            <a href="#" class="item item-sent">--}}
    {{--                                <i class="fas fa-paper-plane"></i>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div><!-- .post-item -->--}}

    {{--                <div class="block-pagination">--}}
    {{--                    <ul class="pagination">--}}
    {{--                        <li class="pagination-item pre"><a href="#"><i class="far fa-arrow-left"></i></a></li>--}}
    {{--                        <li class="pagination-item"><a href="#">1</a></li>--}}
    {{--                        <li class="pagination-item active"><a href="#">2</a></li>--}}
    {{--                        <li class="pagination-item"><a href="#">3</a></li>--}}
    {{--                        <li class="pagination-item"><a href="#">4</a></li>--}}
    {{--                        <li class="pagination-item"><a href="#">...</a></li>--}}
    {{--                        <li class="pagination-item next"><a href="#"><i class="far fa-arrow-right"></i></a></li>--}}
    {{--                    </ul>--}}
    {{--                </div><!-- .block-pagination -->--}}
    {{--            </div><!-- .block-post -->--}}
    {{--        </div><!-- .block-main-->--}}
    {{--    </div>--}}
</div>

@component('html/element/footer')
@endcomponent

<script src="/vendor/slick/slick.min.js"></script>
<script src="/vendor/select2/select2.min.js"></script>
<script src="/pages/common/main.js"></script>
<script src="/pages/job/job.js"></script>
</body>
</html>
