const axios = require('axios');
import {sideBarFilterJob} from "../_elements/side-bar-filter-job";

function updateOccupationToUserSubmitForm(event) {
    event.preventDefault();
    let form = $(this);
    let url = form.data('url');
    let err = form.find('.err-inp');
    if (!$('.list-choose-industry input[type="radio"]:checked').length){
        err.addClass('d-block');
    }else {
        axios.post(url, form.serialize())
            .then(function (response) {
                if (response.status == 200) {
                    location.reload();
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}
function removeErrorUpdateOccupationToUserSubmitForm() {
    let err = $('.occupation_update_user_form .err-inp');
    err.removeClass('d-block');
}

($)(function () {
    // slider Hot Job
    if ($('.js-slideJob').children().length) {
        $('.js-slideJob').slick({
            centerMode: true,
            centerPadding: 'calc(50vw - 498.5px)',
            infinite: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 970,
                    settings: {
                        dots: false,
                        centerPadding: 0,
                    }
                },
                {
                    breakpoint: 678,
                    settings: {
                        dots: false,
                        arrows: false,
                        centerPadding: 0,
                    }
                },
            ]
        });
    }else {
        $('.block-slider').hide();
    }

    //modal Choose Occupations
    if ($('#modalChooseIndustry').length) {
        $('body').addClass('modal-open');
    }
    $(document).on('submit', '.occupation_update_user_form', updateOccupationToUserSubmitForm);
    $(document).on('change', '.list-choose-industry input[type="radio"]', removeErrorUpdateOccupationToUserSubmitForm);

    //ajax loading
    $(document).ajaxStart(function () {
        $('.block-main').addClass('ajax-loading');
    }).ajaxStop(function () {
        $('.block-main').removeClass('ajax-loading');
    });

    //FILTER JOBS
    sideBarFilterJob();

});
