<?php

namespace App\Listeners;

use App\Events\PageNewsShowLoad;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Session\Store;

class UpdateViewsCountForNews
{
    private $session;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle the event.
     *
     * @param  PageNewsShowLoad  $event
     * @return void
     */
    public function handle(PageNewsShowLoad $event)
    {
        $news = $event->news;
        if ( !$this->isJobViewed($news) )
        {
            $news->increment('view_count');
            $this->storePost($news);
        }
    }
    private function isJobViewed($news)
    {
        $viewed = $this->session->get('viewed_news', []);
        return array_key_exists($news->id, $viewed);
    }

    private function storePost($news)
    {
        $key = 'viewed_news.' . $news->id;
        $this->session->put($key, time());
    }
}
