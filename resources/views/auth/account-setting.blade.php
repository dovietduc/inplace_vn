@extends('layouts.app')
@section('meta')
    <title>Cài đặt tài khoản</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/users/account-setting/account-setting.css')}}">
@stop
@section('content')
    <div class="account-setting-page">
        <div class="container">
            <div class="row">
                <div class="col-md-4 box_sidebar">
                    <div class="account-setting-sidebar">
                        <div class="account-setting-sidebar_widget">
                            <h6>Cài đặt thông báo</h6>
                            <nav class="account-setting-navigation">
                                <a href="#">Thông báo qua email</a>
                            </nav>
                        </div>
                        <div class="account-setting-sidebar_widget">
                            <h6>Cài đặt tài khoản</h6>
                            <nav class="account-setting-navigation account-setting-navigation-scroll" id="mainNav">
                                <a href="#1">Ngôn ngữ và khu vực</a>
                                <a href="#2">Cài đặt riêng tư</a>
                                <a href="#3">Đổi mật khẩu</a>
                                <a href="#4">Thêm email</a>
                                <a href="#5">Xóa tài khoản</a>
                            </nav>
                        </div>
                        <div class="account-setting-sidebar_widget">
                            <h6>Cài đặt khác</h6>
                        </div>
                    </div>

                </div>
                <div class="col-md-8 box_content">
                    <div class="account-setting-section hero" id="1">
                        <h3 class="title-section">Ngôn ngữ và khu vực</h3>
                    </div>
                    <div class="account-setting-section" id="2">
                        <h1 class="title-section">Cài đặt riêng tư</h1>
                    </div>
                    <div class="account-setting-section" id="3">
                        <h1 class="title-section">Đổi mật khẩu</h1>
                    </div>
                    <div class="account-setting-section" id="4">
                        <h1 class="title-section">Thêm email</h1>
                    </div>
                    <div class="account-setting-section" id="5">
                        <h1 class="title-section">Xóa tài khoản</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/users/account-setting/account-setting.js')}}"></script>
@stop
