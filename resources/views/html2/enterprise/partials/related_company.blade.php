<div class="related-companies">
    <div class="container">
        <h6 class="related-companies_title">
            Có thể bạn quan tâm
        </h6>
        <div class="row">
            @for ($i = 0; $i < 3; $i++)
                <div class="col-12 col-lg-4 mb-4 mb-lg-0">
                    <div class="component-company-small">
                        <a href="#" title="">
                            <div class="company-small_thumb">
                                <img src="/images/no_img_350x137.jpg" alt="" class="w-100 rounded-top">
                            </div>
                            <div class="company-small_content">
                                <img src="/images/no_img_50x50.jpg" alt="..."
                                     class="company-small_avatar">
                                <h5 class="company-small_name">DeNA Co., Ltd.,</h5>
                            </div>
                        </a>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</div>
