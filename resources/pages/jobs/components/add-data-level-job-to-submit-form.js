function getJobLevel(){
    let level = $(this);
    let levelName = level.data('name');
    let levelId = level.data('id');
    let levelText = level.parents('.filter-control').find('.filter-control__text');
    let levelPlaceholder = levelText.find('span').eq(0);
    if (level.is(":checked")){
        levelPlaceholder.addClass('d-none');
        levelText.append('<span class="txt" data-id="' + levelId + '">'+ levelName +'</span>');
    }else {
        levelText.find('.txt[data-id="' + levelId + '"]').remove();
        if (levelText.children().length < 2){
            levelPlaceholder.removeClass('d-none');
        }
    }
    $('#search-complete').submit();
}

export function addDataLevelJobToFormSubmit() {
   $(document).on('change', '.filter--job-level--checkbox', getJobLevel);
}
