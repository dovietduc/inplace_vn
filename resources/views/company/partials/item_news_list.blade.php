@foreach($news as $newItem)
    <div class="component-posts-item-large">
        <div class="posts-item-thumb">
            <a href="{{ route('news.show', ['slugCompany' => optional($newItem->customer)->slug, 'id' => $newItem->id]) }}"
               title="">
                @if( jjobCheckImageExit($newItem->thumbnail) )
                    <img src="{{ url($newItem->thumbnail) }}" alt="">
                @else
                    <img src="{{ asset('images/no_img_730x286.jpg') }}" alt="">
                @endif
            </a>
        </div>
        <h3 class="posts-item-title">
            <a href="{{ route('news.show', ['slugCompany' => optional($newItem->customer)->slug, 'id' => $newItem->id]) }}"
               title="" {{ $customer->id }}>
                {{ $newItem->title }}
            </a>
        </h3>
        <div class="posts-item-desc txt-format">
            {{ $newItem->description }}
        </div>
        <div class="posts-item-auth">
            <div class="posts-item-auth_info">
                <a href="#" title="" class="auth-avatar">
                    @if( jjobCheckImageExit(optional($newItem->user)->avatar) )
                        <img src="{{ url(optional($newItem->user)->avatar) }}"
                             alt="">
                    @else
                        <img src="{{ asset('/images/no_user_30x30.jpg') }}" alt="">
                    @endif
                </a>
                <div class="auth-name" {{ $newItem->id }}>
                    <h3><a href="#" title="">{{ optional($newItem->user)->name }}</a></h3>
                    <span><i class="far fa-clock"></i> {{ $newItem->created_at->diffForHumans() }}</span>
                </div>
            </div>
            <div class="posts-item-auth_like">
                @include('partials.button-like-heart')
            </div>
        </div>
    </div>
@endforeach
<div class="block-pagination">
    {{ $news->links('vendor.pagination.default') }}
</div>
