@extends('layouts.app')
@section('meta')
    <title>Quy chế hoạt động</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/helper/operationRegulations.css')}}">
@stop
@section('content')
    <div class="operation-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="txt-format text-justify">
                        <h1 class="mb-4">Quy chế hoạt động</h1>
                        <h2>I. Nguyên tắc chung</h2>
                        <p>Trước khi sử dụng trang web Inplace.vn gồm các dịch vụ hay công cụ,
                            bạn phải đồng ý với các <strong>Điều khoản và Điều kiện</strong> sau đây của trang web.
                            Nếu bạn có thắc mắc về những Điều khoản và Điều kiện này, vui lòng liên hệ trực tiếp với
                            đôi ngũ Dịch vụ của Website <a href="http://www.inplace.vn">www.inplace.vn</a>. Mục tiêu
                            của văn bản này là để đề ra sự chấp nhận các Điều khoản và Điều kiện giữa Website <a
                                href="http://www.inplace.vn">www.inplace.vn</a> và người sử dụng.</p>
                        <p>Website <a href="http://www.inplace.vn">www.inplace.vn</a> có quyền thay đổi, chỉnh sửa,
                            thêm hoặc lược bỏ bất kỳ phần nào trong những Điều khoản và Điều kiện này, vào bất cứ
                            lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên trang web mà không cần thông
                            báo trước. Vui lòng kiểm tra các điều kiện này để cập nhật các thay đổi. Và khi bạn tiếp
                            tục sử dụng trang web sau khi các thay đổi về Điều khoản và Điều kiện được đăng tải, có
                            nghĩa là bạn chấp nhận với những thay đổi đó.</p>
                        <p>Khi vào Website <a href="http://www.inplace.vn">www.inplace.vn</a>, người dùng tối thiểu
                            phải 16 tuổi hoặc truy cập dưới sự giám sát của cha mẹ hay người giám hộ hợp pháp nếu
                            dưới 16 tuổi. Website <a href="http://www.inplace.vn">www.inplace.vn</a> cấp cho Thành
                            viên phép sử dụng không thể chuyển nhượng và có thể hủy bỏ để sử dụng Trang web, theo
                            Điều kiện và Điều khoản đã đề ra, cho mục đích sử dụng các công cụ, dịch vụ được cung
                            cấp tại trang web. Nghiêm cấm sử dụng bất kỳ phần nào của Trang web này với mục đích
                            thương mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được Website <a
                                href="http://www.inplace.vn">www.inplace.vn</a> cho phép bằng văn bản. Nếu vi phạm
                            bất cứ điều nào trong Điều kiện và Điều khoản này, Website <a
                                href="http://www.inplace.vn">www.inplace.vn</a> sẽ hủy tư cách thành viên mà không
                            cần báo trước. Trang web này chỉ dùng để cung cấp thông tin về cá nhân và tổ chức cũng
                            như dành cho mục đích tìm việc và tuyển dụng. Một số dịch vụ và tính năng liên quan có
                            thể chỉ hiển thị sau khi đã đăng kí hoặc đã thanh toán các gói dịch vụ đặc biệt. Khi
                            Thành viên đồng ý đăng kí các dịch vụ hoặc tính năng liên quan, Thành viên phải cung cấp
                            thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người sử
                            dụng Trang web phải có trách nhiệm giữ bí mật mật khẩu và các thông tin định danh tài
                            khoản. Chủ tài khoản phải hoàn toàn chịu trách nhiệm cho mọi hoạt động được thực hiện
                            dưới tên tài khoản và mật khẩu đã đăng kí. Hơn nữa, Thành viên phải thông báo cho
                            Website <a href="http://www.inplace.vn">www.inplace.vn</a> biết khi tài khoản và mật
                            khẩu bị truy cập trái phép. Website <a href="http://www.inplace.vn">www.inplace.vn</a>
                            không chịu bất kỳ trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại
                            hoặc mất mát gây ra do Thành viên không tuân thủ quy định. Trong suốt quá trình đăng ký,
                            Thành viên đồng ý nhận email quảng cáo từ Trang web. Sau đó, nếu không muốn tiếp tục
                            nhận mail, Thành viên có thể từ chối bằng cách thay đổi chế độ nhận Email thông báo
                            trong phần Quản Lý Tài Khoản của mình hoặc thông báo tới đường dây nóng của website <a
                                href="http://www.inplace.vn">www.inplace.vn</a>.</p>
                        <h2>II. Quy định chung</h2>
                        <p>Trong Quy chế này, trừ khi chủ thể hoặc ngữ cảnh có yêu cầu khác, các
                            từ và các thuật ngữ sau đây sẽ có ý nghĩa sau đây:</p>
                        <p><a href="http://www.inplace.vn" target="_blank" rel="noopener noreferrer">Inplace.vn</a>
                            là trang web trực thuộc <strong>Công ty TNHH Inplace Vietnam</strong> (“Inplace”). Đây
                            là webstie giao dịch thông tin điện tử, nơi các tổ chức, cá nhân có thể trao đổi thông
                            tin, đăng tin bài, đăng tuyển dụng, tìm kiếm tin bài, việc làm, ứng tuyển, theo dõi,
                            tương tác với các thành viên khác.</p>
                        <p><strong>Thành viên:</strong> bao gồm cá nhân và doanh nghiệp đăng ký sử dụng trang web
                            Inplace.vn, để xem các thông tin, công việc do người sử dụng khác đăng lên hoặc đăng
                            thông báo tuyển dụng, ứng tuyển và thực hiện tuyển dụng ngay trên hệ thống.</p>
                        <p><strong>Sản phẩm:</strong> là một hoặc nhiều dịch vụ có sẵn trong Danh sách Dịch vụ của
                            Inplace.vn thông qua Đơn đặt hàng và/hoặc Hợp đồng Dịch vụ.</p>
                        <p><strong>Sở hữu Trí tuệ:</strong> bất kỳ bằng sáng chế, bản quyền, thiết kế được đăng ký
                            hoặc chưa đăng ký, quyền đối với thiết kế, nhãn hiệu được đăng ký hoặc chưa đăng ký,
                            nhãn hiệu dịch vụ hoặc quyền sở hữu công nghiệp hoặc sở hữu trí tuệ khác và bao gồm các
                            ứng dụng cho bất kỳ mục nào trong những mục trên.</p>
                        <h2>III. Quy trình thực hiện</h2>
                        <h3>1. Nguyên tắc hoạt động</h3>
                        <p><strong>www.Inplace.vn</strong> (sau đây gọi là “Website <a
                                href="http://www.inplace.vn" target="_blank"
                                rel="noopener noreferrer">www.inplace.vn</a>”
                            hay “<strong>INPLACE.VN</strong>”) được thiết kế cho phép những người sử dụng đăng thông
                            tin về bản thân cá nhân, bản thân công ty, thông báo tuyển dụng, thực hiện tuyển dụng
                            và/hoặc xem tin tuyển dụng do những người sử dụng khác đăng lên, hoặc tương tác với
                            những người sử dụng khác.</p>
                        <p>Mọi hoạt động cung cấp dịch vụ trên Website www.inplace.vn được thực hiện công khai, minh
                            bạch, đảm bảo quyền lợi của người dùng.</p>
                        <p>Thành viên tham gia Website www.inplace.vn là cá nhân và/hoặc doanh nghiệp có nhu cầu
                            đăng thông tin, thông báo tuyển dụng và/hoặc xem các công việc do những Thành viên khác
                            đăng lên, hoặc tương tác với những Thành viên khác. Thành viên phải đăng ký kê khai ban
                            đầu các thông tin liên quan, được Website www.inplace.vn chính thức công nhận. Thành
                            viên sử dụng trang web này phải đồng ý rằng:</p>
                        <p>Có đầy đủ năng lực hành vi dân sự.</p>
                        <p>Việc Thành viên truy cập trang web này tại mọi thời điểm, địa điểm Thành viên phải thực
                            hiện đầy đủ các điều khoản trong Quy chế này.</p>
                        <p>Thành viên phải bảo vệ mật khẩu của Thành viên và giám sát các thông tin có liên quan đến
                            tài khoản của Thành viên; Thành viên hiểu và đồng ý rằng Thành viên sẽ chịu trách nhiệm
                            cả về việc tài khoản của Thành viên được sử dụng bởi bất cứ ai mà Thành viên cho phép
                            truy cập vào nó.</p>
                        <p>Các thông tin mà Thành viên cung cấp cho Website www.inplace.vn là thông tin chính xác,
                            đầy đủ và đúng sự thật.</p>
                        <p>Các nội dung trong bản Quy chế này phải tuân thủ theo hệ thống pháp luật hiện hành của
                            Việt Nam. Thành viên khi tham gia vào Website www.inplace.vn phải tự tìm hiểu trách
                            nhiệm pháp lý của mình đối với luật pháp hiện hành của Việt Nam và cam kết thực hiện
                            đúng những nội dung trong Quy chế của Website www.inplace.vn.</p>
                        <h3>2. Quy trình thực hiện</h3>
                        <h4>2.1 Đăng ký tài khoản</h4>
                        <div class="row">
                            <div class="col-sm-5 mb-3">
                                <img src="{{asset('images/img_quy_che_hoat_dong_1.jpg')}}" alt="">
                            </div>
                            <div class="col-sm-7 mb-3">
                                <p>Người dùng có thể đăng ký thành viên thông qua các tài khoản
                                    liên kết <strong>Facebook</strong> và <strong>Linkedin </strong>sẵn có hoặc tạo
                                    tài khoản mới bằng cách điền thông tin vào bản trên</p>
                                <p>Khi đăng ký trở thành Thành viên của Website <a href="http://www.inplace.vn"
                                                                                   target="_blank"
                                                                                   rel="noopener noreferrer">www.inplace.vn</a>
                                    và được Website <a href="http://www.inplace.vn" target="_blank"
                                                       rel="noopener noreferrer">www.inplace.vn</a> chấp thuận,
                                    Thành viên sẽ có thể đăng tin bài, đăng tuyển dụng, theo dõi, ứng tuyển vào các
                                    vị trí việc làm phù hợp được đăng tải trên Website <a
                                        href="http://www.inplace.vn" target="_blank" rel="noopener noreferrer">www.inplace.vn</a>.
                                </p>
                                <p>Sau khi tạo tài khoản, Thành viên phải chịu trách nhiệm hoàn toàn về mật khẩu và
                                    các thông tin khác của tài khoản. Nếu tài khoản của Thành viên có hành động sai
                                    luật nào, đó là trách nhiệm trực tiếp của Thành viên. Website <a
                                        href="http://www.inplace.vn" target="_blank" rel="noopener noreferrer">www.inplace.vn</a>
                                    có quyền điều tra, xóa bỏ hay từ chối các thông tin của bạn.</p>
                            </div>
                        </div>
                        <h4>2.2 Cách tạo profile cho thành viên cá nhân</h4>
                        <div class="row">
                            <div class="col-sm-5 mb-3">
                                <img src="{{asset('images/img_quy_che_hoat_dong_2.jpg')}}" alt="">
                            </div>
                            <div class="col-sm-7 mb-3">
                                Thành viên hoàn tất các mục trong hồ sơ cá nhân bằng cách click vào “<strong>Chỉnh
                                    sửa</strong>” hoặc “<strong>Thêm</strong>” ở mỗi nội dung nhất định. Những nội dung
                                đã được làm mờ sẽ không thể chỉnh sửa, các nội dung khác đều có thể được chỉnh sửa.
                                Ngoài các nội dung đã đưa ra, thành viên cá nhân có thể đăng ảnh đại diện và ảnh nền để
                                profile của mình thêm sinh động
                            </div>
                        </div>
                        <ul>
                            <li>Thành viên có thể xem các Công việc hiện đang được đăng tải bằng việc truy cập vào mục “<strong>Jobs</strong>”/
                                “<strong>Việc Làm</strong>” trên đầu trang.&nbsp;
                            </li>
                            <li>Thành viên có thể xem các Tin Tức hoặc nội dung mới trên hệ thống bằng việc truy cập vào
                                mục “<strong>News</strong>” hoặc “<strong>Khám Phá</strong>” &nbsp;ở trên đầu trang.
                            </li>
                            <li>Thành viên có thể truy cập nhanh vào để tạo bài viết mới bằng cách truy cập vào mục
                                “<strong>Viết Bài</strong>” trên đầu trang.
                            </li>
                            <li>Thành viên có thể thực hiện tìm kiếm bất cứ nội dung gì bằng cách sử dụng thanh công cụ
                                <strong>Search</strong> trên đầu trang.
                            </li>
                        </ul>
                        <p>
                            <img src="{{asset('images/img_quy_che_hoat_dong_3.jpg')}}" alt="">
                        </p>
                        <ul>
                            <li>Thành viên có thể kiểm tra/gửi tin nhắn bằng cách truy cập vào biểu tượng <strong>Tin
                                    nhắn </strong>ở góc trên bên phải
                            </li>
                            <li>Thành viên có thể kiểm tra các thông báo gửi từ hệ thống thông qua biểu tượng <strong>Thông
                                    báo</strong> ở góc trên bên phải
                            </li>
                            <li>Hình đại diện và tên của thành viên hiên lên ở góc trên cùng bên phải là nơi thành viên
                                có thể truy cập vào các tính năng liên quan tới quản lý tài khoản của mình.
                            </li>
                        </ul>
                        <p>
                            <img src="{{asset('images/img_quy_che_hoat_dong_4.png')}}" alt="">
                        </p>
                        <h3>3. Các hành đông bị cấm</h3>
                        <ul class="ul-error">
                            <li>Trái với luật pháp của Việt Nam và quốc tế, xâm phạm quyền lợi của bên thứ 3, và trái
                                với các chính sách Website www.inplace.vn đề ra.
                            </li>
                            <li>Sử dụng trái phép tài khoản của người khác.</li>
                            <li>Đăng bất cứ thông tin tiểu sử không hoàn thành, sai hoặc không chính xác hoặc những
                                thông tin không phải là đơn xin việc chính xác của riêng Thành viên (cá nhân tìm kiếm
                                công việc trên cơ sở toàn thời gian hay bán thời gian)
                            </li>
                            <li>Sử dụng dữ liệu đơn xin việc của Website www.inplace.vn cho bất cứ mục đích nào khác khi
                                là một Doanh nghiệp tìm kiếm Cá nhân, bao gồm mà không hạn chế sử dụng thông tin trong
                                kho dữ liệu này để bán hoặc giới thiệu bất cứ sản phẩm hoặc dịch vụ nào. Gửi thư hoặc
                                thư điện tử, thực hiện những cuộc gọi hoặc gửi fax gạ gẫm liên quan đến việc xúc tiến
                                và/hoặc quảng cáo sản phẩm hoặc dịch vụ đến những Thành viên khác của Website
                                www.inplace.vn
                            </li>
                            <li>Dùng virus, phần mềm độc hại… gây ảnh hưởng đến hoạt động của Website www.inplace.vn hay
                                xâm phạm đến quyền lợi tài sản của người sử dụng/ Thành Viên.
                            </li>
                            <li>Copy hay sửa đổi nội dung trang web bao gồm bản quyền, logo, nhãn hiệu.</li>
                            <li>Sử dụng thông tin của người dùng khác mà không được đồng ý</li>
                        </ul>
                        <h3>4. Chế tài</h3>
                        <p>Đình chỉ/chấm dứt hoạt động của tài khoản. <br>
                            Phạt tiền hoặc có những chế tài luật pháp cho các hành động vi phạm gây thiệt hại.
                        </p>
                        <h3>5. Mâu thuẫn</h3>
                        <p>Nếu có mâu thuẫn phát sinh giữa các thành viên sử dụng www.inplace.vn thì mâu thuẫn sẽ được
                            giải quyết giữa Thành viên với nhau. Website www.inplace.vn có thể, nhưng không mang tính
                            bắt buộc, đưa ra những lời khuyên cho các phía để hoá giải mâu thuẫn. Nếu mâu thuẫn không
                            thể giải quyết giữa hai bên, các hành động pháp lý sẽ được áp dụng. Website www.inplace.vn
                            sẽ không liên quan hay tham dự vào các mâu thuẫn này, các bên đương sự đồng ý giữ cho
                            Website www.inplace.vn, kể cả các công ty liên kết và bất kỳ giám đốc, viên chức, nhân viên,
                            nhà thầu, hoặc đại lý của Website www.inplace.vn, nằm ngoài bất kỳ mâu thuẫn nào xảy ra giữa
                            các thành viên.</p>
                        <h3>6. Bồi hoàn</h3>
                        <p>Thành viên đồng ý bảo vệ Website www.inplace.vn, kể cả các công ty
                            liên kết và bất kỳ giám đốc, viên chức, nhân viên, nhà thầu, hoặc đại lý của Website
                            www.inplace.vn, không chịu thiệt hại và khỏi chịu bất kỳ chi phí nào (bao gồm cả phí
                            luật sư và án phí trên cơ sở bồi hoàn), tiền phạt, các khoản phạt, các khoản bồi thường
                            thiệt hại, và các khoản phải trả, phát sinh từ, được cho là phát sinh từ, hoặc một cách
                            nào đó có liên quan đến:</p>
                        <p>&nbsp; &nbsp;&nbsp;• &nbsp;Bất kỳ sơ suất hay lỗi nào của Thành viên.</p>
                        <p>&nbsp; &nbsp;&nbsp;• &nbsp;Bất kỳ vi phạm nào của Thành viên về thông tin trình bày trên
                            trang web.</p>
                        <p>&nbsp; &nbsp; • &nbsp;Thương hiệu và Bản quyền</p>
                        <p>&nbsp; &nbsp; • &nbsp;Mọi quyền sở hữu trí tuệ đã đăng ký hoặc chưa đăng ký trên trang
                            web Inplace.vn, nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm,
                            hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều
                            là tài sản của chúng tôi. Toàn bộ nội dung của trang web được bảo vệ bởi luật bản quyền
                            của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.</p>
                        <p><em>* Khi Click vào ô "<span style="color: rgb(61, 163, 125);"><strong>Tôi đồng ý với các Điều khoản và Điều kiện</strong></span>"
                                nghĩa là bạn đã đồng ý với các Điều khoản và Điều kiện Website </em><a
                                href="http://www.inplace.vn"><em><strong>www.inplace.vn</strong></em></a><em> đặt
                                ra.</em></p>
                        <h3>7. Giới hạn trách nhiệm trong các trường hợp phát sinh lỗi kỹ thuật</h3>
                        <p>INPLACE.VN cam kết nỗ lực đảm bảo sự an toàn và ổn định của toàn bộ hệ
                            thống kỹ thuật. Tuy nhiên, trong trường hợp xảy ra sự cố do lỗi của Website
                            www.inplace.vn. INPLACE.VN sẽ ngay lập tức áp dụng các biện pháp để đảm bảo quyền lợi
                            cho người sử dụng.</p>
                        <p>Trong quá trình sử dụng website, &nbsp;bắt buộc các thành viên phải thực hiện đúng theo
                            các quy trình hướng dẫn.</p>
                        <p>Ban quản lý Website www.inplace.vn cam kết cung cấp chất lượng dịch vụ tốt nhất cho các
                            thành viên tham gia website. Trường hợp phát sinh lỗi kỹ thuật, lỗi phần mềm hoặc các
                            lỗi khách quan khác dẫn đến Thành viên không thể tham gia giao dịch được thì các Thành
                            viên thông báo cho Ban quản trị website hoặc số hotline CSKH được cung cấp.</p>
                        <p>Tuy nhiên, Ban quản trị Website www.inplace.vn sẽ không chịu trách nhiệm giải quyết trong
                            trường hợp thông báo của các Thành viên không đến được Ban quản lý, phát sinh từ lỗi kỹ
                            thuật, lỗi đường truyền, phần mềm hoặc các lỗi khác không do Ban quản lý gây ra.</p>
                        <h3>8. Quy trình giải quyết tranh chấp, khiếu nại:</h3>
                        <p>Nếu phát sinh tranh chấp giữa Thành viên và Website www.inplace.vn,
                            Website www.inplace.vn cố gắng cung cấp một phương thức trung lập và tiết kiệm để giải
                            quyết tranh chấp nhanh chóng. Theo đó, Thành viên và Website www.inplace.vn đồng ý rằng
                            Website www.inplace.vn sẽ giải quyết theo pháp luật bất kỳ khiếu nại hoặc tranh cãi
                            ngoài Quy chế này hoặc các dịch vụ của Website www.inplace.vn phù hợp với một trong
                            những phần phụ lục bên dưới hoặc như Website www.inplace.vn và Thành viên đồng ý bằng
                            văn bản. Trước khi sử dụng đến các biện pháp thay thế, Website www.inplace.vn khuyến
                            khích Thành viên trước tiên hãy liên hệ với Website www.inplace.vn để tìm được giải pháp
                            phù hợp. Website www.inplace.vn sẽ xem xét yêu cầu hợp lý để giải quyết tranh chấp thông
                            qua các thủ tục giải quyết tranh chấp thay thế, chẳng hạn như trọng tài, thay thế cho
                            kiện tụng.</p>
                        <p>Bất kỳ khiếu nại nào phát sinh trong quá trình sử dụng dịch vụ trên Website
                            www.inplace.vn phải được gởi đến Phòng Hỗ Trợ khách hàng của INPLACE.VN theo hotline
                            được cung cấp Hoặc vào mục Liên hệ ở cuối trang chủ, điền vào mẫu thư liên lạc.</p>
                        <p>Ngay sau khi xảy ra sự kiện phát sinh khiếu nại. INPLACE.VN sẽ căn cứ từng trường hợp cụ
                            thể để có phương án giải quyết cho phù hợp.</p>
                        <p>Mọi tranh chấp giữa người sử dụng với nhau hoặc giữa người sử dụng với Website
                            www.inplace.vn sẽ được giải quyết trên cơ sở thương lượng. Trường hợp không đạt được
                            thỏa thuận như mong muốn, một trong hai bên có quyền đưa vụ việc ra Tòa án nhân dân có
                            thẩm quyền để giải quyết.</p>
                        <h2>IV. Quy trình thanh toán</h2>
                        <p>Người sử dụng thanh toán Phí Dịch vụ (nếu có) chậm nhất 7 ngày làm
                            việc sau khi nhận được hoá đơn hay đề nghị thanh toán.&nbsp;</p>
                        <p>Nếu quá Ngày thanh toán mà Thành viên vẫn chưa thực hiện thanh toán Phí Dịch vụ, thì
                            INPLACE VIETNAM có quyền tạm ngưng cung cấp dịch vụ cho đến khi việc thanh toán được
                            thực hiện đầy đủ.</p>
                        <p>INPLACE VIETNAM sẽ xuất Hóa đơn Thuế GTGT theo quy định của pháp luật Việt Nam hiện hành
                            ngay khi Khách hàng yêu cầu. Khách hàng chịu trách nhiệm về tính chính xác của thông tin
                            xuất Hóa đơn Thuế GTGT.</p>
                        <h2>V. Đảm bảo an toàn giao dịch</h2>
                        <p>Website www.inplace.vn xây dựng cơ chế bảo đảm an toàn giao dịch như
                            sau:</p>
                        <p><strong>Quản lý thông tin của Thành viên:</strong> Khi đăng ký tham gia trên Website
                            www.inplace.vn, Thành viên phải cung cấp đầy đủ các thông tin liên quan và phải hoàn
                            toàn chịu trách nhiệm đối với các thông tin này. Các thông tin cụ thể bao gồm: thông tin
                            về nhân thân đối với cá nhân, thông tin về tư cách pháp lý đối với thành viên là tổ
                            chức, pháp nhân. Các thông tin này sẽ được Website www.inplace.vn đưa vào dữ liệu quản
                            lý.</p>
                        <p><strong>Khi đã trở thành Thành viên trên trang Inplace.vn:</strong> Thành viên phải ký
                            cam kết về uy tín và thông tin tiểu sử, thông tin về đơn xin việc, thông tin về công
                            việc đúng với tiêu đề, mô tả đã đăng trên sàn Website www.inplace.vn.</p>
                        <p><strong>Cơ chế gửi khiếu nại về Thành viên đăng tin dành cho các Khách hàng:</strong>
                            Khách hàng có quyền gửi khiếu nại về những Thành viên / Doanh nghiệp đăng sai thông tin
                            đến Website www.inplace.vn. Khi tiếp nhận những phản hồi này, Website www.inplace.vn sẽ
                            xác nhận lại thông tin, trường hợp đúng như phản ánh của người phản ánh tùy theo mức độ,
                            Website www.inplace.vn sẽ có những biện pháp xử lý kịp thời nhằm bảo vệ quyền lợi của
                            Thành viên.</p>
                        <h2>VI. Bảo vệ thông tin cá nhân của thành viên</h2>
                        <p>Chính sách bảo mật này công bố cách thức mà www.inplace.vn. thu thập,
                            lưu trữ và xử lý thông tin hoặc dữ liệu cá nhân (“Thông tin cá nhân”) của các Thành viên
                            của mình thông qua wesite của www.inplace.vn.</p>
                        <p>www.inplace.vn. cam kết sẽ bảo mật các Thông tin cá nhân của Thành viên, sẽ nỗ lực hết
                            sức và sử dụng các biện pháp thích hợp để các thông tin mà Thành viên cung cấp cho
                            Website www.inplace.vn trong quá trình sử dụng website này được bảo mật và bảo vệ khỏi
                            sự truy cập trái phép.Tuy nhiên, www.inplace.vn. không đảm bảo ngăn chặn được tất cả các
                            truy cập trái phép. Trong trường hợp truy cập trái phép nằm ngoài khả năng kiểm soát của
                            Website www.inplace.vn, www.inplace.vn. sẽ không chịu trách nhiệm dưới bất kỳ hình thức
                            nào đối với bất kỳ khiếu nại, tranh chấp hoặc thiệt hại nào phát sinh từ hoặc liên quan
                            đến truy cập trái phép đó.</p>
                        <p>Thành viên hiểu và đồng ý rằng Website www.inplace.vn có thể tiết lộ cho bên thứ ba, theo
                            cách nặc danh, một vài thông tin kết hợp chứa trong đơn xin việc đăng ký của Thành viên
                            Website www.inplace.vn sẽ không tiết lộ cho bất cứ bên thứ ba tên, địa chỉ, địa chỉ
                            email hay số điện thoại của bạn mà không có sự đồng ý ưu tiên của bạn, ngoại trừ sự cần
                            thiết nhất định hay thích hợp để tuân theo những luật pháp hiện hành hay trong những
                            tiến trình hợp pháp nơi những thông tin như thế là thích hợp.</p>
                        <p>Website www.inplace.vn không bán hoặc cho thuê thông tin cá nhân của Thành viên cho bên
                            thứ ba cho mục đích tiếp thị mà không có sự cho phép của Thành viên. Website
                            www.inplace.vn sử dụng thông tin của Thành viên như mô tả trong mục Chính sách bảo mật
                            của Website www.inplace.vn. Website www.inplace.vn xem bảo vệ sự riêng tư của người sử
                            dụng như một nguyên tắc cộng đồng rất quan trọng. Website www.inplace.vn lưu trữ và xử
                            lý thông tin của Thành viên trên máy tính được bảo vệ về vật lý cũng như các thiết bị an
                            ninh công nghệ. Thành viên có thể truy cập và sửa đổi các thông tin mà Thành viên cung
                            cấp cho Website www.inplace.vn và chọn không nhận những thông tin liên lạc nhất định
                            bằng cách đăng kí vào tài khoản của Thành viên. Website www.inplace.vn sử dụng bên thứ
                            ba để xác minh và xác nhận nguyên tắc bảo mật của Website www.inplace.vn. Để xem được
                            đầy đủ về cách Website www.inplace.vn sử dụng và bảo vệ thông tin cá nhân của Thành
                            viên, xin hãy xem mục Chính sách bảo mật của Website www.inplace.vn. Nếu Thành viên phản
                            đối thông tin của Thành viên được chuyển giao hoặc sử dụng theo cách này xin vui lòng
                            không sử dụng dịch vụ của Website www.inplace.vn.</p>
                        <p>Thành viên được khuyến nghị để nắm rõ những quyền lợi của mình khi sử dụng các dịch vụ
                            của www.inplace.vn. được cung cấp trên website này.</p>
                        <p>www.inplace.vn. đưa ra các cam kết dưới đây phù hợp với các quy định của pháp luật Việt
                            Nam, trong đó bao gồm các cách thức mà Website www.inplace.vn sử dụng để bảo mật thông
                            tin của Thành viên.</p>
                        <h3>1. Mục dích và phạm vi thu thập thông tin</h3>
                        <p>Website www.inplace.vn có thể lấy những thông tin cá nhân bao gồm danh
                            hiệu, tên, giới tính, ngày sinh, địa chỉ email, địa chỉ nhà, số điện thoại nhà, điện
                            thoại di động, số fax, chi tiết thanh toán, chi tiết thẻ thanh toán hoặc chi tiết tài
                            khoản ngân hàng (Website www.inplace.vn không chỉ lấy giới hạn trong những thông tin
                            trên mà có thể lấy thêm những thông tin khác).</p>
                        <p>Website www.inplace.vn sẽ sử dụng thông tin bạn cung cấp để, cung cấp cho bạn các dịch vụ
                            và thông tin thông qua trang web, cùng những điều khác bạn yêu cầu. Ngoài ra, Website
                            www.inplace.vn sẽ sử dụng những thông tin đó để quản lý tài khoản; xác minh và thực hiện
                            những giao dịch tài chính liên quan đến việc thanh toán trực tuyến của bạn; kiểm tra
                            việc tải về tài liệu từ trang web của Website www.inplace.vn; cải tiến giao diện và/hoặc
                            nội dung của các trang trên trang web và tùy chỉnh chúng cho người sử dụng; nhận ra
                            những người truy cập trang web; gửi bạn những thông tin Website www.inplace.vn nghĩ sẽ
                            hữu ích cho bạn hoặc bạn yêu cầu từ Website www.inplace.vn, bao gồm những thông tin về
                            sản phẩm và dịch vụ của Website www.inplace.vn nếu bạn không từ chối liên lạc vì những
                            mục đích này. Khi nhận được sự đồng ý của bạn, Website www.inplace.vn sẽ liên hệ với bạn
                            bằng email với những chi tiết về sản phẩm và dịch vụ khác. Nếu bạn không muốn nhận những
                            thông tin quảng cáo của Website www.inplace.vn, bạn có thể từ chối bất kỳ lúc nào.</p>
                        <p>Nếu bạn chọn cung cấp cho Website www.inplace.vn thông tin cá nhân, bạn đã bằng lòng với
                            việc chuyển tiếp và lưu trữ những thông tin đó trên máy chủ của Website
                            www.inplace.vn.</p>
                        <h4>a. Các Thông Tin Có Thể Nhận Dạng Cá Nhân Nào Được Thu Thập Từ
                            Bạn</h4>
                        <p>Website www.inplace.vn thu thập thông tin theo một số cách từ các mục khác nhau trên
                            website của mình. Một số thông tin cá nhân được tập hợp lại khi bạn đăng ký. Trong khi
                            đăng ký, chúng tôi hỏi tên và địa chỉ email của bạn. Hệ thống cũng có thể hỏi địa chỉ
                            đường phố, tỉnh/thành phố, mã vùng/bưu điện, quốc gia, số điện thoại, thông tin thanh
                            toán và địa chỉ máy (URL) của website của bạn, mặc dù chỉ những trường được đánh dấu sao
                            * trên phần đăng ký mới là thông tin bắt buộc. Website www.inplace.vn cũng thu thập hoặc
                            có thể thu thập thông tin nhân khẩu học không chỉ từ riêng bạn như tuổi tác, ưu tiên,
                            giới tính, các mối quan tâm và sở thích. Đôi khi chúng tôi thu thập hoặc có thể thu thập
                            một kết hợp của hai kiểu thông tin. Ngay khi bạn đăng ký, bạn không còn vô danh với
                            Website www.inplace.vn nữa, bạn có tên truy cập và có thể khai thác đầy đủ các sản
                            phẩm/dịch vụ của Website www.inplace.vn.</p>
                        <p>Ngoài thông tin đăng ký, đôi khi chúng tôi có thể hỏi bạn thông tin cá nhân bao gồm
                            (nhưng không giới hạn) khi bạn đặt đăng quảng cáo tuyển dụng hoặc khai thác các tính
                            năng khác của Website www.inplace.vn. Nếu bạn liên lạc với chúng tôi, chúng tôi có thể
                            giữ một bản ghi nhớ về sự liên lạc này.</p>
                        <p>Mỗi trang trong phạm vi Website www.inplace.vn đều có đường dẫn tới Chính sách Bảo mật
                            này.</p><h4>b. Cookies Là Gì Và Cách Chúng Được Sử Dụng</h4>
                        <p>Là một phần của việc đưa ra và cung cấp các dịch vụ cá nhân và theo yêu cầu Thành viên,
                            Website www.inplace.vn có thể dùng các cookie để lưu và đôi khi theo dõi thông tin về
                            bạn. Cookie là một lượng nhỏ dữ liệu được gửi tới trình duyệt của bạn từ máy chủ web và
                            được lưu trên đĩa cứng máy tính của bạn. Một vài tính năng của Website www.inplace.vn
                            đòi hỏi bạn chấp nhận các cookie mới có thể sử dụng được.</p>
                        <p>Nói chung, chúng tôi dùng các cookie cho các mục đích sau đây:</p>
                        <p>&nbsp; &nbsp; &nbsp;• &nbsp;Nhận dạng và gắn nhãn cho tất cả các công việc “mới” từ lần
                            cuối cùng của bạn ghé thăm website.</p>
                        <p>&nbsp; &nbsp; &nbsp;• &nbsp;Lưu theo yêu cầu hoặc vĩnh viễn tên truy cập và mật khẩu lên
                            máy của bạn để bạn không phải nhập lại mỗi lần ghé thăm website của chúng tôi.</p>
                        <p>&nbsp; &nbsp; &nbsp;• &nbsp;Cho phép bạn “kiểm tra danh sách” các công việc mà bạn muốn
                            đánh dấu để giữ lại và xem sau này.</p>
                        <p>&nbsp; &nbsp; &nbsp;• &nbsp;Các mạng quảng cáo của các công ty đăng việc trên Website
                            www.inplace.vn cũng có thể dùng các cookie của riêng họ.</p>
                        <p>Website www.inplace.vn cũng sẽ tập hợp và có thể tập hợp thông tin nào đó về việc sử dụng
                            website www.inplace.vn của bạn, chẳng hạn những khu vực bạn ghé thăm và những dịch vụ
                            bạn truy nhập. Ngoài ra, Website www.inplace.vn cũng có thể thu thập những thông tin về
                            phần cứng, phần mềm trên máy tính của bạn. Thông tin này có thể bao gồm những không hạn
                            chế địa chỉ IP, kiểu trình duyệt, tên miền, các mục bạn truy nhập và các địa chỉ website
                            tham chiếu.</p>
                        <h3>2. Phạm vi sử dụng thông tin</h3>
                        <p>Bằng việc cung cấp thông tin của mình, bạn đồng ý để Website
                            www.inplace.vn, các công ty liên kết, đơn vị trực thuộc và các thành viên trực thuộc
                            Inplace Vietnam Group có thể sử dụng thông tin của bạn, dù đó là thông tin cá nhân, nhân
                            khẩu học, tập hợp hay kỹ thuật, đều nhằm mục đích điều hành và cải tiến Website
                            www.inplace.vn, tăng cường tiện ích cho người sử dụng hoặc giới thiệu và phân phối các
                            sản phẩm và dịch vụ của chúng tôi.</p>
                        <p>Chúng tôi cũng có thể dùng thông tin thu thập được để thông báo cho bạn về những sản phẩm
                            và dịch vụ do Website www.inplace.vn hay các công ty đối tác cung cấp, hoặc để xin ý
                            kiến của bạn về các sản phẩm và dịch vụ hiện tại hay những sản phẩm và dịch vụ tiềm năng
                            mới.</p>
                        <p>Chúng tôi cũng có thể dùng thông tin liên lạc của bạn để gửi cho bạn email hoặc các thông
                            báo khác về những cập nhật tại website tuyển dụng của Inplace.vn. Nội dung và tần suất
                            của những thông báo này sẽ thay đổi tuỳ thuộc vào thông tin mà chúng tôi có về bạn.
                            Ngoài ra, vào lúc đăng ký, bạn có quyền lựa chọn nhận những thông tin, thông báo và các
                            chương trình khuyến mãi bao gồm nhưng không hạn chế các bản tin miễn phí từ Website
                            www.inplace.vn liên quan đến những chủ đề mà bạn có thể đặc biệt quan tâm.</p>
                        <p>Chúng tôi có một khu vực để bạn có thể liên lạc với chúng tôi. Bất kỳ phản hồi nào bạn
                            gửi đến cho chúng tôi sẽ trở thành tài sản của chúng tôi và chúng tôi có thể dùng phản
                            hồi đó (chẳng hạn các câu chuyện thành công) cho các mục đích tiếp thị, hoặc liên hệ với
                            bạn để có thêm thông tin.</p><h4>a. Ai Đang Thu Thập Thông Tin Của Bạn</h4>
                        <p>Khi được hỏi về các thông tin cá nhân trên website tuyển dụng của Inplace.vn, có nghĩa là
                            bạn đang chia sẻ thông tin đó với riêng Website www.inplace.vn, trừ phi có thông báo cụ
                            thể khác. Tuy nhiên, một số hoạt động do đặc trưng của chúng, sẽ dẫn đến việc thông tin
                            cá nhân của bạn được tiết lộ cho những người sử dụng khác của Website www.inplace.vn
                            biết. Ví dụ, khi bạn điền thông tin cá nhân lên bản đăng quảng cáo tuyển dụng, thông tin
                            này nói chung sẽ được gộp trong công việc của bạn, trừ phi có thông báo cụ thể khác.</p>
                        <h4>b. Thông Tin Của Bạn Có Thể Chia Sẻ Với Ai</h4>
                        <p>Chúng tôi không tiết lộ cho bên thứ ba thông tin cá nhân của bạn, cũng như thông tin cá
                            nhân và nhân khẩu học kết hợp, hoặc thông tin về việc sử dụng Website www.inplace.vn của
                            bạn (chẳng hạn các khu vực bạn ghé thăm, hay các dịch vụ mà bạn truy cập), trừ năm mục
                            sau đây.</p>
                        <p>Chúng tôi có thể để lộ thông tin như vậy cho các nhóm thứ ba nếu bạn đồng ý. Ví dụ, nếu
                            bạn cho biết bạn muốn nhận thông tin về các sản phẩm và dịch vụ của bên thứ ba khi đăng
                            ký một tài khoản trên Website www.inplace.vn, chúng tôi có thể cung cấp thông tin liên
                            hệ của bạn cho bên thứ ba đó, ví dụ người sử dụng lao động, các Doanh nghiệp, người thu
                            thập dữ liệu, nhân viên thị trường hoặc những người khác với mục đích gửi email cho bạn
                            hay liên lạc với bạn theo cách khác. Chúng tôi có thể dùng dữ liệu đã có về bạn (như các
                            mối quan tâm hay sở thích mà bạn đã trình bày) để xác định xem liệu bạn có thể quan tâm
                            đến các sản phẩm hay dịch vụ của một bên thứ ba cụ thể nào không.</p>
                        <p>Chúng tôi có thể tiết lộ thông tin như vậy cho các công ty và cá nhân mà chúng tôi thuê
                            để thay mặt chúng tôi thực hiện các chức năng của công ty. Ví dụ, việc lưu giữ các máy
                            chủ web, phân tích dữ liệu, cung cấp các trợ giúp về marketing, xử lý thẻ tín dụng hoặc
                            các hình thức thanh toán khác, và dịch vụ cho Thành viên. Những công ty và cá nhân này
                            sẽ truy cập tới thông tin cá nhân của bạn khi cần để thực hiện các chức năng của họ,
                            nhưng không chia sẻ thông tin đó với bất kỳ bên thứ ba nào khác.</p>
                        <p>Chúng tôi có thể tiết lộ thông tin như vậy nếu có yêu cầu pháp lý, hay từ một cơ quan
                            chính phủ hoặc nếu chúng tôi tin rằng hành động đó là cần thiết nhằm: (a) tuân theo các
                            yêu cầu pháp lý hoặc chiếu theo quy trình của luật pháp; (b) bảo vệ các quyền hay tài
                            sản của Inplace Vietnam, hoặc các công ty đối tác; (c) ngăn chặn tội phạm hoặc bảo vệ an
                            ninh quốc gia; hoặc (d) bảo vệ an toàn cá nhân của những người sử dụng hay công
                            chúng</p>
                        <p>Chúng tôi có thể tiết lộ và chuyển thông tin như vậy tới một nhóm thứ ba, đối tượng mua
                            lại toàn bộ hay phần lớn công việc kinh doanh của công ty Inplace Vietnam, bằng cách
                            liên kết, hợp nhất hoặc mua toàn bộ hay phần lớn các tài sản của chúng tôi. Ngoài ra,
                            trong tình huống Inplace Vietnam trở thành đối tượng của một vụ khởi kiện phá sản, dù tự
                            nguyện hay miễn cưỡng, thì Inplace Vietnam hay người được uỷ thác có thể bán, cho phép
                            hoặc tiết lộ thông tin như vậy theo cách khác trong quá trình chuyển giao được toà án về
                            phá sản đồng ý.</p>
                        <p>Chúng tôi có thể dùng tên bạn, tên hay logo của công ty bạn, hay thông tin khác về hoặc
                            từ các quảng cáo tuyển dụng hoặc tài khoản xem hồ sơ Cá nhân của bạn cho bất kỳ hay tất
                            cả các mục đích tiếp thị của Inplace Vietnam (hay Website www.inplace.vn). Ví dụ, các
                            tên hay logo công ty có thể được dùng trong quảng cáo trên báo, thư gửi trực tiếp,
                            phương tiện bán hàng, áp phích quảng cáo và các tài liệu khác liên quan đến Website
                            www.inplace.vn hay các tài sản khác của Inplace Vietnam.</p>
                        <p>Website www.inplace.vn cũng có thể chia sẻ thông tin vô danh về khách ghé thăm một trong
                            các Web của công ty (ví dụ, số khách đến mục ‘Tìm việc’ của Website www.inplace.vn) với
                            các Thành viên, đối tác và bên thứ ba khác để họ có thể hiểu về các loại khách tới thăm
                            website của Website www.inplace.vn và cách họ sử dụng site.</p>
                        <p>Website www.inplace.vn có thể hỗ trợ công nghệ, lưu trữ Web và các dịch vụ liên quan khác
                            cho các công ty hàng đầu khác để thiết lập mục tuyển dụng trên website của họ (đôi khi
                            được gọi là “khu vực tuyển dụng”). Thông tin cá nhân và/hoặc có tính nhân khẩu học do
                            bạn cung cấp trong các khu vực tuyển dụng trở thành một phần của cơ sở dữ liệu của
                            Website www.inplace.vn, nhưng không ai có thể truy cập trừ bạn, Inplace Vietnam và công
                            ty có liên quan mà không có sự đồng ý của bạn.</p>
                        <p>Thông tin được thu thập trên trang thuộc khu vực tuyển dụng, hoặc trên trang chia sẻ nhãn
                            hiệu (như trang về một cuộc thi do Website www.inplace.vn và công ty khác đồng tài trợ)
                            có thể trở thành tài sản của công ty đó, hoặc của cả Inplace Vietnam và công ty đó.
                            Trong ví dụ này, việc sử dụng thông tin như vậy của công ty kia có thể phụ thuộc vào
                            chính sách bảo mật của công ty đó và, trong trường hợp bất kỳ nào, Inplace Vietnam không
                            chịu trách nhiệm về việc công ty kia sử dụng thông tin cá nhân và nhân khẩu học của
                            bạn.</p>
                        <h3>3. Thời gian lưu trữ thông tin</h3>
                        <p>Dữ liệu cá nhân của thành viên sẽ được lưu trữ cho đến khi có yêu cầu hủy bỏ hoặc tự
                            thành viên đăng nhập và thực hiện hủy bỏ. Còn lại trong mọi trường hợp thông tin cá nhân
                            thành viên sẽ được bảo mật trên máy chủ Website www.inplace.vn.</p>
                        <h3>4. Địa chỉ của đơn vị thu thập và quản lý thông tin cá nhân</h3>
                        <p><strong>Công ty TNHH INPLACE VIETNAM</strong></p>
                        <p><i class="fas fa-map-marker-alt"></i> <strong> Tại Hà Nội:</strong> Tầng 3, tòa nhà Đại Phát,
                            ngõ 82 Duy Tân, Cầu Giấy, Hà Nôi
                        </p>
                        <p><i class="fas fa-map-marker-alt"></i> <strong>Tại TP Hồ Chí Minh: </strong>Tầng 2, tòa nhà
                            Mekong, só 235-241 Cộng Hòa, Quận
                            Tân Bình, TP Hồ Chí Minh.</p>
                        <h3>5. Phương tiện và công cụ để người dùng tiếp cận và chỉnh sửa dữ liệu cá nhận của mình</h3>
                        <p><strong>Những Lựa Chọn Dành Cho Bạn Khi Thu Thập, Sử Dụng Và Phân Phối
                                Thông Tin Cá Nhân</strong></p>
                        <p>Nếu bạn chọn không đăng ký hoặc cung cấp thông tin cá nhân, bạn sẽ không thể dùng phần
                            lớn tính năng của Website www.inplace.vn.</p>
                        <p>Bạn cũng có các lựa chọn liên quan tới các cookie. Bằng cách thay đổi các ưu tiên trình
                            duyệt của mình, bạn có thể chọn chấp nhận tất cả các cookie, hay được thông báo khi một
                            cookie được thiết lập, hoặc loại bỏ tất cả các cookie. Nếu bạn chọn loại bỏ tất cả các
                            cookie, bạn sẽ không thể dùng các dịch vụ của Website www.inplace.vn có yêu cầu đăng ký
                            sử dụng. Những dịch vụ này bao gồm việc nhận dạng các công việc mới đã được đăng lên từ
                            lần ghé thăm cuối cùng, tự đăng nhập và tính năng kiểm tra danh sách. Bạn vẫn có thể
                            dùng hầu hết các tính năng của website tuyển dụng Inplace.vn ngay cả khi bạn không chấp
                            nhận các cookie.</p>
                        <p><strong>Bạn Có Thể Truy Nhập, Cập Nhật Và Xoá Thông Tin Của Bạn Như Thế Nào</strong></p>
                        <p>Chúng tôi sẽ cung cấp cho bạn các phương tiện đảm bảo thông tin cá nhân của bạn là chính
                            xác và cập nhật. Bạn có thể hiệu chỉnh hoặc xoá hồ sơ của bạn bất cứ lúc nào khi nhấn
                            vào liên kết “My profile” hoặc vào hình ảnh do hệ thống cung cấp ngay khi bạn đăng nhập
                            vào. Khi đăng nhập vào hệ thống trong một khoảng thời gian nào đó, dù bạn đang ở đâu
                            trên website tuyển người Website www.inplace.vn, thông tin của bạn sẽ được giữ nguyên
                            cho đến khi bạn nhấn chuột vào liên kết “Thoát/Logoff” là liên kết có thể truy nhập từ
                            màn hình “My Profile”</p>
                        <p>Nếu bạn là người sử dụng đã đăng ký và quên mật khẩu, bạn có thể nhận lại nó bằng cách
                            gửi email và dùng tính năng “Quên mật khẩu”. Nhấn phím trên bất kỳ trang đăng nhập nào
                            để yêu cầu gửi mật khẩu của bạn cho bạn. Chúng tôi không thể cung cấp mật khẩu của bạn
                            theo các cách khác.</p>
                        <p>Tài khoản Website www.inplace.vn của bạn có thể bị xoá, nhưng làm như vậy sẽ dẫn đến việc
                            không thể truy nhập đến bất kỳ tính năng nào đòi hỏi đăng nhập. Chúng tôi sẽ hoặc có thể
                            giữ một bản sao lưu trữ về tài khoản của bạn song không thể truy nhập trên Internet.</p>
                        <p><strong>Những Biện Pháp Phòng Ngừa An Toàn Chống Mất Mát, Lạm Dụng Hoặc Thay Đổi Thông
                                Tin Của Bạn</strong></p>
                        <p>Ngoài người quản trị Website www.inplace.vn hoặc cá nhân được uỷ quyền khác của Website
                            www.inplace.vn ra, bạn là người duy nhất được truy nhập đến thông tin cá nhân của mình.
                            Đăng ký sử dụng của bạn được bảo vệ bằng mật khẩu để ngăn chặn sự truy nhập trái
                            phép.</p>
                        <p>Chúng tôi khuyến nghị bạn không để lộ mật khẩu của bạn cho bất kỳ ai. Website
                            www.inplace.vn không bao giờ hỏi mật khẩu của bạn qua điện thoại hay qua email tự
                            nguyện. Để bảo đảm an toàn, bạn có thể muốn ra khỏi mạng ngay khi sử dụng xong Website
                            www.inplace.vn. Điều này đảm bảo những người khác không thể truy nhập tới thông tin và
                            thư từ cá nhân của bạn nếu bạn dùng chung máy tính với ai đó hoặc dùng máy tính ở nơi
                            công cộng như thư viện hay quán cà phê Internet.</p>
                        <p>Đáng tiếc là không có dữ liệu nào truyền trên Internet có thể bảo đảm an toàn 100%. Do
                            vậy, mặc dù chúng tôi cố gắng hết sức bảo vệ thông tin cá nhân của bạn, Website
                            www.inplace.vn có thể không thể bảo đảm hoặc cam kết về tính an toàn của thông tin bất
                            kỳ mà bạn chuyển tới chúng tôi hoặc từ dịch vụ trực tuyến của chúng tôi, và bạn phải tự
                            chịu rủi ro. Ngay khi chúng tôi nhận được thông tin bạn gửi tới, chúng tôi sẽ cố gắng
                            hết sức để bảo đảm an toàn trên hệ thống của chúng tôi.</p>
                        <p>Nếu bạn lo lắng về dữ liệu, bạn có quyền yêu cầu truy cập dữ liệu cá nhân của bạn mà
                            Website www.inplace.vn đang nắm giữ hoặc xử lý. Bạn có quyền yêu cầu Website
                            www.inplace.vn chỉnh sửa những dữ liệu không chính xác một cách miễn phí. Bạn cũng có
                            quyền yêu cầu Website www.inplace.vn dừng sử dụng thông tin cá nhân của bạn cho mục đích
                            tiếp thị.</p>
                        <p><strong>Cách Website www.inplace.vn Bảo Vệ Đời Tư Của Trẻ Em.</strong></p>
                        <p>Website www.inplace.vn là website có đối tượng độc giả lớn. Trẻ em sẽ phải xin phép bố mẹ
                            trước khi gửi trực tuyến thông tin cá nhân tới ai đó. Website www.inplace.vn không thể
                            chia sẻ thông tin cá nhân về những người sử dụng dưới 16 tuổi với bên thứ ba. Ngoài ra,
                            Website www.inplace.vn sẽ không gửi bất kỳ email trực tiếp nào đề nghị người sử dụng
                            thông báo họ dưới 16 tuổi.</p>
                        <p><strong>Bạn Biết Gì Nữa Về Đời Tư Trực Tuyến Của Bạn</strong></p>
                        <p>Hãy luôn nhớ rằng bất kỳ lúc nào bạn tự nguyện tiết lộ thông tin cá nhân của bạn trực
                            tuyến, ví dụ qua công việc bạn đăng lên hay qua email, thông tin đó có thể bị người khác
                            thu thập và sử dụng, Tóm lại, nếu bạn gửi thông tin cá nhân trực tuyến có thể truy nhập
                            công khai, bạn có thể nhận sẽ được những thông báo tự nguyện từ những đối tác khác.</p>
                        <p>Cuối cùng, bạn phải tự chịu trách nhiệm về việc giữ bí mật cho mật khẩu và/hoặc các thông
                            tin tài khoản bất kỳ. Vì thế, xin hãy cẩn thận và có trách nhiệm khi nào bạn ở trên
                            mạng.</p>
                        <h3>6. Cam kết bảo mật thông tin của khách hàng</h3>
                        <p>Website www.inplace.vn có biện pháp kĩ thuật và an ninh thích hợp để ngăn chặn truy cập
                            trái phép và bất hợp pháp đến thông tin cá nhân của bạn, hoặc các mất mát, phá hủy và
                            thiệt hại tình cờ đến thông tin. Khi Website www.inplace.vn thu thập thông tin qua trang
                            web, Website www.inplace.vn thu thập dữ liệu cá nhân của bạn trên máy chủ an toàn.
                            Website www.inplace.vn dùng tường lửa trên các máy chủ. Khi Website www.inplace.vn lấy
                            dữ liệu thẻ thanh toán điện tử, Website www.inplace.vn sẽ dùng mã hóa bằng Secure Socket
                            Layer (SSL). Mặc dù Website www.inplace.vn không thể đảm bảo an toàn 100%, hacker cũng
                            khó có thể giải mã dữ liệu của bạn. Bạn không nên gửi đầy đủ chi tiết của thẻ tín dụng
                            hay thẻ ghi nợ khi chưa được mã hóa cho Website www.inplace.vn. Website www.inplace.vn
                            duy trì bảo vệ vật lý, điện tử, thủ tục gắn liền với việc thu thập, lưu trữ và tiết lộ
                            thông tin của bạn. Quy trình bảo vệ của Website www.inplace.vn là Website www.inplace.vn
                            có thể thỉnh thoảng yêu cầu chứng minh nhân dạng trước khi tiết lộ thông tin cá nhân cho
                            bạn. Bạn có trách nhiệm bảo vệ khỏi truy cập trái phép vào mật khẩu và máy tính của
                            bạn.</p>
                        <p>Thông tin cá nhân của thành viên trên Website www.inplace.vn được cam kết bảo mật tuyệt
                            đối theo chính sách bảo vệ thông tin cá nhân của Website www.inplace.vn. Việc thu thập
                            và sử dụng thông tin của mỗi thành viên chỉ được thực hiện khi có sự đồng ý của khách
                            hàng đó trừ những trường hợp pháp luật có quy định khác.</p>
                        <p>Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bên thứ 3 nào về thông tin cá
                            nhân của thành viên khi không có sự cho phép đồng ý từ thành viên.</p>
                        <p>Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá
                            nhân thành viên, Website www.inplace.vn sẽ có trách nhiệm thông báo vụ việc cho cơ quan
                            chức năng điều tra xử lý kịp thời và thông báo cho thành viên được biết.</p>
                        <p>Ban quản lý Website www.inplace.vn yêu cầu các cá nhân khi đăng ký là thành viên, phải
                            cung cấp đầy đủ thông tin cá nhân có liên quan như: Họ và tên, địa chỉ liên lạc, email,
                            số chứng minh nhân dân, điện thoại v.v và chịu trách nhiệm về tính pháp lý của những
                            thông tin trên. Ban quản lý Website www.inplace.vn không chịu trách nhiệm cũng như không
                            giải quyết mọi khiếu nại có liên quan đến quyền lợi của Thành viên đó nếu xét thấy tất
                            cả thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban đầu là không chính
                            xác.</p>
                        <h3>7. Tổng kết</h3>
                        <p>Website www.inplace.vn có thể sửa đổi Chính sách Bảo mật này bất kì lúc nào bằng cách
                            đăng điều khoản chỉnh sửa trên trang web.</p>
                        <h2>VII. Quản lý thông tin xấu</h2>
                        <p>Mọi qui định dưới đây áp dụng cho các khâu tiền kiểm và hậu kiểm của BQT Inplace.vn nếu
                            phát hiện Thành viên vi phạm qui định trong quá trình hoạt động nhằm mục đích tạo môi
                            trường an toàn, lành mạnh và công bằng cho các Thành viên và thành viên đang hoạt động
                            tại Inplace.vn.</p>
                        <p>Vì thế, BQT Inplace.vn khuyến cáo Thành viên nên tìm hiểu kỹ các qui định trước khi đăng
                            bất cứ thông tin nào để tránh những vi phạm không do chủ ý. Những qui định mới sẽ được
                            BQT cập nhật liên tục dựa trên phân tích về thị trường, cộng đồng, các vấn đề phát sinh
                            trong quá trình hoạt động và có hiệu lực ngay khi được đăng tải trên website Website
                            www.inplace.vn.</p>
                        <table class="" style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="width: 15.137%;" rowspan="2" class="">
                                    <div style="text-align: center;"><strong>MỤC</strong></div>
                                </td>
                                <td style="width: 18.6986%; vertical-align: top;" rowspan="2">
                                    <div style="text-align: center;"><strong>QUY ĐỊNH</strong></div>
                                </td>
                                <td style="width: 40.0685%;" rowspan="2">
                                    <div style="text-align: center;"><strong>DIỄN GIẢI</strong></div>
                                </td>
                                <td style="width: 25.9589%;" colspan="2">
                                    <div style="text-align: center;"><strong>HÌNH THỨC XỬ LÝ</strong></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 13.9041%;" class="">
                                    <div style="text-align: center;"><strong>Mức 1</strong></div>
                                </td>
                                <td style="width: 12.8082%;">
                                    <div style="text-align: center;"><strong>Mức 2</strong></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15.137%; vertical-align: middle;" rowspan="3"><strong>Tài
                                        khoản</strong></td>
                                <td style="width: 18.6986%;">
                                    <div style="text-align: left;"><strong>Tên tài khoản</strong></div>
                                </td>
                                <td style="width: 40.0685%;"><p style="text-align: left;">Tên tài khoản có liên quan đến
                                        Inplace.vn, các nick giả dạng BQT Inplace.vn như: Admin Inplace.vn, Webmaster,
                                        kết hợp với hình đại diện có hình ảnh là logo của Inplace.vn được xem làm tài
                                        khoản giả danh Inplace.vn.</p>
                                    <p style="text-align: left;">Tên tài khoản dễ gây hiểu lầm và kích động, vi phạm
                                        thuần phong mỹ tục Việt Nam.</p></td>
                                <td style="width: 13.9041%;">
                                    <div style="text-align: left;">Không duyệt tài khoản</div>
                                </td>
                                <td style="width: 12.8082%;"><br></td>
                            </tr>
                            <tr>
                                <td style="width: 18.6986%;">
                                    <div style="text-align: left;"><strong>Hình đại diện</strong></div>
                                </td>
                                <td style="width: 40.0685%;"><p style="text-align: left;">Hình đại diện sử dụng hình ảnh
                                        phản cảm mang tính chất khiêu dâm, chính trị, tôn giáo, không có bản quyền, trái
                                        với văn hóa và thuần phong mỹ tục Việt Nam.</p>
                                    <p style="text-align: left;">Hình đại diện đơn vị tuyển dụng vi phạm tài sản sở hữu
                                        trí tuệ của đơn vị khác.</p></td>
                                <td style="width: 13.9041%;">
                                    <div style="text-align: left;">Tạm khóa tài khoản 1 tuần &amp; Yêu cầu thay hình đại
                                        diện
                                    </div>
                                </td>
                                <td style="width: 12.8082%;">
                                    <div style="text-align: left;">Khóa tài khoản đến khi nào thực hiện theo yêu cầu
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18.6986%;">
                                    <div style="text-align: left;"><strong>Thông tin liên hệ</strong></div>
                                </td>
                                <td style="width: 40.0685%;" class="">Thông tin liên hệ không rõ ràng bao gồm: tên người
                                    liên lạc, số điện thoại, địa chỉ, địa chỉ email.
                                </td>
                                <td style="width: 13.9041%;">
                                    <div style="text-align: left;">Nhắc nhở &amp; Hướng dẫn</div>
                                </td>
                                <td style="width: 12.8082%;">
                                    <div style="text-align: left;">Khóa tài khoản đến khi nào thực hiện theo yêu cầu
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15.137%; vertical-align: middle;" rowspan="2" class=""><p><strong>Nội
                                            dung&nbsp;</strong><strong>đăng</strong></p><br></td>
                                <td style="width: 18.6986%;">
                                    <div style="text-align: left;"><strong>Tiêu đề bài đăng tuyển dụng</strong></div>
                                </td>
                                <td style="width: 40.0685%;">
                                    <div style="text-align: left;">Tiêu đề không rõ ràng, gây hiểu nhầm, sai chính tả
                                        hoặc không có nghĩa.
                                    </div>
                                </td>
                                <td style="width: 13.9041%;">
                                    <div style="text-align: left;">Từ chối duyệt &amp; Nhắc nhở</div>
                                </td>
                                <td style="width: 12.8082%;"><br></td>
                            </tr>
                            <tr>
                                <td style="width: 18.6986%;">
                                    <div style="text-align: left;"><strong>Mô tả công việc đăng tuyển dụng</strong>
                                    </div>
                                </td>
                                <td style="width: 40.0685%;"><p style="text-align: left;">Trong phần mô tả công việc, mô
                                        tả công việc có thể được trình bày bằng tiếng Việt và/ hoặc tiếng Anh.</p>
                                    <p style="text-align: left;">Khi trình bày bằng ngôn ngữ thuần Việt, ngôn ngữ phải
                                        có dấu, không viết tắt, không dùng ngôn ngữ teen, ngôn ngữ chat gây khó hiểu cho
                                        người đọc.</p>
                                    <p style="text-align: left;">Mô tả công việc phải nêu được đặc điểm của công việc
                                        cần tuyển dụng, không được mô tả sơ sài hoặc không mô tả. Phải điền đầy đủ các
                                        nội dung được liệt kê. Mọi phần mô tả công việc vi phạm quy định trên đều không
                                        được duyệt đăng, hoặc được duyệt đăng nhưng sẽ bị Inplace.vn xoá đi phần thông
                                        tin liên lạc.</p></td>
                                <td style="width: 13.9041%;">
                                    <div style="text-align: left;">Tạm khóa tài khoản 1 tuần &amp; Yêu cầu thay hình đại
                                        diện
                                    </div>
                                </td>
                                <td style="width: 12.8082%;">
                                    <div style="text-align: left;">Khóa tài khoản đến khi nào thực hiện theo yêu cầu
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                            <h2>VIII. Trách nhiệm trong trường hợp phát sinh lỗi kỹ thuật</h2>
                            <p>Website www.inplace.vn cam kết nỗ lực đảm bảo sự an toàn và ổn định của toàn bộ hệ thống
                                kỹ thuật. Tuy nhiên, trong trường hợp xảy ra sự cố do lỗi của Website www.inplace.vn.
                                www.inplace.vn sẽ ngay lập tức áp dụng các biện pháp để đảm bảo quyền lợi cho người mua
                                hàng.</p>
                            <p>Khi sử dụng website, bắt buộc các thành viên phải thực hiện đúng theo các quy trình hướng
                                dẫn.</p>
                            <p>Ban quản lý Website www.inplace.vn cam kết cung cấp chất lượng dịch vụ tốt nhất cho các
                                thành viên tham gia sử dụng website. Trường hợp phát sinh lỗi kỹ thuật, lỗi phần mềm
                                hoặc các lỗi khách quan khác dẫn đến Thành viên không thể thao tác được thì Thành viên
                                thông báo cho Ban Quản Trị Website theo số Hotline được cung cấp.</p>
                            <p>Tuy nhiên, Ban quản lý Website www.inplace.vn sẽ không chịu trách nhiệm giải quyết trong
                                trường hợp thông báo của các Thành viên không đến được Ban quản lý, phát sinh từ lỗi kỹ
                                thuật, lỗi đường truyền, phần mềm hoặc các lỗi khác không do Ban quản lý gây ra.</p>
                            <h2>IX. Quyền và nghĩa vụ của thành viên tham gia Inplace.vn</h2>
                            <h3>1. Quyền của Thành viên</h3>
                            <p>Khi đăng ký trở thành Thành viên của Website www.inplace.vn và được Website
                                www.inplace.vn chấp thuận, Thành viên sẽ được cấp một tên đăng ký và mật khẩu riêng để
                                được vào sử dụng các dịch vụ như tạo và đăng tải hồ sơ để ứng tuyển các vị trí việc làm
                                mà Thành viên cảm thấy phù hợp và/ hoặc nhận những newsletter hoặc gợi ý việc làm phù
                                hợp theo hồ sơ của Thành viên vào địa chỉ email đăng ký với Website www.inplace.vn</p>
                            <p>Thành viên tự quản lý hồ sơ và các tin vài, hình ảnh, tin đăng tuyển của mình trên
                                website www.inplace.vn</p>
                            <p>Thành viên sẽ được nhân viên của Website www.inplace.vn hướng dẫn sử dụng được các công
                                cụ, các tính năng phục vụ cho việc xây dựng các thông tin đăng tuyển, và sử dụng các
                                dịch vụ tiện ích trên Website www.inplace.vn.</p>
                            <p>Thành viên sẽ có thể được hưởng các chính sách ưu đãi do Website www.inplace.vn hay các
                                đối tác thứ ba cung cấp trên Website www.inplace.vn. Các chính sách ưu đãi này sẽ được
                                Website www.inplace.vn giải quyết (nếu có) và sẽ được đăng tải trực tiếp trên Website
                                www.inplace.vn hoặc được gửi trực tiếp đến các Thành viên.</p>
                            <p>Thành viên có quyền đóng góp ý kiến cho Website www.inplace.vn trong quá trình hoạt động.
                                Các kiến nghị được gửi trực tiếp bằng thư hoặc email đến cho Website www.inplace.vn.</p>
                            <h3>2. Nghĩa vụ của Thành viên</h3>
                            <p>Thành viên sẽ tự chịu trách nhiệm về bảo mật, lưu giữ và mọi hoạt động sử dụng dịch vụ
                                dưới tên đăng ký, mật khẩu và hộp thư điện tử của mình. Thành viên có trách nhiệm thông
                                báo kịp thời cho Website www.inplace.vn về những hành vi sử dụng trái phép, lạm dụng, vi
                                phạm bảo mật, lưu giữ tên đăng ký và mật khẩu của mình để hai bên cùng hợp tác xử
                                lý.</p>
                            <p>Thành viên cam kết những thông tin cung cấp cho Website www.inplace.vn và những thông tin
                                đăng tải lên Website www.inplace.vn là chính xác và hoàn chỉnh. Thành viên đồng ý giữ và
                                thay đổi các thông tin trên Website www.inplace.vn là cập nhật, chính xác và hoàn
                                chỉnh.</p>
                            <p>Là người sử dụng, Thành viên cần có trách nhiệm với việc giao tiếp riêng và chịu trách
                                nhiệm với kết quả của việc đăng tải. Thành viên không được phép làm nhưng điều như: đăng
                                tải những nội dung được đăng ký bản quyền nếu Thành viên không phải là người chủ bản
                                quyền hoặc không được người chủ bản quyền cho phép đăng tải nó; đăng tải nội dung tiết
                                lộ những bí mật thương mại nếu Thành viên không làm chủ hoặc không được phép của người
                                chủ; đăng tải những nội dung vi phạm bất cứ quyền sở hữu trí tuệ nào khác của người khác
                                hoặc xâm phạm bảo mật hay quyền công cộng của người khác; đăng tải những nội dung tục
                                tĩu, làm mất danh dự, đe dọa, quấy rầy, sỉ nhục, căm ghét hay gây cản trở cho người sử
                                dụng khác hay bất cứ người nào khác hay tổ chức nào khác; đăng tải những thông tin không
                                có chủ đề hay có chủ đề đặc biệt; đăng tải một hình ảnh hay nội dung sex; đăng tải những
                                mẫu quảng cáo hay xúi giục kinh doanh hay đăng tải nội dung chứa virus, Trojan, bom thời
                                gian… hay những quy trình hay chương trình máy tính khác với mục đích hủy diệt, gây cản
                                trở bất lợi, ngăn chặn lừa dối hay chiếm đoạt bất cứ hệ thống, dữ liệu hay thông tin
                                nào.</p>
                            <p>Thành viên tự chịu trách nhiệm về nội dung, hình ảnh của thông tin Doanh nghiệp và các
                                thông tin khác cũng như toàn bộ quá trình giao dịch với các đối tác/ Cá nhân trên
                                Website www.inplace.vn.</p>
                            <p>Thành viên phải tuân thủ quy định của pháp luật về thanh toán, quảng cáo, khuyến mại, bảo
                                vệ quyền sở hữu trí tuệ, và các quy định của pháp luật có liên quan khác khi tham gia
                                website.</p>
                            <p>Thành viên cam kết, đồng ý không sử dụng dịch vụ của Website www.inplace.vn vào những mục
                                đích bất hợp pháp, không hợp lý, lừa đảo, đe doạ, thăm dò thông tin bất hợp pháp, phá
                                hoại, tạo ra và phát tán virus gây hư hại tới hệ thống, cấu hình, truyền tải thông tin
                                của Website www.inplace.vn Trong trường hợp vi phạm thì Thành viên phải chịu trách nhiệm
                                về các hành vi của mình trước pháp luật.</p>
                            <p>Thành viên cam kết không được thay đổi, chỉnh sửa, sao chép, truyền bá, phân phối, cung
                                cấp và tạo những công cụ tương tự của dịch vụ do Website www.inplace.vn cung cấp cho một
                                bên thứ ba nếu không được sự đồng ý của INPLACE VIETNAM trong bản Quy chế này.</p>
                            <p>Thành viên không được hành động gây mất uy tín của Website www.inplace.vn dưới mọi hình
                                thức như gây mất đoàn kết giữa các Thành viên bằng cách sử dụng tên đăng ký thứ hai,
                                thông qua một bên thứ ba hoặc tuyên truyền, phổ biến những thông tin không có lợi cho uy
                                tín của Website www.inplace.vn.</p>
                            <p>Đăng bất cứ thông tin tiểu sử không hoàn thành, sai hoặc không chính xác hoặc những thông
                                tin không phải là đơn xin việc chính xác của riêng Thành viên (cá nhân tìm kiếm công
                                việc trên cơ sở toàn thời gian hay bán thời gian)</p>
                            <p>Sử dụng dữ liệu đơn xin việc của Website www.inplace.vn cho bất cứ mục đích nào khác khi
                                là một Doanh nghiệp tìm kiếm Cá nhân, bao gồm mà không hạn chế sử dụng thông tin trong
                                kho dữ liệu này để bán hoặc giới thiệu bất cứ sản phẩm hoặc dịch vụ nào. Gửi thư hoặc
                                thư điện tử, thực hiện những cuộc gọi hoặc gửi fax gạ gẫm liên quan đến việc xúc tiến
                                và/hoặc quảng cáo sản phẩm hoặc dịch vụ đến những Thành viên khác của Website
                                www.inplace.vn.</p>
                            <p>Thành viên nhận thức và đồng ý rằng Thành viên sẽ có trách nhiệm với hình thức nội dung
                                và độ chính xác của bất cứ đơn xin việc hay tài liệu chứa trong đó do Thành viên đăng
                                tải trên Website www.inplace.vn..</p>
                            <h2>X. Quyền và trách nhiệm của Website <a href="//www.inplace.vn">www.inplace.vn</a></h2>
                            <h3>1. Quyền của Website <a href="//www.inplace.vn">www.inplace.vn</a></h3>
                            <p>Website www.inplace.vn sẽ tiến hành cung cấp các dịch vụ cho những Thành viên tham gia
                                sau khi đã hoàn thành các thủ tục và các điều kiện bắt buộc bao gồm (nhưng không giới
                                hạn) các thông tin Thành viên dự định đăng tải trên Website www.inplace.vn.</p>
                            <p>Trong trường hợp có cơ sở để chứng minh Thành viên cung cấp thông tin cho Website
                                www.inplace.vn không chính xác, sai lệch, không đầy đủ hoặc vi phạm pháp luật hay thuần
                                phong mỹ tục Việt Nam thì Website www.inplace.vn có quyền từ chối, tạm ngừng hoặc chấm
                                dứt quyền sử dụng dịch vụ của Thành viên.</p>
                            <p>Website www.inplace.vn có thể chấm dứt quyền Thành viên và quyền sử dụng một hoặc tất cả
                                các dịch vụ của Thành viên và sẽ thông báo cho Thành viên trong thời hạn ít nhất là một
                                (01) tháng trong trường hợp Thành viên vi phạm các Quy chế của Website www.inplace.vn
                                hoặc có những hành vi ảnh hưởng đến hoạt động kinh doanh trên Website
                                www.inplace.vn.</p>
                            <p>Website www.inplace.vn có thể chấm dứt ngay quyền sử dụng dịch vụ và quyền của Thành viên
                                nếu Website www.inplace.vn phát hiện Thành viên đã phá sản, bị kết án hoặc đang trong
                                thời gian thụ án, trong trường hợp Thành viên tiếp tục hoạt động có thể gây hại cho
                                Website www.inplace.vn có những hoạt động lừa đảo, giả mạo, gây rối loạn thị trường, gây
                                mất đoàn kết đối với các thành viên khác của Website www.inplace.vn, hoạt động vi phạm
                                pháp luật hiện hành của Việt Nam.</p>
                            <p>Trong trường hợp chấm dứt quyền thành viên và quyền sử dụng dịch vụ thì tất cả các chứng
                                nhận, các quyền của thành viên được cấp sẽ mặc nhiên hết giá trị và bị chấm dứt.</p>
                            <p>Website www.inplace.vn không chịu trách nhiệm khi Thành viên/người sử dụng lấy email của
                                người khác sử dụng với mục đích spam, phá hoại hoặc gửi các nội dung xấu, mang tính chất
                                đồi trụy, ảnh hưởng đến chủ nhân của email đó. Các hành vi và nội dung vi phạm pháp luật
                                Thành viên/ người sử dụng vi phạm sẽ tự chịu trách nhiệm trước pháp luật. Website
                                www.inplace.vn có quyền xóa, khóa tài khoản vi phạm thỏa thuận sử dụng mà không cần báo
                                trước.</p>
                            <p>Website www.inplace.vn chỉ đóng vai trò là nơi để các nhân và doanh nghiệp đăng tải thông
                                tin quảng bá cá nhân, quảng bá văn hoá doanh nghiệp và thông tin về việc làm. Website
                                www.inplace.vn không chịu trách nhiệm đến quá trình giao dịch trực tiếp giữa Doanh
                                nghiệp và Cá nhân. Kết quả là Công ty không thể kiểm soát được chất lượng, độ an toàn
                                hay hợp pháp của công việc hay đơn xin việc được đăng tải, tính xác thực hay chính xác
                                của danh sách được đề ra, khả năng cung cấp công việc cho Cá nhân của Doanh nghiệp hay
                                khả năng của Cá nhân khi xin việc. Thêm vào đó, Thành viên/ người sử dụng hãy ghi chú
                                những nguy hiểm có thể xảy ra khi sử dụng Website www.inplace.vn, bao gồm nhưng không
                                hạn chế sự nguy hiểm khi bị tấn công cơ thể, liên quan đến người lạ, quốc tịch nước
                                ngoài, những người không đủ tuổi hay những người đưa ra những đòi hỏi sai trái. Thành
                                viên nên hiểu rằng tất cả những nguy hiểm liên quan đến những người sử dụng khác mà
                                Thành viên liên hệ thông qua trang Website www.inplace.vn hoàn toàn là vấn đề riêng của
                                Thành viên, Website www.inplace.vn sẽ hoàn toàn không liên quan đến bất kỳ nguy hiểm nào
                                được đề cập như trên xảy ra/ hoặc có thể xảy ra cho Thành viên.</p>
                            <p>Website www.inplace.vn không chịu trách nhiệm pháp lý và đồng thời không kiểm soát được
                                những thông tin do những Thành viên/ người sử dụng khác cung cấp thông qua trang Website
                                www.inplace.vn. Theo lẽ rất tự nhiên, thông tin của những người khác có thể gây khó
                                chịu, có hại, không chính xác và trong một vài trường hợp sẽ được ghi sai tên hoặc giả
                                vờ ghi sai.</p>
                            <p>Website www.inplace.vn không được xem là một Doanh nghiệp, đồng thời Website
                                www.inplace.vn sẽ không chịu trách nhiệm với bất cứ quyết định nào trong công việc, cho
                                dù với lý do nào, do bất cứ tổ chức nào đăng tải công việc trên Website
                                www.inplace.vn.</p>
                            <h3>2. Nghĩa vụ Website <a href="//www.inplace.vn">www.inplace.vn</a></h3>
                            <p>Website www.inplace.vn chịu trách nhiệm xây dựng website bao gồm một số công việc chính
                                như: nghiên cứu, thiết kế, mua sắm các thiết bị phần cứng và phần mềm, kết nối Internet,
                                xây dựng chính sách phục vụ cho hoạt động Website www.inplace.vn trong điều kiện và phạm
                                vi cho phép.</p>
                            <p>Website www.inplace.vn sẽ tiến hành triển khai và hợp tác với các đối tác trong việc xây
                                dựng hệ thống các dịch vụ, các công cụ tiện ích phục vụ cho việc tương tác của các thành
                                viên tham gia và người sử dụng trên Website www.inplace.vn.</p>
                            <p>Website www.inplace.vn chịu trách nhiệm xây dựng, bổ sung hệ thống các kiến thức, thông
                                tin về: nghiệp vụ phát triển thương hiệu, xây dựng nội dung, tuyển dụng, hệ thống văn
                                bản pháp luật thương mại trong nước và quốc tế, thị trường nước ngoài, cũng như các tin
                                tức có liên quan đến hoạt động của Website www.inplace.vn.</p>
                            <p>Website www.inplace.vn sẽ tiến hành các hoạt động xúc tiến, quảng bá Website
                                www.inplace.vn ra thị trường nước ngoài trong phạm vi và điều kiện cho phép, góp phần mở
                                rộng, kết nối đáp ứng các nhu cầu tìm kiếm công việc của và phát triển tại thị trường
                                nước ngoài của Thành viên và nhu cầu tìm kiếm Cá nhân thích hợp của các công ty có nhu
                                cầu tuyển dụng nhân viên.</p>
                            <p>Website www.inplace.vn có biện pháp xử lý kịp thời khi phát hiện hoặc nhận được phản ánh
                                về thông tin/ đăng tải của Thành viên mà thông tin/ đăng tải đó không đúng sự thật và/
                                hoặc vi phạm pháp Quy chế và/ hoặc vi phạm pháp luật trên Sàn giao dịch thương mại điện
                                tử.</p>
                            <p>Website www.inplace.vn sẽ hỗ trợ cơ quan quản lý nhà nước điều tra các hành vi kinh doanh
                                vi phạm pháp luật, cung cấp thông tin đăng ký, lịch sử giao dịch và các tài liệu khác về
                                đối tượng có hành vi vi phạm pháp luật trên website.</p>
                            <p>INPLACE VIETNAM sẽ cố gắng đến mức cao nhất trong phạm vi và điều kiện có thể để duy trì
                                hoạt động bình thường của Website www.inplace.vn và khắc phục các sự cố như: sự cố kỹ
                                thuật về máy móc, lỗi phần mềm, hệ thống đường truyền internet, nhân sự, các biến động
                                xã hội, thiên tai, mất điện, các quyết định của cơ quan nhà nước hay một tổ chức liên
                                quan thứ ba. Tuy nhiên nếu những sự cố trên xẩy ra nằm ngoài khả năng kiểm soát, là
                                những trường hợp bất khả kháng mà gây thiệt hại cho thành viên thì Website
                                www.inplace.vn không phải chịu trách nhiệm liên đới. Website sẽ đảm bảo thực hiện các
                                chắc năng trong phạm vi của mình như sau:</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Đăng ký thiết lập website cung cấp dịch vụ theo quy định và
                                công bố các thông tin đã đăng ký trên trang chủ website.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Thiết lập cơ chế kiểm tra, giám sát để đảm bảo việc cung cấp
                                thông tin của thành viên trên sàn giao dịch thương mại điện tử được thực hiện chính xác,
                                đầy đủ.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Lưu trữ thông tin đăng ký của các thương nhân, tổ chức, cá
                                nhân tham gia sàn giao dịch thương mại điện tử và thường xuyên cập nhật những thông tin
                                thay đổi, bổ sung có liên quan.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Áp dụng các biện pháp cần thiết để đảm bảo an toàn thông tin
                                liên quan đến bí mật kinh doanh của thương nhân, tổ chức, cá nhân và thông tin cá nhân
                                của thành viên.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Có biện pháp xử lý kịp thời khi phát hiện hoặc nhận được phản
                                ánh về hành vi kinh doanh vi phạm pháp luật trên website.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Hỗ trợ cơ quan quản lý nhà nước điều tra các hành vi kinh
                                doanh vi phạm pháp luật, cung cấp thông tin đăng ký, lịch sử sử dụng và các tài liệu
                                khác về đối tượng có hành vi vi phạm pháp luật trên website.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Công bố công khai cơ chế giải quyết các tranh chấp phát sinh
                                trong quá trình tương tác trên website. Khi thành viên trên website phát sinh mâu thuẫn
                                với nhau (giữa Cá nhân và Doanh nghiệp hoặc ngược lại) hoặc bị tổn hại lợi ích hợp pháp,
                                sẽ cung cấp cho thành viên thông tin về Cá nhân/Doanh nghiệp, tích cực hỗ trợ thành viên
                                bảo vệ quyền và lợi ích hợp pháp của mình.</p>
                            <p>&nbsp; &nbsp;&nbsp;• &nbsp;Yêu cầu thương nhân, tổ chức, cá nhân trên là thành viên tại
                                website tử cung cấp các thông tin theo quy định: Tên và địa chỉ trụ sở của thương nhân,
                                tổ chức hoặc tên và địa chỉ thường trú của cá nhân; Số, ngày cấp và nơi cấp giấy chứng
                                nhận đăng ký kinh doanh của thương nhân, hoặc số, ngày cấp và đơn vị cấp quyết định
                                thành lập của tổ chức, hoặc mã số thuế cá nhân của cá nhân; Số điện thoại hoặc một
                                phương thức liên hệ trực tuyến khác.</p>
                            <p>&nbsp; &nbsp; • &nbsp;Loại bỏ khỏi website những thông tin giả mạo, vi phạm quyền sở hữu
                                trí tuệ, dịch vụ vi phạm pháp luật khác khi phát hiện hoặc nhận được phản ánh có căn cứ
                                xác thực về những thông tin này.</p>
                            <h2>XI. Điều khoản áp dụng</h2>
                            <p>Mọi tranh chấp phát sinh giữa Website www.inplace.vn và Thành viên sẽ được giải quyết
                                trên cơ sở thương lượng. Trường hợp không đạt được thỏa thuận như mong muốn, một trong
                                hai bên có quyền đưa vụ việc ra Tòa án nhân dân có thẩm quyền để giải quyết.</p>
                            <p>Quy chế của Website www.inplace.vn chính thức có hiệu lực kể từ ngày đăng lên website địa
                                chỉ tên miền WWW.INPLACE.VN. Website www.inplace.vn có quyền điều chỉnh, thay đổi Quy
                                chế này cho phù hợp với thực tiễn hoạt động và có trách nhiệm thông báo lên Website
                                www.inplace.vn cho các thành viên biết. Quy chế sửa đổi có hiệu lực kể từ ngày có thông
                                báo sửa đổi.Việc thành viên tiếp tục sử dụng dịch vụ sau khi Quy chế sửa đổi được công
                                bố và thực thi đồng nghĩa với việc họ đã chấp nhận Quy chế sửa đổi này.</p>
                            <p>Website www.inplace.vn và Thành viên đồng ý cam kết thực hiện đúng các điều khoản trong
                                nội dung Quy chế này.</p>
                            <p>Bất kỳ tranh cãi, khiếu nại hay tranh chấp phát sinh từ hoặc liên quan đến các Điều khoản
                                và Điều kiện này sẽ được đưa đến và giải quyết cuối cùng bằng trọng tài cá nhân và bí
                                mật trước khi đưa đến một trọng tài tại Thành phố Hồ Chí Minh và chi phối bởi pháp luật
                                Việt Nam. Trọng tài viên phải là người được đào tạo một cách hợp pháp, có kinh nghiệm
                                trong lĩnh vực công nghệ thông tin ở Việt Nam và là độc lập đối với mỗi bên. Như đã đề
                                cập, Website www.inplace.vn có quyền bảo vệ quyền sở hữu trí tuệ và thông tin bí mật
                                thông qua huấn thị hoặc các chỉ thị tương tự từ tòa án.</p>
                            <p>Thành viên sẽ phải bồi thường và Website www.inplace.vn (và các cán bộ, giám đốc, đại lý,
                                các công ty con, công ty liên doanh và nhân viên của Website www.inplace.vn) không phải
                                chịu bất kỳ yêu cầu bồi thường, bao gồm cả phí luật sư, của bất kỳ bên thứ ba nào do
                                hoặc phát sinh từ việc Thành viên vi phạm Thỏa thuận này hoặc vi phạm bất kì luật hoặc
                                quyền lợi của bên thứ ba.</p>
                            <p>KHÔNG ĐẠI LÝ: Không có mối quan hệ đại lý, liên doanh, nhân viên, người sử dụng lao động
                                hoặc nhượng quyền, nhận lại quyền được tạo ra bởi Thỏa thuận này. INPLACE.VN sẽ gửi
                                thông báo cho Thành viên bằng email đến địa chỉ email Thành viên cung cấp cho 'Website
                                www.inplace.vn trong quá trình đăng ký. Thông báo sẽ được cho là đã nhận 24 giờ sau khi
                                email được gửi đi, trừ khi bên gửi được thông báo rằng địa chỉ email không hợp lệ. Ngoài
                                ra, Website www.inplace.vn có thể cung cấp cho Thành viên thông báo pháp lý bằng thư đến
                                địa chỉ được cung cấp trong quá trình đăng ký. Thông báo gửi đến cho Thành viên bằng thư
                                sẽ được cho là đã được nhận sau ba ngày, kể từ ngày gửi thư.</p>
                            <p>Ngoài các biện pháp pháp lý, Website www.inplace.vn có thể, ngay lập tức chấm dứt các
                                Điều khoản và Điều kiện hoặc hủy bỏ bất kỳ hoặc tất cả các quyền lợi của Thành viên được
                                cấp theo các Điều khoản và Điều kiện mà không cần thông báo trước cho Thành viên. Khi
                                chấm dứt Thỏa thuận này, Thành viên sẽ phải ngay lập tức ngừng việc truy cập và sử dụng
                                Trang web, và Website www.inplace.vn, ngoài các biện pháp pháp lý, ngay lập tức thu hồi
                                tất cả các mật khẩu và xác định tài khoản đã cấp cho Thành viên và từ chối truy cập và
                                sử dụng Trang web một phần hoặc toàn bộ. Việc chấm dứt thỏa thuận này sẽ không ảnh hưởng
                                đến quyền và nghĩa vụ tương ứng (bao gồm nhưng không giới hạn nghĩa vụ thanh toán) của
                                các bên phát sinh trước ngày chấm dứt. Hơn nữa, Thành viên đồng ý rằng Trang web sẽ
                                không chịu trách nhiệm với Thành viên hoặc bất kỳ người nào khác kết quả của bất kỳ đình
                                chỉ hoặc chấm dứt đó. Nếu Thành viên không hài lòng với Website www.inplace.vn hoặc bất
                                kỳ điều khoản, điều kiện, quy định, chính sách, hướng dẫn, hoặc thực hành của Công ty
                                TNHH Inplace Vietnam điều hành trang web, biện pháp khắc phục duy nhất là ngưng sử dụng
                                trang web.</p>
                            <h2>XII. Địa chỉ liên hệ công ty sở hữu website <a
                                    href="//www.inplace.vn">www.inplace.vn</a></h2>
                            <p><strong>Công ty TNHH INPLACE VIETNAM</strong></p>
                            <p>Địa chỉ liên hệ:</p>
                            <p><i class="fas fa-map-marker-alt"></i> <strong>Tại Hà Nội:</strong> Tầng 3, tòa nhà Đại Phát, ngõ 82 Duy Tân, Cầu Giấy, Hà Nôi
                            </p>
                            <p><i class="fas fa-map-marker-alt"></i> <strong>Tại TP Hồ Chí Minh:</strong> Tầng 2, tòa nhà Mekong, só 235-241 Cộng Hòa, Quận
                                Tân Bình, TP Hồ Chí Minh.</p>
                        <p><i class="fas fa-phone"></i> <strong>Điện thoại:</strong> 028 6275 5586</p>
                        <p><i class="fas fa-envelope"></i> <strong>Email:</strong> services@inplace.vn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/operationRegulations.js')}}"></script>
@stop
