<div class="job-detail_head">
    <div class="container">
        <div class="box">
            <div class="thumb">
                <div class="row">
                    <div class="col-lg-8 thumb_img-big">
                        <a href="#">
                            @if( jjobCheckImageExit($job->feature_image) )
                                <img src="{{ url($job->feature_image) }}" alt="" class="">
                            @else
                                <img src="{{ asset('images/no_img_730x286.jpg') }}" alt="" class="">
                            @endif
                        </a>
                    </div>
                    <div class="col-lg-4 thumb_img-custom-layout">
                        <div class="row">
                            <div class="col-6 col-lg-12">
                                <a href="#">
                                    <img src="{{ asset('images/no_img_joyful-job.jpg') }}" alt="" class="">
                                </a>
                            </div>
                            <div class="col-6 col-lg-12">
                                <a href="#">
                                    <img src="{{ asset('images/no_img_joyful-life.jpg') }}" alt="" class="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="job-item-tag">

                            <div class="tag-list">

                                @if($job->hot)
                                    <div class="tag-hot-job">
                                        <span>Hot Job</span>
                                    </div>
                                @endif
                                <a href="#" class="tag-item">
                                    {{ $job->short_title }}
                                </a>
                            </div>
                        </div>

                        <h1 class="title">
                            <a href="">
                                {{ $job->title }}
                            </a>
                        </h1>
                    </div>
                    <div class="col-md-4 text-right">
                        <span class="view-count">{{ $job->view_count }} lượt xem</span>
                        <div class="share_social">
                            <a
                                class="btn-share-icon share_facebook_social"
                                target="_blank"
                                href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a
                                class="btn-share-icon share_facebook_social"
                                href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ url()->current() }}"
                                target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                        <div class="button-group">
                            @if(auth()->check())
                                @if( empty($job->userSaveJob()->where('user_id', auth()->id())->count()) )
                                    <button
                                        data-url="{{ route('jobs.save', ['id' => $job->id]) }}"
                                        class="btn-save-job save_job btn btn-custom_light">
                                        <i class="far fa-bookmark"></i>
                                    </button>
                                @else
                                    <button disabled class="btn-save-job saved btn btn-custom_light">
                                        <i class="far fa-bookmark"></i>
                                    </button>
                                @endif
                            @else
                                <a href="{{ route('register') }}"
                                   class="btn-save-job btn btn-custom_light">
                                    <i class="far fa-bookmark"></i>
                            @endif
                            @include('partials.apply-job-button')
                        </div>
                    </div>
                </div>
            </div>


            <div class="company">
                <div class="row align-items-center">
                    <div class="col-sm-7">
                        <div class="d-flex">
                            <div class="logo">
                                <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}">
                                    @if( jjobCheckImageExit(optional($job->customer)->logo) )
                                        <img src="{{ asset(optional($job->customer)->logo) }}" alt="" class="avatar ">
                                    @else
                                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" alt=""
                                             class="avatar ">
                                    @endif
                                </a>
                            </div>
                            <div class="info">
                                <h2 class="title">
                                    <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}">
                                        {{ optional($job->customer)->name }}
                                    </a>
                                </h2>
                                <div class="desc">
                                    {{ jobHelper::countUserFollowCustomer(optional($job->customer)->id) }} người
                                    theo dõi
                                    • {{ $job->created_at->diffForHumans() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 text-right">
                        <div class="talk">
                            {{--<a href="">--}}
                            {{--<img src="/images/icon-chat.png" alt="">--}}
                            {{--Trò chuyện <br>với nhà tuyển dụng--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
