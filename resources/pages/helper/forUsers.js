import {getURLParameter} from '../_elements/getURLParameter.js';
$(function () {
    let id = getURLParameter('k');
    if (id){
        $('#' + id + '-tab').click();
    }
});

