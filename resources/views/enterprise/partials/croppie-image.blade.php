
<div class="inputCroppieImage fixHeight {{ !empty($new) && jjobCheckImageExit($new->thumbnail) ? 'hasImg' : '' }}"
     style="background-image: url('{{  !empty($new) && jjobCheckImageExit($new->thumbnail) ? asset($new->thumbnail) : '' }}')">
    <div class="inputCroppieImage_inpPr">
        <label><i class="fal fa-image"></i> Kéo hoặc nhấp để sửa ảnh</label>
        <input type="file" name="" class="croppieImage_input" accept=".png, .jpg, .jpeg">
    </div>
    <div class="inputCroppieImage_crop">
        <div class="upload-crop-demo"></div>
        <div class="upload-crop-button">
            <button type="button" class="btnRemoveCrop btn btn-grey">Gỡ</button>
        </div>
    </div>
</div>
