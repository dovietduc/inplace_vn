@extends('layouts.app')

@section('meta')
    <title>{{ $newItem->title }}</title>
    <meta property="og:url" content="{{ url()->current() }} "/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{ $newItem->title }}"/>
    <meta property="og:description" content="{{ strip_tags($newItem->description) }}"/>
    <meta property="og:image"
          content="{{ jjobCheckImageExit($newItem->thumbnail) ? url($newItem->thumbnail) : asset('images/no-img-970x380.jpg') }}"/>
    <meta property="og:image:width" content="1110"/>
    <meta property="og:image:height" content="435"/>
    <meta property="fb:app_id" content="2226038500857645"/>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('vendor/froala-editor/css/froala_style.css')}}">
    <link rel="stylesheet" href="{{ asset('pages/news/detail/detail.css') }}">
@stop


@section('content')
    @php
        $customer = $newItem->customer;
        if (!empty($customer)) {
            $customerAdress = $customer->customerAddress()->pluck('city_id');
            $cityCustomer = \App\City::whereIn('id', $customerAdress)->pluck('name')->implode(', ');
        }

    @endphp
    @include('company.partials.company-head-menu')
    <div class="page-detail-news">
        <div class="container">
            <div class="new-detail-thumb">
                @if( jjobCheckImageExit($newItem->thumbnail) )
                    <img src="{{ url($newItem->thumbnail) }}" alt="" />
                @else
                    <img src="{{ asset('/images/no_img_1110x435.jpg') }}" alt="" />
                @endif
            </div>
            @include('news.partials.content-new-detail')
            @include('news.partials.new-relative')
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('pages/news/detail/detail.js') }}"></script>
@stop
