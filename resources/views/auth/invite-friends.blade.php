@extends('layouts.app')
@section('meta')
    <title>Mời bạn bè tham gia Inplace</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/users/invite-friends/invite-friends.css')}}">
@stop
@section('content')
    <div class="invite-friends-page">
        <div class="invite-friends-head">
            <div class="container">
                <div class="row">
                   <div class="col-md-10 col-lg-8 offset-md-1 offset-lg-2 text-center">
                       <h1>Mời bạn bè đến với Inplace</h1>
                       <p>Bạn có thể dễ dàng mời bạn bè của mình gia nhập Inplace bằng các cách sau. Hãy tạo nên cộng đồng cho riêng mình, tăng tương tác, chia sẻ thông tin, nâng cao uy tín của cá nhân và mang lại giá trị cho những người xung quanh.
                       </p><p>Mời càng nhiều bạn, điểm số Inplace Score sẽ càng cao !</p>
                       <a href="" class="btn btn-custom" data-toggle="modal" data-target="#inviteFriendModal"><i class="far fa-envelope"></i> Mời bạn bè qua email</a>
                       <span class="d-inline-block">
                           <a href="" class="btn btn-custom btn-facebook"><i class="fab fa-facebook-square"></i> Chia sẻ qua facebook</a>
                           <a href="" class="btn btn-custom btn-linkedin"><i class="fab fa-linkedin"></i> Chia sẻ qua Linkedin</a>
                       </span>
                   </div>
                </div>
            </div>
        </div>
        <div class="invite-friends-content">
            <div class="container">
                <h2>03 LỢI ÍCH KHI MỜI BẠN BÈ</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="invite-friends-content_col">
                            <img src="{{asset('images/icon_help_friend.png')}}" alt="">
                            <h3>Help your friend</h3>
                            <div class="desc">
                                Giúp bạn bè mình tiếp cận với nhiều cơ hội việc làm, tìm được môi trường làm việc đáng mơ ước và kết nối không giới hạn.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="invite-friends-content_col">
                            <img src="{{asset('images/icon_help_yourself.png')}}" alt="">
                            <h3>Help your friend</h3>
                            <div class="desc">
                                Giúp bạn bè mình tiếp cận với nhiều cơ hội việc làm, tìm được môi trường làm việc đáng mơ ước và kết nối không giới hạn.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="invite-friends-content_col">
                            <img src="{{asset('images/icon_help_community.png')}}" alt="">
                            <h3>Help the community</h3>
                            <div class="desc">
                                Giúp đưa đúng người vào đúng chỗ, góp phần tạo ra nhiều mối quan hệ công việc bền chặt, là động cơ góp phần thúc đẩy sự thịnh vượng chung cho xã hội.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-invite-friend modal fade" id="inviteFriendModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="">
                    @csrf
                    <input type="hidden" name="customer_id" value="">
                    <div class="modal-header align-items-center">
                        <h5 class="modal-title">Mời bạn bè qua email</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                                class="far fa-times-circle"></i></button>
                    </div>
                    <div class="modal-body">
                        <p>Mời những người bạn biết tham gia Inplace. <br>
                            Bạn càng có nhiều kết nối trên Inplace, các bài đăng công việc phù hợp hơn với bạn sẽ được đề xuất.</p>
                        <h4>Mời bạn bè theo địa chỉ email</h4>
                        <textarea class="form-control"
                                  rows="5"
                                  name="employee_invitations"
                                  placeholder="hovaten@gmail.com&#10;nickname@gmail.com&#10;nguoidung@gmail.com"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-custom">
                            <i class="far fa-envelope"></i> Gửi lời mời
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/users/invite-friends/invite-friends.js')}}"></script>
@stop
