<div class="modal-delete-member modal fade" data-member="" tabindex="-1" role="dialog" aria-hidden="true">
    <input type="hidden" class="member_delete_id" value=""/>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <h5 class="modal-title">Xóa thành viên</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                        class="far fa-times-circle"></i></button>
            </div>
            <div class="modal-body">
                <p>Bạn có chắc chắn muốn xóa thành viên này?</p>
                <img class="member-avatar" src="" alt="">
                <h4 class="member-name"></h4>
                <p class="member-desc position"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-grey" data-dismiss="modal" aria-label="Close">Bỏ qua</button>
                <button type="button"
                        data-url="{{ route('enterprise.member.deleteMember') }}"
                        class="btn-delete-member-confirm btn btn-custom">Xóa</button>
            </div>
        </div>
    </div>
</div>
