<?php

namespace App\Http\Controllers;

use App\MemberCompany;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function login()
    {
        return view('html.login.login');
    }

    public function register()
    {
        return view('html.register.register');
    }

    public function verify_email()
    {
        return view('html.verify.verify');
    }

    public function forgot_step1()
    {
        return view('html.forgot.step1');
    }

    public function forgot_step2()
    {
        return view('html.forgot.step2');
    }

    public function forgot_step3()
    {
        return view('html.forgot.step3');
    }

    public function forgot_step4()
    {
        return view('html.forgot.step4');
    }

    public function invite_friends()
    {
        return view('auth.invite-friends');
    }

    public function account_setting()
    {
        return view('auth.account-setting');
    }

	public function getList()
	{
		return view('html.job.list');
	}
    public function getDetail()
    {
        return view('html.job.detail');
    }

    public function profile_Home()
    {
        return view('html.profile.home');
    }
    public function profile_Posts()
    {
        return view('html.profile.posts');
    }
    public function profile_Network()
    {
        return view('html.profile.network');
    }

    public function companyCreate()
    {
        return view('company.create');
    }
    public function companyFollowing()
    {
        return view('company.following');
    }
    public function companyMembers()
    {
        return view('company.members');
    }
    public function dashboard_news_list()
    {
        return view('html.dashboard.news.list');
    }
    public function dashboard_news_detail()
    {
        return view('html.dashboard.news.detail');
    }
    public function dashboard_home()
    {
        return view('html.dashboard.index');
    }
    public function dashboard_jobs_list()
    {
        return view('html.dashboard.jobs.list');
    }
    public function dashboard_jobs_add()
    {
        $customerId = session('customer_id');
        $listUsersFollowCustomer = MemberCompany::where('customer_id', $customerId)->get();
        return view('html.dashboard.jobs.add', compact('listUsersFollowCustomer'));
    }
    public function dashboard_jobs_candidates()
    {
        return view('html.dashboard.jobs.candidates');
    }
    public function save_jobs()
    {
        return view('html.job.save-jobs');
    }
    public function apply_jobs()
    {
        return view('html.job.apply-jobs');
    }
    public function dashboard_info_company()
    {
        return view('html.dashboard.info.company');
    }
    public function dashboard_info_members()
    {
        return view('html.dashboard.info.members');
    }
    public function dashboard_info_members_invite()
    {
        return view('html.dashboard.info.members-invite');
    }
    public function dashboard_info_members_invite_2()
    {
        return view('html.dashboard.info.members-invite-2');
    }
    public function enterprise_candidates_list()
    {
        return view('html.dashboard.candidates.list');
    }
    public function enterprise_candidates_detail()
    {
        return view('enterprise.candidates.detail');
    }
    public function danh_cho_ca_nhan() {
        return view('html.helper.forUsers');
    }
    public function danh_cho_doanh_nghiep() {
        return view('html.helper.forEnterprise');
    }
    public function tro_giup() {
        return view('html.helper.help');
    }
    public function thong_bao() {
        return view('html.helper.notifications');
    }
    public function quy_dinh_bao_mat() {
        return view('html.helper.privacyRegulations');
    }
    public function dieu_khoan_su_dung() {
        return view('html.helper.termsOfUse');
    }
    public function cham_soc_khach_hang() {
        return view('html.helper.cskh');
    }
    public function quy_che_hoat_dong() {
        return view('html.helper.operationRegulations');
    }
    public function goc_doanh_nghiep() {
        return view('html.helper.BusinessCorner');
    }
    public function gioi_thieu() {
        return view('html.helper.aboutUs');
    }

    public function admin_index() {
        return view('html.admin.index');
    }
    public function admin_login() {
        return view('html.admin.login');
    }
    public function admin_news_list() {
        return view('html.admin.news.list');
    }
    public function admin_jobs_list() {
        return view('html.admin.jobs.list');
    }
    public function admin_candidates_list() {
        return view('admin.candidates.list');
    }
    public function admin_candidates_detail() {
        return view('admin.candidates.detail');
    }

//email Template
    public function email_active_acount() {
        return view('emails.active-acount');
    }
    public function email_invite_member() {
        return view('emails.invite-member');
    }
    public function email_customer__user_apply_job() {
        return view('emails.send-to-customer.user-apply-job');
    }



    public function jobMembersEditorSelect() {
        dd(122);

    }
    public function memberList($customerId) {

    }

    public function getTags(Request $request) {

        $search = $request->search;

        if($search == ''){
            $employees = \DB::table('jjob_tags')->orderby('name','asc')->select('id','name')->limit(5)->get();
        }else{
            $employees = \DB::table('jjob_tags')->orderby('name','asc')->select('id','name')->where('name', 'like', '%' .$search . '%')->limit(5)->get();
        }
        return response()->json($employees);
    }

    public function getOccupationSup(Request $request) {
        $search = $request->search;
        $occupation = \DB::table('occupations')->select('id','name')->where('parent_id', $search)->get();
        return response()->json($occupation);
    }

    public function getJobFilter(Request $request) {
        $search = $request->all();
        return response()->json([
            'code' => 200,
            'message' => 'success',
            'data_search' =>  $search,
        ], 200);
    }
    public function getJobSearchKeyworks(Request $request) {
        $search = $request->search;
        $occupationSupportings = \DB::table('occupations')->select('id','name')->where('name', 'like', '%'.$search.'%')->where('parent_id', '!=', 0)->limit(5)->get();
        $occupationDetails = \DB::table('position_occupations')->select('id','name')->where('name', 'like', '%'.$search.'%')->limit(5)->get();
        $viewSearch = view('jobs.partials.filter-job-keyworks-dropdown', compact('occupationSupportings','occupationDetails'))->render();
        return response()->json([
            'code' => 200,
            'message' => 'success',
            'data_search' =>  $viewSearch,
        ], 200);
    }

}
