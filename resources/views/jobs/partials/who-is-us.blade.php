<div class="component-who mb-5">
    <ul class="tab-who nav" role="tablist">

        @foreach($members as $key => $member)
            <li class="col-3">
                <a class="item {{ $key == 0 ? 'active show' : '' }}" data-toggle="tab" href="#tab-{{ $key }}" role="tab"
                   aria-controls="home"
                   aria-selected="true">
                    @if(empty(optional($member->user)->avatar))
                        <img class="avatar" src="{{asset('images/no_user_125x125.jpg')}}" alt="">
                    @else
                        @if(str_contains(optional($member->user)->avatar, 'upload') && jjobCheckImageExit(optional($member->user)->avatar))
                            <img class="avatar" src="{{ asset(optional($member->user)->avatar) }}" alt="">
                        @else
                            <img class="avatar" src="{{ optional($member->user)->avatar }}" alt="">
                        @endif
                    @endif
                    <h5 class="name">{{ optional($member->user)->name }}</h5>
                    <div class="position">{{ $member->position }}</div>
                </a>
            </li>
        @endforeach

    </ul>
    <div class="tab-content">
        @foreach($members as $key => $member)
            <div class="item_content tab-pane {{ $key == 0 ? 'active show' : '' }}" id="tab-{{ $key }}"
                 role="tabpanel" aria-labelledby="home-tab">
                {{ $member->introduction }}
            </div>
        @endforeach
    </div>
</div>
