<div class="txt-format">
    <h1>Liên hệ Inplace</h1>
        <p>Inplace
            luôn ở đây khi bạn cần. Bạn có thể liên hệ với Inplace Team theo nhiều cách khác nhau:</p>
        <ul>
            <li>Qua Email: <a href="mailto:services@inplace.vn">services@inplace.vn</a></li>
            <li>Qua Hotline CSKH: <strong>028 6275 5586</strong></li>
            <li>Qua Fan Page của chúng tôi tại Facebook: <a href="https://www.facebook.com/inplacevietnam" target="_blank">https://www.facebook.com/inplacevietnam</a>
            </li>
            <li>Qua tài khoản Linkedin: <a href="https://www.linkedin.com/company/inplacevietnam" target="_blank">https://www.linkedin.com/company/inplacevietnam</a>
            </li>
        </ul>
        <p>Khi bạn có bất cứ câu hỏi, thắc mắc, góp ý hay yêu cầu nâng cấp dịch vụ, đừng ngần ngại liên hệ với Inplace
            để được hỗ trợ tốt nhất.</p>
</div>
