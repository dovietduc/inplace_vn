<div class="txt-format">
    <h1>Gửi câu hỏi cho Inplace</h1>
    <p>Bạn có thể gửi các câu hỏi cho Inplace để hỏi về các tính năng, công cụ, cách sử dụng website theo mẫu sau đây.
    </p>
    <div class="row">
        <div class="col-md-8 col-lg-7">
            <form method="POST" class="my-4">
                <div class="form-group">
                    <label for="">Tiêu đề *</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Email của bạn *</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Nội dung cần hỏi *</label>
                    <textarea class="form-control" rows="8"></textarea>
                </div>
                <div class="form-group">
                    <label for="">File đính kèm</label><br>
                    <input type="file">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-custom px-5">GỬI</button>
                </div>
            </form>
        </div>
    </div>
    <h4>Một số câu hỏi thường gặp tại Inplace:</h4>
    <p>
        <i class="fal fa-angle-right"></i> <a href="#">5 Lý do vì sao doanh nghiệp nên thực hiện Employer
            Branding</a><br>
        <i class="fal fa-angle-right"></i> <a href="#">Bài học kinh điển về Employer Branding của các ông lớn</a><br>
        <i class="fal fa-angle-right"></i> <a href="#">Tại sao cần có Thương Hiệu Nhà Tuyển Dụng tốt để thu hút nhân tài
            hiệu quả</a>
    </p>
</div>
