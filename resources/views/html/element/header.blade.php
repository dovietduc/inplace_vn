<header class="header">
	<div class="group">
		<a href="" class="logo">
			<img src="/images/logo_short.png" alt="">
		</a>
		<ul class="menu clearfix">
			<li class="active">
				<a href="#">
					<img src="/images/icon_job.png" alt="">
					<span>Việc làm</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="/images/icon_news.png" alt="">
					<span>Khám phá</span>
				</a>
			</li>
			<li>
				<a href="#">
					<img src="/images/icon_post.png" alt="">
					<span>Viết bài</span>
				</a>
			</li>
		</ul>
	</div>
	<div class="group group--right">
		<form class="search">
			<input type="text" placeholder="Nhập tìm kiếm công việc, bạn bè và công ty">
			<button type="submit"><img src="/images/icon_search.png" alt=""></button>
		</form>
		<div class="right">
			<div class="register hidden">
				<span class="txt">Bạn chưa có tài khoản? </span>
				<a href="#" class="link">Đăng ký ngay</a>
			</div>
			<div class="notification">
				<ul class="list clearfix">
					<li><a href="#"><img src="/images/icon_message.png" alt=""></a></li>
					<li><a href="#"><i class="fas fa-bell"></i></a></li>
				</ul>
				<div class="avatar" id="header_avatar">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-display="static">
							<img src="/images/icon_avatar.png" alt="">
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<div class="profile">
								<div class="info">
									<div class="d-flex">
										<a href="#"><h5>John Doe<span class="score">1288Đ</span></h5></a>
									</div>
									<a href="#" class="edit">Chỉnh sửa trang cá nhân</a>
								</div>
								<div class="avatar">
									<a href="#">
										<img src="/images/img_avatar.png" alt="user">
									</a>
								</div>
							</div>
							<div class="bookmark">
								<div class="item">
									<a href="#">
										<i class="far fa-paper-plane"></i>
										<span>Công việc ứng tuyển</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<i class="far fa-bookmark"></i>
										<span>Công việc đã lưu</span>
									</a>
								</div>
							</div>
							<div class="setting">
								<div class="item">
									<a href="#">
										<i class="fas fa-building"></i>
										<span>Công ty đang theo dõi</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<i class="fas fa-user-plus"></i>
										<span>Mời bạn bè tham gia Inplace</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<i class="fas fa-user-cog"></i>
										<span>Cài đặt tài khoản</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<i class="fas fa-sign-out-alt"></i>
										<span>Thoát</span>
									</a>
								</div>
							</div>
							<div class="account-list">
								<div class="item active">
									<a href="#">
										<div class="avatar">
											<img src="/images/img_avatar.png" alt="avatar">
										</div>
										<span class="name">John Doe</span>
										<div class="check">
                                            <i class="fas fa-check"></i>
                                        </div>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<div class="avatar">
											<img src="/images/img_avatar.png" alt="avatar">
										</div>
										<span class="name">HRE Empire Vietnam</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<div class="avatar">
											<img src="/images/img_avatar.png" alt="avatar">
										</div>
										<span class="name">Aglobe Tech VN</span>
									</a>
								</div>
								<div class="item">
									<a href="#">
										<div class="avatar">
											<div class="add-account">
												<i class="fas fa-plus"></i>
											</div>
										</div>
										<span class="create">Tạo trang doanh nghiệp</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</header><!-- /header -->
