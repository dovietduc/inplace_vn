<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->makeAbsoluteUrls();
    }

    /**
     * Make relative urls into absolute urls
     *
     * @return void
     */
    public function makeAbsoluteUrls()
    {
        foreach(Config::get('services') as $key => $config) {

            if ( ! isset($config['redirect'])) continue;

            Config::set("services.$key.redirect", url($config['redirect']));
        }
    }
}