<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class TagsJob extends Model
{
    use BaseEloquent;
    protected $guarded = [];
}
