<?php

namespace App\Http\Controllers\Enterprise;

use App\Customer;
use App\InviteMember;
use App\Jobs\SendEmailJobInviteMember;
use App\MemberCompany;
use App\Occupation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Log;

class MemberController extends Controller
{
    private $inviteMember;
    private $customer;
    private $user;
    private $memberCompany;
    private $occupation;
    public function __construct(InviteMember $inviteMember, Customer $customer,
                                User $user, MemberCompany $memberCompany, Occupation $occupation)
    {
        $this->inviteMember = $inviteMember;
        $this->customer = $customer;
        $this->user = $user;
        $this->memberCompany = $memberCompany;
        $this->occupation = $occupation;
    }

    public function index()
    {
        $members = $this->memberCompany->getManyWhereOrderPagination([
            'customer_id' => session('customer_id')
        ], array("*"), MemberCompany::paginationList);

        return view('enterprise.members.list', compact('members'));
    }

    public function employeeInvitations(Request $request)
    {
        $dataEmail = preg_split('/\r\n|[\r\n]/', trim($request->employee_invitations));
        $customerId = $request->customer_id;
        foreach ($dataEmail as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $token = str_random(40);
               // Insert to invite_member_by_email table
                $this->inviteMember->createData([
                    'email' => trim($email),
                    'customer_id' => $request->customer_id, // where join company
                    'user_id' => auth()->id(), // who is invite
                    'token' => $token
                ]);
                // Send mail to invite member
                $customerData = $this->customer->findById($customerId);
                $userAdded = $this->user->where('email', trim($email))->first();

                $information = [
                    'email' => $email,
                    'customer_id' => $customerId,
                    'token' => $token,
                    'customers' => $customerData,
                    'userAdded' => !empty($userAdded) ? $userAdded : '',
                    'userLogin' => auth()->user()
                ];
                dispatch(new SendEmailJobInviteMember($information));
            }
        }

        return redirect()->route('enterprise.member.users');
    }

    public function showJoinCompany(Request $request, $customerId, $token)
    {
        $member = $this->inviteMember->findWhereFirst('token', $token);
        $getUserInvited = $this->user->where('email', $member->email)->first();
        if (!empty($getUserInvited) && !$getUserInvited->email_verified_at) {
            $this->user->find($getUserInvited->id)->delete();
        }
        $checkUserHaveAcount = $this->user->where('email', $member->email)->first();
        $occupations = $this->occupation->where('parent_id', 0)->get();
        return view('enterprise.members.members-invite', compact('checkUserHaveAcount', 'member', 'token', 'occupations'));

    }

    public function acceptJoinCompany(Request $request, $customerId, $token)
    {
        try {
            $member = $this->inviteMember->findWhereFirst('token', $token);
            $user = $this->user->where('email', $member->email)
                ->whereNotNull('email_verified_at')->first();
            DB::beginTransaction();
            if ( !empty($user) ) {
                $this->memberCompany->createData([
                    'user_id' => $user->id,
                    'customer_id' => $customerId
                ]);
                $this->inviteMember->where('token', $token)->delete();
                session()->forget('join_company_not_have_acount');
                auth()->login($user);
                DB::commit();
                return redirect()->route('enterprise.index', ['customerId' => $customerId]);
            } else {
                return redirect()->back()->with('message', 'Bạn chưa active user');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }

    }

    public function showMemberEdit(Request $request, $id)
    {
        $memberItem = $this->memberCompany->findById($id);
        $member = view('enterprise.modal.edit-member', compact('memberItem'))->render();
        return response()->json([
            'member' => $member,
            'code' => 200
        ]);

    }
    public function postMemberEdit(Request $request, $id)
    {
        $this->memberCompany->updateData($id, [
            'position' => $request->position,
            'introduction' => $request->introduction
        ]);
        $member = $this->memberCompany->findById($id);
        return response()->json([
            'code' => 200,
            'member' => $member
        ]);
    }

    public function deleteMember(Request $request)
    {
        $idMember = $request->id;
        $member = $this->memberCompany->findById($idMember);
        $this->memberCompany->deleteData($idMember);
        return response()->json([
            'code' => 200
        ]);
    }
}
