<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\BaseEloquent;

class TagsMainSideBar extends Model
{
    use BaseEloquent;
    protected $guarded = [];
    protected $table = 'tags_main_side_bars';
}
