@extends('layouts.app')

@section('css')
    <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('pages/companies/home/home.css') }}">
@stop

@section('content')
    <div class="page-enterprise">
        @include('company.partials.company-head-menu-remove-follow')
        <div class="home-enterprise-page">
            <div class="banner-enterprise">
                @if( jjobCheckImageExit($customer->feature_image) )
                    <img src="{{ url($customer->feature_image) }}" alt="" />
                @else
                    <img src="{{ asset('images/no_img_1920x400.jpg') }}" alt="" />
                @endif
            </div>
            <div class="container">
                <div class="company-slogan">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 text-center">
                            <h1 class="company-slogan_title">
                                {{ $customer->slogan }}
                            </h1>
                            <div class="company-slogan_desc">
                                {{ $customer->mission }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="company-top-post">
                    <div class="component-title">
                        <h3 class="title">
                            Bài viết nổi bật
                        </h3>
                        <div class="desc"></div>
                    </div>
                    <a href="{{ route('companies.news', ['slug' => $customer->slug]) }}" class="view-more" title="">
                        Xem tất cả
                    </a>
                    @include('company.partials.hot-new-company')
                </div>
                <div class="company-who">
                    <div class="component-title">
                        <h3 class="title">
                            WHO
                        </h3>
                        <div class="desc">Ai có thể trở thành đồng đội trong tương lai của bạn?</div>
                    </div>
{{--                    <a href="#" class="view-more" title="">--}}
{{--                        Xem tất cả (6)--}}
{{--                    </a>--}}
                    <div class="row">
                        <div class="col-lg-8">
                            @include('company.partials.who-is-customer')
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <!--WHAT section--->
                        @include('company.partials.what-is-customer')
                        <!--END: WHAT section---->
                            <!--WHY section--->
                        @include('company.partials.why-is-customer')
                        <!--END: WHY section---->
                            <!--How section--->
                        @include('company.partials.how-is-customer')
                        <!--END: How section---->
                    </div>
                </div>
            </div>
            <!--Job now section--->
        @include('company.partials.job-company')
        <!--END: Job now section---->
            <!--company info section--->
        @include('company.partials.company-info')
        <!--END: company info section---->
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
    <script src="{{ asset('pages/companies/home/home.js') }}"></script>
@stop
