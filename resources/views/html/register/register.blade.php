<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Đăng ký</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/register/register.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('html/element/header-signin')
	@endcomponent

	<div class="register">
		<div class="holder">
			<div class="sologan">
				<h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
				<div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
			</div><!-- sologan -->

			<div class="form">
				<div class="txt text-center">Bạn chưa có tài khoản?</div>
				<div class="title text-center">HÃY ĐĂNG KÝ NGAY</div>
				
				<ul class="social clearfix">
					<li class="face"><a href="#"><i class="fab fa-facebook-square"></i> Đăng ký với facebook</a></li>
					<li class="linkedin"><a href="#"><i class="fab fa-linkedin"></i> Đăng ký với Linkedin</a></li>
				</ul>

				<div class="or">HOẶC</div>

				<form action="#" method="POST">
					<div class="form-box">
						<div class="messages error">Vui lòng nhập tên trước khi đăng kí</div>
						<div class="form-group error">
							<input type="text" name="user_name" placeholder="Họ và tên" class="form-control">
						</div>

						<div class="form-group error">
							<input type="email" name="user_name" placeholder="Email của bạn" class="form-control">
						</div>

						<div class="form-group error m-0">
							<div class="input-password">
								<a href="#" class="js-displayPassword"><img src="/images/icon-eye.png" alt=""></a>
								<input type="password" name="password" placeholder="Mật khẩu" class="form-control">
							</div>
						</div>

						<p class="notify">Bằng việc click vào nút "Đăng ký", bạn đã đồng ý với các <br> <a href="#">Điều khoản</a> & <a href="#">Điều kiện</a> của chúng tôi</p>
					</div>

					<button type="submit" class="btn btn-block btn-custom">ĐĂNG NHẬP</button>

				</form>
			</div><!-- form -->
		</div>
	</div><!-- .login -->

	@component('html/element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/common/main.js"></script>
	<script src="/pages/register/register.js"></script>
</body>
</html>