<div class="txt-format">
    <h1>Hướng dẫn tạo trang Doanh nghiệp</h1>
    <blockquote>Thương hiệu của bạn sẽ được nâng cao giá trị và thu hút nhiều
        sự quan tâm của người xem chỉ khi bạn thật sự chú trọng và đầu tư kỹ lưỡng vào
        những gì mà bạn truyền tải lên trang doanh nghiệp của mình.
    </blockquote>
    <p>Để tạo trang doanh nghiệp chuyên nghiệp và thật ấn tượng, bạn cần cập nhập thông
        tin lên trang Doanh Nghiệp của mình một cách thường xuyên và liên tục. Hãy đăng
        tải những nội dung thiết thực và luôn có các yếu tố mới mẻ thú vị của riêng
        mình, cố gắng đem đến thông tin hữu ích cho người đọc/ứng viên nhằm mục đích gấy
        ấn tượng tốt với người xem. Tận dụng tối đa những công cụ mà INPLACE cung cấp
        như các tính năng truyền tải bằng bài viết, video, hình ảnh, văn phòng ảo hay
        các bài Quiz, bởi vì chúng sẽ giúp người xem dễ dàng nắm bắt được thông tin,
        thấu hiểu và có ấn tượng sâu sắc hơn về nội dung bạn đã truyền tải.</p>
    <p>Nhưng lưu ý rằng, nếu sự chú trọng chỉ dừng lại về mặt nội dung thì vẫn chưa đủ, bạn
        hãy chú
        trọng cả về mặt hình thức trong cách thể hiện nội dung, chất lượng video, chất
        lượng hình ảnh đăng tải để phù hợp nhất với dung lượng trang yêu cầu nhé.</p>
    <h3>Để tạo trang doanh nghiệp mới, bạn thực hiện theo các bước sau đây:</h3>
    <div class="row">
        <div class="col-sm-5 mt-3">
            <img src="{{asset('images/img_tao_trang_doanh_nghiep_moi.jpg')}}">
        </div>
        <div class="col-sm-7 mt-3">
            <ul>
                <li><strong>Bước 1:</strong> Tạo một tài khoản cá nhân và đăng nhập vào tài
                    khoản của bạn.
                </li>
                <li><strong>Bước 2:</strong> Nhấp vào biểu tượng logo tài khoản ở góc bên
                    phải phía trên màn hình
                    &nbsp;Sau khi các danh mục xuất hiện, nhấp vào mục tạo trang doanh
                    nghiệp
                    &nbsp;
                </li>
                <li><strong>Bước 3:</strong> Sau khi giao diện trang doanh nghiệp hiện ra,
                    tiến hành điền và
                    chỉnh sửa các thông tin liên quan đến doanh nghiệp một cách chính xác và
                    đầy
                    đủ nhất.
                </li>
                <li><strong>Bước 4:</strong> Hoàn tất việc tạo trang doanh nghiệp bằng cách
                    nhấp vào ô hoàn
                    thành.
                </li>
            </ul>
        </div>
    </div>
</div>
