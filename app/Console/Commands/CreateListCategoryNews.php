<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateListCategoryNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-list-category-news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create list category news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('jjob_customer_news_categories')->truncate();
        $data = [
            ['name' => 'Gợi ý của InPlace'],
            ['name' => 'Công ty'],
            ['name' => 'Văn hóa'],
            ['name' => 'Phỏng vấn nhân viên'],
            ['name' => 'WorkStyle'],
            ['name' => 'Chuyện nghề'],
            ['name' => 'Nhà Tuyển dụng'],
            ['name' => 'Sự kiện'],
            ['name' => 'Tech'],
            ['name' => 'Nhà sáng lập'],
            ['name' => 'Startup'],
            ['name' => 'Gallery'],
            ['name' => 'Video'],
            ['name' => 'Văn phòng ảo'],
            ['name' => 'Quiz'],
            ['name' => 'Thông báo']
        ];

        foreach ($data as $dataItem) {
            $dataItem['slug'] = str_slug($dataItem['name']);
            \DB::table('jjob_customer_news_categories')->insert($dataItem);

        }
    }
}
