function renderHtmlAddress() {
    let appendCity = $('.formEditCompanyInfoDashboard').data('city');
    let options = '';
    let keyCity;
    for (keyCity in appendCity) {
        options += '<option value="' + appendCity[keyCity].id + '">' + appendCity[keyCity].name + '</option>';
    }
    let html_rowAdd_address = `<div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group_address_city">
                                        <select
                                            required
                                            name="city[]"
                                            class="dynamicallRowsAddress_city select2-city">
                                            <option value=""></option>
                                            ${options}                                     

                                        </select>
                                        <label><i class="fas fa-map-marker-alt"></i></label>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group_address_branch">
                                        <input
                                            name="address[]"
                                            type="text" value=""
                                            class="dynamicallRowsAddress_branch form-control"
                                            placeholder="Nhập đia chỉ văn phòng / chi nhánh của bạn">
                                        <label><i class="fas fa-map-signs"></i></label>
                                    </div>
                                </div>
                                <button class="dynamicallRowsAddress_remove form-group_address_remove"><i class="fal fa-times"></i></button>
                            </div>`;
    return html_rowAdd_address;
}




function addRowAddress() {
    $('.dynamicallRowsAddress_result').append(renderHtmlAddress());
    chooseCitySelect2Init();
}

function removeRowAddress(e) {
    e.preventDefault();
    $(this).parent().remove();
}

function chooseCitySelect2Init() {
    $('.select2-city').select2({
        placeholder: 'Chọn thành phố'
    });
}

export function dynamicallyRowsAddress() {
    chooseCitySelect2Init();
    $('.dynamicallRowsAddress_btnAdd').on('click', addRowAddress);
    $(document).on('click', '.dynamicallRowsAddress_remove', removeRowAddress);
}
