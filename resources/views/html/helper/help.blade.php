@extends('layouts.app')
@section('meta')
    <title>Trung tâm trợ giúp</title>
@stop
@section('css')
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/help.css')}}">
@stop
@section('content')
    <div class="help-page">
        <div class="help-page-head">
            <div class="container">
                <div class="help-page-head_content">
                    <h3 class="title">
                        Chúng tôi có thể giúp gì cho bạn?
                    </h3>
                </div>
            </div>
        </div>
        <div class="help-page-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 box_sidebar">
                        <button class="menu-sidebar-multi_btn btn btn-custom_light d-block d-md-none">Menu Trợ giúp</button>
                        <ul class="menu-sidebar-multi nav nav-pills">
                            <li class="open">
                                <a href="javascript:;">Bắt đầu <i class="fal fa-chevron-down"></i></a>
                                <ul class="nav">
                                    <li class="nav-item"><a class="active" id="bat-dau_tao-tai-khoan_tab" href="#bat-dau_tao-tai-khoan" role="tab" data-toggle="pill">Tạo tài khoản</a></li>
                                    <li class="nav-item"><a id="bat-dau_dang-nhap-dang-xuat_tab" href="#bat-dau_dang-nhap-dang-xuat" role="tab" data-toggle="pill">Đăng nhập / Đăng xuất</a></li>
                                    <li class="nav-item"><a id="bat-dau_tao-profile_tab" href="#bat-dau_tao-profile" role="tab" data-toggle="pill">Tạo Profile</a></li>
                                    <li class="nav-item"><a id="bat-dau_viet-bai_tab" href="#bat-dau_viet-bai" role="tab" data-toggle="pill">Viết bài</a></li>
                                    <li class="nav-item"><a id="bat-dau_news_tab" href="#bat-dau_news" role="tab" data-toggle="pill">News</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">Những vấn đề thường gặp <i class="fal fa-chevron-down"></i></a>
                                <ul class="nav">
                                    <li class="nav-item"><a id="NVDTG_quen-mat-khau_tab" href="#NVDTG_quen-mat-khau" role="tab" data-toggle="pill">Quên mật khẩu</a></li>
                                    <li class="nav-item"><a id="NVDTG_doi-mat-khau_tab" href="#NVDTG_doi-mat-khau" role="tab" data-toggle="pill">Đổi mật khẩu</a></li>
                                    <li class="nav-item"><a id="NVDTG_lien-he-inplace_tab" href="#NVDTG_lien-he-inplace" role="tab" data-toggle="pill">Liên hệ Inplace</a></li>
                                    <li class="nav-item"><a id="NVDTG_branding_tab" href="#NVDTG_branding" role="tab" data-toggle="pill">Branding</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">Tìm kiếm <i class="fal fa-chevron-down"></i></a>
                                <ul class="nav">
                                    <li class="nav-item"><a id="tim-kiem_tim-kiem-job_tab" href="#tim-kiem_tim-kiem-job" role="tab" data-toggle="pill">Tìm kiếm Job</a></li>
                                    <li class="nav-item"><a id="tim-kiem_tim-kiem-cong-ty_tab" href="#tim-kiem_tim-kiem-cong-ty" role="tab" data-toggle="pill">Tìm kiếm công ty</a></li>
                                    <li class="nav-item"><a id="tim-kiem_tim-kiem-tin-tuc_tab" href="#tim-kiem_tim-kiem-tin-tuc" role="tab" data-toggle="pill">Tìm kiếm tin tức</a></li>
                                    <li class="nav-item"><a id="tim-kiem_tim-kiem-chuc-nang_tab" href="#tim-kiem_tim-kiem-chuc-nang" role="tab" data-toggle="pill">Tìm kiếm chức năng</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">Quản lý tài khoản <i class="fal fa-chevron-down"></i></a>
                                <ul class="nav">
                                    <li class="nav-item"><a id="QLTK_tao-tai-khoan_tab" href="#QLTK_tao-tai-khoan" role="tab" data-toggle="pill">Tạo tài khoản</a></li>
                                    <li class="nav-item"><a id="QLTK_xoa-tai-khoan_tab" href="#QLTK_xoa-tai-khoan" role="tab" data-toggle="pill">Xóa tài khoản</a></li>
                                    <li class="nav-item"><a id="QLTK_che-do-bao-mat_tab" href="#QLTK_che-do-bao-mat" role="tab" data-toggle="pill">Chế độ bảo mật</a></li>
                                    <li class="nav-item"><a id="QLTK_che-do-rieng-tu_tab" href="#QLTK_che-do-rieng-tu" role="tab" data-toggle="pill">Chế độ riêng tư</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">Quản lý nội dung <i class="fal fa-chevron-down"></i></a>
                                <ul class="nav">
                                    <li class="nav-item"><a id="QLND_tao-noi-dung-moi_tab" href="#QLND_tao-noi-dung-moi" role="tab" data-toggle="pill">Tạo nội dung mới</a></li>
                                    <li class="nav-item"><a id="QLND_chinh-sua-noi-dung_tab" href="#QLND_chinh-sua-noi-dung" role="tab" data-toggle="pill">Chỉnh sửa nội dung</a></li>
                                    <li class="nav-item"><a id="QLND_xoa-noi-dung_tab" href="#QLND_xoa-noi-dung" role="tab" data-toggle="pill">Xóa nội dung</a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a id="chia-se-noi-dung_tab" href="#chia-se-noi-dung" role="tab" data-toggle="pill">Chia sẻ nội dung</a></li>
                            <li class="nav-item"><a id="theo-doi-va-follow-up_tab" href="#theo-doi-va-follow-up" role="tab" data-toggle="pill">Theo dõi và Follow up</a></li>
                            <li class="nav-item"><a id="xay-dung-network_tab" href="#xay-dung-network" role="tab" data-toggle="pill">Xây dựng Network</a></li>
                            <li class="nav-item"><a id="QA-Quiz_tab" href="#QA-Quiz" role="tab" data-toggle="pill">Q&A - Quiz</a></li>
                            <li class="nav-item"><a id="van-phong-ao_tab" href="#van-phong-ao" role="tab" data-toggle="pill">Văn phòng ảo</a></li>
                            <li class="nav-item"><a id="quan-ly-job_tab" href="#quan-ly-job" role="tab" data-toggle="pill">Quản lý Job</a></li>
                            <li class="nav-item"><a id="dang-tin-tuyen-dung_tab" href="#dang-tin-tuyen-dung" role="tab" data-toggle="pill">Đăng tin tuyển dụng</a></li>
                            <li class="nav-item"><a id="quan-ly-tuyen-dung_tab" href="#quan-ly-tuyen-dung" role="tab" data-toggle="pill">Quản lý tuyển dụng</a></li>
                            <li class="nav-item"><a id="employer-branding_tab" href="#employer-branding" role="tab" data-toggle="pill">Employer Branding</a></li>
                            <li class="nav-item"><a id="cac-goi-dich-vu-va-thanh-toan_tab" href="#cac-goi-dich-vu-va-thanh-toan" role="tab" data-toggle="pill">Các gói dịch vụ và thanh toán</a></li>
                            <li class="nav-item"><a id="gui-cau-hoi-cho-inplace_tab" href="#gui-cau-hoi-cho-inplace" role="tab" data-toggle="pill">Gửi câu hỏi cho Inplace</a></li>
                            <li class="nav-item"><a id="lien-he_tab" href="#lien-he" role="tab" data-toggle="pill">Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-9 box_content">
                        <div class="tab-content">
{{--                            Bắt đầu--}}
                            <div role="tabpanel" class="tab-pane show active" id="bat-dau_tao-tai-khoan" aria-labelledby="bat-dau_tao-tai-khoan_tab">
                                @include('html.helper.helpTabs.bat-dau.tao-tai-khoan')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="bat-dau_dang-nhap-dang-xuat" aria-labelledby="bat-dau_dang-nhap-dang-xuat_tab">
                                @include('html.helper.helpTabs.bat-dau.dang-nhap-dang-xuat')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="bat-dau_tao-profile" aria-labelledby="bat-dau_tao-profile_tab">
                                @include('html.helper.helpTabs.bat-dau.tao-profile')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="bat-dau_viet-bai" aria-labelledby="bat-dau_viet-bai_tab">
                                @include('html.helper.helpTabs.bat-dau.viet-bai')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="bat-dau_news" aria-labelledby="bat-dau_news_tab">
                                @include('html.helper.helpTabs.bat-dau.news')
                            </div>

{{--                            Những vấn đề thường gặp--}}
                            <div role="tabpanel" class="tab-pane fade" id="NVDTG_quen-mat-khau" aria-labelledby="NVDTG_quen-mat-khau_tab">
                                @include('html.helper.helpTabs.nhung-van-de-thuong-gap.quen-mat-khau')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="NVDTG_doi-mat-khau" aria-labelledby="NVDTG_doi-mat-khau_tab">
                                @include('html.helper.helpTabs.nhung-van-de-thuong-gap.doi-mat-khau')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="NVDTG_lien-he-inplace" aria-labelledby="NVDTG_lien-he-inplace_tab">
                                @include('html.helper.helpTabs.nhung-van-de-thuong-gap.lien-he-inplace')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="NVDTG_branding" aria-labelledby="NVDTG_branding_tab">
                                @include('html.helper.helpTabs.nhung-van-de-thuong-gap.branding')
                            </div>

{{--                            Tìm kiếm--}}
                            <div role="tabpanel" class="tab-pane fade" id="tim-kiem_tim-kiem-job" aria-labelledby="tim-kiem_tim-kiem-job_tab">
                                @include('html.helper.helpTabs.tim-kiem.tim-kiem-job')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tim-kiem_tim-kiem-cong-ty" aria-labelledby="tim-kiem_tim-kiem-cong-ty_tab">
                                @include('html.helper.helpTabs.tim-kiem.tim-kiem-cong-ty')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tim-kiem_tim-kiem-tin-tuc" aria-labelledby="tim-kiem_tim-kiem-tin-tuc_tab">
                                @include('html.helper.helpTabs.tim-kiem.tim-kiem-tin-tuc')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tim-kiem_tim-kiem-chuc-nang" aria-labelledby="tim-kiem_tim-kiem-chuc-nang_tab">
                                @include('html.helper.helpTabs.tim-kiem.tin-kiem-chuc-nang')
                            </div>

{{--                            Quản lý tài khoản--}}
                            <div role="tabpanel" class="tab-pane fade" id="QLTK_tao-tai-khoan" aria-labelledby="QLTK_tao-tai-khoan_tab">
                                @include('html.helper.helpTabs.quan-ly-tai-khoan.tao-tai-khoan')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QLTK_xoa-tai-khoan" aria-labelledby="QLTK_xoa-tai-khoan_tab">
                                @include('html.helper.helpTabs.quan-ly-tai-khoan.xoa-tai-khoan')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QLTK_che-do-bao-mat" aria-labelledby="QLTK_che-do-bao-mat_tab">
                                @include('html.helper.helpTabs.quan-ly-tai-khoan.che-do-bao-mat')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QLTK_che-do-rieng-tu" aria-labelledby="QLTK_che-do-rieng-tu_tab">
                                @include('html.helper.helpTabs.quan-ly-tai-khoan.che-do-rieng-tu')
                            </div>

{{--                            Quản lý nội dung--}}
                            <div role="tabpanel" class="tab-pane fade" id="QLND_tao-noi-dung-moi" aria-labelledby="QLND_tao-noi-dung-moi_tab">
                                @include('html.helper.helpTabs.quan-ly-noi-dung.tao-noi-dung-moi')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QLND_chinh-sua-noi-dung" aria-labelledby="QLND_chinh-sua-noi-dung_tab">
                                @include('html.helper.helpTabs.quan-ly-noi-dung.chinh-sua-noi-dung')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QLND_xoa-noi-dung" aria-labelledby="QLND_xoa-noi-dung_tab">
                                @include('html.helper.helpTabs.quan-ly-noi-dung.xoa-noi-dung')
                            </div>

{{--                            ====--}}
                            <div role="tabpanel" class="tab-pane fade" id="chia-se-noi-dung" aria-labelledby="chia-se-noi-dung_tab">
                                @include('html.helper.helpTabs.chia-se-noi-dung')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="theo-doi-va-follow-up" aria-labelledby="theo-doi-va-follow-up_tab">
                                @include('html.helper.helpTabs.theo-doi-va-follow-up')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="xay-dung-network" aria-labelledby="xay-dung-network_tab">
                                @include('html.helper.helpTabs.xay-dung-network')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="QA-Quiz" aria-labelledby="QA-Quiz_tab">
                                @include('html.helper.helpTabs.QA-Quiz')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="van-phong-ao" aria-labelledby="van-phong-ao_tab">
                                @include('html.helper.helpTabs.van-phong-ao')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="quan-ly-job" aria-labelledby="quan-ly-job_tab">
                                @include('html.helper.helpTabs.quan-ly-job')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="dang-tin-tuyen-dung" aria-labelledby="dang-tin-tuyen-dung_tab">
                                @include('html.helper.helpTabs.dang-tin-tuyen-dung')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="quan-ly-tuyen-dung" aria-labelledby="quan-ly-tuyen-dung_tab">
                                @include('html.helper.helpTabs.quan-ly-tuyen-dung')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="employer-branding" aria-labelledby="employer-branding_tab">
                                @include('html.helper.helpTabs.emplayer-branding')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="cac-goi-dich-vu-va-thanh-toan" aria-labelledby="cac-goi-dich-vu-va-thanh-toan_tab">
                                @include('html.helper.helpTabs.cac-goi-dich-vu-va-thanh-toan')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="gui-cau-hoi-cho-inplace" aria-labelledby="gui-cau-hoi-cho-inplace_tab">
                                @include('html.helper.helpTabs.gui-cau-hoi-cho-inplace')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="lien-he" aria-labelledby="lien-he_tab">
                                @include('html.helper.helpTabs.lien-he')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/help.js')}}"></script>
@stop
