<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class UserFollowCustomer extends Model
{
//    use SoftDeletes;
    use BaseEloquent;
    protected $guarded = [];
    protected $table = 'jjob_customer_follows';
}
