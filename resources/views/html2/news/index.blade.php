@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('/pages/news/list/list.css') }}">
@stop
@section('content')
    @include('html2.partials.header_sub_nav')
    <div class="news-page">
        <div class="container">
            <div class="news-page_top">
                <div class="row">
                    <?php for($i = 0; $i < 4; $i++){?>
                    <div class="col-sm-6 col-lg-3">
                        @include('html2.news.partials.headhunter-item', ['name' => 'Cảm xúc trong nghề của những cô gái làm Tuyển dụng'])
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="news-page_content">
                <div class="row">
                    <div class="col-md-8 box_content">
                        <div class="list-news">
                            @for ($i = 0; $i < 10; $i++)
                                @include('html2.news.partials.item')
                            @endfor
                        </div>
                        <div class="block-pagination">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link arrow" href="#">
                                        <i class="ipl-arrow-left"></i>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">...</a></li>
                                <li class="page-item"><a class="page-link" href="#">9</a></li>

                                <li class="page-item">
                                    <a class="page-link arrow" href="#">
                                        <i class="ipl-arrow-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 box_sidebar">
                        <div class="news-page_sidebar">
                            <div class="row">
                                <div class="col-sm-6 col-md-12 mt-5 mt-md-0">
                                    <h3 class="title-sidebar">Đang hot</h3>
                                    <div class="list-hot-news">
                                        @for ($i = 1; $i <= 5; $i++)
                                            @include('html2.news.partials.item_hot_news', ['number' => $i])
                                        @endfor
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-12 mt-5">
                                    <h3 class="title-sidebar">Xem nhiều</h3>
                                    <div class="list-hot-news">
                                        @for ($i = 1; $i <= 5; $i++)
                                            @include('html2.news.partials.item_hot_news', ['number' => $i])
                                        @endfor
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('/pages/news/list/list.js')}}"></script>
@stop
