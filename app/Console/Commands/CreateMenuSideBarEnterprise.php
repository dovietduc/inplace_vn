<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateMenuSideBarEnterprise extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:menu-sidebar-enterprise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create menu sidebar enterprise';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            [
                'name' => 'Get started'
            ],
            [
                'name' => 'Việc làm'
            ],
            [
                'name' => 'Ứng viên'
            ],
            [
                'name' => 'Bài viết'
            ],
            [
                'name' => 'Tin nhắn'
            ],
            [
                'name' => 'Thống kê'
            ]

        ];

        foreach ($data as $dataItem) {
            \DB::table('menu_enterprise_sidebar')->insert([
                'name' => $dataItem['name'],
                'slug' => str_slug($dataItem['name'])
            ]);
        }


    }
}
