<?php

namespace App\Providers;

use App\Customer;
use App\MemberCompany;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('*', function ($view) {
            if (auth()->check()) {
                $customerIds = MemberCompany::where('user_id', auth()->id())->pluck('customer_id')->toArray();
                $view->with('dataCustomerFollowUserLogin', Customer::whereIn('id', $customerIds)->get());
                if(session()->has('customer_id')) {
                    $view->with('customerCurrent', Customer::find(session('customer_id')));
                }

            }

        });
    }
}
