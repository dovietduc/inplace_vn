@extends('layouts.dashboard')
@section('meta')
    <title>Danh sách ứng viên</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('pages/enterprise/candidates/list.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Danh sách <strong>Ứng viên</strong>
                </h1>
                <div class="button-group">
                    <div class="btn"><strong>10 hồ sơ</strong> đã ứng tuyển</div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-candidates-list">
        <div class="container">
            <div class="dashboard-candidates-list_th">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        Ứng viên
                    </div>
                    <div class="col-sm-6 col-md-4">
                        Thông tin liên hệ
                    </div>
                    <div class="col-sm-7 col-md-3">
                        Ngày ứng tuyển
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-candidate-item" data-member="1">
                <div class="row align-items-center">
                    <div class="col-sm-6 col-md-5">
                        <div class="info">
                            <a href="" class="avatar">
                                <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                            </a>
                            <div class="title">
                                <h3 class="name"><a href="#">Đào Mai Lê</a></h3>
{{--                                <div class="desc"><i class="fas fa-map-marker-alt"></i> TP.Hồ Chí Minh</div>--}}
                                <div class="job-item-tag">
                                    <div class="tag-list">
                                        <a href="#" class="tag-item" target="_blank">
                                            React JS Developer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="about">
                            <i class="fal fa-envelope"></i> <em>votranhoaithanh@gmail.com</em> <br>
                            <i class="fal fa-phone-alt"></i> <strong>0912 345 6789</strong>
                        </div>
                    </div>
                    <div class="col-7 col-md-3 offset-sm-6 offset-md-0">
                        <div class="authority"><i class="fal fa-calendar-alt d-md-none mr-2"></i>24/10/2019</div>
                    </div>
                </div>
            </div>
            <?php } ?>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script src="{{ asset('pages/enterprise/candidates/list.js') }}"></script>
@stop
