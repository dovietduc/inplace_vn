import swal from 'sweetalert';
$(function () {
    $('.job-item_btn-delete').click(function () {
        swal({
            icon: "warning",
            title: "Bạn chắc chắn xóa không?",
            text: "Job bạn lưu sẽ bị xóa khỏi danh sách lưu của bạn!",
            buttons: {
                cancel: {
                    text: "Hủy",
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Xóa",
                    value: true,
                    visible: true,
                    className: "btn-custom",
                    closeModal: true
                }
            }
        }).then(function (result) {
            if (result) {
                //...
            }
        });
    })
});
