<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class UserApplyJob extends Model
{
    use BaseEloquent;
    protected $guarded = [];
    protected $table = 'jjob_user_apply_jobs';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }
}
