@extends('layouts.dashboard')
@section('meta')
    <title>Jobs Add Dashboard</title>
@stop
@section('css')
    {{--CSS Froala editor--}}
    @include('enterprise.partials.css-froala')
    {{--END: Froala Editor--}}
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/jobs/add.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
            </div>
        </div>
    </div>
    <div class="dashboard-job-add">
        <div class="container">
            <form action="{{ route('enterprise.jobs.post.edit', ['id' => $job->id]) }}" method="POST"
                  class="formJobAdd">
                @csrf
                <input type="hidden" value="" name="image_crop_data" class="image_crop_data">
                <input type="hidden" value="" name="type_submit" class="type_submit">
                <input type="hidden" value="" name="show_form_salary" class="show_form_salary">
                <div class="dashboard-job-add_box">
                    <div class="dashboard-job-add_thumb">
                        @include('enterprise.partials.croppie-image-job')
                    </div>
                    <div class="box-width">
                        <div class="dashboard-job-add_title">
                            <div class="component-title">
                                <h3 class="title">Tiêu đề tuyển dụng</h3>
                                <div class="desc">Hãy tạo Tiêu đề tuyển dụng thú vị để thu hút ứng viên tiềm năng của
                                    bạn
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea
                                    class="titleJob form-control autoheight disenter textarea-counter check-value-submit-disable"
                                    name="title"
                                    placeholder="Tiêu đề thể hiện thông điệp mời ứng tuyển đến ứng viên"
                                    maxlength="128">{{ $job->title }}</textarea>
                                <span class="note"><span
                                        class="textarea-counter-result">128</span>/128 ký tự</span>
                            </div>
                            <div class="txt-format">
                                <h4>Tham khảo những tiêu đề phía dưới để viết những tiêu đề tuyển dụng giúp thu hút ứng
                                    viên phù hợp với
                                    sứ mệnh và văn hóa Công ty của bạn:</h4>
                                <ul>
                                    <li>Chiêu mộ nhân tài Sales IT giúp .. khai phá thị trường Nhật bản</li>
                                    <li>Tìm kiếm Sales Manager giúp .. xây dựng thương hiệu IT "make in Vietnam"</li>
                                    <li>Tìm kiếm Marketing Executive làm việc cho Startup đang tăng trưởng nhanh ...
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="dashboard-job-add_location-info">
                            <div class="component-title">
                                <h3 class="title">Thông tin vị trí</h3>
                                <div class="desc">Ai là ứng viên tiềm năng của bạn?</div>
                            </div>
                            <div class="row flex-column">
                                <div class="col-sm-7 col-md-6">
                                    <div class="form-group">
                                        <label>Bộ phận làm việc <span>*</span></label>
                                        <div class="select-occupation">
                                            <input type="hidden" value="{{ $job->occupation_main }}"
                                                   class="select-occupation__value check-value-submit-disable"
                                                   name="occupation_main" placeholder="Chọn bộ phận làm việc">
                                            <div class="select-occupation__result form-control {{ !empty($job->occupation_main) ? 'hasValue' : ''}}">
                                                {{ !empty(optional($job->occupationMain)->name) ? optional($job->occupationMain)->name : 'Chọn bộ phận làm việc' }}
                                            </div>
                                            <ul class="select-occupation__list-options">
                                                @foreach($occupations as $occupationItem)
                                                    <li>
                                                        <span data-id="{{ $occupationItem->id }}"
                                                              data-url="{{ route('enterprise.jobs.ajax.search-occupation-select') }}"
                                                              class="select-occupation__option {{ $occupationItem->id == $job->occupation_main ? 'selected' : ''}}">
                                                            {{ $occupationItem->name }}
                                                        </span>
                                                        @if(!empty($occupationItem->occupationChildrent) && !$occupationItem->occupationChildrent->isEmpty())
                                                            <div class="select-occupation__option__children txt-format">
                                                                <h3>Bao gồm các nghề nghiệp sau:</h3>
                                                                <ul>
                                                                    @foreach($occupationItem->occupationChildrent as $occupationChildrentItem)
                                                                        <li>
                                                                        <span>
                                                                            {{ $occupationChildrentItem->name }}
                                                                        </span>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @php
                                        $occupationMainInstance = $job->occupationMain;
                                    @endphp
                                    <div class="form-group select-occupation-sup"
                                         style= "{{ (!empty($occupationMainInstance) && !$occupationMainInstance->occupationChildrent->isEmpty()) ? 'display: block' : '' }}">
                                        <label>Bộ phận làm việc chi tiết</label>
                                        <select name="occupation_supporting" class="select2__occupation-sup">
                                            <option></option>
                                            @if(!empty($occupationMainInstance) && !$occupationMainInstance->occupationChildrent->isEmpty())
                                                @foreach($occupationMainInstance->occupationChildrent as $occupationSupportingItem)
                                                    <option
                                                        {{ $occupationSupportingItem->id == $job->occupation_supporting ? 'selected' : '' }}
                                                        value="{{ $occupationSupportingItem->id }}">{{ $occupationSupportingItem->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Chi tiết vị trí</label>
                                        <textarea
                                            class="textarea-occupation-detail form-control autoheight disenter textarea-counter"
                                            name="occupation_supporting_detail"
                                            placeholder="Nhập tên vị trí"
                                            maxlength="25">{{ optional($job->occupationSupportingDetail)->name }}</textarea>
                                        <span class="note"><span
                                                class="textarea-counter-result">25</span>/25 ký tự</span>
                                    </div>
                                </div>
                                <div class="col-sm-7 col-md-6">
                                    <div class="form-group">
                                        <label>Cấp bậc <span>*</span></label>
                                        <select class="select2-noSearch check-value-submit-disable" name="job_level_id"
                                                data-placeholder="Chọn cấp bậc">
                                            <option value=""></option>
                                            @foreach($levelsJob as $levelJob)
                                                <option
                                                    {{ $job->job_level_id == $levelJob->id ? 'selected' : '' }}
                                                    value="{{ $levelJob->id }}">
                                                    {{ $levelJob->name }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                {{--                                <div class="col-sm-7 col-md-6">--}}
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label>Loại hình công việc <span>*</span></label>--}}
                                {{--                                        <select class="select2-noSearch check-value-submit-disable" name="work_types_id"--}}
                                {{--                                                data-placeholder="Chọn loại hình công việc">--}}
                                {{--                                            <option value=""></option>--}}

                                {{--                                            @foreach($typesWork as $typeWork)--}}
                                {{--                                                <option--}}
                                {{--                                                    {{ $job->work_types_id == $typeWork->id ? 'selected' : '' }}--}}
                                {{--                                                    value="{{ $typeWork->id }}">--}}
                                {{--                                                    {{ $typeWork->name }}--}}
                                {{--                                                </option>--}}
                                {{--                                            @endforeach--}}

                                {{--                                        </select>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                <div class="col-sm-7 col-md-6">
                                    <div class="form-group">
                                        <label>Khu vực làm việc <span>*</span></label>
                                        <select class="select2-mutiple-3item check-value-submit-disable"
                                                name="citys[]" multiple data-placeholder="Chọn khu vực làm việc">
                                            <option value=""></option>
                                            @foreach($citys as $city)
                                                <option
                                                    {{ $citySelectedJob->contains($city->id) ? 'selected' : '' }}
                                                    value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-7 col-md-6">
                                    <div class="form-group">
                                        <label>Trình độ tiếng Anh</label>
                                        <select
                                            name="english_level_id"
                                            class="select2-noSearch"
                                            data-placeholder="Chọn cấp bậc">
                                            <option value=""></option>
                                            @foreach($englishLevels as $englishLevel)
                                                <option
                                                    {{ $job->english_level_id == $englishLevel->id ? 'selected' : '' }}
                                                    value="{{ $englishLevel->id }}">
                                                    {{ $englishLevel->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard-job-add_content">
                            <div class="component-title">
                                <h3 class="title">Mô tả nội dung tuyển dụng </h3>
                                <div class="desc"></div>
                            </div>
                            <div class="txt-format">
                                <textarea class="inp-froala-editor" name="description"
                                          placeholder="Vị trí này đóng vai trò gì trong Công ty?&#10;Những kỹ năng cần thiết ở vị trí này là gì?&#10;Những tính cách nào sẽ  phù hợp với môi trường Công ty của bạn?"
                                >{!! $job->description !!}</textarea>
                            </div>
                        </div>
                        {{--                        <div class="dashboard-job-add_tag">--}}
                        {{--                            <select--}}
                        {{--                                data-url="{{ route('enterprise.jobs.ajax.search-tags') }}"--}}
                        {{--                                class="select2-mutiple-tags-3item" name="tags[]" multiple--}}
                        {{--                                data-placeholder="Từ khóa công việc (Tối đa 3)">--}}
                        {{--                                @if(!empty($job->tagsJob))--}}
                        {{--                                    @foreach( $job->tagsJob as $tagItem)--}}
                        {{--                                        <option selected value="{{ $tagItem->name }}">{{ $tagItem->name }}</option>--}}
                        {{--                                    @endforeach--}}
                        {{--                                @endif--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}

                        <div class="dashboard-job-add_salary">
                            <div class="component-title">
                                <h3 class="title">Tùy chọn hiển thị lương</h3>
                                <div class="desc"></div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-sm-6 col-lg-3">
                                    <select class="job-salary-select select2-noSearch" name="salary_id">

                                        @foreach($salarysOffer as $salaryOffer)
                                            <option
                                                {{ $job->salary_id == $salaryOffer->id ? 'selected' : '' }}
                                                data-show_form_salary="{{ $salaryOffer->show_form_salary }}"
                                                value="{{ $salaryOffer->id }}">
                                                {{ $salaryOffer->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div
                                    class="col-6 col-sm-6 col-lg-3 job-salary-pending {{ !optional($job->salary)->show_form_salary ? 'd-none' : ''}} ">
                                    <input
                                        name="salary_min"
                                        value="{{ $job->salary_min }}"
                                        type="number"
                                        class="job-salary-from form-control"
                                        placeholder="Từ">
                                </div>
                                <div
                                    class="col-6 col-sm-6 col-lg-3 job-salary-pending {{ !optional($job->salary)->show_form_salary ? 'd-none' : ''}}">
                                    <input
                                        name="salary_max"
                                        value="{{ $job->salary_max }}"
                                        type="number"
                                        class="job-salary-to form-control"
                                        placeholder="Đến">
                                </div>
                                <div
                                    class="col-sm-6 col-lg-3 job-salary-pending {{ !optional($job->salary)->show_form_salary ? 'd-none' : ''}}">
                                    <p class="note">(<span>*</span>) Đơn vị: <strong>$ - USD</strong></p>
                                </div>


                            </div>
                        </div>
                        <div class="dashboard-job-add_members">
                            <div class="component-title">
                                <h3 class="title">Thành viên công ty</h3>
                                <div class="desc"></div>
                            </div>
                            <p class="note">Hiển thị các thành viên liên quan đến Công ty hoặc Vị trí này. <br>
                                Việc giới thiệu về "Con người" giúp công ty bạn tìm được đúng Cộng Sự!</p>
                            <div class="job-members-editor">
                                <div class="d-flex position-relative">
                                    <div class="job-members-editor_result" id="job-members-editor_result">

                                        @foreach($job->membersCompany as $memberItem)
                                            <div class="job-members-editor_result_item">
                                                <input type="hidden" name="members_job[]" value="{{ $memberItem->id }}">
                                                @include('enterprise.partials.show-image-avatar-user', ['member' => $memberItem])
                                                <i class="btn-delete fal fa-times"></i>
                                            </div>
                                        @endforeach

                                    </div>
                                    <div class="position-relative">
                                        <div class="job-members-editor_add"></div>
                                        <div class="job-members-editor_filter">
                                            <div class="member-filter">
                                                <input id="job-members-editor_filter_input" type="text"
                                                       class="form-control" placeholder="Tìm kiếm thành viên">
                                                <label for="job-members-editor_filter_input"><i
                                                        class="fal fa-search"></i></label>
                                            </div>


                                            <ul class="member-list">

                                                @foreach($listUsersFollowCustomer as $memberCompany)
                                                    <li class="job-members-editor_filter_member"
                                                        data-idMember="{{ $memberCompany->id }}">

                                                        @include('enterprise.partials.show-image-avatar-user', ['member' => $memberCompany])
                                                        <div class="info">
                                                            <h5 class="name findname">{{ optional($memberCompany->user)->name }}</h5>
                                                            <p class="station findname">{!! nl2br($memberCompany->position) !!}</p>
                                                            <div class="desc d-none">
                                                                {!! nl2br($memberCompany->introduction) !!}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="job-members-editor_content">

                                    @foreach($job->membersCompany as $memberItem)
                                        <div class="content" data-contentmember="{{ $memberItem->id }}">
                                            <h5 class="name">{{ optional($memberItem->user)->name }}</h5>
                                            <p class="station">{{ $memberItem->position }}</p>
                                            <div class="desc">
                                                {{ $memberItem->introduction }}
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <div class="dashboard-job-add_email-notification">
                            <div class="component-title">
                                <h3 class="title">Email nhận thông báo <span style="color: #f00">*</span></h3>
                                <div class="desc">Chúng tôi sẽ gửi email thông báo khi ứng viên apply
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email"
                                               class="form-control email-notification-input"
                                               name="email"
                                               value="{{ optional($job->emailApplyJobs()->first())->email }}"
                                               placeholder="Nhập email nhận thông báo">
                                        <div class="err-inp"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dashboard-job-add_submit">
                        <button type="button" class="button-draft-job btn btn-custom_light">Lưu nháp</button>
                        <button type="button" class="button-public-job btn btn-custom" disabled>Đăng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    {{--JS Froala editor--}}
    @include('enterprise.partials.js-froala')
    {{--END: Froala editor--}}
    <script src="{{asset('vendor/croppie/croppie.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/jobs/add.js')}}"></script>
    <script>
        $('.select2__occupation-sup').select2({
            minimumResultsForSearch: Infinity,
        });
    </script>

@stop

