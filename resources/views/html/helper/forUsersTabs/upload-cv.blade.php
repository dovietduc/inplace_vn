<div class="txt-format">
    <h1>Hướng dẫn Upload CV</h1>
    <p>Để upload CV, đầu tiên bạn cần tạo tài khoản cá nhân theo:
        Hướng dẫn tạo trang cá nhân mới</p>
    <p>Sau khi tạo tài khoản cá nhân thành công, bạn thực hiện theo các bước dưới đây để
        upload CV của mình:</p>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_upload_cv.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Click vào biểu tượng tài khoản cá nhân của bạn ở
                    góc trên bên phải màn hình
                </li>
                <li><strong>Bước 2:</strong> Chọn mục chỉnh sửa trang cá nhân</li>
                <li><strong>Bước 3:</strong> Trang cá nhân hiện ra, bạn nhìn bên phải và chọn
                    mục <strong>Tải lên&nbsp;</strong><strong>CV của bạn</strong>. (Lưu ý về
                    định dạng file và kích cỡ tối đa cho phép)
                </li>
                <li><strong>Bước 4:&nbsp;</strong>Vui lòng chọn tệp hoặc kéo thả tệp vào nơi yêu
                    cầu và hoàn tất bằng cách nhấp vào mục tải lên.
                </li>
            </ul>
            <p><em><i class="fad fa-info-circle"></i> Nếu bạn muốn thay đổi bằng một CV khác thì hãy xoá CV cũ và upload CV mới
                    bằng cách lặp lại các bước trên.</em></p>
        </div>
    </div>

</div>
