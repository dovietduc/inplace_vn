<?php

namespace App\Http\Controllers\Enterprise;

use App\City;
use App\CompanySize;
use App\Customer;
use App\CustomerAddress;
use App\CustomerIndustry;
use App\CustomerMiddleIndustry;
use App\Traits\StoreImageTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CompanyController extends Controller
{
    private $customer;
    private $companySize;
    private $customerIndustry;
    private $city;
    private $customerAddress;
    private $customerMiddleIndustry;
    use StoreImageTrait;

    public function __construct(Customer $customer, CompanySize $companySize,
                                CustomerIndustry $customerIndustry, City $city,
                                CustomerAddress $customerAddress, CustomerMiddleIndustry $customerMiddleIndustry)
    {
        $this->customer = $customer;
        $this->companySize = $companySize;
        $this->customerIndustry = $customerIndustry;
        $this->city = $city;
        $this->customerAddress = $customerAddress;
        $this->customerMiddleIndustry = $customerMiddleIndustry;
    }

    public function edit($customerId)
    {

        list($customer, $companySizes, $customerIndustrys, $industrySelected, $citys) = $this->informationCompany($customerId);
        return view('enterprise.company.edit', compact('customer', 'companySizes',
            'customerIndustrys', 'industrySelected', 'citys'));
    }

    public function informationCompany($id)
    {
        $customer = $this->customer->findById($id)->load(['customerAddress', 'customerAddress.city']);
        $companySizes = $this->companySize->getAll();
        $customerIndustrys = $this->customerIndustry->getAll();
        $industrySelected = $customer->industrys()->pluck('jjob_industries.id');
        $citys = $this->city->all();
        return [
            $customer,
            $companySizes,
            $customerIndustrys,
            $industrySelected,
            $citys
        ];
    }

    public function updateCustomerBasic(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $dataCustomer = [
                'slogan' => $request->slogan,
                'website' => $request->website,
                'phone' => $request->phone,
                'director' => $request->director,
                'company_size_id' => $request->company_size_id,
                'founded_year' => $request->founded_year,
                'mission' => $request->mission,
                'place_location' => $request->place_location
            ];
            // Update customer information
            $this->customer->updateData($id, $dataCustomer);
            // Update address customer
            $cityFromInput = $request->city;
            $addressFromInput = $request->address;
            if (!empty($cityFromInput[0])) {
                $this->customerAddress->deleteDataWhere(['customer_id' => $id]);
                for ($i = 0; $i < count($request->city); $i++) {
                    $addressInsert = [
                        'city_id' => $cityFromInput[$i],
                        'address' => $addressFromInput[$i],
                        'customer_id' => $id
                    ];
                    $this->customerAddress->createData($addressInsert);
                }
            }
            // Update industry customer
            $customer = $this->customer->findById($id);
            if (!empty($request->industry)) {
                $this->customerMiddleIndustry->deleteDataWhere(['customer_id' => $id]);
                $customer->industrys()->attach($request->industry);
            }
            DB::commit();
            return back();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::debug($e->getMessage() . $e->getLine());
        }

    }

    public function updateCustomerSaveImage(Request $request, $id)
    {
        $dataUpdate = [
        ];
        $logoPath = $this->uploadImageBase($request, 'logo', 'logo');

        if ($request->hasFile('logo')) {
            $dataUpdate['logo'] = 'storage' . $logoPath;
        }
        if ($request->hasFile('feature_image') && empty($request->flag_delete_image) ) {
            $featureImagePath = $this->uploadImageBase($request, 'feature_image', 'feature-image');
            $dataUpdate['feature_image'] = 'storage' . $featureImagePath;
        }

        $this->customer->updateData($id, $dataUpdate);
        return back();
    }

    public function updateWhatIsCustomer(Request $request, $id)
    {
        $dataUpdate = [
            'what_quote_image_1' => $request->what_quote_image_1,
            'what_quote_image_2' => $request->what_quote_image_2,
            'what_we_do' => $request->what_we_do
        ];
        $whatFeatureImageFirstPath =
            $this->uploadImageBase($request, 'what_feature_image_1', 'what-feature-customer');
        $whatFeatureImageSecondPath =
            $this->uploadImageBase($request, 'what_feature_image_2', 'what-feature-customer');

        if ($request->hasFile('what_feature_image_1')) {
            $dataUpdate['what_feature_image_1'] = 'storage' . $whatFeatureImageFirstPath;
        }
        if ($request->hasFile('what_feature_image_2')) {
            $dataUpdate['what_feature_image_2'] = 'storage' . $whatFeatureImageSecondPath;
        }
        $this->customer->updateData($id, $dataUpdate);
        return back();
    }

    public function updateWhyWeDo(Request $request, $id)
    {
        $dataUpdate = [
            'why_quote_image_1' => $request->why_quote_image_1,
            'why_quote_image_2' => $request->why_quote_image_2,
            'why_we_do' => $request->why_we_do
        ];
        $whyFeatureImageFirstPath =
            $this->uploadImageBase($request, 'why_feature_image_1', 'why-we-do');
        $whyFeatureImageSecondPath =
            $this->uploadImageBase($request, 'why_feature_image_2', 'why-we-do');

        if ($request->hasFile('why_feature_image_1')) {
            $dataUpdate['why_feature_image_1'] = 'storage' . $whyFeatureImageFirstPath;
        }
        if ($request->hasFile('why_feature_image_2')) {
            $dataUpdate['why_feature_image_2'] = 'storage' . $whyFeatureImageSecondPath;
        }

        $this->customer->updateData($id, $dataUpdate);
        return back();
    }

    public function updateHowWeDo(Request $request, $id)
    {
        $dataUpdate = [
            'how_quote_image_1' => $request->how_quote_image_1,
            'how_quote_image_2' => $request->how_quote_image_2,
            'how_we_do' => $request->how_we_do
        ];
        $howFeatureImageFirstPath =
            $this->uploadImageBase($request, 'how_feature_image_1', 'how-we-do');
        $howFeatureImageSecondPath =
            $this->uploadImageBase($request, 'how_feature_image_2', 'how-we-do');

        if ($request->hasFile('how_feature_image_1')) {
            $dataUpdate['how_feature_image_1'] = 'storage' . $howFeatureImageFirstPath;
        }
        if ($request->hasFile('how_feature_image_2')) {
            $dataUpdate['how_feature_image_2'] = 'storage' . $howFeatureImageSecondPath;
        }

        $this->customer->updateData($id, $dataUpdate);
        return back();
    }


}
