<ul class="menu-nav-enterprise">
    @php
        $currentRouteName = Route::currentRouteName();
    @endphp
    <li>
        <a class="{{ $currentRouteName == 'companies.index' ? 'active' : '' }}"
           href="{{ route('companies.index', ['slug' => $customer->slug]) }}">
            Trang chủ
        </a>
    </li>
    @foreach($menus as $menu)
        <li>
            <a class="{{ $currentRouteName == $menu->route ? 'active' : ''}}"
               href="{{ route($menu->route, ['slug' => $customer->slug]) }}">
                {{ $menu->name }}
            </a>
        </li>
    @endforeach
</ul>
