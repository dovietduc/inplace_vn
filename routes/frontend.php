<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('frontend')->group(function () {
	Route::get('/login', 'FrontendController@login');
	Route::get('/register', 'FrontendController@register');
	Route::get('/verify_email', 'FrontendController@verify_email');
    Route::get('/invite-friends', 'FrontendController@invite_friends');
    Route::get('/account-setting', 'FrontendController@account_setting');
    Route::prefix('companies')->group(function () {
        Route::get('/create', 'FrontendController@companyCreate');
        Route::get('/following', 'FrontendController@companyFollowing');
        Route::get('/members', 'FrontendController@companyMembers');
    });
	Route::prefix('job')->group(function () {
		Route::get('/list', 'FrontendController@getList');
		Route::get('/detail', 'FrontendController@getDetail');
		Route::get('/saved', 'FrontendController@getSaved');
		Route::get('/applications', 'FrontendController@getApplications');
        Route::get('/save-jobs', 'FrontendController@save_jobs');
        Route::get('/apply-jobs', 'FrontendController@apply_jobs');
	});

	Route::prefix('forgot')->group(function () {
		Route::get('/step1', 'FrontendController@forgot_step1');
		Route::get('/step2', 'FrontendController@forgot_step2');
		Route::get('/step3', 'FrontendController@forgot_step3');
		Route::get('/step4', 'FrontendController@forgot_step4');
	});

    Route::prefix('profile')->group(function () {
        Route::get('/home', 'FrontendController@profile_Home');
        Route::get('/posts', 'FrontendController@profile_Posts');
        Route::get('/network', 'FrontendController@profile_Network');
    });

    Route::prefix('enterprise')->group(function () {
        Route::get('/', 'FrontendController@dashboard_home');
        Route::get('/email-invite-member', 'FrontendController@email_invite_member');
        Route::prefix('news')->group(function () {
            Route::get('/list', 'FrontendController@dashboard_news_list');
            Route::get('/detail', 'FrontendController@dashboard_news_detail');
        });
        Route::prefix('jobs')->group(function () {
            Route::get('/list', 'FrontendController@dashboard_jobs_list');
            Route::get('/add', 'FrontendController@dashboard_jobs_add');
            Route::get('/candidates', 'FrontendController@dashboard_jobs_candidates');
        });
        Route::prefix('info')->group(function () {
            Route::get('/company', 'FrontendController@dashboard_info_company');
            Route::prefix('members')->group(function () {
                Route::get('/', 'FrontendController@dashboard_info_members');
                Route::get('/invite', 'FrontendController@dashboard_info_members_invite');
            });
        });
        Route::prefix('candidates')->group(function () {
            Route::get('/list', 'FrontendController@enterprise_candidates_list');
            Route::get('/detail', 'FrontendController@enterprise_candidates_detail');
        });
    });

    Route::prefix('admin')->group(function () {
        Route::get('/', 'FrontendController@admin_index');
        Route::get('/login', 'FrontendController@admin_login');
        Route::prefix('news')->group(function () {
            Route::get('/list', 'FrontendController@admin_news_list');
        });
        Route::prefix('jobs')->group(function () {
            Route::get('/list', 'FrontendController@admin_jobs_list');
        });
        Route::prefix('candidates')->group(function () {
            Route::get('/list', 'FrontendController@admin_candidates_list');
            Route::get('/detail', 'FrontendController@admin_candidates_detail');
        });
    });

    Route::prefix('helper')->group(function(){
        Route::get('/danh-cho-doanh-nghiep', 'FrontendController@danh_cho_doanh_nghiep');
        Route::get('/danh-cho-ca-nhan', 'FrontendController@danh_cho_ca_nhan');
        Route::get('/tro-giup', 'FrontendController@tro_giup');
        Route::get('/thong-bao', 'FrontendController@thong_bao');
        Route::get('/quy-dinh-bao-mat', 'FrontendController@quy_dinh_bao_mat');
        Route::get('/dieu-khoan-su-dung', 'FrontendController@dieu_khoan_su_dung');
        Route::get('/cham-soc-khach-hang', 'FrontendController@cham_soc_khach_hang');
        Route::get('/quy-che-hoat-dong', 'FrontendController@quy_che_hoat_dong');
        Route::get('/goc-doanh-nghiep', 'FrontendController@goc_doanh_nghiep');
        Route::get('/gioi-thieu', 'FrontendController@gioi_thieu');
    });

    Route::prefix('email')->group(function(){
        Route::get('/active-acount', 'FrontendController@email_active_acount');
        Route::get('/invite-member', 'FrontendController@email_invite_member');
        Route::prefix('/send-to-customer')->group(function(){
            Route::get('/user-apply-job', 'FrontendController@email_customer__user_apply_job');
        });
    });

    Route::get('/jobs/get-tags', 'FrontendController@getTags');

    Route::get('/enterprise/jobs/add/sub-occupation', 'FrontendController@getOccupationSup');


    Route::prefix('/jobs/filter')->group(function(){
        Route::get('', [
            'as' => 'frontend.jobs.filter',
            'uses' => 'FrontendController@getJobFilter',
        ]);
        Route::post('/search', [
            'as' => 'frontend.jobs.filter.keywords',
            'uses' => 'FrontendController@getJobSearchKeyworks',
        ]);
    });

});



