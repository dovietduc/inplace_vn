@extends('layouts.app')
@section('meta')
    <title>Giới thiệu</title>
@stop
@section('css')
    <link
        href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/BusinessCorner.css')}}">
@stop
@section('content')
    <div class="business-corner-page">
        <section class="business-corner-head">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-4">
                        <div class="business-corner-head-content">
                            <h1>Góc doanh nghiệp</h1>
                            <div class="txt-format">
                                <p>Thành công của doanh nghiệp không chỉ dừng lại trong việc xây dựng thành công thương hiệu trên thị trường mà còn nằm ở góc độ xây dựng thương hiệu nhà tuyển dụng. Việc tạo nên một thương hiệu nhà tuyển dụng thành công sẽ giúp các công ty thu hút được những nhân sự tốt và mang đến những lợi ích vô cùng đáng kể để tạo nền tảng cho sự phát triển vững mạnh của công ty. Và để thực hiện điều đó, INPLACE ra đời nhằm hỗ trợ phục vụ các công cụ tiện ích cho doanh nghiệp trên cả hai phương diện là Tuyển Dụng Online và Employer Branding (EB)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="business-corner-create">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8 col-lg-7">
                        <h3 class="business-corner-title">Cách tạo trang Doanh nghiệp</h3>
                        <div class="txt-format">
                            <ul>
                                <li>Đầu tiên, bạn cần tạo một tài khoản cá nhân tại trang chủ INPLACE.&nbsp;</li>
                                <li>Tiếp theo, click vào biểu tượng logo tài khoản cá nhân của bạn ở góc bên phải phía
                                    trên màn hình. Sau khi các danh mục xuất hiện, click vào mục tạo trang doanh nghiệp
                                </li>
                                <li>Điền các thông tin liên quan đến doanh nghiệp một cách chính xác và đầy đủ nhất,
                                    đừng quên upload logo và Slogan khẩu hiệu của doanh nghiệp mình nhé.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 offset-lg-1">
                        <img src="{{asset('images/img_tao_trang_doanh_nghiep_moi.jpg')}}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="business-corner-tools">
            <div class="container">
                <h3 class="business-corner-title">Các công cụ <br>
                    dành cho Doanh nghiệp</h3>
                <div class="row">
                    <div class="col-lg-6 left">
                        <div class="business-corner-tools-item">
                            <i class="fal fa-city"></i>
                            <div class="content">
                                <h4 class="title">Trang chủ:</h4>
                                <div class="txt-format">
                                    Trang chủ:
                                    Giới thiệu lịch sử hình thành, quá trình phát triển, quy mô doanh nghiệp, số lượng nhân sự và văn hóa doanh nghiệp,… thông qua các câu hỏi Who?, What?, Why? và How?.
                                </div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-briefcase"></i>
                            <div class="content">
                                <h4 class="title">Việc làm:</h4>
                                <div class="txt-format">
                                    Đăng tuyển các Job khi doanh nghiệp có nhu cầu tuyển dụng.</div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-file-alt"></i>
                            <div class="content">
                                <h4 class="title">Bài viết:</h4>
                                <div class="txt-format">
                                    Đăng tải những bài viết do cá nhân, doanh nghiệp chia sẻ về những câu chuyện đời thực, những tình huống mà chúng ta thường gặp trong công việc, trong đời sống cá nhân để từ đó đúc kết ra những chân lí, những giá trị cần được tiếp thu.</div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-map-marker-question"></i>
                            <div class="content">
                                <h4 class="title">Quiz:</h4>
                                <div class="txt-format">
                                    Những câu hỏi ngắn gọn, đơn giản và thú vị do doanh nghiệp tạo ra như một một trò chơi hoặc môn thể thao trí óc với mục đích thẩm định sự phù hợp của người tìm việc liên quan đến bất kỳ một chủ đề nào đó của doanh nghiệp (văn hoá, chuyên môn, định hướng phát triển, sự tương thích với các thành viên trong nhóm vv.) Qua đó, doanh nghiệp có cơ hội để tìm ra những người thực sự phù hợp với môi trường doanh nghiệp mình
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 right">
                        <div class="business-corner-tools-item">
                            <i class="fal fa-image"></i>
                            <div class="content">
                                <h4 class="title">Thư viện ảnh:</h4>
                                <div class="txt-format">
                                    Nơi lưu giữ những khoảnh khắc, những kỉ niệm trong các hoạt động của doanh nghiệp. Giúp nâng cao tính thẩm mỹ và tạo nên sự chuyên nghiệp trong việc quảng bá hình ảnh văn hóa doanh nghiệp.
                                </div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-cube"></i>
                            <div class="content">
                                <h4 class="title">Văn phòng ảo:</h4>
                                <div class="txt-format">
                                    Thể hiện không gian đa chiều, thực tế của nơi làm việc.</div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-video"></i>
                            <div class="content">
                                <h4 class="title">Video:</h4>
                                <div class="txt-format">
                                    Trình chiếu một cách sống động các thông tin mà doanh nghiệp muốn truyền tải đến mọi người, các đoạn phỏng vấn các thành viên sáng lập, các thành viên trong nhóm đang có job đăng tuyển, giúp đọc giả có cái nhìn gần gũi và chân thực về doanh nghiệp</div>
                            </div>
                        </div>
                        <div class="business-corner-tools-item">
                            <i class="fal fa-info-circle"></i>
                            <div class="content">
                                <h4 class="title">Q&A:</h4>
                                <div class="txt-format">
                                    Nơi doanh nghiệp có thể làm rõ các thông tin về doanh nghiệp mình dưới dạng Q&A phản ảnh các cuộc trao đổi thông tin giữa mình và những người quan tâm tới doanh nghiệp,  là nơi kết nối giữa doanh nghiệp và người tìm việc thông qua những câu hỏi và câu trả lời xoay quanh các vấn đề trong công việc, hoạt động cũng như các thông tin liên quan khác về công ty và người tìm việc.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="business-corner-management-tools">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 order-lg-2">
                        <h3 class="business-corner-title">Các công cụ quản lý <br>
                            trang Doanh nghiệp (Admin tool)</h3>
                        <div class="txt-format">
                            <h4>QUẢN LÝ &nbsp;NỘI DUNG:</h4>
                            <p>Doanh nghiệp có thể quản lý nội dung đã đăng lên trang DN của mình thông qua công cụ
                                quản lý tập trung trong phần “Cộng cụ Admin” của trang. Công cụ này có thể giúp
                                chỉnh sửa, xoá và tạo mới nội dung cùng lúc và nhanh chóng.<br><br></p><h4>QUẢN
                                LÝ JOBS:</h4>
                            <p>Là công cụ có thể quản lý số việc làm &nbsp;của mình như chỉnh sửa, xoá và tạo
                                mới<br><br></p><h4>QUẢN LÝ ỨNG VIÊN</h4>
                            <p>Là công cụ quản lý tổng số ứng viên đã ứng tuyển vào các job đã đăng bao gồm tên, vị
                                trí ứng tuyển, thời gian ứng tuyển, mức độ tương thích với công ty.<br><br></p><h4>
                                ANALYTICS:</h4>
                            <p>Quản lý số lượng truy cập, mức độ tương tác với trang, đo lường kết quả của các chiến
                                dịch truyền thông, quảng cáo trên hệ thống</p>
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-1">
                        <img src="{{asset('images/img_business_corner_2.jpg')}}" alt="">
                    </div>

                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <section class="business-corner-services">
                        <h3 class="business-corner-title">Các gói dịch vụ <br>
                            hỗ trợ Doanh nghiệp</h3>
                        <div class="txt-format">
                            <p>Mọi người hay doanh nghiệp đều có thể sử dụng gói TRIAL tại
                                www.inplace.vn &nbsp;với mức phí là 0 đồng. Với tài khoản miễn phí này, doanh nghiệp vẫn
                                có thể đăng Job và đăng bài viết nhằm quảng bá thương hiệu nhà tuyển dụng và tương tác
                                với những người dùng khác.</p>
                            <p>Tuy nhiên để đáp ứng tốt hơn những nhu cầu cụ thể của doanh nghiệp, INPLACE có những dịch
                                vụ hỗ trợ nhằm giúp các doanh nghiệp làm tốt công việc Employer Branding của mình hơn và
                                thực hiện tuyển dụng nhanh chóng và hiệu quả hơn. Tuỳ với nhu cầu và ngân sách của doanh
                                nghiệp, Inplace sẽ có các gói dịch vụ tương ứng như STANDARD, ENTERPRISE VÀ PREMIUM để
                                các doanh nghiệp có thể lựa chọn. Để biết rõ hơn về các gói dịch vụ, xin hãy liên lạc số
                                Hotline để gặp Chuyên Viên Tư Vấn Dịch vụ của chúng tôi.</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-6">
                    <section class="business-corner-hotline">
                        <h3 class="business-corner-title">Hotline <br>Tư vấn</h3>
                        <div class="txt-format">
                            <p>Nếu có bất cứ thông tin nào cần được giải đáp về dịch vụ của chúng
                                tôi, xin vui lòng liên hệ đến hotline: 028 6275 5586 hoặc Email: <a
                                    href="mailto:services@inplace.vn">services@inplace.vn</a><br></p>
                            <p>Các bài viết tham khảo dành cho Doanh Nghiệp:<br></p>
                            <p>5 lý do doanh nghiệp cần làm Employer Branding<br></p>
                            <p>Vì sao “Không tương thích văn hoá – Cultural Mismatch” là nguyên nhân hàng đầu khiến
                                doanh nghiệp chảy máu nguồn nhân lực.<br></p>
                            <p>Tuyển dụng theo cách truyền thống – liệu còn khả thi?</p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/BusinessCorner.js')}}"></script>
@stop
