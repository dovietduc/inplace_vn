@extends('layouts.dashboard')
@section('meta')
    <title>Jobs Add Dashboard</title>
@stop
@section('css')
    {{--CSS Froala editor--}}
    @include('html.dashboard.partials._css-Froala')
    {{--END: Froala Editor--}}
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/jobs/add.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
            </div>
        </div>
    </div>
    <div class="dashboard-job-add">
        <div class="container">
            <form action="" method="POST" class="formJobAdd">
                @csrf
                <input type="hidden" value="" name="image_crop_data" class="image_crop_data">
                <div class="dashboard-job-add_box">
                    <div class="dashboard-job-add_thumb">
                        @include('html.dashboard.partials.imageCroppie')
                    </div>
                    <div class="box-width">
                        <div class="dashboard-job-add_title">
                            <textarea class="titleJob autoheight disenter check-value-submit-disable" name="Job-title"
                                      placeholder="Vị trí tuyển dụng"></textarea>
                        </div>
                        <div class="dashboard-job-add_sub-title">
                            <textarea class="form-control autoheight disenter check-value-submit-disable"
                                      name="Job-title-sub" maxlength="80"
                                      placeholder="Tiêu đề rút gọn không quá 80 kí tự"></textarea>
                            <p class="note">* Tiêu đề rút gọn sẽ được hiển thị trong danh sách việc làm đính kèm các bài
                                viết của công ty</p>
                        </div>
                        <div class="dashboard-job-add_content txt-format">
                            <textarea class="inp-froala-editor" name="Job-content"
                                      placeholder="Mô tả công việc (điền các thông tin về Nội dung công việc, Yêu cầu về vị trí này) ..."></textarea>
                        </div>
                        <div class="dashboard-job-add_tag">
                            <select class="select2-mutiple-tags-3item" name="Job-tag" multiple
                                    data-placeholder="Từ khóa công việc (Tối đa 3)">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="dashboard-job-add_location-info">
                            <div class="component-title">
                                <h3 class="title">Thông tin vị trí</h3>
                                <div class="desc"><strong>Khu vực</strong> & <strong>Bộ phận làm việc</strong> được tối
                                    đa 3 lựa chọn
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Cấp bậc <span>*</span></label>
                                        <select class="select2-noSearch check-value-submit-disable" name="Job-rank"
                                                data-placeholder="Chọn cấp bậc">
                                            <option value=""></option>
                                            <option value="Giám đốc">Giám đốc</option>
                                            <option value="Quản lý">Quản lý</option>
                                            <option value="Nhân viên">Nhân viên</option>
                                            <option value="Thực tập sinh">Thực tập sinh</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Loại hình công việc <span>*</span></label>
                                        <select class="select2-noSearch check-value-submit-disable" name="Job-type"
                                                data-placeholder="Chọn loại hình công việc">
                                            <option value=""></option>
                                            <option value="Fulltime">Fulltime</option>
                                            <option value="Parttime">Parttime</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Khu vực làm việc <span>*</span></label>
                                        <select class="select2-mutiple-3item check-value-submit-disable"
                                                name="Job-location" multiple data-placeholder="Chọn khu vực làm việc">
                                            <option value=""></option>
                                            <option value="Hà Nội">Hà Nội</option>
                                            <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                                            <option value="Đà Nẵng">Đà Nẵng</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bộ phận làm việc <span>*</span></label>
                                        <select class="select2-mutiple-3item check-value-submit-disable"
                                                name="Job-working-parts" multiple
                                                data-placeholder="Chọn bộ phận làm việc">
                                            <option value=""></option>
                                            <option value="Frontend Develope">Hành chính nhân sự</option>
                                            <option value="Backend Develope">Backend Develope</option>
                                            <option value="Tester">Tester</option>
                                            <option value="Design">Design</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Trình độ tiếng Anh <span>*</span></label>
                                        <select class="select2-noSearch check-value-submit-disable"
                                                name="Job-level-English" data-placeholder="Chọn cấp bậc">
                                            <option value=""></option>
                                            <option value="Rất tốt">Rất tốt</option>
                                            <option value="Tốt">Tốt</option>
                                            <option value="Khá">Khá</option>
                                            <option value="Trung bình">Trung bình</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-job-add_salary">
                            <div class="component-title">
                                <h3 class="title">Tùy chọn hiển thị lương</h3>
                                <div class="desc"></div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-sm-6 col-lg-3">
                                    <select class="job-salary-select select2-noSearch" name="job-salary-select">
                                        <option value="1" selected>Trong khoảng</option>
                                        <option value="0">You'll love it!</option>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-6 col-lg-3 job-salary-pending">
                                    <input type="number" class="job-salary-from form-control" name="job-salary-from"
                                           placeholder="Từ">
                                </div>
                                <div class="col-6 col-sm-6 col-lg-3 job-salary-pending">
                                    <input type="number" class="job-salary-to form-control" name="job-salary-to"
                                           placeholder="Đến">
                                </div>
                                <div class="col-sm-6 col-lg-3 job-salary-pending">
                                    <p class="note">(<span>*</span>) Đơn vị: <strong>$ - USD</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-job-add_members">
                            <div class="component-title">
                                <h3 class="title">Thành viên công ty</h3>
                                <div class="desc"></div>
                            </div>
                            <p class="note">Hiển thị các thành viên liên quan đến Công ty hoặc Vị trí này. <br>
                                Việc giới thiệu về "Con người" giúp công ty bạn tìm được đúng Cộng Sự!</p>
                            <div class="job-members-editor">
                                <div class="d-flex position-relative">
                                    <div class="job-members-editor_result" id="job-members-editor_result">
                                    </div>
                                    <div class="position-relative">
                                        <div class="job-members-editor_add"></div>
                                        <div class="job-members-editor_filter">
                                            <div class="member-filter">
                                                <input id="job-members-editor_filter_input" type="text"
                                                       class="form-control" placeholder="Tìm kiếm thành viên" autocomplete="off">
                                                <label for="job-members-editor_filter_input"><i
                                                        class="fal fa-search"></i></label>
                                            </div>
                                            <ul class="member-list">
                                                @foreach($listUsersFollowCustomer as $memberCompany)
                                                    <li class="job-members-editor_filter_member"
                                                        data-idMember="{{ $memberCompany->id }}">
                                                        @include('enterprise.partials.show-image-avatar-user', ['member' => $memberCompany])
                                                        <div class="info">
                                                            <h5 class="name findname">{{ $memberCompany->user->name }}</h5>
                                                            <p class="station findname">{!! nl2br($memberCompany->position) !!}</p>
                                                            <div class="desc d-none">
                                                                {!! nl2br($memberCompany->introduction) !!}
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="job-members-editor_content">

                                    <div class="content" data-contentmember="2">
                                        <h5 class="name">Đào Mai Lê</h5>
                                        <p class="station">Frontend Developer</p>
                                        <div class="desc">
                                            Tôi là Đào Mai Lê làm Code frontend tại Inplace
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-job-add_submit">
                        <button type="submit" class="button-draft-job btn btn-custom_light">Lưu nháp</button>
                        <button type="submit" class="button-public-job btn btn-custom" disabled>Đăng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    {{--JS Froala editor--}}
    @include('html.dashboard.partials._js-Froala')
    {{--END: Froala editor--}}
    <script src="{{asset('vendor/croppie/croppie.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('vendor/sortable/sortable.js')}}"></script>
    <script src="{{asset('pages/enterprise/jobs/add.js')}}"></script>
@stop
