<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class JobMiddleMemberCompany extends Model
{
    protected $table = 'job_middle_member_company';
    protected $guarded = [];
    use BaseEloquent;


}
