<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class LevelJob extends Model
{
    protected $table = 'jjob_job_level';
    protected $guarded = [];
    use BaseEloquent;
}
