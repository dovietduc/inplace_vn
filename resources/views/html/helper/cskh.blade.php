@extends('layouts.app')
@section('meta')
    <title>Chăm sóc khách hàng</title>
@stop
@section('css')
    <link
        href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap&subset=cyrillic,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="{{asset('pages/helper/cskh.css')}}">
@stop
@section('content')
    <div class="customer-support-page">
        <div class="customer-support-head">
            <div class="container">
                <div class="customer-support-head_content">
                    <h3 class="title">
                        Chăm sóc khách hàng
                    </h3>
                </div>
            </div>
        </div>
        <div class="customer-support-contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <h2 class="title">Dành cho <br>Cá nhân</h2>
                        <div class="txt-format">
                            <p>Nếu bạn cần hỗ trợ kỹ thuật vui lòng liên hệ:</p>
                            <ul class="sp-contact">
                                <li class="sp-phone">Điện thoại: <strong>028 6275 5586</strong></li>
                                <li class="sp-email">Email: <a href="mailto:support@inplace.vn"
                                              class="color-theme">support@inplace.vn</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h2 class="title">Dành cho <br>Doanh nghiệp</h2>
                        <div class="txt-format">
                            <div class="txt-format">
                                <p>Để được hỗ trợ thông tin về các gói dịch vụ dành cho doanh nghiệp vui lòng liên
                                    hệ:</p>
                                <ul class="sp-contact">
                                    <li class="sp-phone">Điện thoại: <strong>028 6275 5586</strong></li>
                                    <li class="sp-email">Email: <a href="mailto:services@inplace.vn" class="color-theme">services@inplace.vn</a>
                                    </li>
                                </ul>
                                <h4><i class="fad fa-info-circle"></i> <strong>Lưu ý:</strong></h4>
                                    <p>Quy trình tiếp nhận nhu cầu, tư vấn dịch vụ và chăm sóc khách
                                        hàng doanh nghiệp của Inplace sẽ theo các bước sau:</p>
                                    <ul>
                                        <li><strong>Bước 1:</strong> Tiếp nhận yêu cầu về dịch vụ từ phía doanh nghiệp.
                                        </li>
                                        <li><strong>Bước 2: </strong>Tìm hiểu, nghiên cứu và phân tích tổng quan về tình
                                            hình doanh nghiệp và nhu cầu của khách hàng.
                                        </li>
                                        <li><strong>Bước 3:</strong> Lựa chọn và tư vấn gói dịch vụ phù hợp nhất với
                                            doanh nghiệp
                                        </li>
                                        <li><strong>Bước 4:</strong> Lắng nghe ý kiến phản hồi từ doanh nghiệp</li>
                                        <li><strong>Bước 5:</strong> Soạn thảo và ký hợp đồng dịch vụ theo yêu cầu của
                                            doanh nghiệp.
                                        </li>
                                        <li><strong>Bước 6:</strong> Chăm sóc khách hàng định kỳ, thường xuyên theo dõi
                                            để đảm bảo các dịch vụ dành cho khách hàng luôn được thông suốt.
                                        </li>
                                    </ul>
                                    <p>Chúng tôi sẽ luôn luôn lắng nghe và hỗ trợ khách hàng của mình trước, trong và
                                        sau khi hoàn thành dịch vụ.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="customer-support-brand">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                        Chúng tôi cam kết sẽ tiếp nhận mọi cuộc gọi đến từ khách hàng với thái độ phục vụ tận tâm,
                        chuyên nghiệp và nhanh chóng nhất.
                    </div>
                </div>
            </div>
        </div>
        <div class="customer-support-introduce">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <img src="{{asset('images/img_cover_cskh_2.jpg')}}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="txt-format text-justify">
                            <p>INPLACE tập trung mang lại trải nghiệm đặc biệt cho khách hàng và giúp bạn đạt được mục
                                đích khi đến với chúng tôi. Chúng tôi hiểu rằng khách hàng là yếu tố cốt lõi để chúng
                                tôi duy trì và phát triển. Để lắng nghe và thật sự thấu
                                hiểu những tâm tư, những nguyện vọng và những thắc mắc của khách hàng là một thách thức
                                vô cùng to lớn với những công ty trong ngành dịch vụ. Bởi lý do đó, chúng tôi đã thành
                                lập nên một đội ngũ chăm sóc khách hàng tận tâm, hiệu quả và sẵn sàng hỗ trợ khách hàng
                                xuyên suốt thời gian từ khi làm quen đến quá trình sử dụng sản phẩm. Mục tiêu của chúng
                                tôi là đảm bảo khách hàng có thể hiểu và sử dụng dịch vụ một cách hiệu quả nhất để nhận
                                được những giá trị tối đa của các sản phẩm đã mua và tạo tiền đề cho những lần sử dụng
                                dịch vụ của chúng tôi sau này.
                            </p>
                            <p>Chúng tôi sẽ cung cấp dịch vụ hỗ trợ bạn trước, trong và sau khi sử dụng dịch vụ nhằm
                                quản lý quy trình hỗ trợ khách hàng một cách liên tục, thường xuyên, đảm bảo rằng mục
                                tiêu của bạn sẽ được hiện thực hóa và tối đa hóa giá trị. Bên cạnh đó, chúng tôi sẽ
                                không ngừng quảng bá thương hiệu (cá nhân và doanh nghiệp) của bạn đến mọi nơi.”</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/cskh.js')}}"></script>
@stop
