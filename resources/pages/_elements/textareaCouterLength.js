
function getCouterLengthTextarea() {
    let resultStr = $(this).parent().find('.textarea-counter-result');
    let max = $(this).attr('maxLength');
    let characterCount = $(this).val().length;
    resultStr.text(max - characterCount);
}
export function textareaCouterLength() {
    let text = $('.textarea-counter');
    text.each(getCouterLengthTextarea);
    text.on('keyup',getCouterLengthTextarea)
}
