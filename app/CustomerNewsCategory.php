<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class CustomerNewsCategory extends Model
{
    protected $table = 'jjob_customer_news_categories';
    protected $guarded = [];
    use BaseEloquent;
    const inplaceCategory = 1;
}
