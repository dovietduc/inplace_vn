<div class="txt-format">
    <h1>Tạo bài viết mới</h1>
    <p>Trang cá nhân giống như bộ mặt của bạn và là ngôi nhà của riêng bạn, khi bạn tạo ra khác biệt với mọi người thì mới có thể gây sự chú ý đến nhà tuyển dụng. Việc tạo ra những bài viết ấn tượng trên trang cá nhân là cách giúp phô diễn kiến thức, kỹ năng chuyên môn, sự chuyên nghiệp, tính cách, quan điểm sống và phong cách riêng của mình. Điều này sẽ đem lại nhiều lợi ích giúp bạn có cơ hội phát triển nghề nghiệp (được nhà tuyển dụng chú ý đến) mà còn là dịp luyện được thói quen chuyên môn tốt.
        </p><p>Để tạo bài viết mới, bạn vui lòng tạo tài khoản cá nhân theo hướng dẫn tại đường link sau: <a
            href="javascript:;" onclick="$('#bat-dau_tao-tai-khoan_tab').click()">Hướng dẫn tạo tài khoản cá nhân</a>
    </p><p>Sau khi tạo tài khoản cá nhân thành công, bạn thao tác theo 1 trong 2 cách dưới đây để tạo bài viết mới:
    </p>
    <h3>CÁCH 1: Thao tác tại trang Profile cá nhân của mình qua thanh công cụ </h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_moi.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào <strong>Bài Viết</strong></li><li>
                    <strong>Bước 2:</strong> Chọn <strong>Tạo bài vết mới</strong>. Sau đó, điền đầy đủ các nội dung
                    liên quan đến bài viết như tiêu đề, nội dung, lĩnh vực,…</li><li>
                    <strong>Bước 3:</strong> Hoàn tất tin đăng bằng cách nhấp vào ô <strong>Đăng Bài</strong>.</li>
            </ul>
        </div>
    </div>

    <h3>CÁCH 2: Thao tác trực tiếp trên thanh Head Menu</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_moi_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li>
                    <strong>Bước 1:</strong> Nhấp vào <strong>Viết Bài</strong> trên thanh Menu ngang đầu màn hình</li><li>
                    <strong>Bước 2:</strong>  Form tạo bài viết mới sẽ hiện lên. Bạn hãy điền đầy đủ các nội dung liên quan đến bài viết như tiêu đề, nội dung, lĩnh vực,…</li><li>
                    <strong>Bước 3:</strong> Hoàn tất tin đăng bằng cách nhấp vào ô <strong>Đăng Bài</strong>.
                </li>
            </ul>
        </div>
    </div>

</div>
