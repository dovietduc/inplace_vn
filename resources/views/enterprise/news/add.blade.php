@extends('layouts.dashboard')
@section('meta')
    <title>News add Dashboard</title>
@stop
@section('css')
    {{--CSS Froala editor--}}
    @include('enterprise.partials.css-froala')
    {{--END: Froala Editor--}}
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/news/add.css') }}">
    <style>
        .dashboard-news-detail .fr-wrapper>div:first-child:not(.fr-element)>a {
            opacity: 0 !important;
        }
    </style>
@stop
@section('content')
    @php
        $now = Carbon::now();
    @endphp

    @include('enterprise.news.partials.header-customer')

    <div class="dashboard-news-detail">
        <div class="container">
            <form
                action="{{ route('enterprise.post.news.add') }}"
                method="POST" class="formEditNewsDetail">
                @csrf
                <input type="hidden" value="" name="image_crop_data" class="image_crop_data">
                <input type="hidden" value="" name="type_submit" class="type_submit">
                <div class="dashboard-news-detail_head">
                    <div class="news-auth">
                        <a href="#" class="news-auth_avatar">
                            <img src="{{asset("images/no_user_35x35.jpg")}}" alt="">
                        </a>
                        <div class="news-auth_info">
                            <h5 class="news-auth_name">
                                <a href="#">
                                    {{ !empty($customerCurrent) ? $customerCurrent->name : '' }}
                                </a>
                            </h5>
                        </div>
                    </div>
                    <div class="news-category">
                        <select name="category" class="addNew-category js-select2" data-placeholder="Chọn danh mục">
                            <option value=""></option>
                            @foreach($categories as $categoryItem)
                                <option value="{{ $categoryItem->id }}">{{ $categoryItem->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="dashboard-news-detail_box">
                    <div class="dashboard-news-detail_thumb">
                        @include('enterprise.partials.croppie-image')
                    </div>
                    <div class="row px-3 px-md-0">
                        <div class="col-md-10 offset-md-1">
                            <div class="box-width">
                                <div class="dashboard-news-detail_title">
                                    <textarea class="addNew-title autoheight disenter" name="title" placeholder="Tiêu đề"></textarea>
                                </div>
                                <div class="dashboard-news-detail_head">
                                    <div class="news-auth">
                                        <a href="#" class="news-auth_avatar">
                                            <img src="{{asset("images/no_user_35x35.jpg")}}" alt="">
                                        </a>
                                        <div class="news-auth_info">
                                            <h5 class="news-auth_name">
                                                <a href="#">
                                                    {{ auth()->user()->name }}
                                                </a></h5>
                                            {{--<div class="news-auth_desc">Account Manager</div>--}}
                                        </div>
                                    </div>
                                    <div class="news-time">
                                        <i class="far fa-clock"></i> {{ Carbon::now()->format('l, d F, Y') }}
                                    </div>
                                </div>
                                <div class="dashboard-news-detail_content txt-format">
                                    <textarea
                                        class="inp-froala-editor"
                                        data-urlupload="{{ route('enterprise.froalaUpload') }}"
                                        data-urldelete="{{ route('enterprise.froalaDelete') }}"
                                        name="content_news"
                                        placeholder="Nhập nội dung bài viết"
                                    >

                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dashboard-news-detail_submit">
                        <button type="button" class="btn button-draft-news btn-custom_light">Lưu nháp</button>
                        <button type="button" class="button-public-news btn btn-custom" disabled>Đăng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('enterprise.partials.popup-error')
@stop
@section('js')
    {{--JS Froala editor--}}
    @include('enterprise.partials.js-froala')
    {{--END: Froala editor--}}
    <script src="{{asset('vendor/croppie/croppie.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{asset('pages/enterprise/news/add.js')}}"></script>

@stop
