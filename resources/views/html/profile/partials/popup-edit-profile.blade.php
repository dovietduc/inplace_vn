<div id="popupEditProfile" class="popup-edit-profile modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chỉnh sửa Trang cá nhân</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
            <form action="" method="POST" class="formEditProfile">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Họ tên</label>
                        <input class="inp-name form-control" type="text" value="Võ Trần Hoài Thanh" required>
                        <span class="err-name err-inp"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Headline</label>
                        <input class="form-control" type="text" value="Account Manager tại J-JOB Executive Search">
                    </div>
                    <div class="form-group">
                        <label for="">Giới tính</label>
                        <span class="mr-4"><input type="radio" name="gender" id="profile-male"><label
                                for="profile-male">Nam</label></span>
                        <span><input type="radio" name="gender" id="profile-female" checked><label for="profile-female">Nữ</label></span>
                    </div>
                    <div class="form-group">
                        <label for="">Lĩnh vực</label>
                        <select class="js-select2-multi form-control" multiple="multiple">
                            <option>Marketing</option>
                            <option>Headhunt</option>
                            <option>Front-end Deverloper</option>
                            <option>Back-end Deverloper</option>
                            <option>Back-end Deverloper</option>
                            <option>Back-end Deverloper</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Thông tin liên hệ</label>
                        <p class="form-control">Link Profile , Email, Số điện thoại, Ngày sinh ,...
                            <button class="form-control__btn-icon btn-open-popup-edit-contact-info"><i
                                    class="far fa-edit"></i></button>
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-custom">Lưu thay đổi</button>
                </div>
            </form>
        </div>
    </div>
</div>
