<div class="job-detail_content__what">
    <div class="component-title">
        <h3 class="title">What</h3>
        <div class="desc">Sứ mệnh của công ty là gì</div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->what_feature_image_1) )
                        <img src="{{ url($customer->what_feature_image_1) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif

                    <h5 class="title">{{ $customer->what_quote_image_1 }}</h5>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->what_feature_image_2) )
                        <img src="{{ url($customer->what_feature_image_2) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif
                    <h5 class="title">
                        {{ $customer->what_quote_image_2 }}
                    </h5>
            </div>
        </div>
    </div>
    <div class="content">
        {!! $customer->what_we_do  !!}
    </div>
    <div class="viewmore">
        <a href="javascript:;">Xem thêm <i class="fas fa-chevron-down"></i></a>
    </div>
</div>
<div class="job-detail_content__why job-detail_content__what">
    <div class="component-title">
        <h3 class="title">Why</h3>
        <div class="desc">Tại sao chọn chúng tôi?</div>
    </div>
    <div class="row">

        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->why_feature_image_1) )
                        <img src="{{ url($customer->why_feature_image_1) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif
                    <h5 class="title">
                        {{ $customer->why_quote_image_1 }}
                    </h5>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->why_feature_image_2) )
                        <img src="{{ url($customer->why_feature_image_2) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif
                    <h5 class="title">{{ $customer->why_quote_image_2 }}</h5>
            </div>
        </div>

    </div>
    <div class="content">
        {!! $customer->why_we_do  !!}
    </div>
</div>
<div class="job-detail_content__how job-detail_content__what">
    <div class="component-title">
        <h3 class="title">How</h3>
        <div class="desc">Chúng tôi hoạt động như thế nào?</div>
    </div>
    <div class="row">

        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->how_feature_image_1) )
                        <img src="{{ url($customer->how_feature_image_1) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif
                    <h5 class="title">{{ $customer->how_quote_image_1 }}</h5>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="component-how-item">
                    @if( jjobCheckImageExit($customer->how_feature_image_2) )
                        <img src="{{ url($customer->how_feature_image_2) }}"
                             alt="">
                    @else
                        <img src="{{ asset('images/no_img_320x178.jpg') }}"
                             alt="">
                    @endif
                    <h5 class="title">{{ $customer->how_quote_image_2 }}</h5>
            </div>
        </div>

    </div>
    <div class="content">
        {!! $customer->how_we_do  !!}
    </div>
</div>
