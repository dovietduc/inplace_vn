<div class="inputCroppieImage fixHeight {{ !empty($job) && jjobCheckImageExit($job->feature_image) ? 'hasImg' : '' }}"
     style="background-image: url('{{  !empty($job) && jjobCheckImageExit($job->feature_image) ? asset($job->feature_image) : '' }}')">
    <div class="inputCroppieImage_inpPr">
        <label><i class="fal fa-image"></i> Kéo hoặc nhấp để sửa ảnh</label>
        <input type="file" name="" class="croppieImage_input" accept=".png, .jpg, .jpeg">
    </div>
    <div class="inputCroppieImage_crop">
        <div class="upload-crop-demo"></div>
        <div class="upload-crop-button">
            <button type="button" class="btnRemoveCrop btn btn-grey">Gỡ</button>
        </div>
    </div>
</div>
