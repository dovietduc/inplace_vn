<?php

namespace App\Console\Commands;

use App\Occupation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class CreateDataOccupations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:data-occupations-for-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create data occupations for jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            [
                'parent' => 'Công nghệ/ Phần mềm',
                'childrent' => [
                    'Mobile Developer',
                    'Web Developer',
                    'Software Engineer',
                    'System Engineer',
                    'Business Analyst',
                    'Data Analyst',
                    'QA/ Tester'
                ]
            ],
            [
                'parent' => 'Quản lý Sản phẩm/ Dự án',
                'childrent' => [
                    'Technical Leader',
                    'Product Owner',
                    'Project Manager',
                    'Team Leader/ Manager'

                ]
            ],

            [
                'parent' => 'Thiết kế/ Sáng tạo',
                'childrent' => [
                    'UX UI Designer',
                    'Graphic Designer',
                    'Video Creator',
                    '2D/ 3D Desingner'
                ]
            ],

            [
                'parent' => 'Marketing/ PR',
                'childrent' => [
                    'Content Marketing',
                    'Digital Marketing',
                    'Copywriter/ Editor',
                    'PR/ Branding',
                    'Event Marketing',
                    'Account Executive',
                    'Marketing Manager'

                ]
            ],

            [
                'parent' => 'Bán hàng/ Phát triển Kinh doanh',
                'childrent' => [
                    'ToB Sales',
                    'ToC Sales',
                    'Telesales',
                    'Business Development',
                    'Dịch vụ khách hàng'
                ]
            ],

            [
                'parent' => 'Hành chính/ Văn phòng',
                'childrent' => [
                    'Finance',
                    'Accounting',
                    'Recruitment',
                    'HRBP',
                    'Employer Branding',
                    'Đào tạo',
                    'GA',
                    'Secretary/ Assistant',
                    'HR Manager'
                ]
            ],

            [
                'parent' => 'Supply Chain',
                'childrent' => [
                    'Purchasing',
                    'Demand planning',
                    'Inventory control',
                    'Delivery optimization'
                ]
            ],

            [
                'parent' => 'Khối việc làm tiếng Nhật',
                'childrent' => [
                    'Biên phiên dịch tiếng Nhật',
                    'IT Comtor',
                    'Kỹ sư cầu nối (BrSE)',
                    'Sales tiếng Nhật',
                    'Giáo viên tiếng Nhật',
                    'Trợ lý/ Nhân viên văn phòng tiếng Nhật'
                ]
            ],

            [
                'parent' => 'Khác',
            ],


        ];

        try {
            DB::beginTransaction();
            Occupation::truncate();
            foreach ($data as $occItem) {
                $parenteItem = Occupation::create([
                    'name' => $occItem['parent']
                ]);
                if (!empty($occItem['childrent'])) {
                    foreach ($occItem['childrent'] as $childItem) {
                        Occupation::create([
                            'name' => $childItem,
                            'parent_id' => $parenteItem->id
                        ]);
                    }
                }
            }
            DB::commit();
            $this->info('Convert data occupations job complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }


    }


}
