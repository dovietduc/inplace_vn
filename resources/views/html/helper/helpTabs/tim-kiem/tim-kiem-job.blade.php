<div class="txt-format">
    <h1>Tìm kiếm Job</h1>
    <div class="row">
        <div class="col-sm-8 mb-3">
            <p>Để tìm kiếm việc làm do các công ty đăng tải, hãy chọn tại thanh công cụ trên cùng phía bên trái. Màn
                hình sẽ hiển thị một danh sách việc làm được cập nhật liên tục từ các doanh nghiệp đang tìm kiếm nhân
                sự. Bạn có thể sắp xếp danh sách việc làm trên bảng tin theo thứ tự <strong>Đề xuất cho bạn / Mới nhất /
                    Phổ biến</strong> nhất tùy theo mong muốn của bạn. Để tìm kiếm việc làm phù hợp, bạn có thể sử dụng
                bộ lọc bằng cách điền/ lựa chọn các trường tương ứng:<br></p>
        </div>
        <div class="col-sm-4 mb-3">
            <img src="{{asset('images/img_tim_kiem_job.jpg')}}" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 mb-3">
            <p>Ngoài ra, bạn có thể tìm kiếm việc làm thông qua Chủ đề/ từ khóa có sẵn trên hệ thống như:</p>
        </div>
        <div class="col-sm-4 mb-3">
            <img src="{{asset('images/img_tim_kiem_job_2.jpg')}}" alt="">
        </div>
    </div>
    <div class="fr-element fr-view" dir="auto" contenteditable="true" aria-disabled="false" spellcheck="true"><p>Để theo
            dõi việc làm, bạn có 2 lựa chọn: Lưu việc làm và Ứng tuyển.&nbsp;</p><h4>1. Lưu việc làm:</h4>
        <p>Bạn hãy nhấp vào biểu tượng <i class="fas fa-bookmark color-theme"></i> tại góc dưới bên phải của mỗi tin tuyển dụng để lưu việc làm. Sau
            khi lưu, trên thanh công cụ sẽ tự động cập nhận việc làm đó cùng những việc làm bạn đã lưu tại mục <i class="far fa-bookmark"></i>
            <strong>Công việc đã lưu</strong> <span style="color: rgb(204, 204, 204);">(04)</span>. Bạn có thể xem lại
            những việc làm mình đã lưu bất cứ lúc nào.</p><h4>2. Ứng tuyển:</h4>
        <p>Để ứng tuyển, bạn hãy nhấp vào biểu tượng <i class="fas fa-paper-plane color-theme"></i> tại góc dưới bên phải của mỗi tin tuyển dụng. Sau
            khi ứng tuyển, trên thanh công cụ sẽ tự động cập nhận việc làm đó cùng những việc làm bạn đã ứng tuyển tại
            mục <i class="far fa-paper-plane"></i> <strong>Việc làm đã ứng tuyển</strong> <span style="color: rgb(204, 204, 204);">(05)</span>.</p>
    </div>
</div>
