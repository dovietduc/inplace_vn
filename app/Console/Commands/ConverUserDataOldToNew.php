<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Log;

class ConverUserDataOldToNew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:user-data-from-old-to-new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert user data from old to new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Them thong tin va apply email
        try {
            DB::beginTransaction();
            DB::table('jjob_users')->orderBy('id')->chunk(100, function ($users) {
                foreach ($users as $user) {
                    User::create([
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'password' => $user->password,
                        'mobile' => $user->mobile,
                        'avatar' => $user->avatar,
                        'feature_image' => $user->feature_image,
                        'website' => $user->website,
                        'birthday' => !empty($user->birthday) && $user->birthday != '0000-00-00' ? $user->birthday : NULL,
                        'gender' => $user->gender,
                        'about_me' => $user->about_me,
                        'address' => $user->address
                    ]);
                }
            });
            DB::commit();
            $this->info('Convert user complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
