import {adminLayout} from '../adminLayout.js';
const axios = require('axios');
import swal from "sweetalert";

function selectStatusNews() {
    let url = $(this).data('url');
    console.log(url);
    let active = $(this).val();
    axios.post(url, {
        active: active
    })
        .then(function (response) {
            if (response.status == 200) {

            }
        })
        .catch(function (error) {
            console.log(error);
        })
}
$(function () {
    adminLayout();
    $(document).on('change','.select-status-news', selectStatusNews);
});
