@extends('emails.layouts.for-customer')

@section('title', 'Candidate Apply Jobs')

@section('content')
    <p>Xin chào,</p>
    <p>
        Xin chúc mừng, bạn đã nhận được hồ sơ ứng tuyển của
        <strong>{{ $data['name'] }}</strong>
        cho vị trí
        <a href="{{ $data['job_url'] }}" target="_blank">
            {{ $data['job_short_title'] }}
        </a>
        của
        <strong>{{ $data['name_customer'] }}</strong>
        bạn đã đăng trên
        <a
            href="https://inplace.vn" target="_blank">www.inplace.vn</a>.
    </p>
    <p>
        Bạn có thể cập nhật tình hình ứng viên của job này bằng cách truy cập vào công cụ Quản lý của
        <strong>
            J-Job Executive Search
        </strong>
        tại
        <a href="https://inplace.vn" target="_blank">www.inplace.vn</a>
        thông qua tài khoản admin của bạn.
    </p>
    <blockquote>
        <p><strong>→ Mách nhỏ cho nhà tuyển dụng:</strong></p>
        <p>• Để tin tuyển dụng tới được với nhiều
            người, hãy thường xuyên chia sẻ nội dung tuyển dụng. Hiện tại nền tảng INPLACE cho
            phép người dùng chia sẻ thông tin sang các mạng xã hội như Facebook hoặc Linkedin. Do đó bạn chỉ cần
            đăng tuyển tại một nơi nhưng có thể tiếp cận với ứng viên
            trên nhiều nền tảng khác
        </p>
        <p>• Tăng mức độ phù hợp của ứng viên, nhà tuyển dụng nên
            điền đầy đủ thông tin về công ty tại Trang chủ trang doanh nghiệp,
            đăng nhiều Bài Viết giới thiệu đặc trưng văn hóa doanh nghiệp để ứng viên có cái nhìn đầy đủ nhất về môi
            trường làm việc. Như vậy sẽ thúc đẩy quyết định
            ứng tuyển của các ứng viên phù hợp nhất.
        </p>
    </blockquote>
@endsection
