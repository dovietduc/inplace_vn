<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class CustomerMiddleIndustry extends Model
{
    protected $table = 'jjob_customer_industries';
    protected $guarded = [];
    use BaseEloquent;
}
