<p>
    Hầu hết mọi người đều chưa đánh giá đúng tầm quan trọng của niềm tin đối với công việc. Trước giờ, mọi người luôn cho rằng niềm tin chỉ là một thứ vô hình, dễ vỡ, nhưng ít ai biết niềm tin còn là một động lực kinh tế, một kỹ năng có thể học hỏi và đo lường được, là một trong những nhân tố thiết yếu làm nên sự thành công và hiệu quả của bất kỳ cá nhân, tổ chức nào.
</p>

<p>
    Vậy niềm tin là gì? Tại sao mỗi người đều cần xây dựng niềm tin để công việc suôn sẻ hơn và con đường thăng tiến rộng mở hơn? Làm sao để chuyển đổi giá trị niềm tin từ vô hình sang hữu hình để làm gia tăng giá trị bản thân mỗi người, từ đó gia tăng giá trị chung cho tổ chức?
</p>

<p>
    Đánh giá tầm quan trọng của niềm tin, Chị Thơm giới thiệu về nội dung cuốn sách trình bày: “ Niềm tin rất quan trọng. Đối với công việc, vai trò của niềm tin lại càng được khẳng định rõ rệt. Trong gia đình hay tổ chức, niềm tin có mối quan hệ thuận chiều với tốc độ phát triển và nghịch chiều với chi phí”.
</p>
<p class="text-center mb-2">
    <img src="https://via.placeholder.com/770x510" class="w-100" alt="">
</p>
<p class="text-center">
    <span class="text-grey-200">
        John Doe - người thuyết trình về tầm quan trọng của niềm tin đối với mỗi cá nhân
    </span>
</p>
<p>
Có 4 yếu tố cốt lõi quyết định uy tín của một cá nhân bao gồm: "Sự chính trực - Ý định - Năng lực và Kết quả”. Dựa trên bốn yếu tố này, mỗi cá nhân sẽ tự mình xây dựng được bản “kế hoạch hành động” nâng cao uy tín của chính họ” - Thơm nói.
</p>

<h4>Niềm tin trong tổ chức được xây dựng như thế nào?</h4>
<p>
Mở rộng vấn đề niềm tin cá nhân sang tầm quan trọng của niềm tin đối với một tổ chức, Ngọc - người đồng thuyết trình nêu ý kiến: “Niềm tin trong tổ chức được xây dựng dựa trên tổng hòa từ niềm tin của tất cả các cá nhân trong tổ chức đó, của tất cả tính cách và năng lực ở mỗi cá nhân đó. Trong một môi trường có sự tín nhiệm thấp, những kế hoạch và động cơ không rõ ràng tạo nên sự hoài nghi cũng như làm cản trở sự hoàn thành các công việc. Giao tiếp dè chừng, nghi ngờ xét đoán và sự lảng tránh làm giảm hiệu quả làm việc và gia tăng sự lộn xộn. Nhưng khi các cá nhân tin tưởng lẫn nhau, giao tiếp sẽ được cải thiện và hiệu quả làm việc được nâng cao do mọi người cùng hướng tới các mục tiêu chung của nhóm.
</p>
<p>
Khi tồn tại niềm tin vững chắc, tổ chức sẽ nhận được những giá trị gọi là “cổ tức niềm tin”, to lớn và hoàn toàn hữu hình. Ngược lại, khi niềm tin bị đổ vỡ, tổ chức sẽ phải gánh chịu những chi phí tổn thất liệt vào danh mục “thuế niềm tin” không hề nhỏ”.
</p>
<p>
Chốt lại vấn đề, Ngọc khẳng định: “ Niềm tin trong tổ chức là thứ vô hình mà chúng ta không nắm được, nhưng chắc chắn kết quả mà nó mang lại sẽ là những thứ hữu hình, tạo ra giá trị, giúp công ty và từng cá nhân thay đổi theo đuổi theo chiều hướng tích cự hơn”.
</p>
<p>
Sau buổi thuyết trình, các bạn cũng đặt khá nhiều câu hỏi xoay quanh chủ đề về Niềm tin: Làm sao để tăng Niềm tin và sử dụng hiệu quả nó trong công việc?  Niềm tin của cá nhân khác gì với Niềm tin của tổ chức…?, và đưa ra những đánh giá về buổi thuyết trình bổ ích, thú vị.
</p>
<p>
Trong buổi thảo luận, công ty cũng tổng kết tháng và trao thưởng cho Ms Lê Vy - ESS Consultant với giải Spirit Award trong tháng vừa rồi.
</p>
