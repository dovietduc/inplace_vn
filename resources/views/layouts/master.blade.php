<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('partials.favicon-for-website')
    @yield('meta')
    <link href="{{ asset('vendor/bootstrap-v4/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/font-awesome-v5/css/all.css') }}" rel="stylesheet" type="text/css" media="all"/>
    @yield('stylesheetAddon')

</head>
<body>

@yield('content')

<script src="{{ asset('vendor/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-v4/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-v4/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
@yield('scriptAddon')
</body>
</html>
