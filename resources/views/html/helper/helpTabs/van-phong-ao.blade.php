<div class="txt-format">
    <h1>Văn phòng ảo</h1>
    <p>Văn phòng ảo là công cụ giúp doanh nghiệp giới thiệu về không gian làm việc của mình một cách trực quan nhất cho ứng viên và những người viếng thăm trang Doanh Nghiệp của mình. Văn phòng ảo thực chất là công cụ hiển thị hình ảnh 3D. Để thực hiện tính năng này, doanh nghiệp cần đăng tải hình ảnh Panorama hoặc hình ảnh có thể hiển thị được dạng 360*. Đối tới những doanh nghiệp sử dụng gói dịch vụ hỗ trợ Employer Branding có tính phí, Inplace sẽ hỗ trợ miễn phí việc chụp hình và hiển thị văn phòng ảo trên Inplace.
    </p>
    <p>Để đăng tải và chỉnh sửa nội dung liên quan tới Văn Phòng Ảo, bạn cần tạo Trang Doanh Nghiệp, sau đó truy cập vào trang Profile Doanh Nghiệp và tìm tới mục Văn Phòng Ảo.</p>
</div>
