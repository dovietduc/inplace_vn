@extends('layouts.admin')
@section('meta')
    <title>Admin - News list</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/jobs/list.css')}}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
                <form method="get" action="{{ route('admin.job.list') }}">
                    <div class="button-group">
                        <input
                            value="{{ request()->id_job }}"
                            name="id_job"
                            type="text"
                            class="form-control" placeholder="Mã Công việc">
                        <input
                            value="{{ request()->name_job }}"
                            name="name_job"
                            type="text" class="form-control" placeholder="Tên Công việc">
                        <input
                            value="{{ request()->id_customer }}"
                            name="id_customer"
                            type="text" class="form-control" placeholder="Mã Công ty">
                        <input
                            value="{{ request()->name_customer }}"
                            name="name_customer"
                            type="text" class="form-control" placeholder="Tên Công ty">
                        <input type="hidden" name="submit" value="submit">
                        <button type="submit" class="btn btn-custom">
                            Tìm kiếm
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="dashboard-jobs-list">
        <div class="container">
            <div class="dashboard-jobs-list_th">
                <div class="row">
                    <div class="col-xl-7 d-none d-md-block">
                        <div class="row">
                            <div class="col-6">
                                Tiêu đề
                            </div>
                            <div class="col-4">
                                Công ty
                            </div>
                            <div class="col-2">
                                Ứng tuyển
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 d-none d-xl-block">
                        <div class="row">
                            <div class="col-3">
                                Thời gian
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-6">
                                        Hot
                                    </div>
                                    <div class="col-6">
                                        Slider
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                Trạng thái
                            </div>
                            <div class="col-3">
                                Push Top
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @foreach($jobs as $job)
                <div class="dashboard-jobs-item job_item_wrapper_{{ $job->id }}">
                    <div class="row">
                        <div class="col-xl-7">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="dashboard-jobs-item_title">
                                        <h4 class="title">
                                            <a href="{{ route('jobs.show', ['slug' => $job->slug]) }}">
                                                {{ $job->title }}
                                            </a>
                                        </h4>
                                        <div
                                            class="d-flex align-items-center justify-content-between mt-2 mt-md-0 mb-md-2 mb-xl-0 mt-xl-2">
                                            <div class="location">
                                                {{ $job->citys->implode('name', ', ') }}
                                            </div>
                                            <div class="action">
                                                <a href="{{ route('jobs.show', ['slug' => $job->slug]) }}"
                                                   class="action_view"
                                                   title="Xem" target="_blank"><i
                                                        class="far fa-eye"></i></a>
                                                {{--                                            <a href="" class="action_edit" title="Sửa"><i class="far fa-edit"></i></a>--}}
                                                {{--                                            <a href="" class="action_delete" title="Xóa"><i class="far fa-trash-alt"></i></a>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    {{ optional($job->customer)->name }}
                                </div>

                                @if($job->status == \App\Job::statusPublic)
                                    <div class="col-md-2 my-2 my-md-0">
                                        <p class="d-inline-block d-md-block"><strong>9</strong> ứng viên</p>
                                        <a href="#" class="btn btn-custom_light ml-1 mt-0 ml-md-0 mt-md-2">Xem</a>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="col-xl-5">
                            <div class="row">
                                <div class="col-sm-3">
                                    <p class="info-txt">
                                        <small>Ngày đăng: </small><br class="d-none d-sm-block">
                                        {{ Carbon::createFromFormat('Y-m-d H:i:s', $job->created_at)->format('d-m-Y') }}
                                    </p>
                                </div>
                                <div class="col-4 col-sm-3">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="info-txt d-xl-none">
                                                <small>Hot</small>
                                            </p>
                                            @if($job->status == \App\Job::statusPublic)
                                                <label class="checkbox-switch">
                                                    <input
                                                        data-url="{{ route('admin.job.changeHotJob', ['id' => $job->id]) }}"
                                                        class="input_hot_job_aceept"
                                                        type="checkbox" {{ $job->hot ? 'checked' : '' }}>
                                                    <span class="btn-switch"></span>
                                                </label>
                                            @endif
                                        </div>
                                        <div class="col-6">
                                            <p class="info-txt d-xl-none">
                                                <small>Slider</small>
                                            </p>
                                            @if($job->status == \App\Job::statusPublic)
                                                <label class="checkbox-switch">
                                                    <input
                                                        data-url="{{ route('admin.job.changeShowSliderJobHome', ['id' => $job->id]) }}"
                                                        class="input_slider_job_aceept"
                                                        type="checkbox" {{ $job->slider_home ? 'checked' : '' }}>
                                                    <span class="btn-switch"></span>
                                                </label>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="col-4 col-sm-3">
                                    @if($job->status == \App\Job::statusPublic)
                                        <select
                                            name="active"
                                            class="select-status-job"
                                            data-url="{{ route('admin.job.changeActiveJob', ['id' => $job->id]) }}">
                                            @foreach($jobsStatus as $jobsStatusItem)
                                                <option
                                                    {{ $jobsStatusItem->id == $job->active ? 'selected' : '' }}
                                                    value="{{ $jobsStatusItem->id }}">
                                                    {{ $jobsStatusItem->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <div class="col-4 col-sm-3">
                                    @if($job->status == \App\Job::statusPublic)
                                        <button
                                            data-url="{{ route('admin.job.pushTop', ['id' => $job->id]) }}"
                                            class="btn-push-top status-txt">Đẩy Top
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach


            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                {{ $jobs->appends($_GET)->links('vendor.pagination.default') }}
            </div>

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/admin/jobs/list.js')}}"></script>
@stop
