@extends('layouts.app')
@section('meta')
    <title>Điều khoản sử dụng</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/helper/termsOfUse.css')}}">
@stop
@section('content')
    <div class="terms-of-use-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="txt-format text-justify">
                        <h1>Chấp Thuận Điều Khoản & Điều Kiện</h1>
                            <p>Vui lòng đọc kỹ các <strong>Điều Khoản &amp; Điều Kiện</strong> này
                                trước khi truy nhập và sử dụng dịch vụ trên website đăng tuyển dụng Inplace.vn (<em>sau
                                    đây gọi là “Website www.inplace.vn”</em>) của Công ty TNHH Inplace Việt Nam (sau đây
                                gọi là “Công ty”). Bằng việc truy cập và sử dụng Website www.inplace.vn, bạn (sau đây có
                                thể được gọi là “bạn” hoặc “Người sử dụng”, trừ phi ngữ cảnh yêu cầu khác đi) chấp thuận
                                Điều Khoản &amp; Điều Kiện này và đồng ý bị ràng buộc bởi các quy định về sử dụng Dịch
                                vụ trên Website www.Inplace.vn. Nếu bạn có bất kỳ câu hỏi nào về bản thỏa thuận này, vui
                                lòng liên hệ chúng tôi qua email <a href="mailto:services@inplace.vn">services@inplace.vn</a>
                                hoặc hotline được cung cấp.</p>
                            <h2>1. Dịch Vụ Của Inplace</h2>
                            <p>Inplace.vn là một trang Web trên mạng Internet được thiết kế cho phép những người sử dụng
                                đăng các thông tin về bản thân doanh nghiệp, bản thân cá nhân, các thông tin chia sẻ hợp
                                pháp từ các mạng xã hội hoặc tin tức khác, đăng thông báo tuyển dụng và/hoặc xem các
                                công việc do những người sử dụng khác đăng lên, hoặc tương tác với những người sử dụng
                                khác. Inplace.vn do Công ty sở hữu và điều hành. Website www.inplace.vn chứa và có thể
                                chứa các thông tin, tin tức, các ý kiến, văn bản, đồ hoạ, các liên kết, sản phẩm nghệ
                                thuật điện tử, hình ảnh động, âm thanh, video, phần mềm, tranh ảnh, âm nhạc, tiếng động
                                và các nội dung, dữ liệu khác (gọi chung là “nội dung”) được định dạng, tổ chức và thu
                                thập dưới nhiều hình thức khác nhau mà người sử dụng có thể truy nhập tới được, gồm các
                                thư mục, cơ sở dữ liệu và các vùng trên website của Inplace.vn mà người sử dụng có thể
                                thay đổi được, chẳng hạn như đăng quảng cáo tuyển dụng, tải lên các tệp đa phương tiện,
                                đăng ký các hồ sơ người sử dụng và tạo các hồ sơ tự thông báo (“vùng tương tác”).</p>
                            <p>Để có thể sử dụng đầy đủ tiện ích dịch vụ trên Website www.inplace.vn, bạn cần phải đăng
                                ký tạo lập một tài khoản sử dụng và cung cấp Website www.inplace.vn một số thông tin cá
                                nhân nhất định bao gồm mà không giới hạn bởi địa chỉ email, số điện thoại để phục vụ cho
                                việc liên lạc giao tiếp giữa bạn và Website www.inplace.vn cũng như những người sử dụng
                                khác sau này. Bằng việc đăng ký này, bạn đồng ý nhận các Thư thông báo, các thư điện tử,
                                cuộc gọi hoặc các hình thức truyền thông khác về các sản phẩm và dịch vụ của Website
                                www.inplace.vn. Bất cứ khi nào bạn không mong muốn nhận các thư điện tử này nữa, thì bạn
                                hãy thông báo cho Website www.inplace.vn bằng cách chọn chức năng hủy đăng ký có sẵn
                                trên Website www.inplace.vn hoặc tại bên dưới của các thư điện tử của Website
                                www.inplace.vn. Các thông tin cá nhân của bạn sẽ được bảo quản và xử lý một cách bảo mật
                                theo Chính Sách Bảo Mật của Website www.inplace.vn.</p>
                            <p>Bạn có mua các dịch vụ được niêm yết tại Website www.inplace.vn hoặc được cung cấp bởi
                                công ty và việc đặt mua này của bạn sẽ tùy thuộc vào chấp thuận một phần hoặc toàn bộ
                                điều kiện sử dụng của Website www.inplace.vn. Việc chấp thuận cung cấp dịch vụ tại
                                Website www.inplace.vn chỉ có hiệu lực ràng buộc khi bạn đã thanh toán hoặc các thông
                                tin về thanh toán của bạn đã được xác nhận.</p>
                            <p>Công ty bảo lưu quyền thay đổi các dịch vụ, biểu giá và phương thức tính giá và các thay
                                đổi khác của Website www.inplace.vn theo thời gian mà không phải thông báo trước cho
                                người sử dụng nếu thấy phù hợp.</p>
                            <p>Công ty bảo lưu quyền từ chối cung cấp dịch vụ cho các cá nhân, tổ chức, mà theo quy định
                                của pháp luật hoặc theo toàn quyền đánh giá của Công ty:</p>
                            <ol start="1" type="1">
                                <li>Có hoạt động kinh doanh cùng lĩnh vực, nghành nghề với Công ty, bao gồm nhưng không
                                    giới hạn, các hoạt động kinh doanh dịch vụ việc làm thông qua website, ứng dụng
                                    thương mại điện tử bán hàng và/hoặc website, ứng dụng cung cấp dịch vụ thương mại
                                    điện tử (sàn giao dịch thương mại điện tử); và/hoặc
                                </li>
                                <li>Vận hành, quản lý website, ứng dụng liên quan đến tuyển dụng, dịch vụ việc làm tương
                                    tự nền tảng website, ứng dụng của Công ty nhằm mục đích kinh doanh, thu lợi nhuận;
                                    và/hoặc
                                </li>
                                <li>Khai thác, sử dụng các thông tin được cung cấp bởi dịch vụ của Công ty không nhằm
                                    phục vụ cho mục đích tuyển dụng cho chính cá nhân, tổ chức đó; và/hoặc
                                </li>
                                <li>Cung cấp các sản phẩm, dịch vụ mang tính chất cạnh tranh với các dịch vụ hiện có của
                                    Inplace.
                                </li>
                                <li>Cung cấp các sản phẩm, dịch vụ không được cho phép của pháp luật Việt Nam.</li>
                            </ol>
                            <p>Trường hợp, Công ty đã cung cấp dịch vụ cho bạn và/hoặc công ty bạn nhưng sau đó phát
                                hiện ra bạn và/hoặc công ty bạn thuộc một trong các đối tượng mà Công ty từ chối cung
                                cấp dịch vụ nêu trên, thì tùy theo quyết định của Công ty sẽ tiến hành:</p>
                            <ol>
                                <li>Chấm dứt cung cấp dịch vụ và thông báo cho Người sử dụng,</li>
                                <li>Gỡ bỏ toàn bộ tin đăng tuyển (nếu có),</li>
                                <li>Gỡ bỏ, vô hiệu hóa tài khoản sử dụng Website www.inplace.vn nếu xét thấy cần
                                    thiết,
                                </li>
                                <li>Cung cấp các sản phẩm, dịch vụ mang tính chất cạnh tranh với các dịch vụ hiện có của
                                    Inplace.
                                </li>
                                <li>Hoàn trả lại khoản tiền mà bạn đã thanh toán (nếu có) tương ứng với khối lượng dịch
                                    vụ.
                                </li>
                            </ol>
                            <h2>2. Các loại hình dịch vụ và Thanh Toán</h2>
                            <p>Công ty cung cấp các loại hình dịch vụ như sau cho người sử dụng là doanh nghiệp. Trước
                                hết, muốn sử dụng các dịch vụ dành cho doanh nghiệp, người sử dụng cần tạo trang Doanh
                                Nghiệp và tài khoản quản lý trang doanh nghiệp tại website <a
                                    href="http://www.inplace.vn">www.inplace.vn</a>.</p>
                            <h3>2.1 Dịch vụ Hỗ Trợ Phát Triển Thương Hiệu Nhà Tuyển Dụng</h3>
                            <p>Ngay sau khi ký hợp đồng dịch vụ, Công Ty sẽ kích hoạt dịch vụ và gửi đề nghị thanh toán
                                và hoá đơn cho Người sử dụng. Người sử dụng có nghĩa vụ thang toán 100% phí dịch vụ này
                                trong vòng 7 ngày khi nhận được đề nghị thanh toán và hoá đơn trên.</p>
                            <p>Dịch vụ Hỗ Trợ Phát Triển Thương Hiệu Nhà Tuyển Dụng được chia ra các gói với mức giá
                                khác nhau tương đương với các quyền lợi được cấp cho tài khoản Người sử dụng.</p>
                            <p>Đối với gói TRIAL, Người sử dụng có thể sử dụng hữu hạn các tính năng của Website ở mức
                                chi phí là 0 đồng. Các người sử dụng sử dụng gói này chấp nhận rằng Công Ty có thể chấm
                                dứt cung cấp dịch vụ hay thay đổi các quyền lợi của gói này ở bất cứ thời điểm nào mà
                                không cần báo trước cho chủ tài khoản đó.</p>
                            <p>Đối với gói dịch vụ khác gói TRIAL, Công Ty sẽ có nghĩa vụ đảm bảo các quyền lợi như đã
                                quy định trong Hợp Đồng và các Phụ Lục còn hiệu lực &nbsp;cho tới khi gói dịch vụ hết
                                hạn. Trong trường hợp bất khả kháng Công Ty buộc phải chấm dứt một dịch vụ nào đó trước
                                hạn, Công Ty có nghĩa vụ đền bù chi phí tương ứng với dịch vụ bị cắt bỏ trước hạn đó.
                                Người sử dụng và Công Ty sẽ thoả thuận bằng văn bản về việc đền bù này.</p>
                            <p>Đơn giá các gói dịch vụ được hiển thị dưới dạng giá trung bình theo tháng, Người sử dụng
                                có hai phương án lựa chọn là trả trước 6 tháng (mức tối thiểu) hoặc trả trước 1 năm (mức
                                tối đa). Thời hạn hiệu lực của dịch vụ chính là số tháng Người sử dụng thanh toán trước
                                để sử dụng dịch vụ và được bắt đầu tính kể từ ngày kích hoạt dịch vụ.</p>
                            <p>Khi đang sử dụng một gói dịch vụ và người sử dụng doanh nghiệp có nhu cầu mua thêm các
                                dịch vụ phụ trợ mà không muốn nâng cấp đổi gói, Công Ty có thể đồng ý bán thêm các dịch
                                vụ lẻ theo bảng giá công bố tại thời điểm mua. Tuy nhiên các dịch vụ tăng thêm này chỉ
                                có thời hạn tới khi gói dịch vụ chính đang sử dụng của doanh nghiệp hết hạn.</p>
                            <p>Mức chiết khấu, giảm giá hay khuyến mại dịch vụ (nếu có) sẽ không được coi là mặc định và
                                không tự động gia hạn theo hợp đồng.</p>
                            <p>Công Ty sẽ thông báo cho Người sử dụng về thời điểm hết hạn hợp đồng tối thiểu 15 ngày
                                trước ngày hết hạn, Người sử dụng đưa ra quyết định gia hạn hay không gia hạn trước ngày
                                cuối cùng còn hiệu lực của hợp đồng. Nếu quá thời hạn này, Người sử dụng không đưa ra
                                quyết định thì hợp đồng được hiểu là sẽ tự động gia hạn thành hợp đồng mới có thời gian
                                tương đương. Công Ty sau đó sẽ gửi Đề nghị thanh toán cho hợp đồng gia hạn tới Người sử
                                dụng và Người sử dụng có nghĩa vụ thanh toán trong vòng 7 ngày làm việc.</p>
                            <h3>2.2 Dịch vụ Hỗ Trợ Tuyển Dụng</h3>
                            <p>Người sử dụng sẽ trả phí cho dịch vụ này sau khi có kết quả tuyển dụng thành công được
                                ghi nhận thông qua hệ thống website www.inplace.vn. Công Ty sẽ gửi đề nghị thanh toán
                                hoặc hoá đơn cho Người sử dụng sau khi ghi nhận trường hợp tuyển dụng thành công. Người
                                sử dụng có nghĩa vụ thanh toán số tiền phí tuyển dụng trong vòng 7 ngày làm việc sau khi
                                nhận được đề nghị thanh toán hoặc hoá đơn.</p>
                            <p>Thông qua cơ chế vận hành của website www.inplace.vn, Người sử dụng tuyển được nhân sự
                                phù hợp cho vị trí công việc của mình, Người sử dụng đồng ý trả cho Công Ty một khoản
                                phí tương đương với 01 tháng lương trước thuế của vị trí tuyển dụng đã niêm yết nhưng
                                không quá 30,000,000 VND (bằng chữ: Ba mươi triệu đồng) cho một vị trí.</p>
                            <p>Mức chiết khấu, giảm giá hay khuyến mại dịch vụ (nếu có) sẽ không được coi là mặc định và
                                không tự động gia hạn.</p>
                            <p>Công Ty sẽ căn cứ trên bản mô tả công việc và mức lương mà Người sử dụng đã thoả thuận
                                với ứng viên thành công để tính phí dịch vụ tương ứng và gửi đề nghị thanh toán hoặc hoá
                                đơn cho Người sử dụng trong vòng 7 ngày sau khi mỗi trường hợp tuyển dụng được ghi nhận
                                thành công. Người sử dụng có nghĩa vụ thanh toán trong vòng 7 ngày sau khi nhận được hoá
                                đơn.</p>
                            <p>Thời hạn bảo hành dịch vụ là 60 ngày kể từ ngày ứng viên bắt đầu đi làm. Trong thời hạn
                                này nếu vì bất cứ lí do gì đó ứng viên không tới làm việc hoặc nghỉ việc do không đáp
                                ứng được yêu cầu công việc, Công ty sẽ hoàn trả tối đa là 50% phí dịch vụ đã thanh toán.
                                Nếu quá thời hạn này, Người sử dụng đồng ý rằng Công Ty sẽ không phải hoàn trả bất cứ
                                khoản phí nào.</p>
                            <h2>3. Điều Khoản Về Sử Dụng Dịch Vụ</h2>
                            <p>Đối với dịch vụ Hỗ Trợ EB, sau khi người sử dụng đã thanh toán Phí Dịch vụ và/hoặc kích
                                hoạt các Dịch vụ đã đặt mua, chúng tôi sẽ không chấp nhận bất kỳ yêu cầu tạm ngưng hoặc
                                hủy bỏ các Dịch vụ đã đặt mua, hoặc hoàn Phí Dịch vụ, hoặc thay đổi gói dịch vụ có giá
                                trị thấp hơn.</p>
                            <p>Bạn sẽ phải tự tạo một tài khoản với mật khẩu để sử dụng dịch vụ trên Website
                                www.inplace.vn.</p>
                            <p>Bạn được quyền đăng tải nội dung thông tin của mình và/hoặc đường dẫn trên Website
                                www.inplace.vn đến nội dung thông tin tuyển dụng tại trang web của mình và hoàn toàn
                                chịu trách nhiệm về tính chính xác và hợp pháp của các nội dung này.</p>
                            <p>Mỗi gói dịch vụ EB đều có hạn sử dụng (tối thiểu là 6 tháng và đối đa là 1 năm). Khi hết
                                thời hạn sử dụng, bất kỳ Dịch vụ đã mua nhưng chưa được kích hoạt sử dụng sẽ không còn
                                giá trị.</p>
                            <p>Trường hợp do lỗi cố ý của chúng tôi mà sau 7 (bảy) ngày làm việc kể từ ngày bạn có thông
                                báo nhắc nhở bằng văn bản về vi phạm mà chúng tôi vẫn chưa tiến hành khắc phục sai phạm
                                này, chúng tôi sẽ hoàn trả cho bạn Phí Dịch vụ tương ứng với Dịch vụ chưa sử dụng theo
                                Đơn đặt hàng/Hợp đồng Dịch vụ mà chúng tôi không cung cấp theo đúng thỏa thuận giữa hai
                                bên.</p>
                            <h2>4. Quyền Và Trách Nhiệm Của Người Sử Dụng</h2>
                            <h3>4.1 Truy cập Website www.inplace.vn </h3>
                            <p>Bạn có trách nhiệm tự cung cấp tất cả phần cứng, phần mềm, số điện thoại hoặc các thiết
                                bị liên lạc khác và/hoặc dịch vụ kết nối tới Internet và truy cập Website
                                www.inplace.vn, đồng thời có trách nhiệm trả mọi khoản phí truy cập Internet, phí điện
                                thoại hoặc các khoản phí khác phát sinh trong quá trình kết nối Internet và truy cập
                                Website www.inplace.vn.</p>
                            <h3>4.2 Đạo đức người sử dụng</h3>
                            <p>Khi sử dụng Dịch vụ của Inplace và truy cập vào Website www.inplace.vn, bạn nhận thức đầy
                                đủ các điều cấm sau:</p>
                            <ol start="1" type="1">
                                <li>Chống Nhà nước Cộng hoà xã hội chủ nghĩa Việt Nam, phá hoại khối đoàn kết toàn dân,
                                    tuyên truyền, xuyên tạc, kích động hoặc cung cấp thông chống phá Nhà nước Việt Nam;
                                </li>
                                <li>Kích động bạo lực, tuyên truyền chiến tranh xâm lược, gây hận thù giữa các dân tộc
                                    và nhân dân các nước, kích động dâm ô, đồi trụy, tội ác, tệ nạn xã hội, mê tín dị
                                    đoan, phá hoại thuần phong mỹ tục của dân tộc;
                                </li>
                                <li>Tiết lộ bí mật nhà nước, bí mật quân sự, an ninh, kinh tế, đối ngoại và những bí mật
                                    khác đã được pháp luật quy định;
                                </li>
                                <li>Xuyên tạc, vu khống, xúc phạm uy tín của tổ chức, danh dự, nhân phẩm, uy tín của
                                    công dân;
                                </li>
                                <li>Quảng cáo, tuyên truyền hàng hoá, dịch vụ thuộc danh mục cấm đã được pháp luật quy
                                    định;
                                </li>
                                <li>Đề cập đến các vấn đề chính trị và tôn giáo;</li>
                                <li>Sử dụng các từ ngữ vô văn hóa vi phạm truyền thống đạo đức của Việt Nam;</li>
                                <li>Hạn chế hoặc ngăn cản người sử dụng khác sử dụng và hưởng các tính năng tương tác;
                                </li>
                                <li>Gửi hoặc chuyển các thông tin bất hợp pháp, đe doạ, lạm dụng, bôi nhọ, nói xấu,
                                    khiêu dâm, phi thẩm mỹ, xúc phạm hoặc bất kỳ loại thông tin không đứng đắn nào, bao
                                    gồm nhưng không hạn chế việc truyền bá tin tức góp phần hay khuyến khích hành vi
                                    phạm tội, gây ra trách nhiệm pháp lý dân sự hoặc vi phạm pháp luật đia phương, quốc
                                    gia hoặc quốc tế;
                                </li>
                                <li>Gửi hay chuyển các thông tin, phần mềm, hoặc các tài liệu khác bất kỳ, vi phạm hoặc
                                    xâm phạm các quyền của những người khác, trong đó bao gồm cả tài liệu xâm phạm đến
                                    quyền riêng tư hoặc công khai, hoặc tài liệu được bảo vệ bản quyền, tên thương mại
                                    hoặc quyền sở hữu khác, hoặc các sản phẩm phái sinh mà không được sự cho phép của
                                    người chủ sở hữu hoặc người có quyền hợp pháp;
                                </li>
                                <li>Gửi hoặc chuyển thông tin, phần mềm hoặc tài liệu bất kỳ có chứa virus hoặc một
                                    thành phần gây hại khác;
                                </li>
                                <li>Thay đổi, làm hư hại hoặc xóa nội dung bất kỳ hoặc các phương tiện khác mà không
                                    phải là nội dung thuộc sở hữu của bạn, hoặc gây trợ ngại cho những người khác truy
                                    cập đến Website www.inplace.vn;
                                </li>
                                <li>Phá vỡ luồng thông tin bình thường trong một vùng tương tác;</li>
                                <li>Tuyên bố có liên hệ với hay phát ngôn cho một doanh nghiệp, hiệp hội, thể chế hay tổ
                                    chức nào khác mà bạn không được uỷ quyền tuyên bố mối liên hệ đó;
                                </li>
                                <li>Thực hiện tấn công mạng, khủng bố mạng, gián điệp mạng, tội phạm mạng; gây sự cố,
                                    tấn công, xâm nhập, chiếm quyền điều khiển, làm sai lệch, gián đoạn, ngưng trệ, tê
                                    liệt hoặc phá hoại hệ thống thông tin;
                                </li>
                                <li>Sử dụng Website www.inplace.vn cho những mục đích phi pháp, bao gồm nhưng không giới
                                    hạn hành vi vi phạm một quy tắc, chính sách hay hướng dẫn sử dụng nào của nhà cung
                                    cấp dịch vụ Internet cho bạn hay các dịch vụ trực tuyến.
                                </li>
                            </ol>
                            <h3>4.3 Sử dụng dịch vụ</h3>
                            <p>Để sử dụng dịch vụ, Người sử dụng sẽ tự tạo một tài khoản với mật khẩu để sử dụng
                                dịch vụ trên trang web www.inplace.vn. Người sử dụng có trách nhiệm bảo mật mật
                                khẩu và chỉ sử dụng tài khoản và mật khẩu của mình cho mục đích tuyển
                                dụng của chính mình. Công ty được miễn trừ trách nhiệm bảo mật mật khẩu tài khoản,
                                bảo quản tài khoản cũng như kích hoạt sử dụng dịch vụ cho Người sử dụng. Người sử dụng
                                không được tiết lộ, chia sẻ, hoặc bán các thông tin về tài khoản người dùng
                                và mật khẩu này cho bất kỳ bên thứ ba nào khác mà không được sự đồng ý
                                trước bằng văn bản của Công Ty.</p>
                            <p>Sau khi Người sử dụng là doanh nghiệp ký kết Hợp đồng dịch vụ hoặc Đơn Đặt Hàng và/hoặc
                                Công ty đã kích hoạt Đơn Đặt Hàng, thì Người sử dụng đó phải hoàn toàn chịu trách nhiệm
                                về việc sử dụng dịch vụ của mình. Nếu người sử dụng doanh nghiệp có nhu cầu thay đổi
                                loại gói dịch vụ hoặc mua thêm các dịch vụ gia tăng, người sử dụng cần phải có sự đồng ý
                                bằng văn bản của Công Ty và ký phụ lục sửa đổi bổ sung và thanh toán cho sự thay đổi đó
                                nếu có trước khi sự thay đổi này được thực hiện. Công ty có quyền từ chối yêu cầu thay
                                đổi của người sử dụng doanh nghiệp trong trường hợp này.</p>
                            <p>Người sử dụng &nbsp;doanh nghiệp có quyền đăng tải nội dung thông tin về doanh nghiệp,
                                hoạt động, văn hoá, thành viên, hình ảnh, bài viết, video và các nội dung khác liên quan
                                trên trang doanh nghiệp của mình trên website www.inplace.vn. Người sử dụng sẽ hoàn
                                toàn chịu trách nhiệm về tính chính xác và hợp pháp của các nội dung thông
                                tin do mình đăng tải hoặc các nội dung thông tin mà đường dẫn của mình dẫn
                                tới. Inplace có quyền xoá hoặc chỉnh sửa bất cứ nội dung nào nếu nội dung đó không phù
                                hợp với các Điều Kiện và Điều Khoản sử dụng do công ty đề ra liên quan tới Quản Lý Nội
                                Dung. Mọi sự thay đổi trên sẽ được thông báo tới Người sử dụng doanh nghiệp được rõ.</p>
                            <p>Người sử dụng chỉ được sử dụng khai thác thông tin tại website cho mục đích Tuyển
                                Dụng và Xây Dựng Thương Hiệu Nhà Tuyển Dụng (EB) của mình. Người sử dụng không được
                                tiết lộ, bán các thông tin về ứng viên mà mình thu thập được từ việc sử
                                dụng dịch vụ cho bất kỳ một bên thứ ba nào khác dưới mọi hình thức mà
                                không được sự đồng ý trước bằng văn bản của Công Ty.</p>
                            <p>Trường hợp Người sử dụng vi phạm các quy định trên tại điều này thì Công Ty có
                                quyền gỡ bỏ mọi thông tin hoặc đường dẫn trang doanh nghiệp của Người sử dụng
                                và chấm dứt dịch vụ mà không phải trả lại số tiền dịch vụ mà Người sử dụng
                                doanh nghiệp đã thanh toán nhưng chưa sử dụng hết, đồng thời thông báo việc vi
                                phạm với cơ quan nhà nước có thẩm quyền và buộc Người sử dụng bồi thường
                                thiệt hại phát sinh từ vi phạm của Người sử dụng.</p>
                            <h3>4.4 Quy định về nội dung</h3><h4>4.4.1 Đăng việc làm (tuyển dụng)</h4>
                            <p>&nbsp;Khi Người sử dụng đăng tuyển dụng cho một chức vụ, Tiêu đề chức vụ hay Tiêu đề đăng
                                tuyển này sẽ được hiển thị trong trang kết quả tìm kiếm, trang chủ Việc Làm, hay trong
                                trang doanh nghiệp phần Việc Làm (tùy theo dịch vụ Người sử dụng sử dụng) trên trang
                                doanh nghiệp của Công ty, Tiêu đề đăng tuyển này sẽ dẫn tới trang nội dung chi tiết liên
                                quan đến chức vụ đó.</p>
                            <p>Nội dung đăng tuyển chi tiết bao gồm (i) Phần giới thiệu công ty và thành viên sẽ nhập
                                nội dung có sẵn từ trang chủ doanh nghiệp, (ii) tên chức vụ cần tuyển dụng (Tiêu Đề
                                Đăng Tuyển), (iii) mô tả công việc cho chức vụ đó, (iv) yêu cầu về tiêu chuẩn đối
                                với ứng viên cho chức vụ đó (có thể kèm theo các bài trắc nghiệp cho vị trí đó nếu có)
                                và (v) hình ảnh hay video minh hoạ cho tin đăng tuyển.</p>
                            <p>Nội Dung Đăng Tuyển chỉ được phép đề cập đến 1 chức vụ duy nhất là Tiêu Đề Đăng Tuyển.
                                Người sử dụng không được đề cập đến bất kỳ chức vụ nào khác trong bất kỳ phần nào trong
                                Nội Dung Đăng Tuyển. Người sử dụng cũng không được đề cập đến các cấp bậc khác nhau liên
                                quan đến chức vụ đăng tuyển trong cùng Nội Dung Đăng Tuyển.</p>
                            <p>Sau khi đăng tuyển thành công, Người sử dụng sẽ không được thay đổi tên chức vụ đăng
                                tuyển (Tiêu Đề Đăng Tuyển).</p>
                            <p>Số lượng việc làm được đăng tuyển sẽ tương ứng với loại gói dịch vụ Người sử dụng đăng ký
                                sử dụng, nếu như đã sử dụng hết số việc làm đã cấp nhưng Người sử dụng không có nhu cầu
                                thay đổi gói dịch vụ khác, Người sử dụng sẽ phải liên hệ với Công Ty để đăng ký mua thêm
                                dịch vụ gia tăng theo quy định. Dịch vụ gia tăng sẽ có thời hạn tới khi gói dịch vụ hiện
                                hành hết hạn.</p><h4>4.4.2 Đăng bài viết</h4>
                            <p>Bài viết của Người sử dụng hiển thị ở danh sách trang tin tức chung (từ mục Khám Phá) hay
                                chuyện mục Bài Viết trong trang chủ của doanh nghiệp.</p>
                            <p>Một bài viết bao gồm: Tiêu đề bài viết, nội dung bài viết, hình ảnh hay video minh hoạ,
                                từ khoá mô tả tính chất của bài viết.</p>
                            <p>Mỗi bài viết khi được hiển thị sẽ có các tính năng để người dùng tương tác như Thích,
                                Bình Luận, Chia sẻ.</p>
                            <p>Nội dung các bài viết không được đi ngược với quy định quản lý nội dung tại website
                                www.inplace.vn. Công ty có quyền chỉnh sửa, xóa hoặc biên tập trang con trong trang web
                                của Công ty trên cơ sở thông tin do Người sử dụng cung cấp vào bất kỳ thời điểm mà không
                                cần có sự chấp thuận trước của Người sử dụng nhưng sẽ thông báo cho người sử dụng được
                                rõ.</p>
                            <p>Nếu Người sử dụng mua gói dịch vụ có bao gồm hoặc đăng ký dịch vụ hỗ trợ Viết Bài phát
                                triển nội dung làm Thương hiệu nhà tuyển dụng (EB), Công ty sẽ cử đại diện của mình tới
                                tìm hiểu thông tin để viết bài cho doanh nghiệp. Người sử dụng phải có nghĩa vụ hợp tác
                                và tạo điều kiện để đại diện của Công Ty hoàn thành nhiệm vụ. Người sử dụng có quyền đưa
                                ra yêu cầu chỉnh sửa tối đa 3 lần cho mỗi nội dung bài viết đưa ra bởi Công Ty. Công ty
                                và Người sử dụng sẽ cùng thảo luận và đưa ra kế hoạch thực hiện và hoàn thiện các Bài
                                Viết và nội dung phát triển Thương Hiệu Nhà Tuyển Dụng trước khi tiến hành dịch vụ.</p>
                            <h4>4.4.3 Hình ảnh và Video</h4>
                            <p>Hình ảnh hay Video đáp ứng yêu cầu về chất lượng theo thông số kích thước đưa ra bởi
                                website.</p>
                            <p>Hình ảnh hay Video không vi phạm các yếu tố như thuần phong mỹ tục, không mang yếu tố
                                chính trị hay trái với quy định của pháp luật Việt Nam.</p>
                            <p>Công ty sẽ có quyền chỉnh sửa, xóa hoặc biên tập trang con trong trang web của Công ty
                                trên cơ sở thông tin do Người sử dụng cung cấp vào bất kỳ thời điểm mà không cần có sự
                                chấp thuận trước của Người sử dụng.</p><h4>4.4.4 Bài trắc nghiệm (Quiz)</h4>
                            <p>Các bài trắc nghiệm có tác dụng giúp Người sử dụng sàng lọc được các ứng viên phù hợp với
                                môi trường văn hoá công ty cũng như yêu cầu của công việc đang có nhu cầu tuyển dụng,
                                việc tạo và đăng các bài trắc nghiệm cần tuần theo các nguyên tắc sau đây:</p>
                            <p>Người sử dụng hoàn toàn chịu trách nhiệm về nội dung vài trắc nghiệm do mình tạo ra.</p>
                            <p>Người sử dụng tự chịu trách nhiệm tổng hợp và phân tích kết quả các bài trắc nhiệm nhận
                                được.</p>
                            <p>Công Ty có quyền khởi tạo, cung cấp hay chấm dứt tính năng Bài Trắc Nghiệm trên website
                                bất cứ lúc nào và không cần báo trước cho Người sử dụng về điều này.</p><h4>4.4.5 Đăng
                                ký tài khoản số lượng lớn và Tự động. </h4>
                            <p>Các tài khoản được đăng ký một cách tự động và/hoặc có hệ thống với số lượng lớn, theo
                                toàn quyền đánh giá của Công ty, được xem là vi phạm và sẽ áp dụng các xử lý vi phạm
                                theo quy định tại Điều Khoản và Điều Kiện này.</p><h4>4.4.6 Hoạt động không đúng mục
                                đích.&nbsp;</h4>
                            <p>Bất kỳ hành vi lạm dụng và/hoặc sử dụng Website www.inplace.vn sai lệch khỏi mục đích
                                quảng bá thương hiệu và tuyển dụng, tìm kiếm cơ hội việc làm, theo toàn quyền đánh giá
                                bởi Công ty, sẽ có thể được xem là vi phạm và sẽ bị áp dụng các xử lý vi phạm theo quy
                                định tại Điều Khoản và Điều Kiện này.</p><h4>4.4.7 Quấy rầy người dùng khác. </h4>
                            <p>Công ty mong muốn tạo môi trường tự do và lành mạnh cho hoạt động tuyển dụng và tìm kiếm
                                việc làm của tất cả người dùng. Khi sử dụng Website www.inplace.vn, bạn cần cân nhắc cẩn
                                thận các nội dung do mình đưa lên vùng tương tác và các phản hồi đối với người dùng
                                khác. Nếu Công ty nhận được phản ánh về bất kỳ hành vi quấy rầy nào (bao gồm nhưng không
                                giới hạn các hành vi đe dọa, đưa ra các nội dung trống rỗng hoặc chuỗi ký tự/từ ngẫu
                                nhiên không có nghĩa và/hoặc không liên quan đến các nội dung tương tác hợp lý, gửi thư
                                rác, sử dụng các hình thức lừa dối nhằm lôi kéo sự chú ý hoặc nhằm mục đích tiếp thị
                                thương mại) làm ảnh hưởng đến việc sử dụng dịch vụ, cản trở khả năng tiếp cận thông tin,
                                đem lại sự không thoải mái cho người dùng khác, Công ty, ngay lập tức, có quyền tạm dừng
                                cung cấp dịch vụ mà không cần thông báo và/hoặc hoàn lại tiền đối với người dùng bị báo
                                cáo sai phạm để điều tra và xử lý theo quy định tại Điều Khoản và Điều Kiện này.</p>
                            <h2>5. Các Vùng Tương Tác</h2>
                            <p>Bạn thừa nhận, Website www.inplace.vn có thể chứa các vùng tương tác khác nhau, bao gồm
                                nhưng không hạn chế với các công việc. Những vùng tương tác này cho phép phản hồi đến
                                Website www.inplace.vn và tương tác thời gian thực giữa những người sử dụng. Bạn cũng
                                hiểu rằng, Website www.inplace.vn không kiểm soát các thông báo, thông tin hoặc các tệp
                                được phân phối tới các vùng tương tác như vậy và rằng, Website www.inplace.vn có thể cho
                                bạn và những người sử dụng khác khả năng tạo và quản lý một vùng tương tác. Tuy nhiên,
                                Website www.inplace.vn, công ty mẹ, hoặc các chi nhánh, cũng như các giám đốc, nhân
                                viên, những người làm thuê và các đại lý tương ứng không chịu trách nhiệm về nội dung
                                trong vùng tương tác bất kỳ.</p>
                            <p>Việc sử dụng và/hoặc quản lý một vùng tương tác của bạn sẽ bị chi phối bởi Bản thoả thuận
                                này và các quy tắc bổ sung bất kỳ, hoặc bởi các thủ tục hoạt động của vùng tương tác bất
                                kỳ do bạn hay người sử dụng khác thiết lập. Bạn công nhận rằng, Website www.inplace.vn
                                không thể và không có ý định sàng lọc các thông tin trước. Ngoài ra, vì Website
                                www.inplace.vn khuyến khích liên lạc mở và không thiên vị trong các vùng tương tác nên
                                Website www.inplace.vn không thể xác định trước mức độ chính xác hoặc sự phù hợp đối với
                                bản Thoả thuận này về nội dung bất kỳ được chuyển đi trong vùng tương tác. Website
                                www.inplace.vn không chịu trách nhiệm với việc sàng lọc, lập chính sách, hiệu chỉnh,
                                duyệt hoặc giám sát nội dung bất kỳ trong một vùng tương tác.</p>
                            <p>Mặc dù vậy, bạn cũng đồng ý rằng Website www.inplace.vn có quyền giám sát mọi vùng tương
                                tác, đôi lúc để lộ thông tin nào đó nếu cần thiết theo yêu cầu luật pháp, hoặc yêu cầu
                                khác của chính phủ đối với hoạt động của vùng tương tác, hoặc để tự bảo vệ mình hay
                                những người sử dụng khác. Nếu được thông báo nội dung dẫn ra không phù hợp với bản Thỏa
                                thuận này, Website www.inplace.vn có thể thận trọng điều tra và xác định để loại bỏ,
                                hoặc yêu cầu người sử dụng bỏ nội dung đó. Website www.inplace.vn giữ quyền cấm các hành
                                động, truyền đạt tin tức hoặc nội dung trong phạm vi vùng tương tác, hoặc soạn thảo, từ
                                chối gửi, hoặc loại bỏ nội dung bất kỳ, toàn thể hay từng phần mà với đặc quyền của
                                mình, chúng tôi cho rằng (i) vi phạm các điều khoản tiêu chuẩn lúc đó của bản Thỏa thuận
                                này hoặc chuẩn bất kỳ khác nằm trong chính sách của Inplace.vn vẫn còn hiệu lực vào lúc
                                đó, (ii) bất lợi với các quyền của mọi người sử dụng, của Website www.inplace.vn hoặc
                                các nhóm thứ ba khác, (iii) vi phạm luật hiện hành hoặc (iv) những điều không hay
                                khác.</p>
                            <h2>6. Các Quyền Sở Hữu Trí Tuệ</h2>
                            <h3>6.1 Nội dung Website nghề nghiệp Inplace.vn. </h3>
                            <p>Bạn thừa nhận, Inplace cho phép truy cập đến Nội dung được bảo vệ bản quyền, tên thương
                                mại và các quyền sở hữu khác (kể cả quyền sở hữu trí tuệ) (“Quyền Sở hữu Trí tuệ”), và
                                thừa nhận các quyền sở hữu trí tuệ đó là hợp lệ và được bảo vệ trên mọi phương tiện
                                truyền thông hiện có và sau này, trừ những điểm nêu rõ ràng dưới đây, việc sử dụng nội
                                dung của bạn sẽ được quản lý theo các luật bản quyền và các luật sở hữu trí tuệ hiện
                                hành khác.</p>
                            <p>Bạn đồng ý và thừa nhận rằng tất cả các nội dung trên Inplace Website bao gồm nhưng không
                                giới hạn bởi các sơ yếu lý lịch sẽ thuộc quyền sở hữu trí tuệ duy nhất của Website
                                www.inplace.vn và Inplace và bạn không được quyền thay đổi, sao chép, mô phỏng, truyền,
                                phân phối, công bố, tạo ra các sản phẩm phái sinh, hiển thị hoặc chuyển giao, hoặc khai
                                thác nhằm mục đích thương mại bất kỳ phần nào của nội dung, toàn bộ hay từng phần. Tuy
                                nhiên, bạn có thể (i) tạo một bản sao dưới dạng số hoặc hình thức khác để phần cứng và
                                phần mềm máy tính của bạn có thể truy cập và xem được nội dung, (ii) in một bản sao của
                                từng đoạn nội dung, (iii) tạo và phân phối một số lượng hợp lý các bản sao nội dung,
                                toàn bộ hay từng phần, ở dạng bản in hoặc bản điện tử để dùng nội bộ.</p>
                            <p>Bất kỳ bản sao nội dung được phép nào cũng phải được tái tạo ở dạng không thể biến đổi
                                được các thông tri bất kỳ chứa trong nội dung, chẳng hạn như tất cả các thông tin về
                                Quyền Sở hữu Trí tuệ, và các nguồn thông tin ban đầu cho “website nghề nghiệp
                                Inplace.vn” và địa chỉ mạng (URL) của nó. Bạn sẽ không có bất kỳ Quyền Sở hữu Trí tuệ
                                nào qua việc tải xuống hoặc in nội dung. Bất kể các bản sao hoặc việc sử dụng trái phép
                                nào đối với nội dung nào cho mục đích thương mại sẽ bị coi là vi phạm và bị xử lý theo
                                quy định có liên quan của pháp luật và các điều khoản quy định tại bản thỏa thuận
                                này.</p>
                            <h3>6.2 Nội dung do Người sử dụng cung cấp. </h3>
                            <p>Bạn chỉ có thể tải lên vùng tương tác bất kỳ hoặc truyền, gửi, công bố, mô phỏng hoặc
                                phân phối trên hoặc thông qua Website www.inplace.vn phần nội dung, không phụ thuộc vào
                                bất kỳ Quyền Sở hữu Trí tuệ nào, hoặc nội dung mà người giữ Quyền Sở hữu Trí tuệ có sự
                                ủy quyền rõ ràng về việc phân tán trên Internet và trên Website www.inplace.vn mà không
                                có hạn chế gì. Mọi nội dung được đưa ra với sự đồng ý của người sở hữu bản quyền không
                                phải là bạn phải kèm theo câu như “do [tên người chủ sở hữu] sở hữu bản quyền; được dùng
                                theo ủy quyền”. Với việc đưa nội dung lên vùng tương tác bất kỳ, bạn tự động chấp nhận
                                và/hoặc cam đoan rằng, chủ sở hữu của nội dung đó, hoặc là bạn, hoặc là nhóm thứ ba, đã
                                cho Website www.inplace.vn quyền và giấy phép không phải trả tiền bản quyền, lâu dài,
                                không thay đổi, không loại trừ, không hạn chế để sử dụng, mô phỏng, thay đổi, sửa lại,
                                công bố, dịch thuật, tạo các sản phẩm phái sinh, cấp phép con, phân phối, thực hiện và
                                hiển thị nội dung đó, toàn phần hay từng phần, khắp thế giới và/hoặc kết hợp nó với các
                                công việc khác ở dạng bất kỳ, qua các phương tiện truyền thông hoặc công nghệ hiện tại
                                hay sẽ phát triển sau này theo điều khoản đầy đủ của Quyền Sở hữu Trí tuệ bất kỳ trong
                                nội dung đó. Bạn cũng cho phép Website www.inplace.vn cấp giấy phép con cho bên thứ ba
                                quyền không hạn chế để thực hiện bất kỳ quyền nào ở trên với nội dung đó. Bạn cũng cho
                                phép Website www.inplace.vn dùng tên và logo công ty vì các mục đích tiếp thị.</p>
                            <h2>7. Các Liên Kết, Từ Chối Các Bảo Đảm, Giới Hạn Trách Nhiệm</h2>
                            <h3>7.1 Các liên kết. </h3>
                            <p>Bạn hiểu rằng trừ phần nội dung, các sản phẩm và dịch vụ có trên Website www.inplace.vn,
                                Inplace.vn, công ty mẹ, hoặc các chi nhánh, cũng như các giám đốc, nhân viên, người làm
                                công và các đại lý tương ứng kiểm soát, cung cấp không chịu trách nhiệm với nội dung
                                hoặc các dịch vụ của các sites khác trên Internet được kết nối đến hoặc từ Website
                                www.inplace.vn. Tất cả nội dung và các dịch vụ đó đều có thể truy cập được trên Internet
                                bởi bên thứ ba độc lập và không phải là một phần của Website www.inplace.vn hoặc được
                                kiểm soát bởi Inplace.vn. Website www.inplace.vn không xác nhận và cũng không chịu trách
                                nhiệm về tính chính xác, tính đầy đủ, tính hữu dụng, chất lượng và tính sẵn sàng của mọi
                                nội dung hay các dịch vụ có trên các site được kết nối đến hoặc từ Website
                                www.inplace.vn mà đó là trách nhiệm duy nhất của bên thứ ba độc lập đó, và do vậy việc
                                sử dụng của bạn là sự mạo hiểm riêng của bạn. Website www.inplace.vn, công ty mẹ, hoặc
                                các chi nhánh, hoặc các giám đốc, nhân viên, người làm công và các đại lý tương ứng
                                không chịu trách nhiệm pháp lý, trực tiếp hay gián tiếp, với mọi mất mát hay thiệt hại
                                gây ra bởi hoặc bị cho là gây ra bởi việc sử dụng hoặc sự tin cậy của bạn vào mọi nội
                                dung, hàng hóa hoặc các dịch vụ có trên site bất kỳ được kết nối đến hoặc từ Website
                                www.inplace.vn, hoặc do bạn không thể truy cập lên Internet hay site bất kỳ kết nối đến
                                hoặc từ Website www.inplace.vn.</p>
                            <h3>7.2 Từ chối các bảo đảm. </h3>
                            <p>Xin hãy dùng sự suy xét tốt nhất của bạn trong việc đánh giá tất cả các thông tin hoặc
                                các ý kiến được trình bày trên Website www.inplace.vn. Chính sách của Website
                                www.inplace.vn không xác nhận hay phản đối mọi ý kiến do người sử dụng bày tỏ, hoặc nội
                                dung do nhóm độc lập khác cung cấp. Bạn đồng ý rõ ràng rằng việc sử dụng website
                                www.inplace.vn là sự mạo hiểm của riêng bạn. website www.inplace.vn, và công ty mẹ, hoặc
                                các chi nhánh, hoặc các giám đốc, nhân viên, những người làm thuê, các đại lý, các cộng
                                tác viên, hội viên, người cấp phép hoặc các nhà cung cấp khác cung cấp nội dung, dữ
                                liệu, thông tin hoặc các dịch vụ không bảo đảm rằng, website www.inplace.vn hoặc mọi
                                site internet được kết nối đến hoặc từ website www.inplace.vn sẽ không bị ngắt quãng
                                hoặc bị sai sót, rằng các nhược điểm sẽ được sửa chữa, hoặc site này, kể cả các vùng
                                tương tác hoặc máy chủ tạo ra nó không bị nhiễm virus hay các thành phần gây hại khác.
                                và chúng tôi cũng không bảo đảm về các kết quả nhận được từ việc sử dụng website
                                www.inplace.vn hoặc về tính đúng lúc, tính trình tự, tính chính xác, sự ủy quyền, tính
                                đầy đủ, tính hữu dụng, tính bất khả xâm phạm, tính tin cậy, tính sẵn sàng hoặc tính chắc
                                chắn của moi nội dung, thông tin, dịch vụ hoặc giao dịch được cung cấp qua website
                                www.inplace.vn hoặc mọi site được kết nối đến hoặc từ website www.inplace.vn. website
                                www.inplace.vn được cung cấp trên cơ sở “với tư cách là”, “nếu có” mà không có bất kỳ
                                đảm bảo theo hình thức nào, rõ ràng hay ngụ ý, bao gồm nhưng không hạn chế, các vấn đề
                                có tính thương mại hay phù hợp cho một mục đích cụ thể.</p>
                            <h3>7.3 Giới hạn về trách nhiệm pháp lý. </h3>
                            <p>trong bất kỳ trường hợp nào, website www.inplace.vn, công ty mẹ, hoặc các chi nhánh, hoặc
                                các giám đốc, nhân viên, những người làm thuê, các đại lý, các cộng tác viên, hoặc những
                                người cấp phép không chịu trách nhiệm pháp lý với mọi thiệt hại ngẫu nhiên, đặc biệt hay
                                đương nhiên theo hoặc nảy sinh từ bản thỏa thuận này, website www.inplace.vn hoặc mọi
                                site internet được kết nối đến hoặc từ website www.inplace.vn, vì lý do vi phạm hợp
                                đồng, ứng xử sai lầm, sơ xuất hoặc bất kỳ nguyên nhân hành động khác nào, bao gồm nhưng
                                không hạn chế mọi nghĩa vụ pháp lý cho mọi thiệt hại gây bởi hoặc cho là gây bởi mọi
                                thiếu sót trong thực hiện, lỗi bỏ sót, gián đoạn, sự đột biến/hỏng hóc/nhiễu điện, việc
                                xóa, chậm trễ trong hoạt động hoặc chuyển tải, virut máy tính, hỏng đường liên lạc, hỏng
                                thiết bị, lỗi phần mềm, vi phạm, truy cập trái phép, hoặc trộm cắp, phá hoại, thay thế,
                                hoặc sử dụng các bản ghi.</p>
                            <p>Trong bất kỳ trường hợp nào, website www.inplace.vn, công ty mẹ, hoặc các chi nhánh, hoặc
                                các giám đốc, nhân viên, những người làm thuê, các đại lý, các cộng tác viên hoặc những
                                người cấp phép không chịu trách nhiệm pháp lý đối với bạn hoặc mọi nhóm thứ ba khác về
                                mọi quyết định đã ra hoặc hành động mà bạn thực hiện vì tin vào nội dung chứa trong
                                website www.inplace.vn hoặc nội dung chúng đựng trong phạm vi mọi site internet được kết
                                nối với hoặc từ website www.inplace.vn. nội dung trong phạm vi website www.inplace.vn và
                                nội dung trong phạm vi các site internet được kết nối đến hoặc từ website www.inplace.vn
                                có thể có lỗi kỹ thuật, các lỗi không chính xác khác, hoặc các lỗi in.</p>
                            <p>Bạn đồng ý và thừa nhận rõ ràng rằng, website www.inplace.vn không chịu trách nhiệm pháp
                                lý với mọi hành vi nói xấu, tấn công, không trung thực hoặc các hành vi bất hợp pháp
                                khác của người sử dụng bất kỳ. Nếu bạn không bằng lòng với bất kỳ nội dung nào của
                                website www.inplace.vn, hoặc với giao kèo truy cập của website www.inplace.vn, toàn bộ
                                hay từng phần, thì biện pháp duy nhất của bạn là thôi không sử dụng website
                                www.inplace.vn nữa.</p>
                            <h2>8. Tuân Thủ Và Xử Lý Vi Phạm</h2>
                            <p>Bạn phải tuân thủ đúng các quy định tại Đơn đặt hàng, Hợp đồng Dịch vụ, bản Điều khoản
                                &amp; Điều kiện này như là điều kiện tiên quyết cho việc sử dụng Dịch vụ. Trường hợp bạn
                                vi phạm bất kỳ quy định nêu tại Đơn đặt hàng, Hợp đồng Dịch vụ và bản Điều Kiện &amp;
                                Điều Khoản này, thì chúng tôi có quyền gỡ bỏ mọi đăng tuyển, nội dung hoặc đường dẫn của
                                bạn hoặc gỡ bỏ, vô hiệu hóa tài khoản sử dụng Website www.inplace.vn nếu xét thấy cần
                                thiết và tạm dừng và/hoặc chấm dứt việc cung cấp dịch vụ mà không phải trả lại Phí Dịch
                                vụ hoặc số tiền dịch vụ mà bạn đã thanh toán nhưng chưa sử dụng hết. Đồng thời, chúng
                                tôi có quyền buộc bạn bồi thường thiệt hại hoặc mất mát phát sinh từ vi phạm của
                                bạn.</p>
                            <h2>9. Bồi Thường</h2>
                            <p>Bạn đồng ý trả tiền và miễn cho Website www.inplace.vn, công ty mẹ hoặc công ty con và
                                các chi nhánh, các giám đốc, nhân viên, những người làm công và các đại lý của Website
                                www.inplace.vn khỏi các trách nhiệm pháp lý, khiếu kiện và các phí tổn, kể cả các phí
                                hợp lý cho luật sư, nảy sinh từ sự vi phạm bản Thỏa thuận này, từ chính sách bất kỳ
                                khác, từ việc bạn sử dụng hay truy cập Website www.inplace.vn hoặc site internet đựơc
                                kết nối đến hoặc từ Website www.inplace.vn, hoặc về việc truyền nội dung bất kỳ trên
                                Website www.inplace.vn.</p>
                            <h2>10. Bảo Mật</h2>
                            <p>Bạn đồng ý với cam kết bảo mật thông tin liên quan đến Dịch vụ, việc thực hiện giao dịch,
                                và/hoặc bất kỳ thông tin khác thu thập được từ việc thực hiện giao dịch. Bạn sẽ hợp tác
                                ngăn chặn và chống lại hành vi của bất kỳ bên thứ ba nào sao chép và sử dụng trái phép
                                nội dung đăng tuyển hoặc thông tin khác của bạn trên Internet.</p>
                            <p>Bạn không được tiết lộ, chia sẻ hoặc bán các thông tin về ứng viên mà mình thu thập được
                                từ việc sử dụng dịch vụ cho bất kỳ một bên thứ ba nào khác dưới mọi hình thức mà không
                                được sự đồng ý trước bằng văn bản của Công Ty.</p>
                            <p>Ngoài ra, Người sử dụng có trách nhiệm bảo mật tài khoản và mật khẩu, và chỉ sử dụng tài
                                khoản cho mục đích tuyển dụng của mình theo thỏa thuận quy định tại Đơn đặt hàng/Hợp
                                đồng Dịch vụ.</p>
                            <h2>11. Các Vấn Đề Khác</h2>
                            <p>Bản thoả thuận này bao gồm toàn bộ sự thoả thuận giữa Website www.inplace.vn và bạn, và
                                thay thế mọi thoả thuận trước đây về chủ đề này. Website www.inplace.vn có thể sửa đổi
                                bản Thoả thuận này hoặc bất kỳ chính sách khác vào bất cứ lúc nào và tùy từng thời điểm,
                                và sự sửa đổi này sẽ tức thì có hiệu lực ngay khi thông báo về sự sửa đổi đó được công
                                bố ở nơi dễ thấy trên Website www.inplace.vn.</p>
                            <p>Bạn đồng ý xem lại bản Thoả thuận này định kỳ để nhận biết được những điều đã được sửa
                                đổi. Nếu bạn không chấp nhận các sửa đổi này, bạn phải ngưng truy cập Website
                                www.inplace.vn. Sự tiếp tục truy cập của bạn và việc sử dụng Website www.inplace.vn sau
                                thông báo về mọi sửa đổi như vậy sẽ được xem là sự chấp nhận tất cả các sửa đổi như
                                vậy.</p>
                            <p>Nếu điều khoản bất kỳ của bản Thoả thuận này hoặc của mọi chính sách khác không có hiệu
                                lực hoặc không thể thực hiện được, thì phần nội dung đó sẽ được giải thích theo luật
                                hiện hành gần nhất có thể để phản ánh mục đích ban đầu của các bên và các phần còn lại
                                sẽ tiếp tục có đầy đủ hiệu hiệu lực. Việc Website www.inplace.vn không thể đòi hỏi hoặc
                                buộc thực hiện chặt chẽ mọi điều khoản của bản Thoả thuận này sẽ không được coi là sự
                                khước từ điều khoản hay quyền bất kỳ. Bản Thoả thuận này sẽ được điều chỉnh bởi các luật
                                của địa phương hay thành phố sở tại của Website www.inplace.vn, trừ phi có sự mâu thuẫn
                                các quy tắc của các luật, và bạn và Website www.inplace.vn đều phải phục tùng quyền thực
                                thi pháp lý duy nhất của toà án địa phương hay thành phố đó.</p>
                            <p>Bản Thoả thuận này là dành riêng cho bạn và bạn không thể chuyển các quyền của bạn, hoặc
                                nghĩa vụ cho bất kỳ ai. Tất cả các logo, tên chi nhánh, các sản phẩm, tên thương mại hay
                                các nhãn hiệu dịch vụ xuất hiện ở đây có thể là các tên thương mại hay các nhãn dịchvụ
                                của các chủ sở hữu tương ứng của chúng. Các tham chiếu đến tên thương mại bất kỳ, nhãn
                                hiệu dịch vụ và các liên kết đến hoặc từ Website www.inplace.vn đều được thực hiện chặt
                                chẽ để làm rõ và nhận dạng và không cấu thành sự chứng thực của Website www.inplace.vn
                                về các sản phẩm, dịch vụ hoặc thông tin do người chủ sở hữu của tên thương mại, nhãn
                                dịch vụ, hoặc liên kết, đề nghị hoặc sự chứng thực của Website www.inplace.vn bởi người
                                sở hữu các sản phẩm, dịch vụ hoặc liên kết đó.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/helper/termsOfUse.js')}}"></script>
@stop
