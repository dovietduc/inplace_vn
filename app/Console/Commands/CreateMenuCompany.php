<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateMenuCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-menu-company';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create menu company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('menu_enterprise')->truncate();
        $data = [
            [
                'name' => 'Việc làm',
                'route' => 'companies.jobs'
            ],
            [
                'name' => 'Bài viết',
                'route' => 'companies.news'
            ],
            [
                'name' => 'Thành viên',
                'route' => 'companies.member'
            ],
            [
                'name' => 'media',
                'route' => 'companies.media'
            ],
            [
                'name' => 'Văn phòng ảo',
                'route' => 'companies.office'
            ],
            [
                'name' => 'Q&A',
                'route' => 'companies.question'
            ],
            [
                'name' => 'Quiz',
                'route' => 'companies.quiz'
            ]
        ];

        foreach ($data as $dataItem) {
            \DB::table('menu_enterprise')->insert([
                'name' => $dataItem['name'],
                'slug' => str_slug($dataItem['name']),
                'route' => $dataItem['route']
            ]);

        }

    }
}
