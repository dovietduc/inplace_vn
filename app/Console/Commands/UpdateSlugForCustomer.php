<?php

namespace App\Console\Commands;

use App\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class UpdateSlugForCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:slug-for-customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update slug for customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            Customer:: orderBy('id')->chunk(100, function ($customers) {
                foreach ($customers as $customer) {
                    $customer->update(['slug' => str_slug($customer->name) . '-' . $customer->id]);
                }
            });
            DB::commit();
            $this->info('Update slug customer success');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
