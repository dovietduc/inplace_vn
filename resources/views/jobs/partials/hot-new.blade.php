<div class="job-detail_content__hotnews">
    <div class="component-title">
        <h3 class="title">Bài viết nổi bật</h3>
        <div class="desc"></div>
    </div>
    <div class="row">
        @foreach($hotNews as $hotNewItem)
            <div class="col-sm-6 col-md-4">
                <div class="component-posts-item">
                    <a href="{{ route('news.show', ['slugCompany' => optional($hotNewItem->customer)->slug, 'id' => $hotNewItem->id]) }}" class="posts-item-thumb">
                        @if( jjobCheckImageExit($hotNewItem->thumbnail) )
                            <img src="{{ url($hotNewItem->thumbnail) }}" alt=""
                                 class="">
                        @else
                            <img src="{{ asset('images/no_img_203x80.jpg') }}" alt=""
                                 class="">
                        @endif

                    </a>
                    <h3 class="posts-item-title">
                        <a href="{{ route('news.show', ['slugCompany' => optional($hotNewItem->customer)->slug, 'id' => $hotNewItem->id]) }}">
                            {{ $hotNewItem->title }}
                        </a></h3>
                    @if(!empty(optional($hotNewItem->user)))
                        <div class="posts-item-auth">
                            <a href="#" class="avatar">
                                @if( jjobCheckImageExit(optional($hotNewItem->user)->avatar) )
                                    <img src="{{ url(optional($hotNewItem->user)->avatar) }}"
                                         alt="" class="">
                                @else
                                    <img src="{{ asset('images/no_user_35x35.jpg') }}" alt="" class="">
                                @endif

                            </a>
                            <h6 class="name"><a
                                        href="#">{{ optional($hotNewItem->user)->name }}</a>
                            </h6>
                            <div class="desc">Admin</div>
                        </div>
                    @endif

                </div>
            </div>
        @endforeach

    </div>
</div>
