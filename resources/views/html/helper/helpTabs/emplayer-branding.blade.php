<div class="txt-format">
    <h1>Employer Branding</h1>
    <p>Employer Banding (EB) - Thương hiệu nhà tuyển dụng, là một khái niệm đã có từ lâu trên thế giới và tại Việt Nam
        khái niệm này đang dần tạo được chỗ đứng trong giới doanh nghiệp. Hiểu ngắn gọn, đó là quá trình quảng bá một
        công ty, hoặc một tổ chức, với tư cách là một nhà tuyển dụng được yêu thích và được ưu tiên bởi một nhóm nhân sự
        mục tiêu mong muốn. Một công ty nếu muốn thu hút, tuyển dụng và giữ chân người tài hiệu quả thì sẽ cần phải làm
        tốt khía cạnh EB. Như vậy, EB chính là việc làm nổi bật các nét đặc trưng văn hoá của doanh nghiệp, tổ chức để
        từ đó thu hút những nhân sự thực sự phù hợp với văn hoá của tổ chức đó, khiến cho họ yêu quý, ngưỡng mộ, tự hào
        và muốn trở thành một phần của tổ chức đó khi có cơ hội.
        <br><br><strong>Tham khảo thêm các bài viết về Employer branding tại đây:</strong><<br>
        <i class="fal fa-angle-right"></i> <a href="#">5 Lý do vì sao doanh nghiệp nên thực hiện Employer
            Branding</a><br>
        <i class="fal fa-angle-right"></i> <a href="#">Bài học kinh điển về Employer Branding của các ông lớn</a><br>
        <i class="fal fa-angle-right"></i> <a href="#">Tại sao cần có Thương Hiệu Nhà Tuyển Dụng tốt để thu hút nhân tài
            hiệu quả</a>
    </p>
    <h4>INPLACE – Công cụ làm Employer Branding dành cho doanh nghiệp Việt Nam</h4>
    <p>Website www.inplace.vn cung cấp không gian để doanh nghiệp có thể làm thương hiệu nhà tuyển dụng với các công cụ
        như:</p>
    <ul>
        <li><strong>Profile Công ty:</strong> Là nơi mô tả tổng quan về doanh nghiệp như lịch sử hình thành, quá trình
            phát triển, quy mô doanh nghiệp, số lượng nhân sự và văn hóa doanh nghiệp,… thông qua các câu hỏi Who?,
            What?, Why? và How?.
        </li>
        <li><strong>Mục Việc làm:</strong> Đăng tuyển các Job khi doanh nghiệp có nhu cầu tuyển dụng.</li>
        <li><strong>Mục Bài viết:</strong> Đăng tải những bài viết do cá nhân, doanh nghiệp chia sẻ về những câu chuyện
            đời thực, những tình huống mà chúng ta thường gặp trong công việc, trong đời sống cá nhân để từ đó đúc kết
            ra những chân lí, những giá trị cần được tiếp thu.
        </li>
        <li><strong>Mục Thư viện ảnh:</strong> Nơi lưu giữ những khoảnh khắc, những kỉ niệm trong các hoạt động của
            doanh nghiệp.
        </li>
        <li><strong>Mục Video:</strong> Trình chiếu một cách sống động các thông tin mà doanh nghiệp muốn truyền tải đến
            mọi người, các đoạn phỏng vấn các thành viên sáng lập, các thành viên trong nhóm đang có job đăng tuyển,
            giúp đọc giả có cái nhìn gần gũi và chân thực về doanh nghiệp
        </li>
        <li><strong>Mục Văn phòng ảo:</strong> Thể hiện không gian đa chiều, thực tế của nơi làm việc.</li>
        <li><strong>Mục Q&amp;A:</strong> Nơi doanh nghiệp có thể làm rõ các thông tin về doanh nghiệp mình dưới dạng Q&amp;A
            phản ảnh các cuộc trao đổi thông tin giữa mình và những người quan tâm tới doanh nghiệp, &nbsp;là nơi kết
            nối giữa doanh nghiệp và người tìm việc thông qua những câu hỏi và câu trả lời xoay quanh các vấn đề trong
            công việc, hoạt động cũng như các thông tin liên quan khác về công ty và người tìm việc.
        </li>
        <li><strong>Mục Quiz:</strong> Những câu hỏi ngắn gọn, đơn giản và thú vị do doanh nghiệp tạo ra như một một trò
            chơi hoặc môn thể thao trí óc với mục đích thẩm định sự phù hợp của người tìm việc liên quan đến bất kỳ một
            chủ đề nào đó của doanh nghiệp (văn hoá, chuyên môn, định hướng phát triển, sự tương thích với các thành
            viên trong nhóm vv.) Qua đó, doanh nghiệp có cơ hội để tìm ra những người thực sự phù hợp với môi trường
            doanh nghiệp mình.
        </li>
    </ul>
    <p>Hãy để Inplace trở thành trợ thủ đắc lực của doanh nghiệp trong việc xây dựng thương hiệu nhà tuyển dụng (Employer Branding). Ngoài gói dùng thử miễn phí, website luôn có các gói dịch vụ đáp ứng tốt hơn nhu cầu sử dụng của doanh nghiệp. Đừng ngại ngần mà hãy liên hệ số Hotline của chúng tôi để được hỗ trợ tốt nhất.
    </p>
</div>