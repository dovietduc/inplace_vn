<?php

namespace App\Http\Controllers;

use App\Events\PageJobsShowLoad;
use App\JobMiddleMemberCompany;
use App\Jobs\SendEmailToCandidateApplyJobs;
use App\Jobs\SendEmailToUserManagementJobs;
use App\Jobs\SendMailToCustomerMangementJobs;
use App\JobView;
use App\News;
use App\SaveJob;
use App\Traits\StoreImageTrait;
use App\User;
use App\UserApplyJob;
use Illuminate\Http\Request;
use App\Job;
use Validator;
use DB;

class JobController extends Controller
{
    private $job;
    private $saveJob;
    private $news;
    private $jobView;
    private $user;
    private $userApplyJob;
    private $jobMiddleMemberCompany;
    use StoreImageTrait;

    public function __construct(Job $job, SaveJob $saveJob,
                                News $news, JobView $jobView, User $user,
                                UserApplyJob $userApplyJob, JobMiddleMemberCompany $jobMiddleMemberCompany)
    {
        $this->job = $job;
        $this->saveJob = $saveJob;
        $this->news = $news;
        $this->jobView = $jobView;
        $this->user = $user;
        $this->userApplyJob = $userApplyJob;
        $this->jobMiddleMemberCompany = $jobMiddleMemberCompany;
    }

    public function searchTypeHeadFollowNameCustomerOrNameJob(Request $request)
    {
        $getJobsAndCustomer = $this->job->searchTypeHeadFollowNameCustomerOrNameJobAjax($request->q);
        return response()->json($getJobsAndCustomer);
    }


    public function save($idJob)
    {
        try {
            $this->saveJob->firstOrCreate([
                'job_id' => $idJob,
                'user_id' => auth()->id()
            ]);
            return response()->json(['message' => 'Lưu job thành công'], 200);

        } catch (\Exception $e) {
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
        }

    }

    public function show(Job $job)
    {

        $idJob = $job->id;
        $customerId = !empty(optional($job->customer)->id) ? optional($job->customer)->id : '';
        // Login will insert to count views
        if (auth()->check()) {
            $this->jobView->firstOrCreate([
                'job_id' => $idJob,
                'user_id' => auth()->id()
            ]);
        }
        $hotNews =  $this->news->getManyWhereOrderLimit([
            'customer_id' => $customerId
        ], 3, array('*'), 'view_count desc');
        $jobsRelative = $this->job->getManyWhereOrderRandomNumber([
            ['customer_id', $customerId],
            ['id', '<>', $idJob]
        ], 3);
        $industryJob = $job->customerIndustry->pluck('id');
        $jobsFollowIndustry = $this->job->getJobsFollowIndustry($idJob, $industryJob);
        $jobsIdViewed = $this->jobView->where([
            ['user_id', '=', auth()->id()],
            ['job_id', '<>', $idJob]
        ])->pluck('job_id');
        $jobsUserViewed = $this->job->whereIn('id', $jobsIdViewed)->latest()->limit(20)->get();
        $members = $job->membersCompany;

        // Count views
        $job->increment('view_count');
        return view('jobs.show', compact('job', 'hotNews',
            'jobsRelative', 'jobsFollowIndustry', 'jobsUserViewed', 'members'));
    }

    public function applyJob(Request $request, Job $job)
    {
        $messages = [
            'tel.required' => 'Số điện thoại là bắt buộc',
            'tel.max' => 'Số điện thoại tối đa 25 kí tự',
            'file_upload.file' => 'File upload không thể để trống',
            'file_upload.mimes' => 'File cv chỉ chấp nhận doc,docx,pdf,xls,xlsx',
            'file_upload.max' => 'Dung lượng file cv upload tối đa 2mb',
        ];
        $validator = Validator::make($request->all(), [
            'tel' => 'bail|required|max:25',
            'file_upload' => 'bail|file|mimes:pdf,doc,docx,xls,xlsx|max:2000'
        ], $messages);
        try {
            if ($validator->passes()) {
                DB::beginTransaction();
                // Insert apply job to database
                $dataInsert = $this->verifyAndStoreImage($request, 'file_upload', 'apply');
                $dataInsert['user_id'] = auth()->id();
                $dataInsert['job_id'] = $job->id;
                $dataInsert['mobile'] = $request->tel;
                $dataInsert['customer_id'] = $job->customer_id;

                if ( !auth()->user()->mobile ) {
                    $this->user->find(auth()->id())->update(['mobile' => $request->tel]);
                }
                $this->userApplyJob->create($dataInsert);

                $emailUserManagementJob = optional($job->emailApplyJobs()->first())->email;
                $emailCandidateApply = auth()->user()->email;
                $emailCustomerOfJob = optional($job->customer)->email;
                // send mail to candidate apply - user is logging
                $candidateData = [
                    'email' => $emailCandidateApply,
                    'name' => auth()->user()->name,
                    'job_title' => !empty($job) ? $job->title : '',
                    'job_short_title' => !empty($job) ? $job->short_title : '',
                    'job_url' => !empty($job) ? route('jobs.show', ['slug' => $job->slug]) : '',
                    'name_customer' => optional($job->customer)->name,
                    'email_customer' => optional($job->customer)->email,
                    'file_cv_attach' => !empty($dataInsert) ? $dataInsert['file_path_asset'] : '',
                    'file_name_attach' => !empty($dataInsert) ? $dataInsert['file_name_display'] : '',
                    'email_user_admin_job' => $emailUserManagementJob
                ];
                if ( !empty($emailCandidateApply) && filter_var($emailCandidateApply, FILTER_VALIDATE_EMAIL) ) {
                    dispatch(new SendEmailToCandidateApplyJobs($candidateData));
                }
                // send mail to customer of jobs
                if ( !empty($emailCustomerOfJob) && filter_var($emailCustomerOfJob, FILTER_VALIDATE_EMAIL) ) {
                    dispatch(new SendMailToCustomerMangementJobs($candidateData));
                }
                // send mail to user management job
                if ( !empty($emailUserManagementJob) && filter_var($emailUserManagementJob, FILTER_VALIDATE_EMAIL) ) {
                    dispatch(new SendEmailToUserManagementJobs($candidateData));
                }
                DB::commit();
                return response()->json([
                    'status' => 200
                ]);

            } else {
                return response()->json([
                    'responseJSON' => $validator->getMessageBag()->toArray(),
                    'status' => 422
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
        }
    }


    public function showListSaveJob()
    {
        $jobsSaved = auth()->user()
            ->saveJobs()->whereNull('jjob_user_save_jobs.deleted_at')
            ->whereNull('jjob_jobs.deleted_at')
            ->latest('jjob_jobs.created_at')->paginate(Job::paginations);
        return view('jobs.save-job-list', compact('jobsSaved'));
    }

    public function removeSaveJobItem(Request $request, Job $job)
    {
        $this->saveJob->deleteDataWhere([
            'user_id' => auth()->id(),
            'job_id' => $job->id
        ]);
        return response()->json([
            'status' => true
        ]);
    }

    public function showListApplyJob()
    {
        $jobsApplys = auth()->user()
            ->applyJobs()
            ->whereNull('jjob_jobs.deleted_at')
            ->latest('jjob_jobs.created_at')->paginate(Job::paginations);
        return view('jobs.apply-job-list', compact('jobsApplys'));

    }

    public function occupationUpdateUser(Request $request)
    {
        try {
            auth()->user()->update([
                'occupation_id' => $request->occupation_id
            ]);
            return response()->json([
                'code' => 200,
                'message' => 'update success'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'code' => 500,
                'message' => 'Database query error'
            ], 500);
        }

    }
}
