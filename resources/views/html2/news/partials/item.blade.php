<div class="component-posts-item-large">
    <div class="posts-item-company justify-content-between">
        <div class="d-flex align-items-center">
            <a hef="#" title="">

                <img src="/images/no_avatar_company_50x50.jpg" alt="" class="avatar">
            </a>
            <div class="name">
                <h5><a href="http://127.0.0.1:8000/companies/nextsol-205">NextSol</a></h5>
                <p>
                    0 Fan • 1 giờ trước
                </p>
            </div>
        </div>
        <button class="btn-follow-customer follow_customer"
                data-url="http://127.0.0.1:8000/companies/company_followings/162">
            <span class="active">Đã là fan</span>
        </button>
    </div>
    <div class="posts-item-thumb">
        <a href="#" title=""><img src="/images/no_img_730x286.jpg" alt="" class="rounded"></a>
    </div>
    <h3 class="posts-item-title">
        <a href="#" title="">
            Cảm xúc trong nghề của những Cô gái làm Tuyển dụng
        </a>
    </h3>
    <div class="posts-item-desc txt-format">
        Nếu như trước đây, nhân viên chỉ cần vững chuyên môn thì ngày nay điều đó là chưa đủ. Có lẽ thời thế đã đổi
        thay, ngoại hình dần trở thành một tiêu chí lựa chọn của nhà tuyển dụng, trở thành một yếu tố ảnh hưởng đến phát
        triển nghề nghiệp của mỗi nhân viên.
    </div>
    <div class="posts-item-auth">
        <div class="posts-item-auth_info">
            <a href="#" title="" class="auth-avatar">
                <img src="/images/no_user_30x30.jpg" alt="">
            </a>
            <div class="auth-name" 287="">
                <h3><a href="#" title="">Khánh Vy Nguyễn</a></h3>
                <span><i class="far fa-clock"></i> 2 hours from now</span>
            </div>
        </div>
        <div class="posts-item-auth_like">
            <span class="like-count">
                0 Thích
            </span>
            <button class="btn-like-heart toogle_like_button">
                <i class="far fa-heart"></i>
            </button>
        </div>
    </div>
</div>
