<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Invite Member</title>
    <link rel="stylesheet" href="{{asset('pages/enterprise/email/email-invite-member.css')}}">
</head>
<body>
    <section class="email-confirm-members">
        <header>
            <a href="https://inplace.vn/"><img src="{{asset('images/logo.png')}}" alt=""></a>
            <span>Right People in Right Seats</span>
        </header>
        <article>
            <p>Chào <strong>Võ Trần Hoài Thanh</strong>,</p>
            <p>Phuong Nguyen đã thêm bạn làm thành viên của HRE trên Inplace Admin. <br>Tham gia nhóm để tuyển dụng những tài năng lớn!</p>
            <center>
                <a href="">Become a member of HRE</a>
            </center>
            <p>Nếu có nhu cầu nào khác cần hỗ trợ, hãy liên hệ Hotline hoặc email <a href="mailto:support@inplace.vn">support@inplace.vn</a> nhé.</p>
        </article>
        <footer>
            <hr>
            <strong>INPLACE VIETNAM TEAM</strong> <br>
            Match values to place right people to right seats.
        </footer>
    </section>
</body>
</html>
