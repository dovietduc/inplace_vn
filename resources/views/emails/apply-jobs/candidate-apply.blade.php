
@extends('emails.layouts.for-user')

@section('title', 'Candidate Apply Jobs')

@section('content')
    <p>Xin chào, <strong>{{ $candidate['name'] }}</strong></p>
    <p>Inplace ghi nhận bạn đã ứng tuyển vào vị trí <em><u>{{ $candidate['job_short_title'] }}</u></em> tại <strong>{{ $candidate['name_customer'] }}</strong>.
        Hồ sơ ứng tuyển của bạn đang được <strong>{{ $candidate['name_customer'] }}</strong> xem xét. Chúc
        bạn may mắn và nhận được phản hồi tích cực từ <strong>{{ $candidate['name_customer'] }}</strong> cho vị trí này.
    </p>
    <p><em><strong>→ Mẹo nhỏ: </strong>Hãy thường xuyên kiểm tra phần tin nhắn của bạn tại Inplace và các kênh thông tin cá nhân mà bạn đăng trong CV của mình,
        công ty có thể liên lạc với bạn bất cứ lúc nào. Ngoài ra, hãy theo dõi thường xuyên và tương tác với công ty tại trang doanh nghiệp của họ tại Inplace,
        làm các bài Quiz do công ty đưa ra để tăng mức độ tiếp cận, trao đổi thông tin để nhà tuyển dụng hiểu bạn hơn nhé.</em></p>
    <p>INPLACE hi vọng bạn sẽ sớm tìm được công việc phù hợp với nguyện vọng của mình!</p>
@endsection
