<?php

namespace App\Http\Controllers\Enterprise;

use App\Customer;
use App\CustomerNewsCategory;
use App\Events\CreatedCompany;
use App\News;
use App\NewsAdmin;
use App\Scopes\NewsPublicScope;
use App\Traits\StoreImageTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    use StoreImageTrait;
    private $customerNewsCategory;
    private $news;
    private $customer;

    public function __construct(CustomerNewsCategory $customerNewsCategory, NewsAdmin $news,
                                Customer $customer)
    {
        $this->customerNewsCategory = $customerNewsCategory;
        $this->news = $news;
        $this->customer = $customer;
    }

    public function create()
    {
        $categories = $this->customerNewsCategory->getByMultiConditionsModel([
            ['id', '<>', CustomerNewsCategory::inplaceCategory]
        ]);
        return view('enterprise.news.add', compact('categories'));
    }

    public function froalaUpload(Request $request)
    {
        $path = $this->uploadImage($request, 'file', 'froala');
        return stripslashes(response()->json(['link' => asset($path)])->content());
    }

    public function froalaDelete(Request $request)
    {
        $src = str_replace(asset('/'), '', $request->src);
        \File::delete($src);
        return response()->json(['status' => true]);
    }

    public function list(Request $request)
    {
       $customerId = session('customer_id');
        $news = $this->news->getManyWhereOrderPagination([
            'customer_id' =>$customerId,
        ]);
        if (!empty($request->status_news)) {
            $news = $this->news->getManyWhereOrderPagination([
                'customer_id' => $customerId,
                'status' => $request->status_news
            ]);
        }
        return view('enterprise.news.list', compact('news'));
    }

    public function store(Request $request)
    {

        $dirUpload = $this->verifyAndStoreImageJson($request, 'image_crop_data', 'news');
        $dataNews = [
            'title' => $request->title,
            'content' => $request->content_news,
            'slug' => str_slug($request->title),
            'category_id' => $request->category,
            'user_id' => auth()->id(),
            'customer_id' => session('customer_id'),
            'status' => $request->type_submit == News::statusPublic ? News::statusPublic : News::statusDraft
        ];
        if (!empty($dirUpload)) {
            $dataNews['thumbnail'] = 'storage' . $dirUpload;
            $dataNews['thumbnail_storage'] = 'public' . $dirUpload;
        }
        $newInstance = $this->news->createData($dataNews);

        event(new CreatedCompany($newInstance));
        return redirect()->route('enterprise.news.list')->with('message',
            'Bạn vừa đăng một tin tức mới! Nội dung đã đăng của bạn sẽ được xét duyệt trong vòng 24h hoặc sớm hơn');;

    }

    public function edit($id)
    {
        $categories = $this->customerNewsCategory->getByMultiConditionsModel([
            ['id', '<>', CustomerNewsCategory::inplaceCategory]
        ]);
        $new = $this->news->findById($id);
        return view('enterprise.news.edit', compact('categories', 'new'));
    }

    public function postEdit(Request $request, $id)
    {

        $dataUpdate = [
            'title' => $request->title,
            'content' => $request->content_news,
            'slug' => str_slug($request->title),
            'category_id' => $request->category,
            'user_id' => auth()->id(),
            'customer_id' => session('customer_id'),
            'status' => $request->type_submit == News::statusPublic ? News::statusPublic : News::statusDraft
        ];
        $dirUpload = $this->verifyAndStoreImageJson($request, 'image_crop_data', 'news');
        if ($dirUpload) {
            $dataUpdate['thumbnail'] = 'storage' . $dirUpload;
            $dataUpdate['thumbnail_storage'] = 'public' . $dirUpload;
        }
        $this->news->updateData($id, $dataUpdate);
        event(new CreatedCompany($this->news->findById($id)));
        return redirect()->route('enterprise.news.list');
    }

    public function delete($id)
    {
        $this->news->deleteData($id);
        return response()->json(['status' => true], 200);
    }
}
