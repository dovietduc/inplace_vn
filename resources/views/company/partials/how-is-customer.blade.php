<div class="company-how">
    <div class="component-title">
        <h3 class="title">
            How
        </h3>
        <div class="desc">
            Làm thế nào để hoàn thành sứ mệnh?
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 mb-4 mb-sm-0">
            <div class="component-how-item">
                <a href="">
                    @if( jjobCheckImageExit($customer->how_feature_image_1) )
                        <img src="{{ url($customer->how_feature_image_1) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_270x150.jpg') }}" alt="">
                    @endif
                    <h5 class="title">{{ $customer->how_quote_image_1 }}</h5>
                </a>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="component-how-item">
                <a href="">
                    @if( jjobCheckImageExit($customer->how_feature_image_2) )
                        <img src="{{ url($customer->how_feature_image_2) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_270x150.jpg') }}" alt="">
                    @endif
                    <h5 class="title">{{ $customer->how_quote_image_2 }}</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="txt-format mt-3">
        {!! $customer->how_we_do  !!}
    </div>
</div>
