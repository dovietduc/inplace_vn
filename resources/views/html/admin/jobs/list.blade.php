@extends('layouts.admin')
@section('meta')
    <title>Admin - News list</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/jobs/list.css')}}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Vị trí <strong>tuyển dụng</strong></h1>
                <div class="button-group">
                    <input type="text" class="form-control" placeholder="Mã Công việc">
                    <input type="text" class="form-control" placeholder="Tên Công việc">
                    <input type="text" class="form-control" placeholder="Mã Công ty">
                    <input type="text" class="form-control" placeholder="Tên Công ty">
                    <a href="" class="btn btn-custom">Tìm kiếm</a>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-jobs-list">
        <div class="container">
            <div class="dashboard-jobs-list_th">
                <div class="row">
                    <div class="col-xl-7 d-none d-md-block">
                        <div class="row">
                            <div class="col-6">
                                Tiêu đề
                            </div>
                            <div class="col-4">
                                Công ty
                            </div>
                            <div class="col-2">
                                Ứng tuyển
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 d-none d-xl-block">
                        <div class="row">
                            <div class="col-3">
                                Thời gian
                            </div>
                            <div class="col-3">
                                Trạng thái
                            </div>
                            <div class="col-3">
                                Push Top
                            </div>
                            <div class="col-3">
                                Hot Job
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-jobs-item" data-id="<?php echo $i; ?>">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="dashboard-jobs-item_title">
                                    <h4 class="title">
                                        <a href="#">
                                            Chuyên viên Marketing hỗ trợ phát triển thương hiệu J-Job Hà Nội
                                        </a>
                                    </h4>
                                    <div class="d-flex align-items-center justify-content-between mt-2 mt-md-0 mb-md-2 mb-xl-0 mt-xl-2">
                                        <div class="location">
                                            <span>Hà Nội</span>
                                            <span>Hồ Chí Minh</span>
                                            <span>Đà Nẵng</span>
                                            <span>Nha Trang</span>
                                            <span>Hải Phòng</span>
                                            <span>Quảng Ninh</span>
                                        </div>
                                        <div class="action">
                                            <a href="" class="action_view" title="Xem" target="_blank"><i class="far fa-eye"></i></a>
{{--                                            <a href="" class="action_edit" title="Sửa"><i class="far fa-edit"></i></a>--}}
{{--                                            <a href="" class="action_delete" title="Xóa"><i class="far fa-trash-alt"></i></a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                Kokuen Tenko Information Co.,Ltd
                            </div>
                            <div class="col-md-2 my-2 my-md-0">
                                    <p class="d-inline-block d-md-block"><strong>9</strong> ứng viên</p>
                                    <a href="#" class="btn btn-custom_light ml-1 mt-0 ml-md-0 mt-md-2">Xem</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="row">
                            <div class="col-sm-3">
                                <p class="info-txt"><small>Ngày đăng: </small><br class="d-none d-sm-block">10/9/2019</p>
                            </div>
                            <div class="col-4 col-sm-3">
                                <select class="select-status-job">
                                    <option value="0">Waiting...</option>
                                    <option value="1">Active</option>
                                    <option value="2">Deactive</option>
                                </select>
                            </div>
                            <div class="col-4 col-sm-3">
                                <button class="btn-push-top status-txt">Đẩy Top</button>
                            </div>
                            <div class="col-4 col-sm-3">
                                <button class="btn-active-hot-job status-txt">Hot Job</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/admin/jobs/list.js')}}"></script>
@stop
