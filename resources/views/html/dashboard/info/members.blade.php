@extends('layouts.dashboard')
@section('meta')
    <title>Members Dashboard</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('pages/enterprise/info/members.css') }}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Thành viên của <strong>J-JOB Executive Search</strong>
                </h1>
                <div class="button-group">
                    <button class="btn btn-custom" data-toggle="modal" data-target="#addMember"><i
                            class="fas fa-plus mr-1"></i> Thêm thành viên
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-members-list">
        <div class="container">
            <div class="dashboard-members-list_th">
                <div class="row">
                    <div class="col-sm-7 col-xl-4">
                        Thành viên
                    </div>
                    <div class="col-sm 3 col-xl-2">
                        Quyền hạn
                    </div>
                    <div class="d-none d-xl-block col-xl-4">
                        Giới thiệu
                    </div>
                    <div class="col-sm-2 col-xl-2">
                        Thao tác
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-member-item" data-member="1">
                <div class="row align-items-center">
                    <div class="col-sm-7 col-xl-4">
                        <div class="info">
                            <a href="" class="avatar">
                                <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                            </a>
                            <div class="title">
                                <h3 class="name">Đào Mai LÊ</h3>
                                <div class="desc">Frontend Develop</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-sm-3 col-xl-2">
                        <div class="authority">Job Admin</div>
                    </div>
                    <div class="d-none d-xl-block col-xl-4">
                        <div class="about">
                            Hoài Thanh phụ trách về Tuyển dụng, hành chính nhân sự tại J-Job
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-xl-2">
                        <div class="control">
                            <button class="btn-edit-member btn-control"><i class="far fa-edit"></i></button>
                            <button class="btn-delete-member btn-control"><i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="dashboard-member-item waitting" data-member="2">
                <div class="row align-items-center">
                    <div class="col-sm-7 col-xl-4">
                        <div class="info">
                            <a href="" class="avatar">
                                <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                            </a>
                            <div class="title">
                                <h3 class="name">Võ Trần Hoài Thanh</h3>
                                <div class="desc">Chờ active</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-sm-3 col-xl-2">
                        <div class="authority">Basic user</div>
                    </div>
                    <div class="d-none d-xl-block col-xl-4">
                        <div class="about">
                            Hoài Thanh phụ trách về Tuyển dụng, hành chính nhân sự tại J-Job
                        </div>
                    </div>
                    <div class="col-4 col-sm-2 col-xl-2">
                        <div class="control">
                            <button class="btn-delete-member btn-control"><i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>
        </div>
    </div>

    {{--    modal Add member--}}
    <div class="modal-add-member modal fade" id="addMember" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header align-items-center">
                    <h5 class="modal-title">Thêm thành viên mới</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                            class="far fa-times-circle"></i></button>
                </div>
                <div class="modal-body">
                    <p>Bạn có thể thêm thành viên mới vào nhóm tuyển dụng của mình bằng cách gửi email mời.<br>Nhập địa
                        chỉ email của họ bên dưới và nhấp vào "Gửi email".</p>
                    <h4>Mời theo địa chỉ email</h4>
                    <textarea class="form-control" rows="5"
                              placeholder="hovaten@gmail.com&#10;nickname@gmail.com&#10;nguoidung@gmail.com"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom"><i class="far fa-envelope"></i> Gửi email</button>
                </div>
            </div>
        </div>
    </div>
    {{--    modal Delete member--}}
    <div class="modal-delete-member modal fade" data-member="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header align-items-center">
                    <h5 class="modal-title">Xóa thành viên</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                            class="far fa-times-circle"></i></button>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xóa thành viên này?</p>
                    <img class="member-avatar" src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                    <h4 class="member-name">Võ Trần Hoài Thanh</h4>
                    <p class="member-desc">Recruitment Consultant</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-grey" data-dismiss="modal" aria-label="Close">Bỏ qua</button>
                    <button type="button" class="btn-delete-member-confirm btn btn-custom">Xóa</button>
                </div>
            </div>
        </div>
    </div>
    {{--    modal Edit member--}}
    <div class="modal-edit-member modal fade" data-member="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="" method="POST">
                    <div class="modal-header align-items-center">
                        <h5 class="modal-title">Sửa thành viên</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                                class="far fa-times-circle"></i></button>
                    </div>
                    <div class="modal-body">
                        <img class="member-avatar" src="{{asset('images/no_user_125x125.jpg')}}" alt="">
                        <div class="info">
                            <h3 class="member-name">Võ Thị Hoài Thanh</h3>
                            <select class="member-authority" name="member-authority">
                                <option value="Administrator">Administrator</option>
                                <option value="Job Admin">Job Admin</option>
                                <option value="Basic User">Basic User</option>
                                <option value="Interview">Interview</option>
                            </select>
                            <input type="text" name="member-desc" class="member-desc form-control" placeholder="Nhập chức danh thành viên">
                        </div>
                        <div class="about">
                            <textarea class="member-about form-control autoheight" name="member-about" placeholder="Giới thiệu về thành viên này"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-grey" data-dismiss="modal" aria-label="Close">Bỏ qua</button>
                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
{{--    send email add member--}}
    <div class="d-none">
        <section class="email-confirm-members">
            <header>
                <a href="https://inplace.vn/"><img src="{{asset('images/logo.png')}}" alt=""></a>
                <span>Right People in Right Seats</span>
            </header>
            <article>
                <p>Chào <strong>Võ Trần Hoài Thanh</strong>,</p>
                <p>Phuong Nguyen đã thêm bạn làm thành viên của HRE trên Inplace Admin. <br>Tham gia nhóm để tuyển dụng những tài năng lớn!</p>
                <a href="">Become a member of HRE</a>
                <p>Nếu có nhu cầu nào khác cần hỗ trợ, hãy liên hệ Hotline hoặc email <a href="mailto:support@inplace.vn">support@inplace.vn</a> nhé.</p>
            </article>
            <footer>
                <hr>
                <strong>INPLACE VIETNAM TEAM</strong> <br>
                Match values to place right people to right seats.
            </footer>
        </section>
    </div>

@stop
@section('js')
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <script src="{{ asset('pages/enterprise/info/members.js') }}"></script>
@stop
