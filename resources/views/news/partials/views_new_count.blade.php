@foreach($newsView as $newsViewItem)
    <div class="component-posts-item-sidebar">
        <div class="posts-item-thumb">
            <a href="{{ route('news.show', ['slugCompany' => optional($newsViewItem->customer)->slug, 'id' => $newsViewItem->id]) }}" title="" class="">
                @if( jjobCheckImageExit($newsViewItem->thumbnail) )
                    <img src="{{ asset($newsViewItem->thumbnail) }}" alt="..." class="rounded-circle">
                @else
                    <img src="{{ asset('images/no_img_50x50.jpg') }}" alt="..." class="rounded-circle">
                @endif
                <div class="post-order">
                    {{ $loop->index + 1 }}
                </div>
            </a>
        </div>
        <div class="posts-item-content">
            <h6 class="posts-item-title">
                <a href="{{ route('news.show', ['slugCompany' => optional($newsViewItem->customer)->slug, 'id' => $newsViewItem->id]) }}"
                   title=""
                   class=""
                >
                    {{ $newsViewItem->title }}
                </a>
            </h6>
            <div class="posts-item-auth">
                <a href="" class="name">
                    {{ optional($newsViewItem->user)->name }}
                </a>
                <span class="job">
                {{  optional($newsViewItem->category)->name }}
            </span>
            </div>
        </div>
    </div>
@endforeach
