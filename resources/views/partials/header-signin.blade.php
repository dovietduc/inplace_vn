
<header class="header-signin">
	<div class="holder">
		<div class="logo"><a href="{{ route('wellcome') }}"><img src="{{ asset('images/logo.png') }}" alt="logo"></a></div>
		<div class="menu">
			<ul class="clearfix">
				<li><a href="/frontend/helper/gioi-thieu">Giới thiệu</a></li>
				<li><a href="{{ route('home') }}">Tìm việc</a></li>
				<li><a href="/frontend/helper/goc-doanh-nghiep">Góc doanh nghiệp</a></li>
			</ul>
		</div>
		<div class="button">
			<a href="{{ route('login') }}">Đăng nhập</a>
		</div>
		<div class="clear"></div>
	</div>
</header><!-- .header -->
