@extends('layouts.admin')
@section('meta')
    <title>Admin - Candidate list</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/candidates/list.css')}}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">
                    Danh sách <strong>Ứng viên</strong>
                </h1>
            </div>
        </div>
    </div>
    <div class="dashboard-candidates-list">
        <div class="container">
            <div class="dashboard-candidates-list_th">
                <div class="row">
                    <div class="col-md-6 col-xl-4">
                        Ứng viên
                    </div>
                    <div class="col-md-6 col-xl-3">
                        Thông tin liên hệ
                    </div>
                    <div class="col-xl-3 d-none d-xl-block">
                        Công ty
                    </div>
                    <div class="col-xl-2 d-none d-xl-block">
                        Ngày ứng tuyển
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-candidate-item" data-member="1">
                <div class="row align-items-center">
                    <div class="col-md-6 col-xl-4">
                        <div class="info">
                            <a href="" class="avatar">
                                <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                            </a>
                            <div class="title">
                                <h3 class="name"><a href="#">Đào Mai Lê</a></h3>
                                {{--                                <div class="desc"><i class="fas fa-map-marker-alt"></i> TP.Hồ Chí Minh</div>--}}
                                <div class="job-item-tag">
                                    <div class="tag-list">
                                        <a href="#" class="tag-item" target="_blank">
                                            React JS Developer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="contact">
                            <i class="fal fa-envelope"></i> <em>votranhoaithanh@gmail.com</em> <br>
                            <i class="fal fa-phone-alt"></i> <strong>0912 345 6789</strong>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="company"><i class="fal fa-building d-xl-none mr-2"></i>Tinh Vân Technologies</div>
                    </div>
                    <div class="col-md-6 col-xl-2">
                        <div class="date-apply"><i class="fal fa-calendar-alt d-xl-none mr-2"></i>24/10/2019</div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('pages/admin/candidates/list.js')}}"></script>
@stop
