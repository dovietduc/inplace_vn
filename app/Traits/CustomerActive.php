<?php
namespace App\Traits;

use App\MemberCompany;

trait CustomerActive
{
    public function getCustomerActive()
    {
        $memberCompany =  MemberCompany::where([
            'user_id' => auth()->id(),
            'active' => true
        ])->first();
        return $memberCompany->Customer;

    }


}

