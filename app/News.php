<?php

namespace App;

use App\Scopes\NewsPublicScope;
use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use BaseEloquent, SoftDeletes;
    protected $guarded = [];
    protected $table = 'jjob_customer_news';
    const statusDraft = 'draft';
    const statusPublic = 'public';
    const active = 1;
    const rejectActive = 2;
    const waitting = 3;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new NewsPublicScope());
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function newLikes()
    {
        return $this->hasMany(NewsLike::class, 'news_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo(CustomerNewsCategory::class, 'category_id');
    }

    public function searchNewsAdmin($request)
    {
        $dataReturn = new News();
        $dataReturn = $dataReturn
            ->join('jjob_customers', 'jjob_customer_news.customer_id', 'jjob_customers.id');
        if (!empty($request->id_news)) {
            $dataReturn = $dataReturn->where('jjob_customer_news.id', $request->id_news);
        }
        if (!empty($request->name_news)) {
            $dataReturn = $dataReturn->where('jjob_customer_news.title', 'like', '%' . $request->name_news . '%');
        }
        if (!empty($request->id_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.id', $request->id_customer);
        }
        if (!empty($request->name_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.name', 'like', '%' . $request->name_customer . '%');
        }
        $dataReturn = $dataReturn
            ->latest('jjob_customer_news.created_at')
            ->select('jjob_customer_news.*')
            ->paginate(Job::paginations);

        return $dataReturn;

    }

}
