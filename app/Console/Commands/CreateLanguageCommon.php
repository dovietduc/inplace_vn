<?php

namespace App\Console\Commands;

use App\LanguageLevelCommon;
use Illuminate\Console\Command;

class CreateLanguageCommon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:language-common';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create language common';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        LanguageLevelCommon::truncate();
        $data = [
            ['name' => 'Sơ cấp'],
            ['name' => 'Trung cấp'],
            ['name' => 'Cao cấp']
        ];

        foreach ($data as $dataItem) {
            LanguageLevelCommon::create([
                'name' => $dataItem['name']
            ]);
        }
    }
}
