<header class="header-forgot">
	<a href="" class="logo">
		<img src="/images/logo_short.png" alt="">
	</a>
	<ul class="menu clearfix">
		<li><a href="#"><span>Giới thiệu</span></a></li>
		<li><a href="#"><span>Tìm kiếm</span></a></li>
		<li><a href="#"><span>Góc doanh nghiệp</span></a></li>
	</ul>
	<div class="right">
		<div class="register ">
			<span class="txt">Bạn chưa có tài khoản? </span>
			<a href="#" class="link">Đăng ký ngay</a>
		</div>
	</div>
</header><!-- /header -->