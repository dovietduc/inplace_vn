<?php

namespace App\Http\Controllers\Admin;

use App\News;
use App\NewsAdmin;
use App\StatusJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    private $news;
    private $statusJobs;

    public function __construct(NewsAdmin $news, StatusJobs $statusJobs)
    {
        $this->news = $news;
        $this->statusJobs = $statusJobs;
    }

    public function list(Request $request)
    {
        $jobsStatus = $this->statusJobs->latest()->get();
        if (!empty($request->submit)) {
            $news = $this->news->searchNewsAdmin($request);
        } else {
            $news = $this->news->getManyWhereOrderPagination();
        }

        return view('admin.news.list', compact('news', 'jobsStatus'));
    }

    public function changeActiveNews(Request $request, $id)
    {
        $this->news->updateData($id, ['active' => $request->active]);
        return response()->json([
            'code' => 200,
            'message' => 'success'
        ], 200);

    }
}
