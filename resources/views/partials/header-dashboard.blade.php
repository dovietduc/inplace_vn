<header class="header header-dashboard">
    <div class="group">
        <a href="" class="logo">
            <img src="/images/logo_short_light.png" alt="">
        </a>
        <ul class="menu clearfix">
            <li class="active">
                <a href="#">
                    <img src="/images/icon_dashboard.png" alt="">
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ route('home') }}">
                    <img src="/images/icon_job.png" alt="">
                    <span>Việc làm</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="/images/icon_news.png" alt="">
                    <span>Khám phá</span>
                </a>
            </li>

        </ul>
    </div>
    <div class="group group--right">
{{--        <form class="search">--}}
{{--            <input type="text" placeholder="Nhập tìm kiếm công việc, bạn bè và công ty">--}}
{{--            <button type="submit"><img src="/images/icon_search.png" alt=""></button>--}}
{{--        </form>--}}
        @include('partials.menu-dropdown')
    </div>
</header><!-- /header -->
