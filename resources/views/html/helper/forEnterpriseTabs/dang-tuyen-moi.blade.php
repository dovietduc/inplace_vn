<div class="txt-format">
    <h1>Hướng dẫn Đăng tuyển việc làm mới</h1>
    <p>Để đăng tin tuyển dụng mới, bạn vui lòng tạo tài khoản doanh
        nghiệp theo hướng dẫn tại đường link sau: <a href="javascript:;" onclick="$('#tao-trang-doanh-nghiep-moi-tab').click();">Hướng dẫn tạo trang doanh nghiệp</a>.</p>
    <p>Sau khi tạo trang doanh nghiệp thành công, di chuyển đến trang doanh nghiệp mới và
        thao tác theo 1 trong 3 cách dưới đây để tạo tin tuyển dụng:</p>
    <h3>Cách 1: Thao tác với mục <strong>Việc Làm</strong> trên thanh công cụ</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_dang_tuyen_moi.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào mục</li>
                <li><strong>Bước 2:</strong> Chọn đăng tin tuyển dụng mới. Sau đó, điền đầy
                    đủ các thông tin liên
                    quan đến công việc như tên, địa điểm, chức vụ, nội dung công việc, yêu
                    cầu kinh
                    nghiệm v.v..
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất tin đăng bằng cách nhấp vào ô Đăng
                    Bài.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 2: Thao tác trực tiếp với ô tạo tin tuyển dụng mới hiển thị trên trang Profile
        doanh nghiệp</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_dang_tuyen_moi_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Kéo lướt chuột đến phần JobNow</li>
                <li><strong>Bước 2:</strong> Nhấp vào ô tạo tin tuyển dụng mới bên phía góc
                    phải. Sau đó, điền đầy đủ
                    các thông tin liên quan đến công việc như tên, địa điểm, chức vụ, nội
                    dung công
                    việc, yêu cầu kinh nghiệm v.v..
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất tin đăng bằng cách nhấp vào ô đăng
                    bài.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 3: Thao tác tại công cụ <strong>Quản Lý Công Việc</strong> tại trang Admin
        Doanh Nghiệp</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_dang_tuyen_moi_3.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Truy cập công cụ <strong>Admin</strong> của
                    <strong>trang Doanh
                        Nghiệp</strong></li>
                <li><strong>Bước 2:</strong> Chọn phần quản lý Công Việc &nbsp;từ menu bên
                    tay trái
                </li>
                <li><strong>Bước 3:</strong> Tạo tin đăng tuyển mới từ button bên phải của
                    màn hình
                </li>
            </ul>
        </div>
    </div>

    <blockquote>Để tạo một tin đăng hiệu quả bạn nên sử dụng từ khóa trong tiêu đề công
        việc, cung
        cấp các thông tin một cách rõ ràng, chính xác, đồng thời nhấn mạnh quyền lợi của
        nhân viên, đặc trưng văn hoá của môi trường làm việc và liên kết các đường link tới
        những người liên quan tới vị trí này nhé.
    </blockquote>
</div>
