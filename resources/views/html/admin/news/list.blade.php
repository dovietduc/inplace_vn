@extends('layouts.admin')
@section('meta')
    <title>Admin - News list</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/news/list.css')}}">
@stop
@section('content')
    <div class="dashboard-header">
        <div class="container">
            <div class="dashboard-header_content">
                <h1 class="title">Quản lý <strong>Bài viết</strong></h1>
                <div class="button-group">
                    <input type="text" class="form-control" placeholder="Mã Bài viết">
                    <input type="text" class="form-control" placeholder="Tên Bài viết">
                    <input type="text" class="form-control" placeholder="Mã Công ty">
                    <input type="text" class="form-control" placeholder="Tên Công ty">
                    <a href="" class="btn btn-custom">Tìm kiếm</a>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-news-list">
        <div class="container">
            <div class="dashboard-news-list_th">
                <div class="row">
                    <div class="col-5">
                        Tiêu đề
                    </div>
                    <div class="col-7">
                        <div class="row">
                            <div class="col-3">
                                Danh mục
                            </div>
                            <div class="col-3">
                                Công ty
                            </div>
                            <div class="col-3">
                                Người viết bài
                            </div>
                            <div class="col-3">
                                Trạng thái
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++){?>
            <div class="dashboard-news-item">
                <div class="row">
                    <div class="col-lg-5 dashboard-news-item_col" data-id="<?php echo $i; ?>">
                        <div class="dashboard-news-item_title">
                            <div class="row">
                                <div class="col-sm-4 col-md-3 col-lg-4">
                                    <a href="#" class="thumb">
                                        <img src="{{asset('images/no_img_180x71.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="col-sm-8 col-md-9 col-lg-8">
                                    <h4 class="title"><a href="#">Lead, grow, and create impact with our team as our
                                            next Business Developer!</a></h4>
                                    <div class="d-flex align-items-center justify-content-between mt-2">
                                        <div class="time"><i class="far fa-clock"></i> 2019/10/22 - 7h35</div>
                                        <div class="action">
                                            <a href="" class="action_view" title="Xem" target="_blank"><i class="far fa-eye"></i></a>
{{--                                            <a href="" class="action_edit" title="Sửa"><i class="far fa-edit"></i></a>--}}
{{--                                            <a href="" class="action_delete" title="Xóa"><i class="far fa-trash-alt"></i></a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 dashboard-news-item_col">
                        <div class="dashboard-news-item_info">
                            <div class="row align-items-center align-items-lg-start">
                                <div class="col-sm-5 col-lg-3 col_cate">
                                    <span class="info-txt">Phỏng vấn nhân viên</span>
                                </div>
                                <div class="col-sm-7 col-lg-3 col_company">
                                    <span class="info-txt">Kokuen Tenko Information Co.,Ltd</span>
                                </div>
                                <div class="col-sm-5 col-lg-3 col_auth">
                                    <span class="info-txt">Nguyễn Hoài An</span>
                                </div>
                                <div class="col-sm-7 col-lg-3 col_status">
                                    <select class="select-status-job">
                                        <option value="0">Waiting...</option>
                                        <option value="1">Active</option>
                                        <option value="2">Deactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>

            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/admin/news/list.js')}}"></script>
@stop
