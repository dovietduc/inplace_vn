<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class TagsOfJob extends Model
{
    use BaseEloquent;
    protected $guarded = [];
    protected $table = 'jjob_tags';
}
