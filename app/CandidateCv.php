<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateCv extends Model
{
    protected $guarded = [];
    protected $table = 'candidate_cvs';
}
