@extends('layouts.app')
@section('meta')
    <title>Công việc ứng tuyển</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/jobs/apply-jobs/apply-jobs.css')}}">
@stop
@section('content')
    <div class="apply-jobs-page">
        <div class="container">
            <div class="component-title">
                <h3 class="title">Công việc ứng tuyển</h3>
                <div class="desc">Bạn đã ứng tuyển 2 công việc</div>
            </div>
            <div class="row">
                <?php for($i = 0; $i < 12; $i++){ ?>
                <div class="col-sm-6 col-lg-4">
                    @include('html.job.partials.job-save-item')
                </div>
                <?php } ?>
            </div>
            {{--add component pagination--}}
            <div class="block-pagination pagination-page-search">
                <ul class="pagination pagination">
                    <li class="disabled page-item"><span class="page-link">«</span></li>
                    <li class="active page-item"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><span class="page-link">...</span></li>
                    <li class="hidden-xs"><a class="page-link" href="#">129</a></li>
                    <li class="page-item"><a class="page-link" href="#" rel="next">»</a></li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/jobs/apply-jobs/apply-jobs.js')}}"></script>
@stop
