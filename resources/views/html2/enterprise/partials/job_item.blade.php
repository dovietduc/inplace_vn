<div class="item">
    <div class="post-image mb-15">
        <a href="#" title="" class="">
            <img src="https://via.placeholder.com/370x200" class="w-100" alt="">
        </a>
    </div>
    <div class="tags row mb-1">
        <div class="col-9 tag-list">
            <a href="#" title="" class="text-green-300">
                #Tester
            </a>
            <a href="#" title="" class="text-green-300">
                #Client Success Lead
            </a>
        </div>
        <div class="locale col-auto ml-auto text-right">
                                    <span class="text-grey-800">
                                        <i class="far fa-map"></i> Hanoi
                                    </span>
        </div>
    </div>
    <h5 class="post-name">
        <a href="#" title="">
            Tìm kiếm Nhân viên Tester có kinh nghiệm làm dự án thực tế 1 năm
        </a>
    </h5>
</div>
