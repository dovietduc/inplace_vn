<!DOCTYPE html>
<html lang="vi" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job detail</title>
    <link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/select2/select2.min.css" rel="stylesheet"/>
    <link href="/vendor/slick/slick.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/slick/slick-theme.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/pages/jobs/detail/detail.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

@component('html/element/header')
@endcomponent

<div class="main">
    <div class="job-detail_head">
        <div class="container">
            <div class="box">
                <div class="thumb">
                    <div class="row">
                        <div class="col-lg-8">
                            <a href="#"><img src="/images/img_slider_1.png" alt=""></a>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <?php for ($i = 0; $i < 2; $i++){ ?>
                                <div class="col-6 col-lg-12">
                                    <a href="#"><img src="/images/img_slider_1.png" alt=""></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="tag-list">
                                <a href="#" class="tag-item active">Hot Job</a>
                                <a href="#" class="tag-item">Headhunter</a>
                                <a href="#" class="tag-item">Recruitment Consultant</a>
                            </div>
                            <h1 class="title"><a href="">Gia nhập J-Job ngay hôm nay để trở thành Recruitment Consultant
                                    chuyên nghiệp!</a></h1>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="share_social">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                            <div class="button-group">
                                <a href="#" class="btn-save-job save_job saved btn btn-custom_light"><i class="far fa-bookmark"></i></a>
                                <a href="#" class="btn btn-custom font-weight-normal ml-2" data-toggle="modal" data-target="#frm-apply_signin"><i
                                        class="far fa-paper-plane"></i> Ứng tuyển</a>
                                <a href="#" class="" data-toggle="modal" data-target="#frm-apply">Ứng tuyển</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="company">
                    <div class="row align-items-center">
                        <div class="col-sm-7">
                            <div class="d-flex">
                                <div class="logo">
                                    <a href="#"><img src="/images/img_avatar_jjob.png" alt=""></a>
                                </div>
                                <div class="info">
                                    <h2 class="title"><a href="">J-Job Executive Search</a></h2>
                                    <div class="desc">2899 người theo dõi • 1 giờ trước</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 text-right">
                            <div class="talk">
                                <a href="">
                                    <img src="/images/icon-chat.png" alt="">
                                    Trò chuyện <br>với nhà tuyển dụng
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="job-detail_content">
        <div class="container">
            <div class="box">
                <div class="row">
                    <div class="col-lg-8 box_content">
                        <div class="job-detail_content__who">
                            <div class="component-title">
                                <h3 class="title">Who</h3>
                                <div class="desc">Ai có thể trở thành đồng đội của bạn</div>
                            </div>
                            <a href="" class="view-more">Xem tất cả (<span class="count">6</span>)</a>
                            <div class="component-who">
                                <ul class="tab-who nav" role="tablist">
                                    <?php for ($i = 0; $i < 4; $i++){ ?>
                                        <li class="col-3">
                                            <a class="item {{ $i == 0 ?  'active' : ''}}" data-toggle="tab" href="#tab-<?php echo $i;?>" role="tab" aria-controls="home" aria-selected="true">
                                                <img class="avatar" src="/images/job-detail_who.png" alt="">
                                                <h5 class="title">Francis Sim</h5>
                                                <div class="desc">Content Creator</div>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php for ($i = 0; $i < 4; $i++){ ?>
                                    <div class="item_content tab-pane {{ $i == 0 ?  'active' : ''}}" id="tab-<?php echo $i;?>" role="tabpanel" aria-labelledby="home-tab">
                                        <?php echo $i;?>-John Doe phụ trách Customer Service với các Khách hàng Doanh nghiệp của dự án Inplace.vn Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit.
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="job-detail_content__what">
                            <div class="component-title">
                                <h3 class="title">What</h3>
                                <div class="desc">Sứ mệnh của công ty là gì</div>
                            </div>
                            <div class="row">
                                <?php for ($i = 0; $i < 2; $i++){ ?>
                                <div class="col-6">
                                    <div class="component-how-item">
                                        <a href="">
                                            <img src="/images/img_slider_1.png" class="d-block w-100" alt="">
                                            <h5 class="title">J-JOB Executive Search mang đến một Giải pháp nhân sự đầu
                                                tiên
                                                tại Việt Nam.</h5>
                                        </a>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <div class="content">
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                            </div>
                            <div class="viewmore">
                                <a href="javascript:;">Xem thêm <i class="fas fa-chevron-down"></i></a>
                            </div>
                        </div>
                        <div class="job-detail_content__why job-detail_content__what">
                            <div class="component-title">
                                <h3 class="title">Why</h3>
                                <div class="desc">Tại sao chọn chúng tôi?</div>
                            </div>
                            <div class="row">
                                <?php for ($i = 0; $i < 2; $i++){ ?>
                                <div class="col-6">
                                    <div class="component-how-item">
                                        <a href="">
                                            <img src="/images/img_slider_1.png" class="d-block w-100" alt="">
                                            <h5 class="title">J-JOB Executive Search mang đến một Giải pháp nhân sự đầu
                                                tiên
                                                tại Việt Nam.</h5>
                                        </a>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <div class="content">
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                            </div>
                        </div>
                        <div class="job-detail_content__how job-detail_content__what">
                            <div class="component-title">
                                <h3 class="title">How</h3>
                                <div class="desc">Chúng tôi hoạt động như thế nào?</div>
                            </div>
                            <div class="row">
                                <?php for ($i = 0; $i < 2; $i++){ ?>
                                <div class="col-6">
                                    <div class="component-how-item">
                                        <a href="">
                                            <img src="/images/img_slider_1.png" class="d-block w-100" alt="">
                                            <h5 class="title">J-JOB Executive Search mang đến một Giải pháp nhân sự đầu
                                                tiên
                                                tại Việt Nam.</h5>
                                        </a>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <div class="content">
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                                <p>Chúng tôi là một trong những doanh nghiệp tiên phong hoạt động trong lĩnh vực
                                    HR&Tech.
                                    HRE cung cấp Hệ sinh thái Giải pháp nhân sự để Kết nối nhân tài với các Doanh
                                    nghiệp.</p>
                                <p>Nhà tuyển dụng luôn muốn tìm nhân lực có hiệu quả công việc cao nhất, nhưng với chi
                                    phí
                                    nhân lực tối ưu nhất; Người tìm việc luôn muốn một công việc có chế độ đãi ngộ tốt
                                    nhất.
                                    Đâu là điểm cân bằng giữa Nhà tuyển dụng và Người tìm việc?</p>
                            </div>
                        </div>
                        <div class="job-detail_content__detail">
                            <div class="component-title">
                                <h3 class="title">Mô tả công việc</h3>
                                <div class="desc"></div>
                            </div>
                            <div class="content">
                                <p>NTQ Solution is looking for PHP Developer. The ideal candidates will be enthusiastic
                                    developers who are eager to learn and grow their skills in the field</p>

                                <p><strong>** Responsibilities & Duties:</strong></p>
                                <ul>
                                    <li>01 experience years work as PHP</li>
                                    <li>Strong knowledge in Cake, Lavarel</li>
                                    <li>Excellent database knowledge especially MySQL</li>
                                    <li>Strong knowledge in JavaScript, CSS, HTML</li>
                                    <li>01 experience years work as PHP</li>
                                </ul>

                                <h3>JOB REQUIREMENTS</h3>
                                <p><strong>** Job requirements</strong></p>
                                <ul>
                                    <li>01 experience years work as PHP</li>
                                    <li>Strong knowledge in Cake, Lavarel</li>
                                    <li>Excellent database knowledge especially MySQL</li>
                                    <li>Strong knowledge in JavaScript, CSS, HTML</li>
                                    <li>01 experience years work as PHP</li>
                                </ul>
                                <p>• Experience with AngularJS or similar Javascript MVC framework</p>
                                <p>• Good knowledge in NodeJS</p>

                                <h3>BENEFITS</h3>
                                <ul>
                                    <li>01 experience years work as PHP</li>
                                    <li>Strong knowledge in Cake, Lavarel</li>
                                    <li>Excellent database knowledge especially MySQL</li>
                                    <li>Strong knowledge in JavaScript, CSS, HTML</li>
                                    <li>01 experience years work as PHP</li>
                                </ul>
                            </div>
                            <div class="button-group">
                                <a href="#" class="btn btn-custom"><i class="far fa-paper-plane"></i> Ứng tuyển</a>
                                <a href="#" class="btn-save-job save_job saved btn btn-custom_light"><i class="far fa-bookmark"></i></a>
                                <a href="#" class="btn btn-custom_light"><i class="far fa-share-square"></i> Chia sẻ</a>
                            </div>
                        </div>
                        <div class="job-detail_content__hotnews">
                            <div class="component-title">
                                <h3 class="title">Bài viết nổi bật</h3>
                                <div class="desc"></div>
                            </div>
                            <div class="row">
                                <?php for($i = 0; $i < 3; $i++){?>
                                <div class="col-sm-4">
                                    <div class="component-posts-item">
                                        <a href="#" class="posts-item-thumb"><img src="/images/img_slider_1.png" alt=""
                                                                       class="w-100 d-block"></a>
                                        <h3 class="posts-item-title"><a href="#">3 câu hỏi quen thuộc của CEO Amazon trước khi tuyển.</a></h3>
                                        <div class="posts-item-auth">
                                            <a href="#" class="avatar"><img src="/images/icon_avatar.png" alt=""></a>
                                            <h6 class="name"><a href="#">John Doe</a></h6>
                                            <div class="desc">PHP Deverloper</div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="job-detail_content__company">
                            <div class="component-title">
                                <h3 class="title">Thông tin công ty</h3>
                                <div class="desc"></div>
                            </div>
                            <table class="component-info-company">
                                <tr>
                                    <td>Lĩnh vực hoạt động:</td>
                                    <td><span>Tecknology</span></td>
                                </tr>
                                <tr>
                                    <td>Quy mô doanh nghiệp:</td>
                                    <td>201-500 nhân viên</td>
                                </tr>
                                <tr>
                                    <td>Đặc tính văn hóa:</td>
                                    <td><span class="tab">Trẻ trung</span><span class="tab">Năng động</span><span
                                            class="tab">Sáng tạo</span></td>
                                </tr>
                                <tr>
                                    <td>Năm thành lập:</td>
                                    <td>2003</td>
                                </tr>
                                <tr>
                                    <td>Trụ sở chính:</td>
                                    <td>Tầng 3 Tòa nhà Đại Phát, 82 Duy Tân, Q.Cầu Giấy, TP Hà Nội</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-4 box_sidebar">
                        <div class="around">
                            <h4 class="title">Box 360</h4>
                            <ul>
                                <li><a href="#"><i class="far fa-building"></i>Company Home</a></li>
                                <li><a href="#"><i class="far fa-newspaper"></i>Bài viết</a></li>
                                <li><a href="#"><i class="far fa-images"></i>Gallery</a></li>
                                <li><a href="#"><i class="fas fa-video"></i>Video</a></li>
                                <li><a href="#"><i class="fas fa-cube"></i>Thăm văn phòng ảo</a></li>
                                <li><a href="#"><i class="fas fa-info-circle"></i>Q&A</a></li>
                                <li><a href="#"><i class="far fa-question-circle"></i>Quiz</a></li>
                            </ul>
                        </div>
                        <div class="around">
                            <h4 class="title">Bản đồ</h4>
                            <div class="map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0352557770116!2d105.7805400148321!3d21.031275285996948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab4caa5f340f%3A0xbca58c0af1aae5fc!2zODIgUGjhu5EgRHV5IFTDom4sIEThu4tjaCBW4buNbmcgSOG6rXUsIEPhuqd1IEdp4bqleSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1570086699350!5m2!1svi!2s"
                                    width="600" height="450" frameborder="0" style="border:0;"
                                    allowfullscreen=""></iframe>
                            </div>
                        </div>
                        <div class="around">
                            <h3>Gia nhập J-Job ngay hôm nay để trở thành Recruitment Consultant ...</h3>
                            <ul>
                                <li><a href=""><i class="far fa-tag"></i>Trưởng nhóm</a></li>
                                <li><a href=""><i class="far fa-id-card"></i>Mỹ thuật, thiết kế, marketing</a></li>
                                <li><a href=""><i class="fas fa-map-marker-alt"></i>Hồ Chí Minh, Hà Nội</a></li>
                                <li><a href=""><i class="far fa-comment-dots"></i>Tiếng Anh
                                        <small>(Advanced)</small><br>Tiếng
                                        nhật <small>(N3)</small></a></li>
                                <li><a href=""><i class="far fa-credit-card"></i>650-900$</a></li>
                            </ul>
                        </div>
                        <div class="around other">
                            <h3>Vị trí khác công ty đang tuyển dụng</h3>
                            <?php for($i = 0;$i < 3;$i++){?>
                            <div class="item">
                                <div class="thumb">
                                    <a href="#"><img src="/images/img_slider_1.png" alt=""></a>
                                </div>
                                <div class="content">
                                    <div class="item_date">20/09/2018</div>
                                    <h4 class="item_title"><a href="#">Tìm kiếm Middle/Senior Ruby on Rails
                                            Developers</a></h4>
                                    <div class="item_address">Hà Nội, TP Hồ Chí Minh</div>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="job-detail_footer">
        <div class="list-job">
            <div class="container">
                <h3 class="list-job_title">Công việc cùng phòng ban</h3>
                <div class="list-job_slick">
                    <?php for($i = 0;$i < 10;$i++){?>
                    <div class="item">
                        <div class="list-job_item">
                            <div class="company">
                                <a href="#" class="logo"><img src="/images/img_avatar.png" alt=""></a>
                                <h4 class="title"><a href="#">NTQ Solution</a></h4>
                            </div>
                            <div class="thumb">
                                <a href="#"><img src="/images/img_slider_1.png" alt=""></a>
                            </div>
                            <h3 class="title"><a href="">Tìm kiếm các PHP Developer làm việc cho các dự án tại Nhật</a></h3>
                            <div class="tag-list">
                                <a href="#" class="tag-item active">Hot job</a>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <div class="list-job bg-white">
            <div class="container">
                <h3 class="list-job_title">Công việc cùng phòng ban</h3>
                <div class="list-job_slick">
                    <?php for($i = 0;$i < 10;$i++){?>
                    <div class="item">
                        <div class="list-job_item">
                            <div class="company">
                                <a href="#" class="logo"><img src="/images/img_avatar.png" alt=""></a>
                                <h4 class="title"><a href="#">NTQ Solution</a></h4>
                            </div>
                            <div class="thumb">
                                <a href="#"><img src="/images/img_slider_1.png" alt=""></a>
                            </div>
                            <h3 class="title"><a href="">Tìm kiếm các PHP Developer làm việc cho các dự án tại Nhật</a></h3>
                            <div class="tag-list">
                                <a href="#" class="tag-item active">Hot job</a>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
@include('html.element.popup-applyJob')
@component('html/element/footer')
@endcomponent

<script src="/vendor/bootstrap-v4/bootstrap.min.js"></script>
<script src="/vendor/slick/slick.min.js"></script>
<script src="/vendor/select2/select2.min.js"></script>
<script src="/vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js"></script>
<script src="/pages/common/main.js"></script>
<script src="/pages/jobs/detail/detail.js"></script>

</body>
</html>
