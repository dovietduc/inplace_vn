<?php

namespace App\Http\Controllers;

use App\CustomerNewsCategory;
use App\Events\PageJobsShowLoad;
use App\Events\PageNewsShowLoad;
use App\Job;
use App\MenuCompany;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class NewsController extends Controller
{
    private $menuCompany;
    private $job;
    private $news;
    private $customerNewsCategory;
    public function __construct(MenuCompany $menuCompany, Job $job,
                                News $news, CustomerNewsCategory $customerNewsCategory)
    {
        $this->menuCompany = $menuCompany;
        $this->job = $job;
        $this->news = $news;
        $this->customerNewsCategory = $customerNewsCategory;

    }

    public function show($slugCompany, $id)
    {
        $newItem = $this->news->findById($id);
        $customerId = $newItem->customer_id;
        $menus = $this->menuCompany->getAll();
        $jobRelatest = $this->job->getManyWhereOrderLimitNumber([
            'customer_id' => $customerId
        ]);

        $newsRelatest = $this->news->where([
            ['customer_id', '=', $customerId],
            ['id', '<>', $id]
        ])
            ->withCount(['newLikes' => function (Builder $query) {
            $query->where('deleted_flag', false);
        }])->latest()->limit(3)->get();
        // Count views post
        $newItem->increment('view_count');
        return view('news.show', compact('newItem', 'menus',
            'jobRelatest', 'newsRelatest'));
    }

    public function index()
    {
        $categoryNews = $this->customerNewsCategory->getAll();
        $newsHeader = $this->news->getManyWhereOrderRandomNumber([
            'hot' => true
        ], 4);
        $news = $this->news->getManyWhereOrderPagination();
        $newsHot = $this->news->getManyWhereOrderLimit([
           ], 5, array("*"), 'total_like desc' );
        $newsView = $this->news->getManyWhereOrderLimit([
        ], 5, array("*"), 'view_count desc' );
        return view('news.index', compact('categoryNews', 'newsHeader', 'news', 'newsHot', 'newsView'));
    }

    public function category($slug)
    {
        $category = $this->customerNewsCategory->findWhereFirst('slug', $slug);
        $categoryNews = $this->customerNewsCategory->getAll();
        $newsHeader = $this->news->getManyWhereOrderRandomNumber([
            'hot' => true
        ], 4);
        $news = $this->news->getManyWhereOrderPagination([
            'category_id' => $category->id
        ]);
        $newsHot = $this->news->getManyWhereOrderLimit([
        ], 5, array("*"), 'total_like desc' );
        $newsView = $this->news->getManyWhereOrderLimit([
        ], 5, array("*"), 'view_count desc' );
        return view('news.index', compact('categoryNews', 'newsHeader', 'news', 'newsHot', 'newsView'));
    }


}
