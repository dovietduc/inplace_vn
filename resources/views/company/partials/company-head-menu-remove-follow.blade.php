<div class="nav-enterprise">
    <div class="container">
        <div class="nav-enterprise-box">

            <div class="info-nav-enterprise">
                <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}" class="company-avatar">
                    @if( jjobCheckImageExit($customer->logo) )
                        <img alt="..." src="{{ url($customer->logo) }}" class="">
                    @else
                        <img alt="..." src="{{ asset('images/no_avatar_company_100x100.jpg') }}" class="">
                    @endif
                </a>
                <div class="company-info">
                    <h3 class="company-name">
                        <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}">
                            {{ $customer->name }}
                        </a>
                    </h3>
                    <div>
                        @if(auth()->check())
                            @include('partials.follows-customer.button-follow-customer')
                        @else
                            <a href="{{ route('register') }}"
                               class="btn-follow-customer">
                                <span>Trở thành fan</span>
                            </a>
                        @endif
                        <div class="company-tag-info">
                            <span class="d-inline-block">{{ $customer->industrys->implode('name', ', ') }}</span>
                            @include('company.partials.city-head-menu')
                             <span
                                class="d-inline-block label_count_user_follow">
                                {{ jobHelper::countUserFollowCustomer($customer->id) }}
                                Fan
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            @include('company.partials.menu-head-menu')

        </div>
    </div>
</div>
