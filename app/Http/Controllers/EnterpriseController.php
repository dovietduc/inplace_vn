<?php

namespace App\Http\Controllers;

use App\Customer;
use App\MemberCompany;
use Illuminate\Http\Request;

class EnterpriseController extends Controller
{
    private $customer;
    public function __construct(Customer $customer, MemberCompany $memberCompany)
    {
        $this->customer = $customer;
        $this->memberCompany = $memberCompany;
    }

    public function index($customerId = null)
    {
        if (!empty($customerId) && is_numeric($customerId)) {
            session(['customer_id' => $customerId]);
        }
        if ($customerId == 'user') {
            // remove session
            session()->forget('customer_id');
            return redirect()->route('home');
        }

        return view('enterprise.index');
    }
}
