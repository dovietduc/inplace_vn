<?php
Route::get('/enterprise/{customerId?}', [
    'as' => 'enterprise.index',
    'uses' => 'EnterpriseController@index'
]);
Route::group(['middleware' => ['check.sessioncustomer'], 'prefix' => 'enterprise'], function(){

    Route::prefix('companies')->group(function () {
        Route::get('/{customerId}/edit', [
            'as' => 'enterprise.companies.edit',
            'uses' => 'Enterprise\CompanyController@edit'
        ]);

        Route::post('/{id}/edit/customer-basic', [
            'as' => 'enterprise.companies.edit.customer-basic',
            'uses' => 'Enterprise\CompanyController@updateCustomerBasic',
        ]);

        Route::post('/{id}/edit/save-image-feature', [
            'as' => 'enterprise.companies.edit.save-image',
            'uses' => 'Enterprise\CompanyController@updateCustomerSaveImage',
        ]);
        Route::post('/{id}/edit/what-is-customer', [
            'as' => 'enterprise.companies.edit.what-is-customer',
            'uses' => 'Enterprise\CompanyController@updateWhatIsCustomer',
        ]);
        Route::post('/{id}/edit/why-we-do', [
            'as' => 'enterprise.companies.edit.why-we-do',
            'uses' => 'Enterprise\CompanyController@updateWhyWeDo',
        ]);
        Route::post('/{id}/edit/how-we-do', [
            'as' => 'enterprise.companies.edit.how-we-do',
            'uses' => 'Enterprise\CompanyController@updateHowWeDo',
        ]);
    });



    Route::prefix('news')->group(function () {

        Route::get('/add', [
            'as' => 'enterprise.news.add',
            'uses' => 'Enterprise\NewsController@create',
        ]);
        Route::post('/add', [
            'as' => 'enterprise.post.news.add',
            'uses' => 'Enterprise\NewsController@store',
        ]);
        Route::get('/list', [
            'as' => 'enterprise.news.list',
            'uses' => 'Enterprise\NewsController@list',
        ]);
        Route::get('/edit/{id}', [
            'as' => 'enterprise.news.edit',
            'uses' => 'Enterprise\NewsController@edit',
        ]);

        Route::post('/edit/{id}', [
            'as' => 'enterprise.news.post.edit',
            'uses' => 'Enterprise\NewsController@postEdit',
        ]);

        Route::post('/delete/{id}', [
            'as' => 'enterprise.news.delete',
            'uses' => 'Enterprise\NewsController@delete',
        ]);

    });


    Route::prefix('jobs')->group(function () {
        Route::get('/list', [
            'as' => 'enterprise.jobs.list',
            'uses' => 'Enterprise\JobController@list',
        ]);
        Route::get('/add', [
            'as' => 'enterprise.jobs.add',
            'uses' => 'Enterprise\JobController@create',
        ]);
        Route::post('/add', [
            'as' => 'enterprise.post.jobs.add',
            'uses' => 'Enterprise\JobController@store',
        ]);
        Route::get('/edit/{id}', [
            'as' => 'enterprise.jobs.edit',
            'uses' => 'Enterprise\JobController@edit',
        ]);

        Route::post('/edit/{id}', [
            'as' => 'enterprise.jobs.post.edit',
            'uses' => 'Enterprise\JobController@postEdit',
        ]);
        Route::get('/ajax/search-tags', [
            'as' => 'enterprise.jobs.ajax.search-tags',
            'uses' => 'Enterprise\JobController@searchTags',
        ]);
        Route::post('/delete/{id}', [
            'as' => 'enterprise.jobs.delete',
            'uses' => 'Enterprise\JobController@delete',
        ]);

        Route::post('/ajax/search-occupation-select', [
            'as' => 'enterprise.jobs.ajax.search-occupation-select',
            'uses' => 'Enterprise\JobController@searchOccupationSelect',
        ]);


    });

    Route::prefix('member')->group(function () {
        Route::get('/users', [
            'uses' => 'Enterprise\MemberController@index',
            'as' => 'enterprise.member.users',
        ]);
        Route::post('/employee-invitations', [
            'uses' => 'Enterprise\MemberController@employeeInvitations',
            'as' => 'enterprise.member.employee-invitations',
        ]);

        Route::get('/edit/{id}', [
            'uses' => 'Enterprise\MemberController@showMemberEdit',
            'as' => 'enterprise.member.showMemberEdit',
        ]);
        Route::post('/edit/{id}', [
            'uses' => 'Enterprise\MemberController@postMemberEdit',
            'as' => 'enterprise.member.postMemberEdit',
        ]);
        Route::post('/delete', [
            'uses' => 'Enterprise\MemberController@deleteMember',
            'as' => 'enterprise.member.deleteMember',
        ]);


    });

    Route::post('/froala/upload', [
        'uses' => 'Enterprise\NewsController@froalaUpload',
        'as' => 'enterprise.froalaUpload',
    ]);
    Route::post('/delete/froala', [
        'uses' => 'Enterprise\NewsController@froalaDelete',
        'as' => 'enterprise.froalaDelete',

    ]);


    Route::prefix('candidates')->group(function () {
        Route::get('/list', [
            'uses' => 'Enterprise\CandidateController@index',
            'as' => 'enterprise.candidates.index',
        ]);
        Route::get('detail/{id}', [
            'uses' => 'Enterprise\CandidateController@show',
            'as' => 'enterprise.candidates.show',
        ]);

    });

});

Route::get('/click/companies/{customerId}/employee_invitations/{token}', [
    'uses' => 'Enterprise\MemberController@showJoinCompany',
    'as' => 'enterprise.member.showJoinCompany',
]);
Route::get('/companies/{customerId}/employee_invitations/{token}/accept', [
    'uses' => 'Enterprise\MemberController@acceptJoinCompany',
    'as' => 'enterprise.member.acceptJoinCompany',
]);


