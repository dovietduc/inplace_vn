<?php

namespace App\Console\Commands;

use App\TagsJob;
use Illuminate\Console\Command;

class CreateDataJobTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-data-job-tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create data job tags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            ['name' => 'Marketing'],
            ['name' => 'Designer'],
            ['name' => 'Sales'],
            ['name' => 'PHP Developer'],
            ['name' => 'Mobile Developer'],
        ];
        foreach ($data as $item) {
            $item['slug'] = str_slug($item['name']);
            TagsJob::create($item);
        }

    }
}
