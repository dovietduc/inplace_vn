<div class="row">
    @foreach ($hotNews as $hotNewIem)
        <div class="col-sm-6 col-md-4">
            <div class="component-posts-item">
                <a class="posts-item-thumb" href="{{ route('news.show', ['slugCompany' => optional($hotNewIem->customer)->slug, 'id' => $hotNewIem->id]) }}" title="">
                    @if( jjobCheckImageExit($hotNewIem->thumbnail) )
                        <img src="{{ url($hotNewIem->thumbnail) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_350x137.jpg') }}" alt="">
                    @endif
                </a>
                <h3 class="posts-item-title">
                    <a href="#" title="">
                        {{ $hotNewIem->title }}
                    </a>
                </h3>
            </div>
        </div>
    @endforeach
</div>
