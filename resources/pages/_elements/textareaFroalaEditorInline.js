export function textareaFroalaEditorInline(){
    let token = $('meta[name="csrf-token"]').attr('content');
    let urlupload = $('.inp-froala-editor').data('urlupload');
    let urldelete = $('.inp-froala-editor').data('urldelete');
    let placeholderTxt = $('.inp-froala-editor').attr('placeholder');

    $('.inp-froala-editor').froalaEditor({
        iconsTemplate: 'font_awesome_5',
        placeholderText: placeholderTxt,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'color', 'fontSize', 'emoticons', '-', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertVideo', 'insertLink', 'insertFile', 'undo', 'redo'],
        toolbarButtonsXS: null,
        toolbarButtonsSM: null,
        toolbarButtonsMD: null,
        imageMove: true,
        imageUploadParam: 'file',
        imageUploadMethod: 'post',
        imageUploadURL: urlupload,
        imageUploadParams: {
            froala: 'true',
            _token: token
        },
        videoUpload: false,
        videoResize: true,
        videoResponsive: true,
        videoMove: false,
        videoEditButtons: ['videoReplace', 'videoRemove', 'videoDisplay', 'videoAlign', 'videoSize'],
    }).on('froalaEditor.image.removed', function (e, editor, $img) {
        $.ajax({
            method: 'POST',
            url: urldelete,
            data: {
                src: $img.attr('src')
            },
            success: function (data) {

            }
        });
    });
    $('p[data-f-id="pbf"]').remove();
}
