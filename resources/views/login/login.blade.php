<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/login/login.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('element/header-signin')
	@endcomponent

	<div class="login">
		<div class="holder">
			<div class="sologan">
				<h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
				<div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
			</div><!-- sologan -->

			<div class="form">
				<div class="title text-center">XIN CHÀO</div>
				<div class="txt text-center">Bạn chỉ còn một bước nữa để gặp team mơ ước của mình.</div>

				<ul class="social clearfix">
					<li class="face"><a href="#"><i class="fa fa-facebook-official"></i> Đăng nhập với facebook</a></li>
					<li class="linkedin"><a href="#"><i class="fa fa-linkedin-square"></i> Đăng nhập với Linkedin</a></li>
				</ul>

				<div class="or">HOẶC</div>

				<form action="#" method="POST">
					<div class="form-group">
						<div class="messages error">Email không hợp lệ.</div>
						<input type="email" name="user_name" placeholder="Email của bạn" class="form-control">
					</div>

					<div class="form-group">
						<div class="input-password">
							<a href="#" class="js-displayPassword"><img src="/images/icon-eye.png" alt=""></a>
							<input type="password" name="password" placeholder="Mật khẩu" class="form-control">
						</div>
					</div>

					<div class="feature">
						<div class="item">
							<div class="custom-checkbox">
								<input type="checkbox" name="save" id="save">
								<label for="save">Ghi nhớ mật khẩu</label>
							</div>
						</div>
						<div class="item">
							<a href="#">Bạn quên mật khẩu?</a>
						</div>
					</div>

					<div class="form-group button">
						<button type="submit" class="btn btn-block btn-custom">ĐĂNG NHẬP</button>
					</div>

					<div class="regiter text-center">Bạn chưa có tài khoản? <a href="#">Đăng ký ngay </a></div>
				</form>
			</div><!-- form -->
		</div>
	</div><!-- .login -->

	@component('element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/login/login.js"></script>
</body>
</html>
