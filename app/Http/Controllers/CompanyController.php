<?php

namespace App\Http\Controllers;

use App\City;
use App\CompanySize;
use App\Customer;
use App\CustomerAddress;
use App\CustomerIndustry;
use App\Events\CreatedCompany;
use App\Job;
use App\MemberCompany;
use App\MenuCompany;
use App\News;
use App\NewsLike;
use App\TagsJob;
use App\UserFollowCustomer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class CompanyController extends Controller
{
    private $news;
    private $job;
    private $userFollowCustomer;
    private $menuCompany;
    private $newsLike;
    private $tagsJob;
    private $customerIndustry;
    private $companySize;
    private $customer;
    private $memberCompany;
    private $customerAddress;
    private $city;

    public function __construct(News $news, Job $job, UserFollowCustomer $userFollowCustomer,
                                MenuCompany $menuCompany, NewsLike $newsLike, TagsJob $tagsJob,
                                CustomerIndustry $customerIndustry, CompanySize $companySize,
                                Customer $customer, MemberCompany $memberCompany, CustomerAddress $customerAddress,
                                    City $city)
    {
        $this->news = $news;
        $this->job = $job;
        $this->userFollowCustomer = $userFollowCustomer;
        $this->menuCompany = $menuCompany;
        $this->newsLike = $newsLike;
        $this->tagsJob = $tagsJob;
        $this->customerIndustry = $customerIndustry;
        $this->companySize = $companySize;
        $this->customer = $customer;
        $this->memberCompany = $memberCompany;
        $this->customerAddress = $customerAddress;
        $this->city = $city;

    }

    public function index(Customer $customer)
    {
        $customerId = $customer->id;
        $hotNews = $this->news->getManyWhereOrderLimit([
            'customer_id' => $customerId
        ], 3, array('*'), 'view_count desc');

        $jobs = $this->job->getManyWhereOrderLimitNumber([
            'customer_id' => $customerId
        ], 20);
        $menus = $this->menuCompany->getAll();
        $members = $this->memberCompany->getManyWhereOrderLimit(['customer_id' => $customerId], 4);
        $members->map(function ($member) {
            $member['news'] = $this->getNewsLatestForMember($member);
            return $member;
        });
        $customerCity = '';
        $citys = $customer->customerAddress()->pluck('city_id')->toArray();
        if (!empty($citys)) {
            $customerCity = $this->city->whereIn('id', $citys)->get()->implode('name', '|');
        }
        return view('company.index', compact('customer', 'hotNews', 'jobs',
            'menus', 'members', 'customerCity'));

    }

    public function getNewsLatestForMember($member)
    {
        $newItem = $this->news->where([
            'customer_id' => $member->customer_id,
            'user_id' => $member->user_id
        ])->latest()->first();
        return $newItem;
    }

    public function follow($id)
    {
        $customerFollow = $this->userFollowCustomer->getManyWhereOrderFirst([
            'customer_id' => $id,
            'user_id' => auth()->id()
        ]);

        if (!empty($customerFollow)) {
            // Update status
            $statusFlag = !$customerFollow->status;
            $this->userFollowCustomer->updateData($customerFollow->id, [
                'status' => $statusFlag
            ]);

        } else {
            // Create user follow customer
            $customerFollow = $this->userFollowCustomer->create([
                'customer_id' => $id,
                'user_id' => auth()->id()
            ]);
            $statusFlag = $customerFollow->status;
        }
        if (!empty($statusFlag)) {
            return response()->json([
                'message' => 'Trở thành fan',
                'user_follow_count' => \App\Components\Job::countUserFollowCustomer($id),
                'is_fan' => false
            ], 200);
        } else {
            return response()->json([
                'message' => 'Đã là fan',
                'user_follow_count' => \App\Components\Job::countUserFollowCustomer($id),
                'is_fan' => true
            ], 200);
        }
    }

    public function media(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        return view('company.build', compact('customer', 'menus'));
    }

    public function office(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        return view('company.build', compact('customer', 'menus'));
    }

    public function question(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        return view('company.build', compact('customer', 'menus'));
    }

    public function quiz(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        return view('company.build', compact('customer', 'menus'));
    }

    public function member(Customer $customer)
    {
        $customerId = $customer->id;
        $menus = $this->menuCompany->getAll();
        $members = $this->memberCompany->getManyWhereOrderLimit(['customer_id' => $customerId], 4);
        $members->map(function ($member) {
            $member['news'] = $this->getNewsLatestForMember($member);
            return $member;
        });
        $getAllMember = $this->memberCompany->getByMultiConditionsModel(['customer_id' => $customerId]);
        return view('company.members', compact('customer', 'menus', 'members', 'getAllMember'));
    }

    public function news(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        $news = $this->news->getManyWhereOrderPagination([
            'customer_id' => $customer->id
        ]);
        $hotNews = $this->news->getManyWhereOrderLimitNumber([
            'customer_id' => $customer->id,
            'hot' => true
        ]);
        return view('company.news', compact('customer', 'menus', 'news', 'hotNews'));
    }

    public function jobs(Customer $customer)
    {
        $menus = $this->menuCompany->getAll();
        $jobs = $this->job->getManyWhereOrderPagination([
            'customer_id' => $customer->id
        ]);
        $tags = $this->tagsJob->getAll();
        return view('company.jobs',
            compact('customer', 'menus', 'jobs', 'tags'));
    }

    public function likeNews(Request $request, $id)
    {
        $newInstance = $this->news->find($id);
        $statusLike = $newInstance->newLikes()->where([
            'user_id' => auth()->id()
        ])->first();
        // have like
        if (empty($statusLike)) {
            // create
            $this->newsLike->createData([
                'user_id' => auth()->id(),
                'news_id' => $id
            ]);
            $delete_flag = false;

        } else {
            // update delete_flag
            $delete_flag = !$statusLike->deleted_flag;
            $statusLike->update(['deleted_flag' => $delete_flag]);

        }
        if (empty($delete_flag)) {
            $newInstance->increment('total_like');
        } else {
            $newInstance->decrement('total_like');
        }

        $likeHtml = view('partials.like-news.button-like-heart', [
            'newItem' => $newInstance
            ])->render();
        if ($request->like_news_type) {
            $likeHtml = view('partials.like-news.button-like-heart-detail-new', [
                'newItem' => $newInstance
            ])->render();
        }
        return response()->json([
            'like_html' => $likeHtml
        ], 200);


    }

    public function userLikeNews($id)
    {
        $newInstance = $this->news->find($id);
        $statusLike = $newInstance->newLikes()->where([
            'user_id' => auth()->id()
        ])->first();
        // have like
        if (empty($statusLike)) {
            // create
            $this->newsLike->createData([
                'user_id' => auth()->id(),
                'news_id' => $id
            ]);
            $delete_flag = false;

        } else {
            // update delete_flag
            $delete_flag = !$statusLike->deleted_flag;
            $statusLike->update(['deleted_flag' => $delete_flag]);

        }
        if (empty($delete_flag)) {
            $newInstance->increment('total_like');
        } else {
            $newInstance->decrement('total_like');
        }

        $likeHtml = view('partials.button-like-heart-detail-new', [
            'newItem' => $this->news->find($id),
            'delete_flag' => $delete_flag
        ])->render();
        return response()->json([
            'like_html' => $likeHtml
        ], 200);


    }



    public function jobsTag(Customer $customer, $slugtag)
    {
        $menus = $this->menuCompany->getAll();
        $jobTag = $this->tagsJob->getManyWhereOrderFirst([
            'slug' => $slugtag
        ]);
        $tags = $this->tagsJob->getAll();
        $jobs = $this->job->getManyWhereOrderPagination([
            ['title', 'like', '%' . $jobTag->name . '%'],
            ['customer_id', '=', $customer->id]
        ]);
        return view('company.jobs', compact('menus', 'tags', 'jobs', 'customer', 'jobTag'));

    }

    public function create()
    {
        $industrys = $this->customerIndustry->getAll();
        $companySizes = $this->companySize->getAll();
        return view('company.create', compact('industrys', 'companySizes'));
    }

    public function postCreate(Request $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer->createData([
                'name' => $request->name,
                'website' => $request->website,
                'phone' => $request->phone,
                'company_size_id' => $request->company_size_id,
                'slogan' => $request->slogan,
                'user_id' => auth()->id(),
                'slug' => str_slug($request->name),
                'email' => $request->email
            ]);
            $customer->industrys()->attach($request->industry);
            session(['customer_id' => $customer->id]);
            $this->memberCompany->createData([
                'user_id' => auth()->id(),
                'customer_id' => $customer->id
            ]);
            event(new CreatedCompany($customer));

            DB::commit();
            return redirect()->route('enterprise.index', ['customerId' => $customer->id]);

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('message: ' . $e->getMessage() . 'Line: ' . $e->getLine());
        }

    }

    public function following()
    {
        $customersFollow = auth()->user()->followCustomers()->paginate(10);
        return view('company.following', compact('customersFollow'));
    }
}
