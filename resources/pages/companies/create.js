let inpTel = $('.formCreateCompany .inputTel');
let inpName = $('.formCreateCompany .inputName');
let inpEmail = $('.formCreateCompany .inputEmail');
let errTel = $('.formCreateCompany .err-tel');
let errName = $('.formCreateCompany .err-name');
let errEmail = $('.formCreateCompany .err-email');

function pageScrollTop() {
    $('html,body').animate({
        scrollTop: $(".company-create-page").offset().top
    }, 500);
}
function nameValid(e){
    e.preventDefault();
    $(this).removeClass('error');
    errName.text('').hide();
    if (e.target.validity.valueMissing){
        $(this).addClass("error");
        errName.text("Tên doanh nghiệp không được để trống!").show();
        pageScrollTop();
    }
}
function telValid(event){
    event.preventDefault();
    $(this).removeClass('error');
    errTel.text('').hide();
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        errTel.text("Số điện thoại không được để trống!").show();
        pageScrollTop();
    }
    if (event.target.validity.patternMismatch) {
        $(this).addClass("error");
        errTel.text("Vui lòng nhập đúng định dạng số điện thoại!").show();
        pageScrollTop();
    }
}
function emailValid(event){
    event.preventDefault();
    $(this).removeClass('error');
    errEmail.text('').hide();
    if (event.target.validity.valueMissing){
        $(this).addClass("error");
        errEmail.text("Email Công ty không được để trống!").show();
        pageScrollTop();
    }
    if (event.target.validity.typeMismatch) {
        $(this).addClass("error");
        errEmail.text("Vui lòng nhập đúng định dạng email!").show();
        pageScrollTop();
    }
}
$(function () {
    $('.select-2').select2();
    $('.select-2-multiple').select2({
        placeholder: "Chọn lĩnh vực hoạt động"
    });

    $(".select-2-nosearch").select2({
        minimumResultsForSearch: Infinity
    });
    inpTel.on('change', telValid);
    inpTel.on('invalid', telValid);
    inpName.on('change', nameValid);
    inpName.on('invalid', nameValid);
    inpEmail.on('change', emailValid);
    inpEmail.on('invalid', emailValid);
})

