function resize ($text) {
    $text.css({
        'height': 'auto',
        'resize': 'none',
        'overflow': 'hidden'
    });
    $text.css('height', $text[0].scrollHeight+'px');
}
export function textareaAutoHeight(){
    let textAutoHeight = $('textarea.autoheight');
    let textDisEnter = $('textarea.disenter');

    textAutoHeight.each(function(){
        $(this).attr('rows',1);
        resize($(this));
    });

    textAutoHeight.on('input', function(){
        resize($(this));
    });

    textDisEnter.on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $(this).blur();
        }
    });
}
