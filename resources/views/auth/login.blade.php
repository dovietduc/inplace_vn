@extends('layouts.login')

@section('meta')
    <title>Login - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link href="{{ asset('pages/users/login/login.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @include('partials.header-signin')

    <div class="login">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="sologan">
                        <h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
                        <div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex justify-content-center justify-content-lg-end">
                    <div class="form">
                        <div class="title text-center">XIN CHÀO</div>
                        <div class="txt text-center">Bạn chỉ còn một bước nữa để gặp team mơ ước của mình.</div>

                        <ul class="social clearfix">
                            @include('partials.button-social')
                        </ul>

                        <div class="or">HOẶC</div>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                @error('email')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                                @enderror
                                @error('password')
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                                @enderror

                                <input id="email"
                                       placeholder="Email của bạn"
                                       type="email"
                                       class="form-control"
                                       name="email"
                                       value="{{ old('email') }}"
                                       required
                                       autocomplete="email"
                                       autofocus
                                >
                            </div>

                            <div class="form-group">
                                <div class="input-password">
                                    <a href="#" class="js-displayPassword"><img src="{{ asset('images/icon-eye.png') }}"
                                                                                alt=""></a>
                                    <input id="password"
                                           type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password"
                                           placeholder="Mật khẩu"
                                           required
                                           autocomplete="current-password"
                                           minlength="8"
                                    >
                                </div>
                            </div>

                            <div class="feature">
                                <div class="item">
                                    <div class="custom-checkbox">
                                        <input class="form-check-input" type="checkbox"
                                               name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember">Ghi nhớ mật khẩu</label>
                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{ route('password.request') }}">Bạn quên mật khẩu?</a>
                                </div>
                            </div>

                            <div class="form-group button">
                                <button type="submit" class="btn btn-block btn-custom">ĐĂNG NHẬP</button>
                            </div>

                            <div class="regiter text-center">Bạn chưa có tài khoản?
                                <a href="{{ route('register') }}">
                                    Đăng ký ngay
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .login -->

    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/users/login/login.js') }}"></script>
@stop
