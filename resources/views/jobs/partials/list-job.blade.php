
<div class="block-post">
    @foreach($jobs as $jobItem)
        <div class="component-job-item-large">
            <div class="job-item-company">
                <a href="{{ route('companies.index', ['slug' => optional($jobItem->customer)->slug]) }}">
                    @if( jjobCheckImageExit(optional($jobItem->customer)->logo) )
                        <img src="{{ url(optional($jobItem->customer)->logo) }}" alt="" class="avatar">
                    @else
                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" alt="" class="avatar">
                    @endif
                </a>
                <div class="name">
                    <h5>
                        <a href="{{ route('companies.index', ['slug' => optional($jobItem->customer)->slug]) }}">
                            {{ optional($jobItem->customer)->name }}</a>
                    </h5>
                    @if(!empty(optional($jobItem->customer)->id))
                        <p>
                            {{ jobHelper::countUserFollowCustomer($jobItem->customer->id) }} Fan
                        </p>
                    @endif
                </div>
            </div>

            <div class="job-item-thumb">
                <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="">
                    @if( jjobCheckImageExit($jobItem->feature_image) )
                        <img class="w-100"
                             src="{{ asset($jobItem->feature_image) }}" alt="">
                    @else
                        <img src="{{ asset('images/no_img_631x247.jpg') }}" alt="" class="w-100">
                    @endif
                </a>
            </div>
            <div class="job-item-tag">

                @if($jobItem->hot)
                    <div class="tag-hot-job">
                        <span>Hot Job</span>
                    </div>
                @endif

                <div class="tag-list">
                    @if(!empty($jobItem->short_title))
                        <span class="tag-item">
                        {{ $jobItem->short_title }}
                    </span>
                    @endif
                </div>
            </div>

            <h3 class="job-item-title">
                <a href="{{ route('jobs.show', ['slug' => $jobItem->slug]) }}" title="">
                    {{ $jobItem->title }}
                </a>
            </h3>
            <div class="job-item-desc txt-format">
                {!! str_limit( strip_tags($jobItem->description), $limit = 400, $end = '...') !!}
            </div>
            <div class="job-item-action">
                <div class="job-item-locate">
                    <span class="mr-4">
                        <i class="far fa-map"></i>
                        {{ !empty($jobItem->citys) ? optional($jobItem->citys)->implode('name', ', ') : '' }}
                    </span>
                    <span>
                        <i class="far fa-clock"></i> {{ $jobItem->created_at->diffForHumans() }}
                    </span>
                </div>
                <div class="job-item-share">
                    @if(auth()->check())
                        @include('partials.button-save-job-icon')
                    @else
                        <a href="{{ route('register') }}"
                           class="icon-save-job">
                            <i class="far fa-bookmark text-grey-200"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div><!-- .post-item -->
    @endforeach


    <div class="block-pagination pagination-page-search">
        {{ $jobs->appends($_GET)->links('vendor.pagination.default') }}
    </div><!-- .block-pagination -->

</div><!-- .block-post -->
