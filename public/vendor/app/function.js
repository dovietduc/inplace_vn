var loading01 = '<img class="img-loading-01" src="/img/loadding.gif"/>';
var loading02 = '<img class="img-loading-02" src="/img/loadding.gif"/>';
var returnSuccess = 'success';

function getClassElement(className) {
    return "." + className;
}
function showErrorMessage(msg) {
    return '<em class="_error">' + msg + '</em>';
}
function showSuccessfullyMessage(msg) {
    return '<em class="_success">' + msg + '</em>';
}
function removeAllHtml(classElement) {
    $(classElement).each(function () {
        $(classElement).html('');
    });
}

function submitForm(classForm, classLoading, classReload, urlReload) {
    $('.btnSubmit').prop('disabled', true);
    $(classLoading).html(loading01);
    $(classForm).ajaxForm({
        target: classLoading,
        delegation: true,
        dataType: 'text',
//        dataType: 'script',
        success: function (result)
        {
            console.log('result:' + result);
            $('._error').html('');
            var data = $.parseJSON(result);
            if (data[0] === "error") {
                $.each(data[1], function (index, value) {
                    $(getClassElement(index)).html(showErrorMessage(value));
                });
            } else {
                if (urlReload === 'reload') {
                    location.reload();
                }
                loadPage(classReload, urlReload);
                $(".close").click()
            }
            $(classLoading).html('');
            $('.btnSubmit').prop('disabled', false);
        },
        error: function (result)
        {
            $('.btnSubmit').prop('disabled', false);
            $(classLoading).html(showErrorMessage('Lưu dữ liệu thất bại'));
        }
    }).submit();

}

function loadPage(elementLoad, url) {
    $(elementLoad).html(loading02);
    $.ajax({
        url: url,
        success: function (data) {
            $(elementLoad).html(data);
        },
        error: function (data) {
            //todo:
            $(elementLoad).html('Có lỗi xảy ra, vui lòng refresh lại trình duyệt và thử lại');
        }
    });
}

function deleteData(elementLoad, url) {
    $(elementLoad).html(loading01);
    $.ajax({
        url: url,
        success: function (data) {
            $(elementLoad).html(data);
        },
        error: function (data) {
            //todo:
            $(elementLoad).html('Có lỗi xảy ra, vui lòng refresh lại trình duyệt và thử lại');
        }
    });
}

function updateProfile(classForm, classLoading, classReload, type) {
    $(classLoading).html(loading01);
    var urlReload = '/profile?viewType=' + type;
    submitForm(classForm, classLoading, classReload, urlReload);
}

function getOptionLevelLanguage(_this, elementLoad, url) {
    url = url + '/' + $(_this).val();
    $.ajax({
        url: url,
        success: function (data) {
            $(elementLoad).html(data);
        },
        error: function (data) {
            //todo:
        }
    });
}


function saveLinkGithub(e) {
    e.preventDefault();
    var url = $('.link-git').data('link-github');
    $.ajax({
        url: url,
        data: { link_github : $('.get-value').val() },
        success: function (data) {
            $('.table-website tbody').html(data);
        },
        error: function (data) {

        }
    });
}


function editLinkGithub() {

    var $listEdit = $('.list-edit');
    $listEdit.hide();
    $listEdit.prev().prev().show();
    $listEdit.prev().prev().prev().show();
    $listEdit.prev().prev().prev().find('input').attr('value',  $('.wname-show').text());

}

function deleteLinkGithub() {
    bootbox.confirm({
        message: "Bạn thật sự muốn xóa không ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            url = $('.link-git').data('link-github');
            if (result) {
                $.ajax({
                    url: url,
                    data: {action: 'delete'},
                    success: function (data) {
                        $('td.wname-show').text('');
                    },
                    error: function (data) {

                    }
                });
            }
        }
    });

}

function changeFileType(element) {
    if($(element).val() == 1) {
        $('.text-upload input').prop('disabled', false);
        $('.text-upload textarea').prop('disabled', false);
    } else {
        $('.text-upload input').prop('disabled', true);
        $('.text-upload textarea').prop('disabled', true);
        $('.description').html('');
        $('.title').html('');
    }

}