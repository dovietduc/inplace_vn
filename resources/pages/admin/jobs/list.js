import {adminLayout} from '../adminLayout.js';
import swal from "sweetalert";

const axios = require('axios');

function activeHotJob() {
    let isChecked = $(this).is(":checked") ? true : false;
    let url = $(this).data('url');
    axios.post(url, {
        isChecked: isChecked
    })
        .then(function (response) {

        })
        .catch(function (error) {
            console.log(error);
        })
}

function activeShowSliderJobHome() {
    let isChecked = $(this).is(":checked") ? true : false;
    let url = $(this).data('url');
    axios.post(url, {
        isChecked: isChecked
    })
        .then(function (response) {

        })
        .catch(function (error) {
            console.log(error);
        })
}

function pushTop() {
    let btn = $(this);
    let url = $(this).data('url');
    let titleJob = btn.parents('.dashboard-jobs-item').find('.title a').text().trim();
    swal({
        icon: "warning",
        title: "Bạn muốn đẩy top Job này?",
        text: titleJob,
        buttons: {
            cancel: {
                text: "Hủy",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Đẩy Top",
                value: true,
                visible: true,
                className: "btn-custom",
                closeModal: true
            }
        }
    }).then(function (result) {
        if (result) {
            axios.post(url)
                .then(function (response) {
                    if (response.status == 200) {
                        swal({
                            icon: "success",
                            title: "Job này đã được đẩy Top!",
                            text: titleJob,
                            buttons: {
                                confirm: {
                                    text: "OK",
                                    value: true,
                                    visible: true,
                                    className: "btn-custom",
                                    closeModal: true
                                }
                            }
                        })
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    });
}

function selectStatusJob() {
    let url = $(this).data('url');
    let active = $(this).val();
    axios.post(url, {
        active: active
    })
        .then(function (response) {
            if (response.status == 200) {
                swal({
                    icon: "success",
                    title: "Cập nhật trạng thái thành công!",
                    buttons: {
                        confirm: {
                            text: "OK",
                            value: true,
                            visible: true,
                            className: "btn-custom",
                            closeModal: true
                        }
                    }
                })
            }
        })
        .catch(function (error) {
            swal({
                icon: "error",
                title: "Có lỗi!",
                buttons: {
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn-custom",
                        closeModal: true
                    }
                }
            })
        })
}

$(function () {
    adminLayout();
    $(document).on('change', '.select-status-job', selectStatusJob);
    $(document).on('click', '.btn-push-top', pushTop);
    $(document).on('change', '.input_hot_job_aceept', activeHotJob);
    $(document).on('change', '.input_slider_job_aceept', activeShowSliderJobHome);
});
