@if(empty(optional($member->user)->avatar))
    <img class="member-avatar" src="{{asset('images/no_user_65x65.jpg')}}" alt="">
@else
    @if(str_contains(optional($member->user)->avatar, 'upload') && jjobCheckImageExit(optional($member->user)->avatar))
        <img class="member-avatar" src="{{ asset(optional($member->user)->avatar) }}" alt="">
    @else
        <img class="member-avatar" src="{{ optional($member->user)->avatar }}" alt="">
    @endif

@endif
