<?php

namespace App;


use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobAdmin extends Model
{
    use BaseEloquent, SoftDeletes;
    protected $table = 'jjob_jobs';
    protected $guarded = [];
    const paginations = 15;
    const statusDraft = 'draft';
    const statusPublic = 'public';
    const active = 1;
    const rejectActive = 2;
    const waitting = 3;

    protected static function boot()
    {
        parent::boot();
    }

    public function scopePublic($query)
    {
        return $query->where('jjob_jobs.status', Job::statusPublic);
    }

    public function scopeActive($query)
    {
        return $query->where('jjob_jobs.active', Job::active);
    }

    public function scopePopular($query)
    {
        return $query->where('jjob_jobs.status', Job::statusPublic)
            ->where('jjob_jobs.active', Job::active);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function citys()
    {
        return $this->belongsToMany(City::class, 'jjob_job_cities', 'job_id', 'city_id');
    }

    public function userSaveJob()
    {
        return $this->hasMany(SaveJob::class, 'job_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function customerIndustry()
    {
        return $this->belongsToMany(Industry::class, 'jjob_job_industries',
            'job_id', 'industry_id');
    }

    public function jobLevel()
    {
        return $this->belongsTo(LevelJob::class, 'job_level_id');
    }

    public function salary()
    {
        return $this->belongsTo(Salary::class, 'salary_id');
    }

    public function jobViews()
    {
        return $this->hasMany(JobView::class, 'job_id');
    }

    public function tagsJob()
    {
        return $this->belongsToMany(TagsOfJob::class, 'jjob_job_tags', 'job_id', 'tag_id');
    }

    public function membersCompany()
    {
        return $this->belongsToMany(MemberCompany::class, 'job_middle_member_company', 'job_id', 'user_add_member_id');
    }

    public function levelEnglish()
    {
        return $this->belongsTo(LanguageLevelCommon::class, 'english_level_id');
    }

    public function emailApplyJobs()
    {
        return $this->hasMany(EmailApplyJobs::class, 'job_id');
    }

    public function occupationMain() {
        return $this->belongsTo(Occupation::class, 'occupation_main');
    }
    public function occupationSupporting() {
        return $this->belongsTo(Occupation::class, 'occupation_supporting');
    }
    public function occupationSupportingDetail() {
        return $this->belongsTo(PositionOccupation::class, 'occupation_supporting_detail');
    }

    // Model for admin
    public function searchJobsAdmin($request)
    {
        $dataReturn = new JobAdmin();
        $dataReturn = $dataReturn
            ->join('jjob_customers', 'jjob_jobs.customer_id', 'jjob_customers.id');
        if (!empty($request->id_job)) {
            $dataReturn = $dataReturn->where('jjob_jobs.id', $request->id_job);
        }
        if (!empty($request->name_job)) {
            $dataReturn = $dataReturn->where('jjob_jobs.title', 'like', '%' . $request->name_job . '%');
        }
        if (!empty($request->id_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.id', $request->id_customer);
        }
        if (!empty($request->name_customer)) {
            $dataReturn = $dataReturn->where('jjob_customers.name', 'like', '%' . $request->name_customer . '%');
        }
        $dataReturn = $dataReturn
            ->latest('jjob_jobs.created_at')
            ->select('jjob_jobs.*')
            ->paginate(JobAdmin::paginations);

        return $dataReturn;
    }

}
