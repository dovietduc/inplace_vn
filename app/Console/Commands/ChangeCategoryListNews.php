<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ChangeCategoryListNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:name-category-list-news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change name category list news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('jjob_customer_news_categories')->truncate();
        $data = [
            [
                'name' => 'Gợi ý của InPlace',
                'id' => 1
            ],
            [
                'name' => 'Công ty',
                'id' => 4
            ],
            [
                'name' => 'Văn hóa',
                'id' => 2
            ],
            [
                'name' => 'Phỏng vấn nhân viên',
                'id' => 5
            ],
            [
                'name' => 'WorkStyle',
                'id' => 13
            ],
            [
                'name' => 'Chuyện nghề',
                'id' => 3
            ],
            [
                'name' => 'Nhà Tuyển dụng',
                'id' => 7
            ],
            [
                'name' => 'Sự kiện',
                'id' => 8
            ],
            [
                'name' => 'Tech',
                'id' => 6
            ],
            [
                'name' => 'Nhà sáng lập',
                'id' => 10
            ],
            [
                'name' => 'Startup',
                'id' => 11
            ],
            [
                'name' => 'Văn phòng ảo',
                'id' => 12
            ],
            [
                'name' => 'Khác',
                'id' => 14
            ],
        ];

        foreach ($data as $dataItem) {
            $dataItem['slug'] = str_slug($dataItem['name']);
            \DB::table('jjob_customer_news_categories')->insert($dataItem);

        }
    }
}
