import swal from 'sweetalert';
import {dashboardLayout} from '../dashboard.js';
import {textareaAutoHeight} from '../../_elements/textareaAutoHeight.js';
import {readFileImageCroppie} from '../../_elements/inputCroppieImage.js';
import {textareaFroalaEditorInline} from '../../_elements/textareaFroalaEditorInline.js';


function checkValueEmpty(){
    let cateVal = $('.addNew-category').val();
    let titleVal = $('.addNew-title').val();
    if (cateVal && titleVal && ($('.inputCroppieImage').hasClass('hasImg') || $('.upload-crop-demo').html().trim().length)){
        $('.button-public-news').removeAttr('disabled');
    }else {
        $('.button-public-news').attr('disabled', true);
    }
}

$(function () {
    dashboardLayout();
    // on change show preview image and move crop before upload
    readFileImageCroppie();
    textareaAutoHeight();
    $('.fixHeight').each(function () {
        var a = $(this);
        var h = a.width() * 435 / 1110;
        a.height(h);
        $(window).resize(function () {
            a.height(a.width() * 435 / 1110);
        })
    });
    $('.js-select2').select2({
        placeholderText: "Chọn danh mục"
    });
    //froala editor
    textareaFroalaEditorInline();
    //END: froala editor
    $(document).on('change', '.addNew-category', checkValueEmpty);
    $(document).on('keyup', '.addNew-title', checkValueEmpty);
    $(document).on('change', '.croppieImage_input', checkValueEmpty);
    $(document).on('click', '.btnRemoveCrop', checkValueEmpty);
});



