<div class="row content-new-detail">
    <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
        <h1 class="new-detail-title">
            {{ $newItem->title }}
        </h1>
        <div class="new-detail-auth">
            <div class="new-detail-auth-info">
                <a href="#" title="" class="auth-avatar">
                    @if( jjobCheckImageExit(optional($newItem->user)->avatar) )
                        <img src="{{ url(optional($newItem->user)->avatar) }}" alt="">
                    @else
                        <img src="{{ asset('/images/no_user_50x50.jpg') }}" alt="">
                    @endif
                </a>
                <div class="auth-name">
                    <h3><a href="#" title="">{{ optional($newItem->user)->name }}</a></h3>
                    <span class="">{{ $newItem->created_at->diffForHumans() }}</span>
                </div>
            </div>
            <div class="new-detail-auth-activity posts-item-auth_like">
                <span class="like-count">
                    {{ $newItem->view_count }} lượt xem
                </span>
                <div style="display: inline-block" class="like_wrapper">
                    @if(auth()->check())
                        @include('partials.like-news.button-like-heart-detail-new')
                    @else
                        <a href="{{ route('register') }}"
                           class="btn-like-heart">
                            <i class="far fa-heart"></i>
                        </a>
                    @endif
                </div>

                <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}"
                   target="_blank"
                   title=""
                   class="btn-share-icon share_facebook_social">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </div>
        </div>
        <div class="new-detail-text txt-format text-justify fr-view">
            {!! $newItem->content !!}
        </div>

        @include('news.partials.job-relative-new')
        <div class="new-detail-auth justify-content-end">
            <div class="new-detail-auth-activity posts-item-auth_like">
                <div style="display: inline-block" class="like_wrapper">
                    @if(auth()->check())
                        @include('partials.like-news.button-like-heart-detail-new')
                    @else
                        <a href="{{ route('register') }}"
                           class="btn-like-heart">
                            <i class="far fa-heart"></i>
                        </a>
                    @endif
                </div>
                <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}"
                   target="_blank"
                   class="btn-share-icon share_facebook_social">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </div>
        </div>
        <div class="article-auth-intro">
            <div class="article-auth-intro_item">
                <div class="row">
                    <div class="col-md-9">
                        <div class="auth-intro-content">
                            <a href="#" title="" class="auth-intro-thumb">
                                <img src="{{ asset('/images/no_avatar_company_80x80.jpg') }}" alt="">
                            </a>
                            <div class="auth-intro-text">
                                <h3 class="title">
                                    <a href="#" title="">
                                        {{ optional($newItem->customer)->name }}
                                    </a>
                                </h3>
                                <div class="desc">
                                    {!! str_limit( strip_tags($newItem->description), $limit = 300, $end = '...') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="col-md-3 text-right d-flex d-md-block justify-content-end align-items-center mt-2 mt-md-0">

                        @include('partials.follows-customer.news-detail')

                        <div class="auth-intro-sub">
                            <span class="label_count_user_follow">
                                {{ jobHelper::countUserFollowCustomer($customer->id) }} Fan
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
