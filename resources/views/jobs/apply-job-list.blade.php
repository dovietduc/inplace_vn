@extends('layouts.app')
@section('meta')
    <title>Công việc ứng tuyển</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/jobs/save-jobs/save-jobs.css')}}">
@stop
@section('content')
    <div class="save-jobs-page">
        <div class="container">
            <div class="component-title">
                <h3 class="title">Công việc ứng tuyển</h3>
                <div class="desc">Bạn có {{ auth()->user()->applyJobs()
                ->whereNull('jjob_jobs.deleted_at')->count() }} công việc ứng tuyển</div>
            </div>
            <div class="row">
                @include('jobs.partials.jobs-apply')
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/jobs/save-jobs/save-jobs.js')}}"></script>
@stop
