<?php

namespace App\Console\Commands;

use App\LevelJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class InsertMoreLevelJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:more-level-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert more level jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataLevelMore = [
            1 => 'Cộng tác viên',
            2 => 'Thực tập sinh',
            3 => 'Mới tốt nghiệp',
            5 => 'Chuyên viên cao cấp',
            6 => 'Trưởng nhóm/Giám sát',
            8 => 'Trưởng chi nhánh'
        ];
        try {
            DB::beginTransaction();

            foreach ($dataLevelMore as $key => $levelItem) {
                LevelJob::create([
                    'order_id' => $key,
                    'name' => $levelItem
                ]);
            }

            DB::commit();
            $this->info('Convert data industry customer complete');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
