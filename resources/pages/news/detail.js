const axios = require('axios');

function toogleLikeNewsDetail() {
    let url = $(this).data('url');
    axios.get(url, {
        params: {
            like_news_type: 'news_detail_screen'
        }
    })
        .then(function (response) {
            $('.like_wrapper').html(response.data.like_html);
        })
        .catch(function (error) {
            console.log(error);
        });
}

function renderHtmlFollow(data) {
    return `<span class="${data.message=='Đã là fan' ? 'active' : ''}">${data.message}</span>`;
}

function followCustomerNewsDetail() {
    let url = $(this).data('url');
    axios.get(url, {
        params: {
            follow_type: 'news_detail_screen'
        }
    })
        .then(function (response) {
            $('.folow_button_wrapper').html(renderHtmlFollow(response.data));
            if ( $( ".label_count_user_follow" ).length ) {
                $('.label_count_user_follow').html(response.data.user_follow_count + ' Fan');
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}

$(function () {
    $(document).on('click','.toogle_like_button_news_detail', toogleLikeNewsDetail);
    $(document).on('click','.follow_customer_news_detail', followCustomerNewsDetail);
});
