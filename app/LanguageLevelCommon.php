<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;

class LanguageLevelCommon extends Model
{
    use BaseEloquent;
    protected $table = 'language_level_common';
    protected $guarded = [];
}
