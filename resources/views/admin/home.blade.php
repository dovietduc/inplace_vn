@extends('layouts.admin')
@section('meta')
    <title>Admin - Tổng quan</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/admin/home.css')}}">
@stop
@section('content')
    <div class="admin-home-page">
        <div class="container">
            <h1>Tổng quan</h1>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('pages/admin/home.js')}}"></script>
@stop
