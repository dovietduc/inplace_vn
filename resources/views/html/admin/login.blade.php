
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quản trị hệ thống</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-v4/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/font-awesome-v5/css/all.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset('pages/admin/login.css')}}">
</head>
<body>
<div class="container hero">
    <div class="inner">
        <img class="logo" src="{{asset('images/logo_short_w80.png')}}" alt="">
        <h1>Đăng nhập quản trị</h1>
        <form action="" method="POST">
            <div class="form-group">
                <label>Email:</label>
                <input type="email" class="form-control" placeholder="Nhập email" required>
            </div>
            <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" placeholder="Nhập mật khẩu" required>
            </div>
            <button type="submit" class="btn-submit btn btn-custom mt-3">Đăng nhập</button>
            <div class="mt-2"><a href="/" class="link-comback-home"><i class="fal fa-long-arrow-left mr-2"></i>Quay lại trang chủ</a></div>
        </form>
    </div>
</div>
<script src="{{ asset('vendor/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('pages/admin/login.js') }}"></script>
</body>
</html>
