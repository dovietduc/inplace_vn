<?php

Route::prefix('admin')->group(function () {

    Route::get('/login/inplace-TKcI1sovoSmqmku', [
        'as' => 'admin.login',
        'uses' => 'Admin\AdminController@login',
    ]);

    Route::post('/login/inplace-TKcI1sovoSmqmku', [
        'as' => 'admin.postLogin',
        'uses' => 'Admin\AdminController@postLogin',
    ]);

    // Create middleware for system admin
    Route::group(['middleware' => ['admin']], function(){
        Route::get('/home', [
            'as' => 'admin.home',
            'uses' => 'Admin\AdminController@home'
        ]);

        Route::prefix('job')->group(function () {
            Route::get('/list', [
                'as' => 'admin.job.list',
                'uses' => 'Admin\JobController@list'
            ]);
            Route::post('/ajax/change-active-job/{id}', [
                'as' => 'admin.job.changeActiveJob',
                'uses' => 'Admin\JobController@changeActiveJob'
            ]);
            Route::post('/ajax/change-hot-job/{id}', [
                'as' => 'admin.job.changeHotJob',
                'uses' => 'Admin\JobController@changeHotJob'
            ]);
            Route::post('/ajax/change-show-slider-job-home/{id}', [
                'as' => 'admin.job.changeShowSliderJobHome',
                'uses' => 'Admin\JobController@changeShowSliderJobHome'
            ]);
            Route::post('/ajax/push-top/{id}', [
                'as' => 'admin.job.pushTop',
                'uses' => 'Admin\JobController@pushTop'
            ]);

        });

        Route::prefix('news')->group(function () {
            Route::get('/list', [
                'as' => 'admin.news.list',
                'uses' => 'Admin\NewsController@list'
            ]);
            Route::post('/ajax/change-active-news/{id}', [
                'as' => 'admin.news.changeActiveNews',
                'uses' => 'Admin\NewsController@changeActiveNews'
            ]);
        });

        Route::prefix('companies')->group(function () {
            Route::get('/update', [
                'as' => 'admin.companies.update',
                'uses' => 'Admin\CompanyController@update'
            ]);
            Route::post('/update', [
                'as' => 'admin.companies.postUpdate',
                'uses' => 'Admin\CompanyController@postUpdate'
            ]);

        });

    });


});


