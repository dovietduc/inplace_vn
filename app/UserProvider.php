<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProvider extends Model
{
    protected $table = 'jjob_user_providers';
    protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
