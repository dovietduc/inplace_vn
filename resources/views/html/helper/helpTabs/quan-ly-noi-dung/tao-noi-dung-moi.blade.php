<div class="txt-format">
    <h1>Hướng dẫn Tạo Nội dung mới - Trang Cá nhân</h1>
    <p>Trang cá nhân giống như bộ mặt của bạn và là ngôi nhà của
        riêng bạn, khi bạn tạo ra khác biệt với mọi người thì mới có thể gây sự chú ý
        đến nhà tuyển dụng. Việc tạo ra những bài viết ấn tượng trên trang cá nhân là
        cách giúp phô diễn kiến thức, kỹ năng chuyên môn, sự chuyên nghiệp, tính cách,
        quan điểm sống và phong cách riêng của mình. Điều này sẽ đem lại nhiều lợi ích
        giúp bạn có cơ hội phát triển nghề nghiệp (được nhà tuyển dụng chú ý đến) mà còn
        là dịp luyện được thói quen chuyên môn tốt.</p>
    <p>Để tạo bài viết mới, bạn vui lòng tạo tài khoản cá nhân theo: <a href="javascript:;"
                                                                        onclick="$('#tao-trang-ca-nhan-moi-tab').click()">Hướng
            dẫn tạo trang
            cá nhân mới</a>.</p>
    <p>Sau khi tạo tài khoản cá nhân thành công, bạn thao tác theo 1 trong 2 cách dưới
        đây để tạo bài viết mới:</p>
    <h3>Cách 1: Thao tác tại trang Profile cá nhân của mình qua thanh công cụ</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_moi.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào Bài Viết</li>
                <li><strong>Bước 2:</strong> Chọn Tạo bài vết mới. Sau đó, điền đầy đủ các
                    nội dung liên quan đến
                    bài viết như tiêu đề, nội dung, lĩnh vực,…
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất bài viết bằng cách nhấp vào ô Đăng
                    Bài.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 2: Thao tác trực tiếp trên thành Head Menu</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_moi_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vàoViết Bài trên thanh Menu ngang đầu màn
                    hình
                </li>
                <li><strong>Bước 2:</strong> Form tạo bài viết mới sẽ hiện lên. Bạn hãy điền
                    đầy đủ các nội dung
                    liên quan đến bài viết như tiêu đề, nội dung, lĩnh vực,…
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất bài viết bằng cách nhấp vào ô đăng
                    bài.
                </li>
            </ul>
        </div>
    </div>
    <p><br></p>
    <h1>Hướng dẫn Tạo Nội dung mới - Trang Doanh nghiệp</h1>
    <p>Để tạo bài viết, bạn vui lòng tạo tài khoản doanh nghiệp theo
        <a href="javascript:;" onclick="$('#tao-trang-doanh-nghiep-moi-tab').click();">Hướng dẫn tạo trang doanh nghiệp mới</a>.</p>
    <p>Sau khi tạo trang doanh nghiệp thành công, di chuyển đến trang doanh nghiệp mới
        và thao tác theo 1 trong 2 cách dưới đây để tạo bài viết mới:</p>
    <h3>Cách 1: Thao tác với mục bài viết trên thanh công cụ tại trang Profile Doanh
        Nghiệp của mình</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Nhấp vào mục <strong>Bài Viết</strong></li>
                <li><strong>Bước 2:</strong> Chọn <strong>Tạo Bài Viết Mới</strong>. Sau đó,
                    điền đầy đủ các nội dung liên quan đến bài viết như tiêu đề, nội dung,
                    mô tả tính chất bài viết
                    v.v..
                </li>
                <li><strong>Bước 3:</strong> Hoàn tất bài viết bằng cách nhấp vào ô <strong>Đăng
                        Bài</strong>.
                </li>
            </ul>
        </div>
    </div>

    <h3>Cách 2: Thao tác trực tiếp tại công cụ <strong>Quản Lý Bài Viết</strong> trong
        công cụ <strong>Admin</strong> của Doanh Nghiệp</h3>
    <div class="row">
        <div class="col-sm-5 mb-3">
            <img src="{{asset('images/img_tao_bai_viet_2.jpg')}}" alt="">
        </div>
        <div class="col-sm-7 mb-3">
            <ul>
                <li><strong>Bước 1:</strong> Truy cập vào công cụ Admin trang Doanh Nghiệp
                </li>
                <li><strong>Bước 2:</strong> Chọn tab Quản Lý <strong>Bài Viết</strong> ở
                    cột
                    menu bên tay trái
                </li>
                <li><strong>Bước 3:</strong> Click vào <strong>Tạo Bài Viết Mới</strong> ở
                    góc
                    phải màn hình
                </li>
            </ul>
        </div>
    </div>
</div>
