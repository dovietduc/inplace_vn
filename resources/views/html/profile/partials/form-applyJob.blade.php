<form class="form-ApplyJob" method="POST" enctype="multipart/form-data" id="form_applyJob">
    <p class="mb-3">Ứng tuyển nhanh hơn khi bạn upload sẵn File CV. Hồ sơ sẽ được tự động gửi đến cho Nhà
        tuyển dụng.</p>
    <div class="frm-apply__box">
        <div class="frm-apply__box-row">
            <div class="left">
                <label for="">Email</label>
            </div>
            <div class="right">
                <input type="text"
                       class="form-control"
                       value="{{ auth()->user()->email }}"
                       disabled="disabled"
                       placeholder="Email ứng tuyển"/>
            </div>
        </div>
        <div class="frm-apply__box-row">
            <div class="left">
                <label for="">Họ tên</label>
            </div>
            <div class="right">
                <input type="text"
                       class="inp-name form-control"
                       value="{{ auth()->user()->name }}"
                       placeholder="Họ tên"
                       required
                />
                <span class="err-name inp-err"></span>
            </div>
        </div>
        <div class="frm-apply__box-row">
            <div class="left">
                <label for="">Điện thoại</label>
            </div>
            <div class="right">
                <input type="tel"
                       class="inp-tel form-control"
                       value=""
                       placeholder="Số điện thoại"
                       maxlength="25"
                       name="tel"
                       value="{{ auth()->user()->mobile  }}"
                       required pattern="(?=(.*\d){4})[\s\d\/\+\-\(\)\[\]\.]+"/>
                <span class="err-tel inp-err"></span>
            </div>
        </div>
        <div class="frm-apply__box-row">
            <div class="left">
                <label for="">CV</label>
            </div>
            <div class="right">
                <div class="btn-UpFile apply-inp-btnFile file_upload-choose">
                    <label class="btn btn-custom_light" for=""><span>Upload CV</span></label>
                    <div class="hasFile">
                        <div class="hasFile_around">
                            <div class="icon-file">
                                <img src="" alt="">
                            </div>
                            <div class="hasFile_info">
                                <div class="info_left">
                                    <h6 class="name">Upload File</h6>
                                    <p class="alert-done">Tải lên hoàn tất</p>
                                </div>
                                <div class="info_right">
                                    <span class="size">.Kb</span>
                                    <i class="del-file far fa-trash-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input class="apply-inp-file"
                           name="file_upload"
                           type="file"
                           accept="application/msword, application/pdf, .docx, .doc, .pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                </div>
                <span class="apply-err-file inp-err file_upload-error"></span>
                <div class="note">Vui lòng tải lên file định dạng .doc .docx .pdf dưới 2MB</div>
            </div>
        </div>
        <div class="frm-apply__box-row">
            <div class="left"></div>
            <div class="right">
                <div class="group-button-form">
                    <button type="submit" class="form-ApplyJob_btnSubmit btn btn-custom">Lưu thay đổi</button>
                    <button type="reset" class="btn btn-close">Bỏ qua</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div id="frm-apply_success"
     class="alert-form-success modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="far fa-times-circle"></i>
                </button>
                <div class="icon-success"><i class="fal fa-check"></i></div>
                <h2 class="title">Xin chúc mừng</h2>
                <div class="desc">Hồ sơ của bạn đã được lưu thay đổi thành công!</div>
            </div>
        </div>
    </div>
</div>
