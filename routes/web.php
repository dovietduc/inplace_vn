<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'wellcome',
    'uses' => 'Auth\RegisterController@showRegistrationFormHome',
    'middleware' => 'guest'
]);


Auth::routes(['verify' => true]);
Route::get('/email/verify-acount/{id}', 'Auth\LoginController@validateAcount')->name('validateAcount');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/list-job', 'HomeController@listJob')->name('listJob');
Route::get('/social-get-email', 'SocialController@fillEmail')->name('socialGetEmail');
Route::post('/social-get-email', 'SocialController@socialGetEmail');
Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
Route::get('/user/profile/edit/{id?}', [
    'as' => 'profile.index',
    'uses' => 'ProfileController@index',
    'middleware' => 'auth'
]);


Route::prefix('profile')->group(function () {
    Route::prefix('croppie')->group(function () {
        Route::post('/upload-image-avatar', [
            'as' => 'croppie.upload-image-avatar',
            'uses' => 'ProfileController@uploadImageAvatar',
        ]);
        Route::post('/upload-image-cover', [
            'as' => 'croppie.upload-image-cover',
            'uses' => 'ProfileController@uploadImageCover',
        ]);
    });
    Route::post('apply-job', [
        'as' => 'profile.apply-job',
        'uses' => 'ProfileController@applyJob',
    ]);

});


Route::prefix('jobs')->group(function () {

    Route::get('/', [
        'as' => 'jobs.index',
        'uses' => 'JobController@index',
    ]);
    Route::get('search/ajax', [
        'as' => 'jobs.search',
        'uses' => 'HomeController@index',
    ]);
    Route::get('search/typehead/follow-name-customer-or-name-jobs', [
        'as' => 'jobs.searchJobTypeheadCustomer',
        'uses' => 'JobController@searchTypeHeadFollowNameCustomerOrNameJob',
    ]);
    Route::post('save/{id}', [
        'as' => 'jobs.save',
        'uses' => 'JobController@save',
    ]);
    Route::get('/show/{job}', [
        'as' => 'jobs.show',
        'uses' => 'JobController@show',
        'middleware' => ['viewscount']
    ]);

    Route::post('/apply-job/{job}', [
        'as' => 'jobs.applyJob',
        'uses' => 'JobController@applyJob',
    ]);
    Route::get('/show-list-save-job', [
        'as' => 'jobs.show-list-save-job',
        'uses' => 'JobController@showListSaveJob',
    ]);
    Route::post('/remove-save-job-item/{job}', [
        'as' => 'jobs.remove-save-job-item',
        'uses' => 'JobController@removeSaveJobItem',
    ]);
    Route::get('/show-list-apply-job', [
        'as' => 'jobs.show-list-apply-job',
        'uses' => 'JobController@showListApplyJob',
    ]);

    Route::post('ajax/occupation-update-user', [
        'as' => 'jobs.ajax.occupation-update-user',
        'uses' => 'JobController@occupationUpdateUser',
    ]);

});


Route::prefix('companies')->group(function () {
    Route::get('/create/customer', [
        'as' => 'companies.create',
        'uses' => 'CompanyController@create',
        'middleware' => 'auth'
    ]);

    Route::post('/post-create/customer', [
        'as' => 'company.postCreate',
        'uses' => 'CompanyController@postCreate',
        'middleware' => 'auth'
    ]);

    Route::get('/{customer}', [
        'as' => 'companies.index',
        'uses' => 'CompanyController@index',
    ]);
    Route::get('/company_followings/{id}', [
        'as' => 'companies.follow',
        'uses' => 'CompanyController@follow',
    ]);
    Route::get('/{customer}/media', [
        'as' => 'companies.media',
        'uses' => 'CompanyController@media',
    ]);
    Route::get('/{customer}/office', [
        'as' => 'companies.office',
        'uses' => 'CompanyController@office',
    ]);
    Route::get('/{customer}/question', [
        'as' => 'companies.question',
        'uses' => 'CompanyController@question',
    ]);
    Route::get('/{customer}/quiz', [
        'as' => 'companies.quiz',
        'uses' => 'CompanyController@quiz',
    ]);
    Route::get('/{customer}/member', [
        'as' => 'companies.member',
        'uses' => 'CompanyController@member',
    ]);
    Route::get('/{customer}/news', [
        'as' => 'companies.news',
        'uses' => 'CompanyController@news',
    ]);
    Route::get('/{customer}/jobs', [
        'as' => 'companies.jobs',
        'uses' => 'CompanyController@jobs',
    ]);
    Route::get('/news/like/{id}', [
        'as' => 'companies.news.like',
        'uses' => 'CompanyController@likeNews',
    ]);
    Route::get('/{customer}/jobs/{slugtag}', [
        'as' => 'companies.jobs.tag',
        'uses' => 'CompanyController@jobsTag',
    ]);

    Route::get('user-is/following', [
        'as' => 'companies.following',
        'uses' => 'CompanyController@following',
    ]);


});

Route::get('/{slugCompany}/articles/{id}', [
    'as' => 'news.show',
    'uses' => 'NewsController@show'
]);
Route::get('/list', [
    'as' => 'news.index',
    'uses' => 'NewsController@index',
    'middleware' => ['verified']

]);
Route::get('/category/{slug}', [
    'as' => 'news.category',
    'uses' => 'NewsController@category'
]);

















