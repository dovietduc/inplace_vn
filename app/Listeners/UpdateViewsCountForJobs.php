<?php

namespace App\Listeners;

use App\Events\PageJobsShowLoad;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Session\Store;

class UpdateViewsCountForJobs
{
    private $session;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle the event.
     *
     * @param  PageJobsShowLoad  $event
     * @return void
     */
    public function handle(PageJobsShowLoad $event)
    {
        $job = $event->job;
        if ( !$this->isJobViewed($job) )
        {
            $job->increment('view_count');
            $this->storePost($job);
        }
    }
    private function isJobViewed($job)
    {
        $viewed = $this->session->get('viewed_jobs', []);
        return array_key_exists($job->id, $viewed);
    }

    private function storePost($job)
    {
        $key = 'viewed_jobs.' . $job->id;
        $this->session->put($key, time());

    }
}
