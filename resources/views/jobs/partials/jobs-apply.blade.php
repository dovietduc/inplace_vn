@foreach($jobsApplys as $jobsApply)
    <div class="col-sm-6 col-lg-4 item-save-job">
        <div class="component-job-item-save">
            <a href="{{ route('jobs.show', ['slug' => $jobsApply->slug]) }}" class="job-item-thumb">

                @if( jjobCheckImageExit($jobsApply->feature_image) )
                    <img
                        src="{{ asset($jobsApply->feature_image) }}" alt="">
                @else
                    <img src="{{ asset('images/no_img_350x137.jpg') }}" alt="" >
                @endif

            </a>
            <div class="job-item-content">
                <h3 class="title">
                    <a href="{{ route('jobs.show', ['slug' => $jobsApply->slug]) }}" title="">
                        {{ $jobsApply->title }}
                    </a>
                </h3>
                <div class="desc">
                    {!! str_limit( strip_tags($jobsApply->description), $limit = 300, $end = '...') !!}
                </div>
            </div>
            <div class="job-item-company">
                <a class="avatar" href="{{ route('companies.index', ['slug' => optional($jobsApply->customer)->slug]) }}">
                    @if( jjobCheckImageExit(optional($jobsApply->customer)->logo) )
                        <img src="{{ url(optional($jobsApply->customer)->logo) }}" alt="" class="avatar">
                    @else
                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" alt="" class="avatar">
                    @endif

                </a>
                <h4 class="name">{{ optional($jobsApply->customer)->name }}</h4>
            </div>
            <button
                data-url="{{ route('jobs.remove-save-job-item', ['slug' => $jobsApply->slug]) }}"
                class="job-item_btn-delete">
                <i class="fal fa-times"></i>
            </button>
        </div>
    </div>

@endforeach
<div class="col-12">
    {{ $jobsApplys->links('vendor.pagination.default') }}
</div>

