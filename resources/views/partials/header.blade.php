<header class="header">
    <div class="group">

        <a href="{{ route('wellcome') }}" class="logo">
            <img src="{{ asset('images/logo_short.png') }}" alt="">
        </a>
        <ul class="menu clearfix">
            <li class="active">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/icon_job.png') }}" alt="">
                    <span>Việc làm</span>
                </a>
            </li>
            <li>
                <a href="{{ route('news.index') }}">
                    <img src="{{ asset('images/icon_news.png') }}" alt="">
                    <span>Khám phá</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="{{ asset('images/icon_post.png') }}" alt="">
                    <span>Viết bài</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="group group--right">
{{--        <form class="search">--}}
{{--            <input type="text" id="global-search" placeholder="Nhập tìm kiếm công việc, bạn bè và công ty">--}}
{{--            <button><img src="/images/icon_search.png" alt=""></button>--}}
{{--            <div class="header-search-result">--}}
{{--                <div class="users-result">--}}

{{--                </div>--}}
{{--                <div class="companies-result">--}}

{{--                </div>--}}
{{--                <div class="jobs-result">--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}

        @include('partials.menu-dropdown')

    </div>
</header><!-- /header -->
