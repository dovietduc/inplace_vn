<div class="item col-lg-4 mb-5 mbl-lg-0">
    <div class="image-post">
        <a href="#" title="">
            <img src="https://via.placeholder.com/370x210" class="w-100" alt="">
        </a>
    </div>
    <h6 class="name-post font-weight-bold mb-10">
        <a href="#" title="">
            Chiêm ngưỡng một trong những văn phòng làm việc đẹp nhất Hà Nội
        </a>
    </h6>
    <div class="auth-post d-flex">
        <div class="auth-info media">
            <img src="https://via.placeholder.com/48x48" class="auth-avatar rounded-circle mr-10" alt="...">
            <div class="media-body align-self-center ">
                <div class="auth-name mt-0 font-weight-bold text-black-400 line-height-sm">
                    <a href="#" title="">
                        John Doe
                    </a>
                </div>
                <div class="d-flex">
                    <div class="auth-role-name text-size-12 text-grey-200">
                        <span class="">Director/manager</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="auth-activity-info align-self-center ml-auto">
            <div class="counter d-inline-block">
                                <span class="text-size-13 text-grey-200">
                                    128k Thích
                                </span>
            </div>

        </div>
    </div>
</div>
