<div class="quote-master">

    <ul class="row list-person nav " id="pills-tab" role="tablist">
        @for ($i = 0; $i < 4; $i++)
            <li class="col-6 col-md-3 col-lg-2 text-center nav-item">
                <a href="#quote-message-{{ $i }}" aria-controls="quote-message-{{ $i }}"
                   id="person-of-quote-{{ $i }}" data-toggle="tab"
                   aria-selected="{{ $i == 0 ? "true":'false' }}" role="tab"
                   class="item nav-link {{ $i == 0 ? 'active':'' }}">

                    <img src="https://via.placeholder.com/140x140" class="person-avatar rounded-circle mb-3" alt="">
                    <p class="m-0 person-name font-weight-bold">
                        Francis Sim
                    </p>
                </a>
            </li>
        @endfor
    </ul>

    <div class="tab-content pt-20">
        @for ($i = 0; $i < 4; $i++)
            <div class="tab-pane fade {{ $i == 0 ? 'show active':'' }}" id="quote-message-{{ $i }}"
                 aria-labelledby="person-of-quote-{{ $i }}" role="tabpanel">
                <div class="message-auth mb-10">
                    <span class="auth-name-of-message font-weight-bold">
                        Đỗ Như Quỳnh
                    </span>
                    <span class="text-grey-200 ml-4">Content Creator</span>
                </div>
                <div class="message-content mb-25">
                    <div class="text-black-400 js-short-content" data-msg-more="Xem thêm" data-msg-less="Thu gọn"
                         data-msg-length="100">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet, nunc eget laoreet
                        sa agittis, quam ligula sodales orci, congue imperdiet eros tortor ac lectus. Duis eget nisl
                        orci. Aliquam mattis purus non mauris blandit id luctus felis convallis. Integer varius egestas
                        vestibulum. Nullam a dolor arcu, ac tempor elit. Donec
                    </div>
                </div>

                <div class="row post-who-box">
                    <div class="col-12 col-lg-3 mb-3 mb-lg-0">
                        <a href="#" title="" class="">
                            <img src="https://via.placeholder.com/270x150" alt="" class="post-image w-100">
                        </a>
                    </div>
                    <div class="col-12 col-lg-7">
                        <div class="post-name">
                            <a href="#" title="" class="text-size-16 font-weight-bold">
                                Lê Diệp Kiều Trang trả lời câu hỏi: Tại sao phải học hành vất vả 16 năm rồi đi làm cũng
                                chẳng liên quan
                            </a>
                            <span class="post-time text-grey-200 font-weight-regular text-size-13">
                    (26/05/2017)
                </span>
                        </div>
                        <div class="post-desc">
                <span class="text-black-400">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum elit tortor, tempus in consectetur sit amet, mollis sit amet diam. Estibulum sit amet malesuada libero, commodo posuere sapien.
                Aliquam porttitor consequat nisi...
                </span>
                            <a href="#" title="" class="">
                                Xem thêm
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>
