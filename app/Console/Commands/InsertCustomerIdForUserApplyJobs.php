<?php

namespace App\Console\Commands;

use App\Job;
use App\UserApplyJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;

class InsertCustomerIdForUserApplyJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:customer-id-for-user-apply-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert customer id for user apply jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            UserApplyJob:: orderBy('id')->chunk(100, function ($userApplyJobs) {
                foreach ($userApplyJobs as $userApplyJob) {
                    $job = Job::find($userApplyJob->job_id);
                    if (!empty($job)) {
                        $userApplyJob->update(['customer_id' => $job->customer_id]);
                    }

                }
            });
            DB::commit();
            $this->info('Update customer id success');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Loi: ' . $e->getMessage() . 'Dong: ' . $e->getLine());
        }
    }
}
