@extends('layouts.app')
@section('meta')
    <title>Posts Profile</title>
@stop
@section('css')
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/croppie/croppie.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('pages/profile/network/network.css') }}">
@stop
@section('content')
    <div class="profile-pages">
        @include('html.profile.partials.profile-header-pages')
        <div class="profile-networkPage">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 box_content mb-5 mb-md-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="title-sidebar">Network của tôi</h3>
                            <div class="network-tag">
                                <a href="" class="tag-item active">Tất cả</a>
                                <a href="" class="tag-item">Chờ duyệt</a>
                            </div>
                        </div>
                        <div class="network-list">
                            <?php for ($i = 0; $i < 10; $i++){?>
                            <div class="network-item">
                                <div class="network-item-info">
                                    <a href="#" class="network-item-avatar">
                                        <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                                    </a>
                                    <div class="network-item-content">
                                        <h5 class="network-item-name"><a href="#">Nguyễn Thu Thảo</a></h5>
                                        <div class="network-item-desc">
                                            Consultant at J-Job Executive Search
                                        </div>
                                        <span class="network-item-address">Hanoi</span>
                                    </div>
                                </div>
                                <div class="network-item-btn-group">
                                    <button class="btn-accept-friend btn btn-custom_light"><span>Xác nhận</span></button>
                                    <button class="btn-refuse-friend"><i class="fal fa-times"></i></button>
                                </div>
                            </div>
                            <div class="network-item">
                                <div class="network-item-info">
                                    <a href="#" class="network-item-avatar">
                                        <img src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                                    </a>
                                    <div class="network-item-content">
                                        <h5 class="network-item-name"><a href="#">Nguyễn Thu Thảo</a></h5>
                                        <div class="network-item-desc">
                                            Consultant at J-Job Executive Search
                                        </div>
                                        <span class="network-item-address">Hanoi</span>
                                    </div>
                                </div>
                                <div class="network-item-btn-group">
                                    <button class="btn btn-custom_light"><span>Nhắn tin</span></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4 box_sidebar">
                        @include ('html.profile.partials.profile-sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/moment-with-locales.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapDatetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('vendor/croppie/croppie.js') }}"></script>
    <script src="{{asset('pages/profile/profile.js')}}"></script>
    <script src="{{asset('pages/profile/network/network.js')}}"></script>
@stop
