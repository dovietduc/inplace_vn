@extends('layouts.app')
@section('meta')
    <title>Members Invite</title>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('pages/enterprise/info/members-invite.css')}}">
@stop
@section('content')
    @if (\Session::has('message'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Cảnh báo!</strong> {!! \Session::get('message') !!}
        </div>
    @endif
    @if(empty($checkUserHaveAcount))
        <div class="members-invite-page" data-url="{{ url()->current() }}">
            <div class="container">
                <p class="text-welcome">
                    Bạn đã được <strong>{{ optional($member->user)->name }}</strong> mời làm thành viên của <strong>{{ optional($member->customer)->name }}</strong>.
                    <br>
                    Bạn cần đăng ký tài khoản Inplace trước khi xác nhận là thành viên của công ty.
                </p>
                <div class="members-invite-page_form-register">
                    <form action="{{ route('register') }}" method="POST" id="register_invite_form">
                        <input name="join_company" type="hidden" value="{{ url()->current() }}"/>
                        @csrf
                        <div class="form-group">
                            <input type="email"
                                   name="email"
                                   value="{{ $member->email }}" class="form-control"
                                   readonly required placeholder="Email của bạn">
                        </div>
                        <div class="form-group">
                            <input name="name" type="text" class="form-control empty-validate" required placeholder="Họ và tên">
                            <div class="err-inp text-left"></div>
                        </div>
                        <div class="form-group">
                            <label for="input-password" class="label-input-password"></label>
                            <input name="password" type="password" id="input-password"
                                   class="form-control empty-validate" required placeholder="Mật khẩu">
                            <div class="err-inp text-left"></div>
                        </div>

                        <div class="form-group">
                            <select name="occupation_id" class="select-industry form-control" required>
                                <option value="">Chọn nghành nghề</option>
                                @foreach($occupations as $occupationItem)
                                    <option value="{{ $occupationItem->id }}">
                                        {{ $occupationItem->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-custom" type="submit">Đăng ký</button>
                    </form>
                </div>
            </div>
        </div>

    @else
        <div class="members-invite-page">
            <div class="container">
                <a href="#" class="avatar">
                    @if(empty($checkUserHaveAcount->avatar))
                        <img class="member-avatar" src="{{asset('images/no_user_65x65.jpg')}}" alt="">
                    @else
                        @if(str_contains($checkUserHaveAcount->avatar, 'upload') && jjobCheckImageExit($checkUserHaveAcount->avatar))
                            <img class="member-avatar" src="{{ asset($checkUserHaveAcount->avatar) }}" alt="">
                        @else
                            <img class="member-avatar" src="{{ $checkUserHaveAcount->avatar }}" alt="">
                        @endif

                    @endif
                </a>
                <p class="text-welcome">
                    {{ optional($member->user)->name }} has visited you to become a member of {{ optional($member->customer)->name }} on Inplace Admin
                </p>
                <a
                    href="{{ route('enterprise.member.acceptJoinCompany', ['customerId' => optional($member->customer)->id, 'token' => $token]) }}"
                    class="btn btn-custom">
                    Tham gia
                </a>
            </div>
        </div>
    @endif
@stop
@section('js')
    <script src="{{asset('pages/enterprise/info/members-invite.js')}}"></script>
@stop
