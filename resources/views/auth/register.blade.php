@extends('layouts.login')

@section('meta')
    <title>Register - inplace.vn</title>
@stop

@section('stylesheetAddon')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    <link href="{{ asset('pages/users/register/register.css') }}" rel="stylesheet" type="text/css" media="all"/>
@stop

@section('content')
    @include('partials.header-signin')

    <div class="register">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="sologan">
                        <h1 class="title">Matching Values to Place <br> Right People in Right Seats</h1>
                        <div class="txt">Khớp các giá trị để đặt đúng người vào đúng chỗ.</div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex justify-content-center justify-content-lg-end">
                    <div class="form">
                        <div class="txt text-center">Bạn chưa có tài khoản?</div>
                        <div class="title text-center">HÃY ĐĂNG KÝ NGAY</div>

                        <ul class="social clearfix">
                            @include('partials.button-social')
                        </ul>

                        <div class="or">HOẶC</div>

                        <form action="{{ route('register') }}" method="post" id="signupForm">
                            <div class="form-box">
                                @csrf

                                @if ($errors->has('email'))
                                    <div class="messages error" role="alert">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                                <div class="form-group first">
                                    <input type="text"
                                           name="name"
                                           required
                                           placeholder="Họ và tên"
                                           class="form-control"
                                           value="{{ old('name') }}"
                                           oninvalid="this.setCustomValidity('Xin hãy nhập họ tên')"
                                           onchange="this.setCustomValidity('')"
                                    >
                                </div>

                                <div class="form-group">
                                    <input type="email"
                                           name="email"
                                           id="email"
                                           placeholder="Email của bạn"
                                           required
                                           class="form-control"
                                           value="{{ old('email') }}"
                                    >
                                </div>
                                <div class="form-group">
                                    <div class="input-password">
                                        <a href="#" class="js-displayPassword"><img src="/images/icon-eye.png" alt=""></a>
                                        <input
                                            type="password"
                                            name="password"
                                            placeholder="Mật khẩu"
                                            class="form-control"
                                            minlength="8"
                                            required
                                            id="password"
                                        >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <select name="occupation_id" class="select-industry" required>
                                        <option></option>
                                        @foreach($occupations as $occupationItem)
                                            <option value="{{ $occupationItem->id }}">
                                                {{ $occupationItem->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>


                                <p class="notify">Bằng việc click vào nút "Đăng ký", bạn đã đồng ý với các <br> <a href="#">Điều
                                        khoản</a> & <a href="#">Điều kiện</a> của chúng tôi</p>
                            </div>

                            <div class="form-group button">
                                <button type="submit" class="btn btn-block btn-custom">ĐĂNG KÍ</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .login -->

    @include('partials.footer')

@stop

@section('scriptAddon')
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('pages/common/main.js') }}"></script>
    <script src="{{ asset('pages/users/register/register.js') }}"></script>
@stop


