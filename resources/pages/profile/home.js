//validate form Apply Job
let cvApply = $('.apply-inp-file');
let cvButton = $('.apply-inp-btnFile');
let showErrorUpload = $('.apply-err-file');

function checkValidateUploadFile() {
    let fileExtension = ['doc', 'docx', 'pdf'];
    let fileExtentionCurrent = cvApply.val().split('.').pop().toLowerCase();
    showErrorUpload.text('');
    cvButton.removeClass('error active');
    let flag = true;
    if ( !cvApply.val() ) {
        cvButton.addClass('error');
        showErrorUpload.text('File upload không thể để trống');
        return false;
    }
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        showErrorUpload.text('Bạn chưa tải hoặc tải sai định dạng file cho phép!');
        flag = false;
    }
    if ( cvApply.val() && (cvApply[0].files[0].size / 1024) > (2 * 1024) ) {
        showErrorUpload.text('File CV của bạn vượt quá dung lượng cho phép!');
        flag = false;
    }
    if (!flag) {
        cvApply.val('');
        cvButton.addClass('error');
    }
    return flag;
}

function uploadFileValid() {
    let checkStatus = checkValidateUploadFile();
    let fileExtentionCurrent = cvApply.val().split('.').pop().toLowerCase();
    if (!checkStatus) {
        return false;
    }
    if (!$('.apply-inp-btnFile').hasClass('error')) {
        let reader = new FileReader();
        let hasFile = $(this).parent().find('.hasFile');
        cvButton.addClass('active');
        hasFile.find('.name').text(this.files[0].name);
        hasFile.find('.size').text(Math.round(this.files[0].size / 1024) + 'Kb');
        reader.onload = function (e) {
            if (fileExtentionCurrent == 'pdf') {
                cvButton.find('img').attr('src', '/images/pdf.png');
            } else if (fileExtentionCurrent == 'docx' || fileExtentionCurrent == 'doc') {
                cvButton.find('img').attr('src', '/images/doc.png');
            }
        };
        reader.readAsDataURL(this.files[0]);
    }
}

function deleteFileUpload() {
    cvButton.removeClass('active');
    cvApply.val('');
}

function SubmitUpload(e) {
    e.preventDefault();
    let checkStatus = checkValidateUploadFile();
    if (!checkStatus) {
        return false;
    } else {
        let options = {
            success: function (data) {
                if (data.status === 422) {
                    let errors = data.responseJSON;
                    $.each(data.responseJSON, function (key, value) {
                        $('.' + key + '-error').text(value[0]);
                        $('.' + key + '-choose').addClass('error');
                    });
                } else {
                    $("#frm-apply_success").modal();
                    $('body').removeClass('modal-open').css('padding-right','0');
                    $('.modal-backdrop').hide();
                    setTimeout(function () {
                        location.reload();
                    },3000);
                    cvApply.val('');
                }
            },

        };
        $(this).ajaxSubmit(options);
        return false;
    }
}


$(function () {
    cvApply.on('change', uploadFileValid);
    $('.del-file').on('click', deleteFileUpload);
    $(document).on('submit', '#form_applyJob', SubmitUpload);
});
