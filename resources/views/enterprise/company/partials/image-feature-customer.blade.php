<div class="section-info_form">
    <form
        action="{{ route('enterprise.companies.edit.save-image', ['id' => $customer->id]) }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Ảnh logo<span>(128x128px)</span></label>
                </div>
                <div class="col-md-8">
                    <div class="form-group_upload-image">
                        <img src="{{ !empty($customer->logo) ? asset($customer->logo) : '' }}">
                        <input
                            name="logo"
                            type="file"
                            class="inp-file-image"
                            accept=".png,.jpg,.jpeg"/>
                    </div>
                    <hr>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4 text-md-right">
                    <label class="section-info_label" for="">Ảnh bìa<span>(2560x800px)</span></label>
                </div>
                <div class="col-md-8">
                    <div class="form-group_upload-image">
                        <img src="{{ !empty($customer->feature_image) ? asset($customer->feature_image) : '' }}">
                        <input
                            name="feature_image"
                            type="file"
                            class="inp-file-big-image"
                            accept=".png,.jpg,.jpeg"/>
                    </div>
                    <div class="custom-checkbox mt-3">
                        <input
                            value="1"
                            name="flag_delete_image"
                            class="form-check-input"
                            type="checkbox"
                            id="delete-cover-company">
                        <label for="delete-cover-company">Xóa ảnh này</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 offset-md-4">
                    <div class="form-group_submit">
                        <button type="submit" class="btn btn-custom">Cập nhật</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
