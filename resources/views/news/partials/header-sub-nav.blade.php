<div class="header-sub-menu">
    <ul class="header-sub-menu_ul">

        @foreach($categoryNews as $categoryNewsItem)
            <li class="{{  !empty($slug) && $categoryNewsItem->slug == $slug  ? 'active' : '' }}">
                <a href="{{ route('news.category', ['slug' => $categoryNewsItem->slug]) }}">
                    {{ $categoryNewsItem->name  }}
                </a>
            </li>
        @endforeach

    </ul>
</div>
