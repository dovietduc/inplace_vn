<?php

namespace App;

use App\Traits\BaseEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberCompany extends Model
{
    protected $table = 'user_add_members';
    protected $guarded = [];
    use BaseEloquent;
    const paginationList = 10;

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
