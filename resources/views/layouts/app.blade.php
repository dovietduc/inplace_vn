<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('partials.favicon-for-website')
    @yield('meta')
    <link href="{{ asset('vendor/bootstrap-v4/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/font-awesome-v5/css/all.css') }}" rel="stylesheet" type="text/css" media="all"/>
    @yield('css')
</head>
<body>
@include('partials.header')
@yield('content')
@include('partials.footer')

<script src="{{ asset('vendor/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-v4/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-v4/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/theia-sticky-sidebar-master/theia-sticky-sidebar.min.js') }}"></script>
@yield('js')
<script src="{{ asset('pages/common/main.js') }}"></script>


</body>
</html>
