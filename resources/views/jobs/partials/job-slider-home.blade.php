<section class="js-slideJob job-slider">
    @foreach($JobsSliderHome as $JobSliderHomeItem)
        <div class="job-item">
            <div class="detail">
                <h2>
                    <a href="{{ route('jobs.show', ['slug' => $JobSliderHomeItem->slug]) }}">
                        {{ $JobSliderHomeItem->title }}
                    </a>
                </h2>
                <div class="company">
                    <a href="{{ route('companies.index', ['slug' => optional($JobSliderHomeItem->customer)->slug]) }}">
                        <div class="avatar">
                            @if( jjobCheckImageExit(optional($JobSliderHomeItem->customer)->logo) )
                                <img src="{{ url(optional($JobSliderHomeItem->customer)->logo) }}" alt="">
                            @else
                                <img src="{{ asset('images/no_img_50x50.jpg') }}" alt="">
                            @endif

                        </div>
                        <h5>
                            {{ optional($JobSliderHomeItem->customer)->name }}
                        </h5>
                    </a>
                </div>
            </div>
            <a href="{{ route('jobs.show', ['slug' => $JobSliderHomeItem->slug]) }}" class="thumbnail">
                @if( jjobCheckImageExit($JobSliderHomeItem->feature_image) )
                    <img src="{{ url($JobSliderHomeItem->feature_image) }}" alt="">
                @else
                    <img src="{{ asset('images/no-img-970x380.jpg') }}" alt="">
                @endif
            </a>

        </div>
    @endforeach

</section>
