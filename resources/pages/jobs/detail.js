$('.tab-who a').hover(function () {
    $(this).tab('show')
});
$('.job-detail_content__what .viewmore a').click(function () {
    $('.job-detail_content__what .content').addClass('open');
    $('.job-detail_content__why').slideDown();
    $('.job-detail_content__how').slideDown();
    $(this).parent().remove();
});
$('.list-job_slick').slick({
    infinite: true,
    dots: false,
    arrows: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    lazyLoad: 'ondemand',
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 500,
    nextArrow: '<button class="slick_nav slick_next"><i class="fal fa-chevron-right"></i></button>',
    prevArrow: '<button class="slick_nav slick_prev"><i class="fal fa-chevron-left"></i></button>',
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
});


//validate form Apply Job
let telApply = $('.apply-inp-tel');
let cvApply = $('.apply-inp-file');
let cvButton = $('.apply-inp-btnFile');
let showErrorUpload = $('.apply-err-file');
let showErrorTel = $('.apply-err-tel');

function telValid(event) {
    event.preventDefault();
    $(this).removeClass('error');
    showErrorTel.text('');
    if (event.target.validity.valueMissing) {
        $(this).addClass("error");
        showErrorTel.text("Bạn chưa nhập số điện thoại!");
    }
    if (event.target.validity.patternMismatch) {
        $(this).addClass("error");
        showErrorTel.text("Vui lòng nhập đúng định dạng số điện thoại!");
    }
}

function checkValidateUploadFile() {
    let fileExtension = ['doc', 'docx', 'pdf'];
    let fileExtentionCurrent = cvApply.val().split('.').pop().toLowerCase();
    showErrorUpload.text('');
    cvButton.removeClass('error active');
    let flag = true;
    if ( !cvApply.val() ) {
        cvButton.addClass('error');
        showErrorUpload.text('File upload không thể để trống');
        return false;
    }
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        showErrorUpload.text('Bạn chưa tải hoặc tải sai định dạng file cho phép!');
        flag = false;
    }
    if ( cvApply.val() && (cvApply[0].files[0].size / 1024) > (2 * 1024) ) {
        showErrorUpload.text('File CV của bạn vượt quá dung lượng cho phép!');
        flag = false;
    }
    if (!flag) {
        cvApply.val('');
        cvButton.addClass('error');
    }
    return flag;
}

function uploadFileValid() {

    let checkStatus = checkValidateUploadFile();
    let fileExtentionCurrent = cvApply.val().split('.').pop().toLowerCase();
    if (!checkStatus) {
        return false;
    }
    if (!$('.apply-inp-btnFile').hasClass('error')) {
        let reader = new FileReader();
        let hasFile = $(this).parent().find('.hasFile');
        cvButton.addClass('active');
        hasFile.find('.name').text(this.files[0].name);
        hasFile.find('.size').text(Math.round(this.files[0].size / 1024) + 'Kb');
        reader.onload = function (e) {
            if (fileExtentionCurrent == 'pdf') {
                cvButton.find('img').attr('src', '/images/pdf.png');
            } else if (fileExtentionCurrent == 'docx' || fileExtentionCurrent == 'doc') {
                cvButton.find('img').attr('src', '/images/doc.png');
            }
        };
        reader.readAsDataURL(this.files[0]);
    }
}

function deleteFileUpload() {
    cvButton.removeClass('active');
    cvApply.val('');
}

function SubmitUpload(e) {

    e.preventDefault();
    let checkStatus = checkValidateUploadFile();
    if (!checkStatus) {
        return false;
    } else {
        let options = {
            success: function (data) {
                if (data.status === 422) {
                    let errors = data.responseJSON;
                    $.each(data.responseJSON, function (key, value) {
                        $('.' + key + '-error').text(value[0]);
                        $('.' + key + '-choose').addClass('error');
                    });
                } else {
                    $("#frm-apply").modal('hide');
                    $("#frm-apply_success").modal('show');
                    cvApply.val('');
                }
            },

        };
        $(this).ajaxSubmit(options);
        return false;
    }
}

$(function () {
    telApply.on('change', telValid);
    telApply.on('invalid', telValid);
    cvApply.on('change', uploadFileValid);
    $('.del-file').on('click', deleteFileUpload);
    $(document).on('submit', '#form_apply_job', SubmitUpload);
});

