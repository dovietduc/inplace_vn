<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forgot step 3</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/forgot/forgot.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('html/element/header-forgot')
	@endcomponent

	<div class="forgot-content">
		<h1 class="tittle">Đặt lại mật khẩu của bạn</h1>
		<p class="txt">Vui lòng tạo một mật khẩu mới cho tài khoản của bạn.</p>
		<form class="form">
			<div class="form-group">
				<label>Mật khẩu mới</label>
				<input type="password" class="form-control" placeholder="Nhập mật khẩu mới">
			</div>
			<div class="form-group">
				<label>Xác nhận mật khẩu mới</label>
				<input type="password" class="form-control" placeholder="Nhập lại mật khẩu">
				<p class="form-alert"><i class="fas fa-exclamation-circle"></i>Mật khẩu chưa khớp</p>
			</div>      
			<button type="submit" class="btn btn-custom">Đặt mật khẩu mới</button>
		</form>
	</div><!-- /forgot-content -->
	
	@component('html/element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/common/main.js"></script>
	<script src="/pages/forgot/forgot.js"></script>
</body>
</html>