<div class="txt-format">
    <h1>Xoá Nội dung - Trang Cá nhân</h1>
    <p>Với cá nhân, bạn có thể xoá bài viết của bạn dễ dàng bằng cách sau:</p>
    <ul>
        <li>Tại Profile cá nhân, click vào phần “<strong>Bài Viết</strong>”</li>
        <li>Tại danh sách bài viết, chọn biểu tượng “<strong>Xoá</strong>” tại tiêu đề mỗi bài viết</li>
    </ul>
    <p><br></p>
    <h1>Xoá Nội dung - Trang Doanh nghiệp</h1>
    <p>Với
        trang doanh nghiệp, bạn có thể dùng công cụ Quản Lý Bài Viết trong phần Admin trang doanh nghiệp</p>
    <ul>
        <li>Truy cập vào phần Admin trang Doanh Nghiệp</li>
        <li>Chọn mục Quản Lý “<strong>Bài Viết</strong>” từ dãy menu bên tay trái màn hình</li>
        <li>Từ danh sách bài viết hiện ra, bạn có thể chọn biểu tượng “<strong>Xoá</strong>” xuất hiện ở mỗi tiêu đề bài viết.</li>
    </ul>
</div>
