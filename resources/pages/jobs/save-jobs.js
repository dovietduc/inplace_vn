import swal from 'sweetalert';
const axios = require('axios');

function deleteJob() {
    let url = $(this).data('url');
    let that = $(this);
    swal({
        icon: "warning",
        title: "Bạn chắc chắn xóa không?",
        text: "Job bạn lưu sẽ bị xóa khỏi danh sách lưu của bạn!",
        buttons: {
            cancel: {
                text: "Hủy",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Xóa",
                value: true,
                visible: true,
                className: "btn-custom",
                closeModal: true
            }
        }
    }).then(function (result) {
        if (result) {
            axios.post(url)
                .then(function (response) {
                    if (response.status) {
                        that.parents('.item-save-job').remove();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    });

}
$(function () {
    $('.job-item_btn-delete').on('click', deleteJob);
});
