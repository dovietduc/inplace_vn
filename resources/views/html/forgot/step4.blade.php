<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Forgot step 4</title>
	<link href="/vendor/bootstrap-v4/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/vendor/font-awesome-v5/css/all.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="/pages/forgot/forgot.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>

	@component('html/element/header-forgot')
	@endcomponent

	<div class="forgot-content">
		<p class="tittle text-center"><i class="far fa-check-circle"></i></p>
		<p class="txt">Chúc mừng bạn đã đổi mật khẩu thành công!</p>
		<form class="form">
			<div class="form-group">
				<label>Email</label>
				<input type="text" placeholder="Email hoặc số điện thoại" class="form-control">
			</div>
			<div class="form-group">
				<label>Xác nhận mật khẩu mới</label>
				<input type="password" class="form-control" placeholder="Nhập mật khẩu mới">
			</div>      
			<button type="submit" class="btn btn-custom">Đăng nhập</button>
		</form>
	</div><!-- /forgot-content -->
	
	@component('html/element/footer')
	@endcomponent

	<script src="/vendor/jquery/jquery-2.1.4.min.js"></script>
	<script src="/pages/common/main.js"></script>
	<script src="/pages/forgot/forgot.js"></script>
</body>
</html>