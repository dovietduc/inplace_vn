@if( empty($jobItem->userSaveJob()->where('user_id', auth()->id())->count()) )
    <button
        data-url="{{ route('jobs.save', ['id' => $jobItem->id]) }}"
        class="icon-save-job save_job">
        <i class="far fa-bookmark text-grey-200"></i>
    </button>
@else
    <button class="icon-save-job saved">
        <i class="fas fa-bookmark"></i>
    </button>
@endif
