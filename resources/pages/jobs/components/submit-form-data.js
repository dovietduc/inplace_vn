function submitForm(event) {
    event.preventDefault();
    let queryStr = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type: 'GET',
        url: url,
        data: queryStr,
        success: function (data) {
            $("#list_job_content").html(data.list_job);
        }
    });
}
export function submitFormData() {
    $(document).on('submit', '#search-complete', submitForm);
}
