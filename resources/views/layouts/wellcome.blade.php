<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('partials.favicon-for-website')
    @yield('meta')
    <link href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/wow/animate.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('common/style.css') }}" type="text/css" rel="stylesheet" media="all"/>

    @yield('stylesheetAddon')
</head>
<body>
<div class="main-container">

    @yield('content')

</div>

<script src="{{ asset('vendor/jquery/jquery-2.1.4.min.js') }}"></script>
<script src="{{ url('vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/wow/wow.min.js') }}"></script>

    @yield('scriptAddon')

</body>
</html>
