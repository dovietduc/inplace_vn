<div class="list-job bg-white">
    <div class="container">
        <h3 class="list-job_title">Công việc đã xem</h3>
        <div class="list-job_slick">

            @foreach($jobsUserViewed as $jobViewed)
                <div class="item">
                    <div class="list-job_item">
                        <div class="company">
                            <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}" class="logo" title="{{ optional($jobViewed->customer)->name }}">
                                @if( jjobCheckImageExit(optional($jobViewed->customer)->logo) )
                                    <img src="{{ asset(optional($jobViewed->customer)->logo) }}"
                                         alt="">
                                @else
                                    <img src="{{ asset('images/no_avatar_company_30x30.jpg') }}"
                                         alt="">
                                @endif
                            </a>
                            <h4 class="title">
                                <a href="{{ route('companies.index', ['slug' => $customer->slug]) }}" title="{{ optional($jobViewed->customer)->name }}">
                                    {{ optional($jobViewed->customer)->name }}
                                </a>
                            </h4>
                        </div>
                        <div class="thumb">
                            <a href="{{ route('jobs.show', ['slug' => $jobViewed->slug]) }}" title="{{ $jobViewed->title }}">
                                @if( jjobCheckImageExit($jobViewed->feature_image) )
                                    <img src="{{ asset($jobViewed->feature_image) }}" alt="">
                                @else
                                    <img src="{{ asset('images/no_img_255x100.jpg') }}" alt="">
                                @endif
                            </a>
                        </div>
                        <h3 class="title">
                            <a href="{{ route('jobs.show', ['slug' => $jobViewed->slug]) }}" title="{{ $jobViewed->title }}">
                                {{ $jobViewed->title }}
                            </a>
                        </h3>
                        <div class="job-item-tag">
                            @if(!empty($jobViewed->hot))
                                <div class="tag-hot-job">
                                    <span>Hot Job</span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>
