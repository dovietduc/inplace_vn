const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css').version();

mix.js('resources/pages/main.js', 'public/pages/common').version();

//user
mix.js('resources/pages/users/login.js', 'public/pages/users/login')
    .sass('resources/sass/pages/users/login.scss', 'public/pages/users/login').version();

mix.js('resources/pages/users/register.js', 'public/pages/users/register')
    .sass('resources/sass/pages/users/register.scss', 'public/pages/users/register').version();

mix.js('resources/pages/users/forgot.js', 'public/pages/users/forgot')
    .sass('resources/sass/pages/users/forgot.scss', 'public/pages/users/forgot').version();

mix.js('resources/pages/users/verify.js', 'public/pages/users/verify')
    .sass('resources/sass/pages/users/verify.scss', 'public/pages/users/verify').version();

mix.js('resources/pages/users/fill-email.js', 'public/pages/users/fill-email')
    .sass('resources/sass/pages/users/fill-email.scss', 'public/pages/users/fill-email').version();

mix.js('resources/pages/users/invite-friends.js', 'public/pages/users/invite-friends')
    .sass('resources/sass/pages/users/invite-friends.scss', 'public/pages/users/invite-friends').version();

mix.js('resources/pages/users/account-setting.js', 'public/pages/users/account-setting')
    .sass('resources/sass/pages/users/account-setting.scss', 'public/pages/users/account-setting').version();

//job
mix.js('resources/pages/jobs/list.js', 'public/pages/jobs/list')
	.sass('resources/sass/pages/jobs/list.scss', 'public/pages/jobs/list').version();

mix.js('resources/pages/jobs/detail.js', 'public/pages/jobs/detail')
    .sass('resources/sass/pages/jobs/detail.scss', 'public/pages/jobs/detail').version();

mix.js('resources/pages/jobs/save-jobs.js', 'public/pages/jobs/save-jobs')
    .sass('resources/sass/pages/jobs/save-jobs.scss', 'public/pages/jobs/save-jobs').version();

mix.js('resources/pages/jobs/apply-jobs.js', 'public/pages/jobs/apply-jobs')
    .sass('resources/sass/pages/jobs/apply-jobs.scss', 'public/pages/jobs/apply-jobs').version();

//companies
mix.js('resources/pages/companies/company.js', 'public/pages/companies').version();

mix.js('resources/pages/companies/create.js', 'public/pages/companies/create')
    .sass('resources/sass/pages/companies/create.scss', 'public/pages/companies/create').version();

mix.js('resources/pages/companies/following.js', 'public/pages/companies/following')
    .sass('resources/sass/pages/companies/following.scss', 'public/pages/companies/following').version();

mix.js('resources/pages/companies/home.js', 'public/pages/companies/home')
    .sass('resources/sass/pages/companies/home.scss', 'public/pages/companies/home').version();

mix.js('resources/pages/companies/members.js', 'public/pages/companies/members')
	.sass('resources/sass/pages/companies/members.scss', 'public/pages/companies/members').version();

mix.js('resources/pages/companies/gallery.js', 'public/pages/companies/gallery')
    .sass('resources/sass/pages/companies/gallery.scss', 'public/pages/companies/gallery').version();

mix.js('resources/pages/companies/videos.js', 'public/pages/companies/videos')
    .sass('resources/sass/pages/companies/videos.scss', 'public/pages/companies/videos').version();

mix.js('resources/pages/companies/jobs.js', 'public/pages/companies/jobs')
    .sass('resources/sass/pages/companies/jobs.scss', 'public/pages/companies/jobs').version();

mix.js('resources/pages/companies/post.js', 'public/pages/companies/post')
    .sass('resources/sass/pages/companies/post.scss', 'public/pages/companies/post').version();

mix.js('resources/pages/companies/q-a.js', 'public/pages/companies/q-a')
    .sass('resources/sass/pages/companies/q-a.scss', 'public/pages/companies/q-a').version();

mix.js('resources/pages/companies/quiz.js', 'public/pages/companies/quiz')
    .sass('resources/sass/pages/companies/quiz.scss', 'public/pages/companies/quiz').version();

mix.js('resources/pages/companies/virtual-office.js', 'public/pages/companies/virtual-office')
    .sass('resources/sass/pages/companies/virtual-office.scss', 'public/pages/companies/virtual-office').version();

//news
mix.js('resources/pages/news/list.js', 'public/pages/news/list')
    .sass('resources/sass/pages/news/list.scss', 'public/pages/news/list').version();

mix.js('resources/pages/news/detail.js', 'public/pages/news/detail')
    .sass('resources/sass/pages/news/detail.scss', 'public/pages/news/detail').version();

//profile
mix.js('resources/pages/profile/profile.js', 'public/pages/profile').version();

mix.js('resources/pages/profile/home.js', 'public/pages/profile/home')
    .sass('resources/sass/pages/profile/home.scss', 'public/pages/profile/home').version();

mix.js('resources/pages/profile/posts.js', 'public/pages/profile/posts')
    .sass('resources/sass/pages/profile/posts.scss', 'public/pages/profile/posts').version();

mix.js('resources/pages/profile/network.js', 'public/pages/profile/network')
    .sass('resources/sass/pages/profile/network.scss', 'public/pages/profile/network').version();


//enterprise
mix.js('resources/pages/enterprise/dashboard.js', 'public/pages/enterprise').version();

mix.js('resources/pages/enterprise/home/home.js', 'public/pages/enterprise/home')
	.sass('resources/sass/pages/enterprise/home/home.scss', 'public/pages/enterprise/home').version();

mix.js('resources/pages/enterprise/news/list.js', 'public/pages/enterprise/news')
	.sass('resources/sass/pages/enterprise/news/list.scss', 'public/pages/enterprise/news').version();

mix.js('resources/pages/enterprise/news/add.js', 'public/pages/enterprise/news')
	.sass('resources/sass/pages/enterprise/news/add.scss', 'public/pages/enterprise/news').version();

mix.js('resources/pages/enterprise/jobs/list.js', 'public/pages/enterprise/jobs')
	.sass('resources/sass/pages/enterprise/jobs/list.scss', 'public/pages/enterprise/jobs').version();

mix.js('resources/pages/enterprise/jobs/add.js', 'public/pages/enterprise/jobs')
	.sass('resources/sass/pages/enterprise/jobs/add.scss', 'public/pages/enterprise/jobs').version();

mix.js('resources/pages/enterprise/jobs/candidates.js', 'public/pages/enterprise/jobs')
    .sass('resources/sass/pages/enterprise/jobs/candidates.scss', 'public/pages/enterprise/jobs').version();

mix.js('resources/pages/enterprise/info/company.js', 'public/pages/enterprise/info')
	.sass('resources/sass/pages/enterprise/info/company.scss', 'public/pages/enterprise/info').version();

mix.js('resources/pages/enterprise/info/members.js', 'public/pages/enterprise/info')
	.sass('resources/sass/pages/enterprise/info/members.scss', 'public/pages/enterprise/info').version();

mix.js('resources/pages/enterprise/info/members-invite.js', 'public/pages/enterprise/info')
    .sass('resources/sass/pages/enterprise/info/members-invite.scss', 'public/pages/enterprise/info').version();

mix.js('resources/pages/enterprise/email/email-invite-member.js', 'public/pages/enterprise/email')
    .sass('resources/sass/pages/enterprise/email/email-invite-member.scss', 'public/pages/enterprise/email').version();

mix.js('resources/pages/enterprise/candidates/list.js', 'public/pages/enterprise/candidates')
    .sass('resources/sass/pages/enterprise/candidates/list.scss', 'public/pages/enterprise/candidates').version();

mix.js('resources/pages/enterprise/candidates/detail.js', 'public/pages/enterprise/candidates')
    .sass('resources/sass/pages/enterprise/candidates/detail.scss', 'public/pages/enterprise/candidates').version();


//helper
mix.js('resources/pages/helper/forEnterprise.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/forEnterprise.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/forUsers.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/forUsers.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/help.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/help.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/notifications.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/notifications.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/privacyRegulations.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/privacyRegulations.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/termsOfUse.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/termsOfUse.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/operationRegulations.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/operationRegulations.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/BusinessCorner.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/BusinessCorner.scss', 'public/pages/helper').version();

mix.js('resources/pages/helper/aboutUs.js', 'public/pages/helper')
    .sass('resources/sass/pages/helper/aboutUs.scss', 'public/pages/helper').version();

//admin
mix.js('resources/pages/admin/login.js', 'public/pages/admin')
    .sass('resources/sass/pages/admin/login.scss', 'public/pages/admin').version();

mix.js('resources/pages/admin/home.js', 'public/pages/admin')
    .sass('resources/sass/pages/admin/home.scss', 'public/pages/admin').version();

mix.js('resources/pages/admin/jobs/list.js', 'public/pages/admin/jobs')
    .sass('resources/sass/pages/admin/jobs/list.scss', 'public/pages/admin/jobs').version();

mix.js('resources/pages/admin/news/list.js', 'public/pages/admin/news')
    .sass('resources/sass/pages/admin/news/list.scss', 'public/pages/admin/news').version();

mix.js('resources/pages/admin/candidates/list.js', 'public/pages/admin/candidates')
    .sass('resources/sass/pages/admin/candidates/list.scss', 'public/pages/admin/candidates').version();

mix.js('resources/pages/admin/candidates/detail.js', 'public/pages/admin/candidates')
    .sass('resources/sass/pages/admin/candidates/detail.scss', 'public/pages/admin/candidates').version();

//Email Template
mix.js('resources/pages/email/email.js', 'public/pages/email')
    .sass('resources/sass/pages/email/email-layout.scss', 'public/pages/email').version();

