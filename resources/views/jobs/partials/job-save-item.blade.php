@foreach($jobsSaved as $jobSavedItem)
    <div class="col-sm-6 col-lg-4 item-save-job">
        <div class="component-job-item-save">
            <a href="{{ route('jobs.show', ['slug' => $jobSavedItem->slug]) }}" class="job-item-thumb">

                @if( jjobCheckImageExit($jobSavedItem->feature_image) )
                    <img
                         src="{{ asset($jobSavedItem->feature_image) }}" alt="">
                @else
                    <img src="{{ asset('images/no_img_350x137.jpg') }}" alt="" >
                @endif

            </a>
            <div class="job-item-content">
                <h3 class="title">
                    <a href="{{ route('jobs.show', ['slug' => $jobSavedItem->slug]) }}" title="">
                        {{ $jobSavedItem->title }}
                    </a>
                </h3>
                <div class="desc">
                    {!! str_limit( strip_tags($jobSavedItem->description), $limit = 300, $end = '...') !!}
                </div>
            </div>
            <div class="job-item-company">
                <a class="avatar" href="{{ route('companies.index', ['slug' => optional($jobSavedItem->customer)->slug]) }}">
                    @if( jjobCheckImageExit(optional($jobSavedItem->customer)->logo) )
                        <img src="{{ url(optional($jobSavedItem->customer)->logo) }}" alt="" class="avatar">
                    @else
                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" alt="" class="avatar">
                    @endif

                </a>
                <h4 class="name">{{ optional($jobSavedItem->customer)->name }}</h4>
            </div>
            <button
                data-url="{{ route('jobs.remove-save-job-item', ['slug' => $jobSavedItem->slug]) }}"
                class="job-item_btn-delete">
                <i class="fal fa-times"></i>
            </button>
        </div>
    </div>

@endforeach
<div class="col-12">
    {{ $jobsSaved->links('vendor.pagination.default') }}
</div>

