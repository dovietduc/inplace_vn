    <div class="company-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="component-title">
                    <h3 class="title">
                        Thông tin công ty
                    </h3>
                    <div class="desc"></div>
                </div>
                <div class="company-info_brand">
                    @if( jjobCheckImageExit($customer->feature_image) )
                        <img src="{{ url($customer->logo) }}" class="avatar"
                             alt="...">
                    @else
                        <img src="{{ asset('images/no_avatar_company_50x50.jpg') }}" class="avatar"
                             alt="...">
                    @endif
                    <h5 class="name">
                        {{ $customer->name }}
                    </h5>
                </div>
                <table class="component-table-info">
                    <tr>
                        <td>
                            Lĩnh vực hoạt động:
                        </td>
                        <td>
                            {{ $customer->industrys->implode('name', ', ') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Website:
                        </td>
                        <td>
                            <a href="{{ $customer->website }}" title="" class="text-green-300">
                                <span>{{ $customer->website }}</span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Quy mô doanh nghiệp:
                        </td>
                        <td>
                            {{ optional($customer->CompanySize)->size }} nhân viên
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Giám đốc:
                        </td>
                        <td>
                            {{ $customer->director }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Văn hóa doanh nghiệp:
                        </td>
                        <td>
                            <span class="tab">Trẻ trung</span>
                            <span class="tab">Năng động</span>
                            <span class="tab">Sáng tạo</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Năm thành lập:
                        </td>
                        <td>
                            {{ $customer->founded_year }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trụ sở chính:
                        </td>

                        <td>
                            {{ optional($customer->customerAddress()->where('head_office', true)->first())->address }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-12 col-lg-6">
                {!!  $customer->place_location !!}
            </div>
        </div>
    </div>
</div>

